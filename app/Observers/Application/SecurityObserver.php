<?php

namespace App\Observers\Application;

use App\Entities\User\AccountSecurity;
use Illuminate\Support\Facades\Auth;
use App\Notifications\Application\SecurityUpdate;


class SecurityObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function created(AccountSecurity $detail)
    {
        $user = Auth::user();
        $user->notify(new SecurityUpdate($user->first_name));
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function updated(AccountSecurity $detail)
    {

    }

    /**
     * Handle the task "deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function deleted(AccountSecurity $detail)
    {
        //
    }

    /**
     * Handle the task "restored" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function restored(AccountSecurity $detail)
    {
        //
    }

    /**
     * Handle the task "force deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function forceDeleted(AccountSecurity $detail)
    {
        //
    }
}
