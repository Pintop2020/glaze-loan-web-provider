<?php

namespace App\Observers\Application;

use App\Entities\User\UserDetail;
use Illuminate\Support\Facades\Auth;
use App\Notifications\Application\SuccessNotification;
use App\Notifications\Application\KycStatus;


class KycObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function created(UserDetail $detail)
    {
        $user = Auth::user();
        $user->notify(new SuccessNotification($user->first_name));
    }
}
