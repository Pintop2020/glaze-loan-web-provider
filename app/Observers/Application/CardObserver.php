<?php

namespace App\Observers\Application;

use App\Entities\Transaction\Card;
use Illuminate\Support\Facades\Auth;
use App\Notifications\Application\CardUpdate;


class CardObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function created(Card $detail)
    {
        $user = Auth::user();
        $user->notify(new CardUpdate($user->first_name));
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function updated(Card $detail)
    {

    }

    /**
     * Handle the task "deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function deleted(Card $detail)
    {
        //
    }

    /**
     * Handle the task "restored" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function restored(Card $detail)
    {
        //
    }

    /**
     * Handle the task "force deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function forceDeleted(Card $detail)
    {
        //
    }
}
