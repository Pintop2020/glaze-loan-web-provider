<?php

namespace App\Observers\Loan;

use App\Entities\Offer\Status;
use App\Entities\Loan\Disbursement;
use App\Notifications\Loan\LoanStatusNotification;
use Illuminate\Support\Facades\Auth;
use App\Entities\User\Referral\Referral;
use App\Entities\Transaction\Transaction;

class LoanStatusObserver
{
    public function created(Status $status)
    {
    	if($status->status == 'declined'){
    		$status->loan->user->notify(new LoanStatusNotification($status->loan->user->first_name, 'declined'));
    	}elseif($status->status == 'approved'){
    		$status->loan->user->notify(new LoanStatusNotification($status->loan->user->first_name, 'approved', $status->loan->id));
    	}
    }

    public function updated(Status $status)
    {
        $user = $status->loan->user;
        $ref = Referral::where('user_id', $user->id)->first();
    	if($status->isDirty('status')){
    		if($status->status == 'active'){
                if(!$status->loan->disbursement) $disbursement = Disbursement::create(['user_id'=>$user->id, 'loan_id'=>$status->loan->id]);
                $user = $status->loan->user;
                $offer = $status->loan->offer->data();
                if($ref){
                    $toPay = $offer->amount * (1/100);
                    $bal = $ref->referree()->first()->wallet->amount + $toPay;
                    $ref->referree()->first()->wallet->update(['amount'=>$bal]);
                    $data = [
                        'amount'=>$toPay,
                        'type'=>'earnings',
                        'description'=>'1% interest on Referral ('.$user->getFullname().')',
                        'payment_option'=>'transfer',
                        'reference'=>\App\Http\Controllers\User\UserController::randomId(),
                        'status'=>'success',
                    ];
                    $ref->referree()->first()->transactions()->save(new Transaction(['details'=>json_encode($data)]));
                }
    			$user->notify(new LoanStatusNotification($status->loan->user->first_name, 'active'));
    		}
    	}else if($status->isDirty('status')){
    		if($status->status == 'repaid'){
    			$status->loan->user->notify(new LoanStatusNotification($status->loan->user->first_name, 'repaid'));
    		}
    	}
    }
}
