<?php

namespace App\Observers\Loan;

use App\Entities\Loan\Repayment;
use App\Notifications\Loan\RepaymentNotification;

class RepaymentObserver
{
    
    public function created(Repayment $repayment)
    {
        $loan = $repayment->loan;
        $user = $loan->user;
        $offer = $loan->offer;
        $sum = $loan->repayments()->sum('amount');
        $toPayAll = $offer->data()->amount;
        $toPayAll += $offer->data()->amount * ($offer->data()->interest/100);
        if($toPayAll - $sum > 0){
        	$user->notify(new RepaymentNotification($user->first_name));
        } else {
        	$loan->status->update(['status'=>'repaid']);
        	$user->notify(new RepaymentNotification($user->first_name));
        }
    }
}
