<?php

namespace App\Observers\Loan;

use App\Entities\Loan\Loan;
use App\Entities\User\UserDetail;
use Illuminate\Support\Facades\Auth;
use App\Notifications\Loan\LoanApplication;
use App\Notifications\Loan\LoanStatus;
use Spatie\Permission\Models\Role;

class LoanApplicationObserver
{
    public function created(Loan $loan)
    {
        $user = Auth::user();
        $role = Role::where('name', 'marketer')->first();
        $loan->stages()->attach($role);
        $user->notify(new LoanApplication($user->first_name, $loan->data()));
    }
}
