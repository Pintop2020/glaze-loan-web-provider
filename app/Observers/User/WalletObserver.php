<?php

namespace App\Observers\User;

use App\Entities\User\Wallet;
use App\Notifications\User\WalletNotification;

class WalletObserver
{
    public function updated(Wallet $wallet)
    {
        if($wallet->isDirty('amount')){
        	$old_amount = $wallet->getOriginal('amount'); 
            $amount = $wallet->amount; 
            $wallet->user->notify(new WalletNotification($wallet->user->first_name, $old_amount, $amount));
        }
    }
}
