<?php

namespace App\Observers\User;

use App\Entities\User\User;
use App\Entities\User\Setting;
use App\Entities\User\Wallet;
use App\Entities\User\Referral\RefereeDetail;
use App\Http\Controllers\User\UserController;
use App\Notifications\User\WelcomeMail;
use App\Notifications\User\UpdateNotification;
use App\Notifications\User\AccessNotification;
use App\Notifications\User\VerificationNotification;
use App\Notifications\User\AdminNotification;
use App\Http\Controllers\GlobalMethods as SuperM;

class UserObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function created(User $user)
    {
        if(!$user->is_admin){
            $user->wallet()->save(new Wallet());
            $data = [
                'save_activity_log'=>true,
                'email_when_activity'=>true,
                'email_when_new_browser_logged'=>false,
                'news_letter'=>false
            ];
            $user->setting()->save(new Setting(['details'=>json_encode($data)]));
            $user->my_referral_code()->save(new RefereeDetail(['code'=>SuperM::randomId('referee_details', 'code')]));
            $paystackdata = [
                'first_name'=>$user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->mobile,
                'metadata' => [
                    'id_on_platform'=>$user->id,
                    'middle_name' => $user->middle_name,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                ]
            ];
            $savePaystack = SuperM::paystackPost("https://api.paystack.co/customer", $paystackdata);
            $user->paystack_id = $savePaystack['data']['id'];
            $user->save();
            $user->syncRoles(['user']);
        }
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function updated(User $user)
    {
        if($user->isDirty('active')){
            $status = $user->active; 
            if($status) $user->notify(new AccessNotification($user->first_name, false));
            else $user->notify(new AccessNotification($user->first_name, true));
        }else if($user->isDirty('email_verified_at')){
            $user->notify(new VerificationNotification($user->first_name));
            $user->notify(new WelcomeMail($user->first_name));
        }else if($user->isDirty('is_admin')){
            if($user->is_admin) $user->notify(new AdminNotification($user->first_name));
        }/*else {
            $user->notify(new UpdateNotification($user->first_name));
        }*/
    }

    /**
     * Handle the task "deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the task "restored" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the task "force deleted" event.
     *
     * @param  \App\Task  $task
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
