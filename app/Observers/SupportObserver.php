<?php

namespace App\Observers;

use App\Entities\Support;
use App\Notifications\SupportNotification;

class SupportObserver
{
    public function created(Support $support)
    {
    	$user = $support->user;
        $user->notify(new SupportNotification($user->first_name, $support->id()));
    }
}
