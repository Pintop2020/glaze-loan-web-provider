<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Auth::user()->my_details){
            return redirect('/application')->with('error_bottom', "<script>$(function(){Swal.fire({title: 'You are almost there!', text: 'Please update your KYC details!',icon: 'error'})});</script>");
        }else if(!Auth::user()->account_security){
            return redirect('/application')->with('error_bottom', "<script>$(function(){Swal.fire({title: 'You are almost there!',text: 'Please create a secure transaction pin for your account!',icon: 'error'})});</script>");
        }else if(Auth::user()->cards()->count() < 1){
            return redirect('/application')->with('error_bottom', "<script>$(function(){Swal.fire({title: 'You are almost there!',text: 'Please add at lease one debit/credit card to your profile!',icon: 'error'})});</script>");
        }
        return $next($request);
    }
}
