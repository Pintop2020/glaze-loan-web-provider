<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check()) {
            if (Auth::user()->is_admin) { 
                return $next($request);
            }

            return redirect()->route('admin.home')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'error',title: 'You don\'t have admin access.',showConfirmButton: false,timer: 3000});});</script>");

        }
        return redirect()->route('login');
    }
}