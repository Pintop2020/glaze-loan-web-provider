<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/save-passport',
        '/save-id',
        '/save-company-id',
        '/save-employment-letter',
        '/save-utility-bill',
        '/save-statements',
        '/applications/uploads',
    ];
}
