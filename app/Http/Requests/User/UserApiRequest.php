<?php

namespace App\Http\Requests\User;

use Shamaseen\Repository\Generator\Utility\Request;

/**
 * Class UserRequest
 * @package App\Http\Requests\User
 */
class UserApiRequest extends Request
{
    protected $rules = [
        'email' => ['required', 'email', 'max:255']
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        parent::rules();
        $method = $this->method();
        if (null !== $this->get('_method', null)) {
            $method = $this->get('_method');
        }
        $this->offsetUnset('_method');
        switch ($method) {
            case 'DELETE':
            case 'GET':
                $this->rules = [];
                break;

            case 'POST':
                $this->rules = [
                    'email' => ['required', 'email', 'unique:users', 'max:255'],
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                    'mobile' => ['required', 'unique:users'],
                    'first_name' => ['required', 'string'],
                    'last_name' => ['required', 'string'],
                    'middle_name' => ['required', 'string'],
                    'security_question' => ['required', 'string'],
                    'security_answer' => ['required', 'string'],
                ];

                break;
                // in case of edit
            case 'PUT':
            case 'PATCH':
                $this->rules = [
                    'email' => ['required', 'email', 'max:255'],
                    'password' => ['required']
                ];

                break;
            default:
                break;
        }

        return $this->rules;
    }
}