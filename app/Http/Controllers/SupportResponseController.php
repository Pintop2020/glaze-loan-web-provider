<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupportResponseRequest;
use App\Http\Resources\SupportResponseResource;
use App\Interfaces\SupportResponseInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class SupportResponseController
 * @package App\Http\Controllers
 * @property-read SupportResponseInterface $interface
 */
class SupportResponseController extends Controller
{

    protected $routeIndex = 'supportResponses';

    protected $pageTitle = 'SupportResponse';
    protected $createRoute = 'supportResponses.create';

    protected $viewIndex = 'supportResponses.index';
    protected $viewCreate = 'supportResponses.create';
    protected $viewEdit = 'supportResponses.edit';
    protected $viewShow = 'supportResponses.show';

    /**
     * SupportResponseController constructor.
     * @param SupportResponseInterface $interface
     * @param SupportResponseRequest $request
     * @param SupportResponseResource $resource
     */
    public function __construct(SupportResponseInterface $interface, SupportResponseRequest $request)
    {
        parent::__construct($interface, $request, new  SupportResponseResource([]));
        $this->middleware('role:user|support|super admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function closeSupport($id)
    {
        return $this->interface->closeSupport($id);
    }
}
