<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\GlobalVariables as SuperV;
use Illuminate\Support\Facades\Auth;
use App\Entities\Loan\Loan;
use App\Entities\Loan\Defaulters;
use App\Http\Controllers\Invokable\Loan\Defaults;
use Illuminate\Support\Facades\Crypt;
use App\Entities\Loan\Upload;
use App\Entities\Loan\Folder;
use File;

class GlobalMethods extends Controller
{
    public static function paystackGet($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HTTPHEADER => [
            "authorization: Bearer ".SuperV::$secret_key,
            "content-type: application/json",
            "cache-control: no-cache",
          ],
        ));
        $response = curl_exec($curl);
        return json_decode($response);
    }

    public static function paystackPost($url, $param){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($param),
        CURLOPT_HTTPHEADER => [
            "authorization: Bearer ".SuperV::$secret_key,
            "content-type: application/json",
            "cache-control: no-cache",
          ],
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $tranx = json_decode($response,true);
        return $tranx;
    }

    public static function getEarnings($days = 7){
        $user = Auth::user();
        $date = \Carbon\Carbon::today()->subDays($days);
        $data['earnings'] = $user->transactions()->where([['details->type', 'earnings'],['created_at', '>=', $date]])->orWhere([['details->type', 'profits'],['created_at', '>=', $date]])->sum('details->amount');
        $data['this_day'] = self::calculatePercentEarnings($days, $user, true);
        $data['last_day'] = self::calculatePercentEarnings($days, $user);
        return $data;
    } 

    public static function calculatePercentEarnings($days, $user, $iscurrent = false){
        $earnPercent = 0;
        $date = \Carbon\Carbon::today()->subDays($days);
        $dateThat = \Carbon\Carbon::today()->subDays($days*2);
        $forMonth = $user->transactions()->where('details->type', 'earnings')->orWhere('details->type', 'profits')->sum('details->amount');
        if($iscurrent){
            $forMonthAmount = $user->transactions()->whereDate('created_at', '>=', $date)->where('details->type', 'earnings')->orWhere('details->type', 'profits')->sum('details->amount');
            $earnPercent += $forMonthAmount > 0 ? ($forMonthAmount / $forMonth)*100 : 0;
        }else {
            $forMonthAmount = $user->transactions()->whereDate('created_at', '>=', $dateThat)->where('details->type', 'earnings')->orWhere('details->type', 'profits')->sum('details->amount');
            $earnPercent = $forMonthAmount > 0 ? ($forMonthAmount / $forMonth)*100 : 0;
        }
        return $earnPercent;
    }

    public function monthlyProfits($user, $iscurrent = false){
        $earnAmount = 0;
        if($iscurrent){
            $earnAmount += $user->transactions()->whereDate('created_at', '>=', Carbon::now()->startOfMonth()->subMonth()->toDateString())->where('details->type', 'earnings')->orWhere('details->type', 'profits')->sum('details->amount');
        }else {
            $earnAmount = $user->transactions()->whereDate('created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())->where('details->type', 'earnings')->orWhere('details->type', 'profits')->sum('details->amount');
        }
        return $earnAmount;
    }   

    public static function getSingleDueLoans(){
        $user = Auth::user();
        $loan = $user->loans()->whereHas('status', function($query){
            $query->latest()->where('status', 'active');
        })->latest()->first();
        if($loan){
            $offer = $loan->offer->data();
            $counts = $offer->tenor;
            $repayments = $loan->repayments()->sum('amount');
            $amount = 0;
            $dateDue = array();
            $forMonths = array();
            for($i=0;$i<$counts;$i++){
                $date = new \DateTime(self::getPaymentDate($i, $loan));
                $cur = time();
                $future = strtotime($date->format('Y-m-d H:m:s'));
                $futured = strtotime($date->format('Y-m-d'));
                $dateDiff = $future - $cur;
                if($futured == date('Y-m-d') || $dateDiff < 1){
                    $amount += self::duePayment($loan);
                    $dateDue[] = $date->format('d M, Y h:i A');
                    $forMonths[] = $i+1;
                }
            }
            $difference = $amount-$repayments;
            if($difference > 0){
                $loanData = [
                    'amount_due'=>$difference,
                    'date_due'=>$dateDue,
                    'count'=>$forMonths,
                    'defaults'=>$loan->defaults()->sum('amount')
                ];
                return $loanData;
            }
        }
    }

    public static function getPaymentDate($i, $loan){
        $return = '';
        $offerDets = $loan->offer->data();
        $moratorium = 0;
        $ddifm = '';
        $dpro = $offerDets->moratorium*1;
        $dten = $offerDets->tenor*1;
        $deductMonth = 0;
        if($offerDets->payment_starts == "this")
            $deductMonth = 1;
        if($dpro > 0){
            $datetime = new DateTime(self::getTenorLastDay($offerDets->tenor-$deductMonth, $loan->status()->first()->updated_at));
            $datetime->modify('+'.$dpro.' day');
            $ddifm = $datetime->format('Y-m-d H:i:s');
        }
        if($i == 0){
            if($dpro > 0 && $dten < 2){
                $return = $ddifm;
            }else {
                $return = date('Y', strtotime(self::getTenorLastDay(1, $loan->status()->first()->updated_at))).'-'.date('m', strtotime(self::getTenorLastDay(1-$deductMonth, $loan->status()->first()->updated_at))).'-'.self::getRepayDate($offerDets->pay_date, self::getTenorLastDay(1-$deductMonth, $loan->status()->first()->updated_at));
            }
        }else {
            if(ucwords(date('M', strtotime(self::getTenorLastDay($i+1-$deductMonth, $loan->status()->first()->updated_at)))) == 'Jan'){
                $return = date('Y', strtotime(self::getTenorLastDay($i+1-$deductMonth, $loan->status()->first()->updated_at)));
            }else {
                $return = date('Y', strtotime(self::getTenorLastDay($i, $loan->status()->first()->updated_at)));
            }
            $return .= '-'.date('m', strtotime(self::getTenorLastDay($i+1-$deductMonth, $loan->status()->first()->updated_at))).'-'.self::getRepayDate($offerDets->pay_date, self::getTenorLastDay($i, $loan->status()->first()->updated_at));
        }
        
        return $return;
    }

    public static function duePayment($loan){
        $offerDets = $loan->offer->data();
        $processing = $offerDets->amount  * ($offerDets->processing/100);
        $interest_monthly = ($offerDets->amount+$offerDets->management) * ($offerDets->interest/100);
        return ($offerDets->amount / $offerDets->tenor)+$interest_monthly;
    }

    public static function getOwing($user){
        $loan = $user->loans()->whereHas('status', function($query){
            $query->latest()->where('status', 'active');
        })->latest()->first();
        if(!$loan) return 0;
        $repayments = $loan->repayments()->sum('amount');
        $offer = $loan->offer->data();
        $interest = (($offer->amount+$offer->management) * ($offer->interest/100))*$offer->tenor;
        $owing = ($offer->amount+$interest) - $repayments;
        return $owing;
    }

    public static function ordinal($number) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($number % 100) >= 11) && (($number%100) <= 13))
            return $number. 'th';
        else
            return $number. $ends[$number % 10];
    }

    public static function getBank($bank, $account){
        $bank = self::paystackGet("https://api.paystack.co/bank/resolve?account_number=".$account."&bank_code=".$bank);
        return json_encode($bank);
    }

    public static function search_states($search, $array) { 
        $ret = 0;
        foreach($array as $key => $arr) {
            if($arr->state->name == ucwords($search)){
                $ret += $key;
            }
        }
        return $ret;
    }

    public static function search_banks($search, $array) { 
        $ret = 0;
        foreach($array as $key => $arr) {
            if($arr->code == ucwords($search)){
                $ret += $key;
            }
        }
        return $ret;
    }

    public static $colors = ['primary', 'danger', 'info', 'warning', 'purple', 'blue', 'light'];

    public static function getRandom(){
        shuffle(static::$colors);
        return static::$colors[0];
    }

    public static function randomId($table, $column = 'reference'){
        $id = str_random(8);
        $validator = \Validator::make([$column=>$id],[$column=>'unique:'.$table]);
        if($validator->fails()){
            return $this->randomId();
        }
        return $id;
    }

    public static function decrypt($id)
    {
        $id = Crypt::decryptString($id);
        return $id;
    }

    public static function number_format_short( $n, $precision = 2 ) {
        if ($n < 900) {
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }

    public static function getSizeOfFolder($folder, $loan = null){
        $size = 0.0;
        $folder = Folder::find($folder);
        $uploads = $folder->uploads()->get();
        if($loan){
            $size += \Storage::size('public/'.$loan->uploads->statements);
            $size += \Storage::size('public/'.$loan->uploads->utility_bill);
            $size += \Storage::size('public/'.$loan->uploads->employment_letter);
        }
        foreach($uploads as $upload){
            $size += \Storage::size('public/'.$upload->resource_url);
        }
        return self::size_converter($size);
    }

    public static function size_converter($size){
        $kb = round($size/1000,2);
        $mb = round($size/1000000,2);
        if($kb < 128) return $kb.' KB';
        return $mb.' MB';
    }

    public static function getFileThumb($file){
        $df = explode('.', $file);
        $ext = $df[count($df)-1];
        if($ext == 'png' || $ext == 'jpg' || $ext == 'gif' || $ext == 'jpeg' || $ext == 'ico')
            return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><path d="M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z" style="fill:#755de0" /><path d="M27.2223,43H44.7086s2.325-.2815.7357-1.897l-5.6034-5.4985s-1.5115-1.7913-3.3357.7933L33.56,40.4707a.6887.6887,0,0,1-1.0186.0486l-1.9-1.6393s-1.3291-1.5866-2.4758,0c-.6561.9079-2.0261,2.8489-2.0261,2.8489S25.4268,43,27.2223,43Z" style="fill:#fff" /><path d="M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z" style="fill:#b5b3ff" /></svg>';
        elseif($ext == 'pdf')
            return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><path d="M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z" style="fill:#f26b6b" /><path d="M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z" style="fill:#f4c9c9" /><path d="M46.3342,44.5381a4.326,4.326,0,0,0-2.5278-1.4289,22.436,22.436,0,0,0-4.5619-.3828A19.3561,19.3561,0,0,1,35.82,37.9536a56.5075,56.5075,0,0,0,1.3745-6.0858,2.339,2.339,0,0,0-.4613-1.8444,1.9429,1.9429,0,0,0-1.5162-.753h-.0014a1.6846,1.6846,0,0,0-1.3893.6966c-1.1493,1.5257-.3638,5.219-.1941,5.9457a12.6118,12.6118,0,0,0,.7236,2.1477,33.3221,33.3221,0,0,1-2.49,6.1052,20.3467,20.3467,0,0,0-5.9787,3.4413,2.5681,2.5681,0,0,0-.8861,1.8265,1.8025,1.8025,0,0,0,.6345,1.3056,2.0613,2.0613,0,0,0,1.3942.5313,2.2436,2.2436,0,0,0,1.4592-.5459,20.0678,20.0678,0,0,0,4.2893-5.3578,20.8384,20.8384,0,0,1,5.939-1.1858A33.75,33.75,0,0,0,42.96,47.7858,2.6392,2.6392,0,0,0,46.376,47.55,2.08,2.08,0,0,0,46.3342,44.5381ZM27.6194,49.6234a.8344.8344,0,0,1-1.0847.0413.4208.4208,0,0,1-.1666-.2695c-.0018-.0657.0271-.3147.4408-.736a18.0382,18.0382,0,0,1,3.7608-2.368A17.26,17.26,0,0,1,27.6194,49.6234ZM34.9023,30.848a.343.343,0,0,1,.3144-.1514.6008.6008,0,0,1,.4649.2389.853.853,0,0,1,.1683.6722v0c-.1638.92-.4235,2.381-.8523,4.1168-.0125-.05-.0249-.1-.037-.1506C34.6053,34.0508,34.3523,31.5779,34.9023,30.848ZM33.7231,43.5507a34.9732,34.9732,0,0,0,1.52-3.7664,21.2484,21.2484,0,0,0,2.2242,3.05A21.8571,21.8571,0,0,0,33.7231,43.5507Zm11.7054,2.97a1.3085,1.3085,0,0,1-1.6943.0887,33.2027,33.2027,0,0,1-3.0038-2.43,20.9677,20.9677,0,0,1,2.8346.3335,2.97,2.97,0,0,1,1.7406.9647C45.8377,46.1115,45.6013,46.3483,45.4285,46.5212Z" style="fill:#fff" /></svg>';
        else if($ext == 'pptx' || $ext == 'ppt' || $ext == 'pptm')
            return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><path d="M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z" style="fill:#f25168" /><path d="M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z" style="fill:#ff9fb6" /><path d="M44.1405,46H27.8595A1.86,1.86,0,0,1,26,44.1405V34.8595A1.86,1.86,0,0,1,27.8595,33H44.14A1.86,1.86,0,0,1,46,34.86v9.2808A1.86,1.86,0,0,1,44.1405,46ZM29.1454,44H42.8546A1.1454,1.1454,0,0,0,44,42.8546V36.1454A1.1454,1.1454,0,0,0,42.8546,35H29.1454A1.1454,1.1454,0,0,0,28,36.1454v6.7093A1.1454,1.1454,0,0,0,29.1454,44Z" style="fill:#fff" /><path d="M36.4218,34.268a.7112.7112,0,0,1-.5048-.2093l-2.1431-2.1428a.7143.7143,0,0,1,1.01-1.01l2.1428,2.1431a.7142.7142,0,0,1-.5051,1.2192Z" style="fill:#fff" /><path d="M36.4218,34.268a.7142.7142,0,0,1-.5048-1.2192L38.06,30.9057a.7141.7141,0,0,1,1.01,1.01l-2.1426,2.1428A.7113.7113,0,0,1,36.4218,34.268Z" style="fill:#fff" /><path d="M32.1356,49.268a.7054.7054,0,0,1-.3665-.102.7145.7145,0,0,1-.2451-.98l2.1431-3.5713a.7142.7142,0,0,1,1.2247.735l-2.1426,3.5711A.7144.7144,0,0,1,32.1356,49.268Z" style="fill:#fff" /><path d="M40.7083,49.268a.7138.7138,0,0,1-.6129-.3463L37.9526,45.35a.7143.7143,0,0,1,1.225-.735L41.32,48.1866a.7137.7137,0,0,1-.6121,1.0814Z" style="fill:#fff" /><path d="M35.12,37H30.9a.5007.5007,0,1,1,0-1h4.22a.5007.5007,0,1,1,0,1Z" style="fill:#fff" /><path d="M41.9758,43H37.5471a.5056.5056,0,1,1,0-1.0065h4.4286a.5056.5056,0,1,1,0,1.0065Z" style="fill:#fff" /><path d="M38.14,40H33.9775a.5.5,0,1,1,0-1H38.14a.5.5,0,1,1,0,1Z" style="fill:#fff" /></svg>';
        elseif($ext == 'xlsx' || $ext == 'xlsm' || $ext == 'xlsb' || $ext == 'xltx' || $ext == 'csv')
            return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><path d="M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z" style="fill:#36c684" /><path d="M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z" style="fill:#95e5bd" /><path d="M42,31H30a3.0033,3.0033,0,0,0-3,3V45a3.0033,3.0033,0,0,0,3,3H42a3.0033,3.0033,0,0,0,3-3V34A3.0033,3.0033,0,0,0,42,31ZM29,38h6v3H29Zm8,0h6v3H37Zm6-4v2H37V33h5A1.001,1.001,0,0,1,43,34ZM30,33h5v3H29V34A1.001,1.001,0,0,1,30,33ZM29,45V43h6v3H30A1.001,1.001,0,0,1,29,45Zm13,1H37V43h6v2A1.001,1.001,0,0,1,42,46Z" style="fill:#fff" /></svg>';
        else if($ext == 'zip')
            return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><g><rect x="16" y="14" width="40" height="44" rx="6" ry="6" style="fill:#7e95c4" /><rect x="32" y="17" width="8" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="32" y="22" width="8" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="32" y="27" width="8" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="32" y="32" width="8" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="32" y="37" width="8" height="2" rx="1" ry="1" style="fill:#fff" /><path d="M35,14h2a0,0,0,0,1,0,0V43a1,1,0,0,1-1,1h0a1,1,0,0,1-1-1V14A0,0,0,0,1,35,14Z" style="fill:#fff" /><path d="M38.0024,42H33.9976A1.9976,1.9976,0,0,0,32,43.9976v2.0047A1.9976,1.9976,0,0,0,33.9976,48h4.0047A1.9976,1.9976,0,0,0,40,46.0024V43.9976A1.9976,1.9976,0,0,0,38.0024,42Zm-.0053,4H34V44h4Z" style="fill:#fff" /></g></svg>';
        else 
            return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72"><g><path d="M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z" style="fill:#599def" /><path d="M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z" style="fill:#c2e1ff" /><rect x="27" y="31" width="18" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="27" y="36" width="18" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="27" y="41" width="18" height="2" rx="1" ry="1" style="fill:#fff" /><rect x="27" y="46" width="12" height="2" rx="1" ry="1" style="fill:#fff" /></g></svg>';
    }

    public static function getTenorLastDay($tenor, $time){
        $dten = $tenor*1;
        return date('Y-m-d h:i:s', strtotime("+ ".$dten." months", strtotime($time)));
    }

    public static function getRepayDate($day, $timestamp){
        $totalDays = date('t', strtotime($timestamp));
        $return = $day;
        if($day > $totalDays){
            $return = $totalDays;
        }
        return $return;
    }

    public static function previewer($file){
        $dfile = explode('.', $file);
        $key = count($dfile)-1;
        $ext = $dfile[$key];
        $return = '';
        if($ext == 'txt'){
            $return .= asset($file);
        }else if($ext == 'doc'){
            $return .= "https://view.officeapps.live.com/op/view.aspx?src=".env('APP_URL').'/'.$file;
        }else if($ext == 'docx'){
            $return .= "https://view.officeapps.live.com/op/view.aspx?src=".env('APP_URL').'/'.$file;
        }else if($ext == 'xls'){
            $return .= "https://view.officeapps.live.com/op/view.aspx?src=".env('APP_URL').'/'.$file;
        }else if($ext == 'xlsx'){
            $return .= "https://view.officeapps.live.com/op/view.aspx?src=".env('APP_URL').'/'.$file;
        }else if($ext == 'pdf'){
            $return .= asset($file);
        }else{
            $return .= 'nil';
        }
        return $return;
    }

    public static function removeSpacesNTabs($string){
        return preg_replace( "/\r|\n/", "", preg_replace( "/\t/", "",$string));
    }

    public static function removeHeader($string){
        return str_replace('<?xml version="1.0"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">', '', $string);
    }

    public static function getDisburseAmount($offer){
        $offerDets = json_decode($offer->offer_details);
        $processing = $offerDets->amount  * ($offerDets->processing/100);
        $deduct = $processing + $offerDets->outstanding;
        $todisburse = $offerDets->amount - $deduct;
        return $todisburse;
    }

    public static function transCharge($amount, $isAddFees=true){
        //$final = 0;
        /*if($isAddFees){
            if($amount >= 2500){
                $final += ($amount+100)/(1-0.015);
            }else {
                $final += $amount/(1-0.015);
            }
        }else {
            $final += $amount;
        }*/
        $final = $amount*100;
        return round($final);
    }
}
