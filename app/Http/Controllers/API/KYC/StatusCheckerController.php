<?php

namespace App\Http\Controllers\API\KYC;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Auth;
use App\Entities\User; 
use App\Http\Controllers\GlobalMethods as SuperM;
use Carbon\Carbon;

class StatusCheckerController extends Controller
{

	use AppResponse;

    public function __invoke()
    {
    	$user = Auth::user();
    	$data = [
    		'email_status' => $this->checkEmailStatus($user),
    		'kyc_status' => $this->checkKyc($user),
    		'pin_status' => $this->checkPin($user),
    		'card_status' => $this->checkCard($user),
            'banks' => SuperM::paystackGet('https://api.paystack.co/bank')->data
    	];
    	return $this->success('Verification complete.', $data);
    }

    private function checkEmailStatus($user)
    {
    	return $user->hasVerifiedEmail();
    }

    private function checkKyc($user)
    {
    	$kyc = $user->my_details;
    	if(!$kyc) return false;
    	return true;
    }

    private function checkPin($user)
    {
    	if(!$user->account_security) return false;
    	return true;
    }

    private function checkCard($user)
    {
    	if($user->cards()->count() > 0) return true;
    	return false;
    }
    
}
