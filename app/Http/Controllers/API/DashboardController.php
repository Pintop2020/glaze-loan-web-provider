<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Auth;
use App\Entities\User; 
use App\Http\Controllers\GlobalMethods as SuperM;

class DashboardController extends Controller
{
	use AppResponse;

    public function __invoke()
    {
    	return $this->success('Data retrieved', $this->compileData());
    }

    private function wallet()
    {
    	return Auth::user()->wallet->amount;
    }

    private function transactionSum()
    {
    	$user = Auth::user();
    	return ['income'=>$user->transactions()->where('details->type', 'credit')->orWhere('details->type', 'earnings')->sum('details->amount'), 'spending'=>$user->transactions()->where('details->type', '!=', 'credit')->where('details->type', '!=', 'earnings')->sum('details->amount')*1];
    }

    private function loans()
    {
    	return ['loans'=>Auth::user()->loans()->sum('details->amount'), 'owing'=>Auth::user()->getOwing()];
    }

    public function compileData()
    {
    	$data = [
    		'user' => Auth::user(),
            'my_details' => Auth::user()->my_details,
    		'notifications' => Auth::user()->unreadNotifications,
    		'transactions' => $this->transactionSum(),
    		'wallet' => $this->wallet()*1,
    		'loans' => $this->loans()
    	];
    	return $data;
    }

}
