<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\AppResponse;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;
use App\Entities\User\User;

class ForgotPasswordController extends Controller
{
	use AppResponse;

    public function __invoke() {
    	try {
    		$credentials = request()->validate(['email' => 'required|email']);
    		$find = User::where('email', $credentials)->first();
    		if($find != null){
    			Password::sendResetLink($credentials);
	        	return $this->success("Reset password link sent to your email address!");
    		}else {
    			return $this->error("User with email address not found!");
    		}
    	}catch(\Exception $e){
    		return $this->error($e->getMessage());
    	}
        
    }
}
