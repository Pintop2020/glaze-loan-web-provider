<?php

namespace App\Http\Controllers;

use App\Http\Requests\SiteSettingRequest;
use App\Http\Resources\SiteSettingResource;
use App\Interfaces\SiteSettingInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class SiteSettingController
 * @package App\Http\Controllers
 * @property-read SiteSettingInterface $interface
 */
class SiteSettingController extends Controller
{

    protected $routeIndex = 'siteSettings';

    protected $pageTitle = 'SiteSetting';
    protected $createRoute = 'siteSettings.create';

    protected $viewIndex = 'siteSettings.index';
    protected $viewCreate = 'siteSettings.create';
    protected $viewEdit = 'siteSettings.edit';
    protected $viewShow = 'siteSettings.show';

    /**
     * SiteSettingController constructor.
     * @param SiteSettingInterface $interface
     * @param SiteSettingRequest $request
     * @param SiteSettingResource $resource
     */
    public function __construct(SiteSettingInterface $interface, SiteSettingRequest $request)
    {
        parent::__construct($interface, $request, new  SiteSettingResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
