<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Requests\Transaction\CardRequest;
use App\Http\Resources\Transaction\CardResource;
use App\Interfaces\Transaction\CardInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class CardController
 * @package App\Http\Controllers\Transaction
 * @property-read CardInterface $interface
 */
class CardController extends Controller
{

    protected $routeIndex = 'cards';

    protected $pageTitle = 'Card';
    protected $createRoute = 'cards.create';

    protected $viewIndex = 'cards.index';
    protected $viewCreate = 'cards.create';
    protected $viewEdit = 'cards.edit';
    protected $viewShow = 'cards.show';

    /**
     * CardController constructor.
     * @param CardInterface $interface
     * @param CardRequest $request
     * @param CardResource $resource
     */
    public function __construct(CardInterface $interface, CardRequest $request)
    {
        parent::__construct($interface, $request, new  CardResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function initiateCardLink(){
      return $this->interface->initiateCardLink();
    }

    public function verifyPayment(){
      return $this->interface->verifyPayment();
    }

    public function initiateCardAccess()
    {
        return $this->interface->initiateCardAccess();
    }
}
