<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\UploadRequest;
use App\Http\Resources\Loan\UploadResource;
use App\Interfaces\Loan\UploadInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class UploadController
 * @package App\Http\Controllers\Loan
 * @property-read UploadInterface $interface
 */
class UploadController extends Controller
{

    protected $routeIndex = 'uploads';

    protected $pageTitle = 'Upload';
    protected $createRoute = 'uploads.create';

    protected $viewIndex = 'uploads.index';
    protected $viewCreate = 'uploads.create';
    protected $viewEdit = 'uploads.edit';
    protected $viewShow = 'uploads.show';

    /**
     * UploadController constructor.
     * @param UploadInterface $interface
     * @param UploadRequest $request
     * @param UploadResource $resource
     */
    public function __construct(UploadInterface $interface, UploadRequest $request)
    {
        parent::__construct($interface, $request, new  UploadResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->interface->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return $this->interface->destroy($id);
    }

    public function trash()
    {
        return $this->interface->trash();
    }

    public function restore($id)
    {
        return $this->interface->restore($id);
    }
}
