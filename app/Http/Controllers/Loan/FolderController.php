<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\FolderRequest;
use App\Http\Resources\Loan\FolderResource;
use App\Interfaces\Loan\FolderInterface;
use Shamaseen\Repository\Generator\Utility\Controller;
use App\Entities\User\User;
use App\Entities\Loan\Upload;
use App\Entities\Loan\Folder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

/**
 * Class FolderController
 * @package App\Http\Controllers\Loan
 * @property-read FolderInterface $interface
 */
class FolderController extends Controller
{

    protected $routeIndex = 'folders';

    protected $pageTitle = 'Folder';
    protected $createRoute = 'folders.create';

    protected $viewIndex = 'folders.index';
    protected $viewCreate = 'folders.create';
    protected $viewEdit = 'folders.edit';
    protected $viewShow = 'folders.show';

    /**
     * FolderController constructor.
     * @param FolderInterface $interface
     * @param FolderRequest $request
     * @param FolderResource $resource
     */
    public function __construct(FolderInterface $interface, FolderRequest $request)
    {
        parent::__construct($interface, $request, new  FolderResource([]));
        $this->middleware('role:marketer', ['only' => ['create_admin']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->saveUpload($this->request);
    }

    public function show_main($id)
    {
        return $this->interface->show_main($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function create_admin($id)
    {
        return $this->interface->create_admin($id);
    }
}
