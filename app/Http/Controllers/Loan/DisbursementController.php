<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\DisbursementRequest;
use App\Http\Resources\Loan\DisbursementResource;
use App\Interfaces\Loan\DisbursementInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class DisbursementController
 * @package App\Http\Controllers\Loan
 * @property-read DisbursementInterface $interface
 */
class DisbursementController extends Controller
{

    protected $routeIndex = 'disbursements';

    protected $pageTitle = 'Disbursement';
    protected $createRoute = 'disbursements.create';

    protected $viewIndex = 'disbursements.index';
    protected $viewCreate = 'disbursements.create';
    protected $viewEdit = 'disbursements.edit';
    protected $viewShow = 'disbursements.show';

    /**
     * DisbursementController constructor.
     * @param DisbursementInterface $interface
     * @param DisbursementRequest $request
     * @param DisbursementResource $resource
     */
    public function __construct(DisbursementInterface $interface, DisbursementRequest $request)
    {
        parent::__construct($interface, $request, new  DisbursementResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
