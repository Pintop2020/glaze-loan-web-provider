<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\LoanRequest;
use App\Http\Resources\Loan\LoanResource;
use App\Interfaces\Loan\LoanInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

use DB;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class LoanController
 * @package App\Http\Controllers\Loan
 * @property-read LoanInterface $interface
 */
class LoanController extends Controller
{

    protected $routeIndex = 'loans';

    protected $pageTitle = 'Loan';
    protected $createRoute = 'loans.create';

    protected $viewIndex = 'loans.index';
    protected $viewCreate = 'loans.create';
    protected $viewEdit = 'loans.edit';
    protected $viewShow = 'loans.show';

    /**
     * LoanController constructor.
     * @param LoanInterface $interface
     * @param LoanRequest $request
     * @param LoanResource $resource
     */
    public function __construct(LoanInterface $interface, LoanRequest $request)
    {
        parent::__construct($interface, $request, new  LoanResource([]));
        $this->middleware('role:marketer|risk|coo|super admin|director|chairman|transaction officer', ['only' => ['custom_index', 'action', 'returnBack']]);
        $this->middleware('role:transaction officer', ['only' => ['disburse']]);
        $this->middleware('role:user', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->interface->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return $this->interface->destroy($id);
    }

    public function saveEmployment(){
        return $this->interface->saveEmployment();
    }

    public function saveUtility(){
        return $this->interface->saveUtility();
    }

    public function saveStatements(){
        return $this->interface->saveStatements();
    }

    public function singleLoan($id){
        $id = Crypt::decryptString($id);
        $data['loan'] = Loan::find($id);
        $data['repayments'] = SuperM::getSingleDueLoans();
        return view('loans.single_mine', $data);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////* Admin Methods here *////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function custom_index($type)
    {
        return $this->interface->custom_index($type);
    }

    public function action($action, $id)
    {
        return $this->interface->action($action, $id);
    }

    public function returnBack($id)
    {
        return $this->interface->returnBack($id);
    }

    public function disburse($id)
    {
        return $this->interface->disburse($id);
    }

    public function clearLoan($id) {
        return $this->interface->clearLoan($id);
    }
}
