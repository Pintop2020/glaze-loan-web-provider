<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\CommentRequest;
use App\Http\Resources\Loan\CommentResource;
use App\Interfaces\Loan\CommentInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class CommentController
 * @package App\Http\Controllers\Loan
 * @property-read CommentInterface $interface
 */
class CommentController extends Controller
{

    protected $routeIndex = 'comments';

    protected $pageTitle = 'Comment';
    protected $createRoute = 'comments.create';

    protected $viewIndex = 'comments.index';
    protected $viewCreate = 'comments.create';
    protected $viewEdit = 'comments.edit';
    protected $viewShow = 'comments.show';
    protected $model = 'App\Entities\Loan\Comment';
    protected $modelLoan = 'App\Entities\Loan\Loan';

    /**
     * CommentController constructor.
     * @param CommentInterface $interface
     * @param CommentRequest $request
     * @param CommentResource $resource
     */
    public function __construct(CommentInterface $interface, CommentRequest $request)
    {
        parent::__construct($interface, $request, new  CommentResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }
    /*public function show($id)
    {
        $user = Auth::user();
        $loan = $this->modelLoan::find($id);
        $data['user'] = $user;
        $data['entries'] = $this->modelLoan::whereHas('comments', function($query) use($user){
            $query->where('user_id', $user->id)->orWhereHas('tagged_staff', function($query2) use($user){
                $query2->where('email', $user->email);
            });
        })->paginate(10);
        $data['loan'] = $loan;
        $all = $loan->comments()->doesntHave('user_read')->get();
        foreach($all as $comment){
            $comment->user_read()->attach($comment->id);
        }
        return view($this->viewShow, $data);
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function add($id)
    {
        return $this->interface->add($id);
    }
}
