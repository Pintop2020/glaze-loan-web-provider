<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\RepaymentRequest;
use App\Http\Resources\Loan\RepaymentResource;
use App\Interfaces\Loan\RepaymentInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class RepaymentController
 * @package App\Http\Controllers\Loan
 * @property-read RepaymentInterface $interface
 */
class RepaymentController extends Controller
{

    protected $routeIndex = 'repayments';

    protected $pageTitle = 'Repayment';
    protected $createRoute = 'repayments.create';

    protected $viewIndex = 'repayments.index';
    protected $viewCreate = 'repayments.create';
    protected $viewEdit = 'repayments.edit';
    protected $viewShow = 'repayments.show';
    protected $model = 'App\Entities\Loan\Repayment';

    /**
     * RepaymentController constructor.
     * @param RepaymentInterface $interface
     * @param RepaymentRequest $request
     * @param RepaymentResource $resource
     */
    public function __construct(RepaymentInterface $interface, RepaymentRequest $request)
    {
        parent::__construct($interface, $request, new  RepaymentResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->interface->update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function view()
    {
        $data['repayments'] = SuperM::getSingleDueLoans();
        return view('repayments.view', $data);
    }

    public function payFromWallet()
    {
        return $this->interface->payFromWallet();
    }

    public function initiateCardLink(){
        return $this->interface->initiateCardLink();
    }

    public function payFromCardConfirm(){
        return $this->interface->payFromCardConfirm();
    }

    public function updatePayments($id)
    {
        return $this->interface->updatePayments($id);
    }

    public function charge_auth()
    {
        return $this->interface->charge_auth();
    }
}
