<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\CreditCheckRequest;
use App\Http\Resources\Loan\CreditCheckResource;
use App\Interfaces\Loan\CreditCheckInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class CreditCheckController
 * @package App\Http\Controllers\Loan
 * @property-read CreditCheckInterface $interface
 */
class CreditCheckController extends Controller
{

    protected $routeIndex = 'creditChecks';

    protected $pageTitle = 'CreditCheck';
    protected $createRoute = 'creditChecks.create';

    protected $viewIndex = 'creditChecks.index';
    protected $viewCreate = 'creditChecks.create';
    protected $viewEdit = 'creditChecks.edit';
    protected $viewShow = 'creditChecks.show';

    /**
     * CreditCheckController constructor.
     * @param CreditCheckInterface $interface
     * @param CreditCheckRequest $request
     * @param CreditCheckResource $resource
     */
    public function __construct(CreditCheckInterface $interface, CreditCheckRequest $request)
    {
        parent::__construct($interface, $request, new  CreditCheckResource([]));
        $this->middleware('role:marketer', ['only' => ['create_admin']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->interface->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function initiatePayment()
    {
        return $this->interface->initiatePayment();
    }

    public function create_admin($id)
    {
        return $this->interface->create_admin($id);
    }

    public function initiateFromWallet()
    {
        return $this->interface->initiateFromWallet();
    }

    public function initiateFromCard()
    {
        return $this->interface->initiateFromCard();
    }
}
