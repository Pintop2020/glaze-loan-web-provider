<?php

namespace App\Http\Controllers\Loan;

use App\Http\Requests\Loan\DefaulterRequest;
use App\Http\Resources\Loan\DefaulterResource;
use App\Interfaces\Loan\DefaulterInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class DefaulterController
 * @package App\Http\Controllers\Loan
 * @property-read DefaulterInterface $interface
 */
class DefaulterController extends Controller
{

    protected $routeIndex = 'defaulters';

    protected $pageTitle = 'Defaulter';
    protected $createRoute = 'defaulters.create';

    protected $viewIndex = 'defaulters.index';
    protected $viewCreate = 'defaulters.create';
    protected $viewEdit = 'defaulters.edit';
    protected $viewShow = 'defaulters.show';

    /**
     * DefaulterController constructor.
     * @param DefaulterInterface $interface
     * @param DefaulterRequest $request
     * @param DefaulterResource $resource
     */
    public function __construct(DefaulterInterface $interface, DefaulterRequest $request)
    {
        parent::__construct($interface, $request, new  DefaulterResource([]));
        $this->middleware('role:transaction officer', ['only' => ['clear']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function clear($id)
    {
        return $this->interface->clear($id);
    }
}
