<?php

namespace App\Http\Controllers\Offer;

use App\Http\Requests\Offer\SignatureRequest;
use App\Http\Resources\Offer\SignatureResource;
use App\Interfaces\Offer\SignatureInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class SignatureController
 * @package App\Http\Controllers\Offer
 * @property-read SignatureInterface $interface
 */
class SignatureController extends Controller
{

    protected $routeIndex = 'signatures';

    protected $pageTitle = 'Signature';
    protected $createRoute = 'signatures.create';

    protected $viewIndex = 'signatures.index';
    protected $viewCreate = 'signatures.create';
    protected $viewEdit = 'signatures.edit';
    protected $viewShow = 'signatures.show';

    /**
     * SignatureController constructor.
     * @param SignatureInterface $interface
     * @param SignatureRequest $request
     * @param SignatureResource $resource
     */
    public function __construct(SignatureInterface $interface, SignatureRequest $request)
    {
        parent::__construct($interface, $request, new  SignatureResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

}
