<?php

namespace App\Http\Controllers\Offer;

use App\Http\Requests\Offer\OfferRequest;
use App\Http\Resources\Offer\OfferResource;
use App\Interfaces\Offer\OfferInterface;
use Shamaseen\Repository\Generator\Utility\Controller;
use Illuminate\Support\Facades\Crypt;

/**
 * Class OfferController
 * @package App\Http\Controllers\Offer
 * @property-read OfferInterface $interface
 */
class OfferController extends Controller
{

    protected $routeIndex = 'offers';

    protected $pageTitle = 'Offer';
    protected $createRoute = 'offers.create';

    protected $viewIndex = 'offers.index';
    protected $viewCreate = 'offers.create';
    protected $viewEdit = 'offers.edit';
    protected $viewShow = 'offers.show';

    /**
     * OfferController constructor.
     * @param OfferInterface $interface
     * @param OfferRequest $request
     * @param OfferResource $resource
     */
    public function __construct(OfferInterface $interface, OfferRequest $request)
    {
        parent::__construct($interface, $request, new  OfferResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    public function createNew($id)
    {
        return $this->interface->createNew($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->interface->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->interface->update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function sign($id){
        return $this->interface->sign($id);
    }
}
