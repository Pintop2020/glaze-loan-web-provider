<?php

namespace App\Http\Controllers\Offer;

use App\Http\Requests\Offer\StatusRequest;
use App\Http\Resources\Offer\StatusResource;
use App\Interfaces\Offer\StatusInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class StatusController
 * @package App\Http\Controllers\Offer
 * @property-read StatusInterface $interface
 */
class StatusController extends Controller
{

    protected $routeIndex = 'statuses';

    protected $pageTitle = 'Status';
    protected $createRoute = 'statuses.create';

    protected $viewIndex = 'statuses.index';
    protected $viewCreate = 'statuses.create';
    protected $viewEdit = 'statuses.edit';
    protected $viewShow = 'statuses.show';

    /**
     * StatusController constructor.
     * @param StatusInterface $interface
     * @param StatusRequest $request
     * @param StatusResource $resource
     */
    public function __construct(StatusInterface $interface, StatusRequest $request)
    {
        parent::__construct($interface, $request, new  StatusResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
