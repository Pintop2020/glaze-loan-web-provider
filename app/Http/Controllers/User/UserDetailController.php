<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\UserDetailRequest;
use App\Http\Resources\User\UserDetailResource;
use App\Interfaces\User\UserDetailInterface;
use Shamaseen\Repository\Generator\Utility\Controller;
use App\Entities\User\UserDetail;
use App\Entities\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class UserDetailController
 * @package App\Http\Controllers\User
 * @property-read UserDetailInterface $interface
 */
class UserDetailController extends Controller
{

    protected $routeIndex = 'userDetails';

    protected $pageTitle = 'UserDetail';
    protected $createRoute = 'userDetails.create';

    protected $viewIndex = 'userDetails.index';
    protected $viewCreate = 'userDetails.create';
    protected $viewEdit = 'userDetails.edit';
    protected $viewShow = 'userDetails.show';

    /**
     * UserDetailController constructor.
     * @param UserDetailInterface $interface
     * @param UserDetailRequest $request
     * @param UserDetailResource $resource
     */
    public function __construct(UserDetailInterface $interface, UserDetailRequest $request)
    {
        parent::__construct($interface, $request, new  UserDetailResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->interface->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return $this->interface->destroy($id);
    }

    public function kyc() {
        return $this->interface->kyc();
    }

    public function savePassport(){
        return $this->interface->savePassport();
    }

    public function saveID(){
        return $this->interface->saveID();
    }

    public function saveCompanyID(){
        return $this->interface->saveCompanyID();
    }

    public function checkUploads(){
        return $this->interface->checkUploads();
    }

    public function getBvn($bvn, $phone){
        return $this->interface->getBvn($bvn, $phone);
    }

    public function getBank($bank, $account){
        return $this->interface->getBank($bank, $account);
    }

    public function preview() {
        return $this->interface->preview();
    }

}
