<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\UserRequest;
use App\Http\Resources\User\UserResource;
use App\Interfaces\User\UserInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class UserController
 * @package App\Http\Controllers\User
 * @property-read UserInterface $interface
 */
class UserController extends Controller
{

    protected $routeIndex = 'users';

    protected $pageTitle = 'User';
    protected $createRoute = 'users.create';

    protected $viewIndex = 'users.index';
    protected $viewCreate = 'users.create';
    protected $viewEdit = 'users.edit';
    protected $viewShow = 'users.show';

    /**
     * UserController constructor.
     * @param UserInterface $interface
     * @param UserRequest $request
     * @param UserResource $resource
     */
    public function __construct(UserInterface $interface, UserRequest $request)
    {
        parent::__construct($interface, $request, new  UserResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->interface->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    public function login()
    {
      return $this->interface->login();
    }

    public function storeUser()
    {
      return "dfygdjhkj";
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->interface->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->interface->update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function home() {
        return $this->interface->home();
    }

    public function verify_referee($ref){
        return $this->interface->verify_referee($ref);
    } 

    public function perm($id)
    {
        return $this->interface->perm($id);
    }

    public function custom_index($type){
        return $this->interface->custom_index($type);
    }

    public function profile()
    {
        return $this->interface->profile();
    }

    public function security()
    {
        return $this->interface->security();
    }

    public function notificationSetting()
    {
        return $this->interface->notificationSetting();
    }

    public function notificationSettingPost()
    {
        return $this->interface->notificationSettingPost();
    }

    public function securityPost()
    {
        return $this->interface->securityPost();
    }

    public function logout()
    {
        return $this->interface->logout();
    }

    public static function number_format_short( $n, $precision = 2 ) {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

      // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
      // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }

        return $n_format . $suffix;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////* Staff Methods here *////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function staff(){
        return $this->interface->staff();
    }

    public function new_staff(){
        return $this->interface->new_staff();
    }

    public function new_staff_post(){
        return $this->interface->new_staff_post();
    }

}
