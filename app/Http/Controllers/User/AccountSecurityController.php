<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\AccountSecurityRequest;
use App\Http\Resources\User\AccountSecurityResource;
use App\Interfaces\User\AccountSecurityInterface;
use Shamaseen\Repository\Generator\Utility\Controller;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class AccountSecurityController
 * @package App\Http\Controllers\User
 * @property-read AccountSecurityInterface $interface
 */
class AccountSecurityController extends Controller
{
    use AppResponse;

    protected $routeIndex = 'accountSecurities';

    protected $pageTitle = 'AccountSecurity';
    protected $createRoute = 'accountSecurities.create';

    protected $viewIndex = 'accountSecurities.index';
    protected $viewCreate = 'accountSecurities.create';
    protected $viewEdit = 'accountSecurities.edit';
    protected $viewShow = 'accountSecurities.show';

    /**
     * AccountSecurityController constructor.
     * @param AccountSecurityInterface $interface
     * @param AccountSecurityRequest $request
     * @param AccountSecurityResource $resource
     */
    public function __construct(AccountSecurityInterface $interface, AccountSecurityRequest $request)
    {
        parent::__construct($interface, $request, new  AccountSecurityResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function validatePin(){
        return $this->interface->validatePin();
    }
}
