<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\LoginLogRequest;
use App\Http\Resources\User\LoginLogResource;
use App\Interfaces\User\LoginLogInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class LoginLogController
 * @package App\Http\Controllers\User
 * @property-read LoginLogInterface $interface
 */
class LoginLogController extends Controller
{

    protected $routeIndex = 'loginLogs';

    protected $pageTitle = 'LoginLog';
    protected $createRoute = 'loginLogs.create';

    protected $viewIndex = 'loginLogs.index';
    protected $viewCreate = 'loginLogs.create';
    protected $viewEdit = 'loginLogs.edit';
    protected $viewShow = 'loginLogs.show';

    /**
     * LoginLogController constructor.
     * @param LoginLogInterface $interface
     * @param LoginLogRequest $request
     * @param LoginLogResource $resource
     */
    public function __construct(LoginLogInterface $interface, LoginLogRequest $request)
    {
        parent::__construct($interface, $request, new  LoginLogResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function user_login_logs($id){
        return $this->interface->user_login_logs($id);
    }
}
