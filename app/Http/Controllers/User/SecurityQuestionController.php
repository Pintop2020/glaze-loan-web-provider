<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\SecurityQuestionRequest;
use App\Http\Resources\User\SecurityQuestionResource;
use App\Interfaces\User\SecurityQuestionInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class SecurityQuestionController
 * @package App\Http\Controllers\User
 * @property-read SecurityQuestionInterface $interface
 */
class SecurityQuestionController extends Controller
{

    protected $routeIndex = 'securityQuestions';

    protected $pageTitle = 'SecurityQuestion';
    protected $createRoute = 'securityQuestions.create';

    protected $viewIndex = 'securityQuestions.index';
    protected $viewCreate = 'securityQuestions.create';
    protected $viewEdit = 'securityQuestions.edit';
    protected $viewShow = 'securityQuestions.show';

    /**
     * SecurityQuestionController constructor.
     * @param SecurityQuestionInterface $interface
     * @param SecurityQuestionRequest $request
     * @param SecurityQuestionResource $resource
     */
    public function __construct(SecurityQuestionInterface $interface, SecurityQuestionRequest $request)
    {
        parent::__construct($interface, $request, new  SecurityQuestionResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
