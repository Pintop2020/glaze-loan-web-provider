<?php

namespace App\Http\Controllers\User\Referral;

use App\Http\Requests\User\Referral\RefereeDetailRequest;
use App\Http\Resources\User\Referral\RefereeDetailResource;
use App\Interfaces\User\Referral\RefereeDetailInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class RefereeDetailController
 * @package App\Http\Controllers\User\Referral
 * @property-read RefereeDetailInterface $interface
 */
class RefereeDetailController extends Controller
{

    protected $routeIndex = 'refereeDetails';

    protected $pageTitle = 'RefereeDetail';
    protected $createRoute = 'refereeDetails.create';

    protected $viewIndex = 'refereeDetails.index';
    protected $viewCreate = 'refereeDetails.create';
    protected $viewEdit = 'refereeDetails.edit';
    protected $viewShow = 'refereeDetails.show';

    /**
     * RefereeDetailController constructor.
     * @param RefereeDetailInterface $interface
     * @param RefereeDetailRequest $request
     * @param RefereeDetailResource $resource
     */
    public function __construct(RefereeDetailInterface $interface, RefereeDetailRequest $request)
    {
        parent::__construct($interface, $request, new  RefereeDetailResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
