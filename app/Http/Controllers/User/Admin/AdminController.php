<?php

namespace App\Http\Controllers\User\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entities\Loan\Loan;
use App\Entities\Transaction\Transaction;
use Carbon\Carbon;
use App\Traits\AppResponse;

class AdminController extends Controller
{
	use AppResponse;

    public function __construct()
    {
        $this->middleware('is_admin');
    }

    public function home(){
    	$user = Auth::user();
    	$data['user'] = $user;
    	$data['this_month_loans'] = $this->getMonthLoanSum(true);
    	$data['this_month_percent'] = $this->getMonthLoanPercentage();
    	$data['this_week_loans'] = $this->getWeekLoanSum(true);
    	$data['this_week_percent'] = $this->getWeekLoanPercentage(true);
        $data['transactions'] = Transaction::latest()->limit(5)->get();
    	return view('admin.home', $data);
    }

    private static function getMonthLoanSum($isCurrent = false){
    	if($isCurrent) return Loan::whereDate('created_at', '>=', Carbon::now()->startOfMonth()->subMonth()->toDateString())->sum('details->amount');
    	else return Loan::whereDate('created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())->sum('details->amount');
    }

    private static function getMonthLoanPercentage(){
    	$thisMonth = static::getMonthLoanSum(true);
    	$lastMonth = static::getMonthLoanSum();
    	$overAllLoan = Loan::sum('details->amount');
    	$result = $thisMonth > 0 && $lastMonth > 0 ? (($thisMonth-$lastMonth)/$overAllLoan)*100 : 0;
    	return $result;
    }

    private static function getWeekLoanSum($isCurrent = false){
    	if(!$isCurrent){
			$previous_week = strtotime("-1 week +1 day");
			$start_week = strtotime("last sunday midnight",$previous_week);
			$end_week = strtotime("next saturday",$start_week);
			$start_week = date("Y-m-d",$start_week);
			$end_week = date("Y-m-d",$end_week);
			$result = Loan::whereBetween('created_at', [$start_week, $end_week])->sum('details->amount');
			return $result;
    	}else {
    		$result = Loan::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('details->amount');
    		return $result;
    	}
    }

    private static function getWeekLoanPercentage(){
    	$thisWeek = static::getWeekLoanSum(true);
    	$lastWeek = static::getWeekLoanSum();
    	$overAllLoan = Loan::sum('details->amount');
    	$result = $thisWeek > 0 && $lastWeek > 0 ? (($thisWeek-$lastWeek)/$overAllLoan)*100 : 0;
    	return $result;
    }
}
