<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\AppResponse;

class NotificationController extends Controller
{
    use AppResponse;


    public function view($id)
    {
    	$user = Auth::user();
    	$notif = $user->notifications()->where('id', $id)->first();
    	$notif->update(['read_at'=>\Carbon\Carbon::now()]);
    	$data = [
    		'user'=>$user,
    		'notification'=>$notif,
    	];
    	return view('notifications.view', $data);
    }

    public function read()
    {
    	$user = Auth::user();
    	$user->unreadNotifications()->update(['read_at'=>\Carbon\Carbon::now()]);
    	return redirect()->back();
    }

    public function index()
    {
        $user = Auth::user();
        $data = $user->notifications()->latest()->get();
        $user->notifications->markAsRead();
        return $this->success("Entries retrieved", $data);
    }
}
