<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupportRequest;
use App\Http\Resources\SupportResource;
use App\Interfaces\SupportInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class SupportController
 * @package App\Http\Controllers
 * @property-read SupportInterface $interface
 */
class SupportController extends Controller
{

    protected $routeIndex = 'supports';

    protected $pageTitle = 'Support';
    protected $createRoute = 'supports.create';

    protected $viewIndex = 'supports.index';
    protected $viewCreate = 'supports.create';
    protected $viewEdit = 'supports.edit';
    protected $viewShow = 'supports.show';

    /**
     * SupportController constructor.
     * @param SupportInterface $interface
     * @param SupportRequest $request
     * @param SupportResource $resource
     */
    public function __construct(SupportInterface $interface, SupportRequest $request)
    {
        parent::__construct($interface, $request, new  SupportResource([]));
        $this->middleware('role:user|support|super admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->interface->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->interface->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return $this->interface->store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->interface->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }

    public function closeSupport($id)
    {
        return $this->interface->closeSupport($id);
    }
}
