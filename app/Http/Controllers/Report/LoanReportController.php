<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\LoanReport;
use Excel;

class LoanReportController extends Controller
{

	public function __construct()
    {
        $this->middleware('role:super admin|risk|coo|transaction officer|director|chairman');
    }

    public function __invoke()
    {
    	return view('reports.loanReports');
    }

    public function generate() 
	{
		$name = 'Loan_report_generated_on_'.date('M_d_Y_h_i_A').'.xlsx';
	    return Excel::download(new LoanReport, $name);
	}
}
