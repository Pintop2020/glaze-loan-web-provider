<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\RepaymentReport;
use Excel;

class RepaymentReportController extends Controller
{
    public function __invoke()
    {
    	return view('reports.repaymentReports');
    }

    public function generate() 
	{
		$name = 'Repayments_report_generated_on_'.date('M_d_Y_h_i_A').'.xlsx';
	    return Excel::download(new RepaymentReport, $name);
	}
}