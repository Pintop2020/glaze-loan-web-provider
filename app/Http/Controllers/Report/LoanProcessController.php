<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\LoanProcess;
use Excel;
use App\Entities\User\User;

class LoanProcessController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:super admin|risk|coo|transaction officer|director|chairman');
    }

    public function __invoke()
    {
    	$data['staff'] = User::whereHas('roles', function($query){ $query->where('name', 'Marketer'); })->get();
    	return view('reports.loanProcess', $data);
    }

    public function generate() 
	{
		$name = 'Loan_process_generated_on_'.date('M_d_Y_h_i_A').'.xlsx';
	    return Excel::download(new LoanProcess, $name);
	}
}
