<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\CustomerBase;
use Excel;

class CustomerBaseController extends Controller
{
	public function __construct()
	{
		$this->middleware('role:marketer');
	}

    public function __invoke()
    {
    	return view('reports.customerBase');
    }

    public function generate() 
	{
		$name = 'Customer_base_report_generated_on_'.date('M_d_Y_h_i_A').'.xlsx';
	    return Excel::download(new CustomerBase, $name);
	}
}
