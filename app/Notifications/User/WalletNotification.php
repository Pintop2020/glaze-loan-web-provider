<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WalletNotification extends Notification
{
    use Queueable;

    public $first_name, $old_amount, $amount;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($first_name, $old_amount, $amount)
    {
        $this->first_name = $first_name;
        $this->old_amount = $old_amount;
        $this->amount = $amount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->old_amount > $this->amount) return (new MailMessage)
            ->greeting('Hello '.ucwords($this->first_name))
            ->line('Your Glaze wallet was debited ₦'.number_format($this->old_amount - $this->amount, 2))
            ->line('Your new wallet balance is ₦'.number_format($this->amount,2))
            ->line('Thank you for using our application!');

        if($this->old_amount < $this->amount) return (new MailMessage)
            ->greeting('Hello '.ucwords($this->first_name))
            ->line('Your Glaze wallet has been credited with ₦'.number_format($this->amount- $this->old_amount, 2))
            ->line('Your new wallet balance is ₦'.number_format($this->amount,2))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if($this->old_amount > $this->amount) return [
            'body' => 'Your Glaze wallet was debited ₦'.number_format($this->old_amount - $this->amount, 2).'. Your new wallet balance is ₦'.number_format($this->amount,2),
            'icon' => 'icon icon-circle bg-success-dim ni ni-curve-up-left',
        ];
        if($this->old_amount < $this->amount) return [
            'body' => 'Your Glaze wallet has been credited with ₦'.number_format($this->amount- $this->old_amount, 2).'. Your new wallet balance is ₦'.number_format($this->amount,2),
            'icon' => 'icon icon-circle bg-success-dim ni ni-curve-up-left',
        ];
    }
}
