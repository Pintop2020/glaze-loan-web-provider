<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AccessNotification extends Notification
{
    use Queueable;

    public $first_name, $isBlocked;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($first_name, $isBlocked)
    {
        $this->first_name = $first_name;
        $this->isBlocked = $isBlocked;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->isBlocked) return (new MailMessage)
                    ->greeting('Hello '.ucwords($this->first_name))
                    ->line('We are sorry to inform you that your account has been restricted from accessing our platform. Please contact support to get your account back.')
                    ->line('Thank you for using our application!');
        return (new MailMessage)
                    ->greeting('Hello '.ucwords($this->first_name))
                    ->line('Your account has been granted your permission back to you. You can now access your account and enjoy the benfits of our platform.')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if($this->isBlocked) return [
            'body' => 'Oops!!! You have been blocked from accessing our <span>platform</span>.',
            'icon' => 'icon icon-circle bg-danger-dim ni ni-curve-down-right',
        ];

        return [
            'body' => 'Hurray!!! You been granted permission to access our platform once again.',
            'icon' => 'icon icon-circle bg-success-dim ni ni-curve-up-right',
        ];
    }
}
