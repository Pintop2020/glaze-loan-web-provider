<?php

namespace App\Notifications\Application;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SuccessNotification extends Notification
{
    use Queueable;

    public $first_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello '.ucwords($this->first_name))
                    ->line('<img style="width:88px; margin-bottom:24px;" src="'.asset('images/kyc-success.png').'" alt="Verified"><h2 style="font-size: 18px; color: #1ee0ac; font-weight: 400; margin-bottom: 8px;">Your KYC has been submitted successfully.</h2><p>You are almost there.</p>')
                    ->line('You have updated your KYC successfully! Click the button below to view your changes')
                    ->action('View KYC', url('/application/preview'))
                    ->line('If you did not perform this action, please contact our support or simply change the password to your profile.')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'body' => 'Hurray!!! You have successfully updated your <span>KYC</span>.',
            'icon' => 'icon icon-circle bg-info-dim ni ni-curve-down-right',
        ];
    }
}
