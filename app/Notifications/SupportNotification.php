<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SupportNotification extends Notification
{
    use Queueable;

    public $first_name, $ticket_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($first_name, $ticket_id)
    {
        $this->first_name = $first_name;
        $this->ticket_id = $ticket_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */


    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hi '.ucwords($this->first_name))
                    ->line('Thanks for contacting Glaze Credit Limited! Your request has been received and we\'ll get back to you as soon as possible.')
                    ->line('If you\'d like to send more information, kindly reply to this email. You can also review the status of this request and add comments via your support help desk.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
