<?php

namespace App\Notifications\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LoanApplication extends Notification
{
    use Queueable;

    public $first_name, $loan;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($first_name, $loan)
    {
        $this->first_name = $first_name;
        $this->loan = $loan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello '.ucwords($this->first_name))
                    ->line('This is a notification of an action that just occured on your profile.')
                    ->line('You have applied for a loan of NGN'.number_format($this->loan->amount,2).' for a tenor of '.$this->loan->tenor.' months.')
                    ->line('This loan has an interest of '.$this->loan->interest.'% and processing fee of '.$this->loan->processing.'%.')
                    ->line('This loan will be processed for approval and verification withing 24 working hours.')
                    ->action('View Loans', url('/loans'))
                    ->line('If you did not perform this action, please contact our support or simply change the password to your profile.')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'body' => 'Hurray!!! You have successfully updated your <span>KYC</span>.',
            'icon' => 'icon icon-circle bg-info-dim ni ni-curve-down-right',
        ];
    }
}
