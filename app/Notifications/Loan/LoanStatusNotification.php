<?php

namespace App\Notifications\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LoanStatusNotification extends Notification
{
    use Queueable;

    public $first_name, $status, $loan;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($first_name, $status, $loan=null)
    {
        $this->first_name = $first_name;
        $this->status = $status;
        $this->loan = $loan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->status == 'declined') return (new MailMessage)
                    ->greeting('Hello '.ucwords($this->first_name))
                    ->line('We acknowledge the receipt of your application for a credit facility. At the moment, we are unable to proceed with your request, kindly click on this link below to login and check our other products.')
                    ->action('Sign In', route('login'))
                    ->line('Thank you for using our application!');

        if($this->status == 'approved') return (new MailMessage)
                    ->greeting('Hurray!')
                    ->line('We are glad to inform you that your loan has been approved.')
                    ->line('Please click the button below to view and sign your offer letter now!')
                    //->action('View Loan', route('loans.view.single', \Illuminate\Support\Facades\Crypt::encryptString($this->loan)))
                    ->line('Thank you for using our application!');

        if($this->status == 'active') return (new MailMessage)
                    ->greeting('Hurray!')
                    ->line('We are glad to inform you that your loan has disbursed to your glaze wallet..')
                    ->line('Login to your profile to pay out into your bank account.')
                    ->line('Thank you for using our application!');

        if($this->status == 'repaid') return (new MailMessage)
                    ->greeting('Congratulations!')
                    ->line('Your loan repayment is now complete.')
                    ->line('You can take another loan with us now.')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if($this->status == 'approved') return [
            'body' => 'Hurray!!! Your <span>loan</span> has been approved.',
            'icon' => 'icon icon-circle bg-success-dim ni ni-curve-up-right',
        ];
        if($this->status == 'declined')return [
            'body' => 'Oh no!!! Your <span>loan</span> has been declined.',
            'icon' => 'icon icon-circle bg-danger-dim ni ni-curve-down-right',
        ];
        if($this->status == 'active')return [
            'body' => 'Yes!!! Your <span>loan</span> has been disbursed.',
            'icon' => 'icon icon-circle bg-success-dim ni ni-curve-down-right',
        ];
        if($this->status == 'repaid')return [
            'body' => 'Hurray!!! Your <span>loan</span> has been repaid.',
            'icon' => 'icon icon-circle bg-success-dim ni ni-curve-down-right',
        ];
    }
}
