<?php

namespace App\Repositories\Offer;

use App\Entities\Offer\Offer;
use App\Interfaces\Offer\OfferInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Entities\Loan\Loan;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Traits\AppResponse;

/**
 * Class OfferRepository
 * @package App\Repositories\Offer
 * @property-read Offer $model
 */
class OfferRepository extends AbstractRepository implements OfferInterface
{
    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Offer::class;
    }

    public function createNew($id)
    {
        $data['loan'] = Loan::find(SuperM::decrypt($id));
        return view('offers.create', $data);
    }

    public function store()
    {
        $loan = Loan::find(request()->loan);
        if(!$loan->offer){
            if($loan->folder){
                $loan->offer()->save(new Offer(['offer_details'=>request()->except('_token', 'loan')]));
                return redirect('/loans/get/pending')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'You have created offer for this loan.',showConfirmButton: false,timer: 3000});});</script>");
            }else {
                return redirect('/loans/get/pending')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'error',title: 'This loan doesn't have a folder attached to it,showConfirmButton: false,timer: 3000});});</script>");
            }
        }else {
            return redirect('/loans/get/pending')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'error',title: 'This loan already have an offer attached.',showConfirmButton: false,timer: 3000});});</script>");
        }
    }

    public function edit($id)
    {
        $data['entity'] = $this->model->find(SuperM::decrypt($id));
        return view('offers.edit', $data);
    }

    public function update($entityId=0,$attributes=[])
    {
        $entity = $this->model->find($entityId);
        $data = request()->except('_token', '_method');
        $entity->offer_details = $data;
        $entity->save();
        return redirect('/loans/get/pending')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Offer edited successfully!',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function show($entityId)
    {
        if(request()->wantsJson()){
            $loan = Loan::find($entityId);
            $offer = $loan->offer;
            $data['offer'] = $offer;
            $data['loan'] = $loan;
            $saveName = 'offers/offer_letter_'.uniqid().'_'.time().'_'.mt_rand(000000,999999).'.pdf';
            $pdf = PDF::loadView('offers.show', $data)->save($saveName);
            $data['url'] = $saveName;
            return $this->success('Offer Retrieved', $data);
        }else {
            $offer = $this->model->find(SuperM::decrypt($entityId));
            $data['offer'] = $offer;
            $data['loan'] = $offer->loan;
            $saveName = 'offers/offer_letter_'.uniqid().'_'.time().'_'.mt_rand(000000,999999).'.pdf';
            $pdf = PDF::loadView('offers.show', $data)->save($saveName);
            $header = SuperM::previewer($saveName);
            return view('offers.previewer', ['file'=>$header]);
        }
    }

    public function sign($id)
    {
        $offer = $this->model->find(SuperM::decrypt($id));
        $data['offer'] = $offer;
        $data['loan'] = $offer->loan;
        if(!$offer->signature){
            return view('offers.sign', $data);
        }else {
            return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'This loan has already been signed!', 'error');});</script>");
        }
    }
}
