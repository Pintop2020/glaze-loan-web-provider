<?php

namespace App\Repositories\Offer;

use App\Entities\Offer\Signature;
use App\Entities\Offer\Offer;
use App\Entities\Loan\Loan;
use App\Interfaces\Offer\SignatureInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class SignatureRepository
 * @package App\Repositories\Offer
 * @property-read Signature $model
 */
class SignatureRepository extends AbstractRepository implements SignatureInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Signature::class;
    }

    public function store()
    {
        $loan = Loan::find(request()->loan);
        if($loan->offer->signature){
            return redirect('/loans')->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'This loan has already been signed!', 'error');});</script>");
        }
        $compare = '<svg xmlns="http://www.w3.org/2000/svg" width="15cm" height="15cm" viewBox="0 0 398 198"><g fill="#fff"><rect x="0" y="0" width="398" height="198"></rect><g fill="none" stroke="#000" stroke-width="2"></g></g></svg>';
        if(SuperM::removeSpacesNTabs(request()->signature) == $compare || SuperM::removeSpacesNTabs(request()->signature2) == $compare){
            return redirect()->back()->with('message', '<div class="alert alert-danger">Please ensure the two signatory sheet provided are signed.</div>');
        }else {
            $loan->offer->signature()->save(new Signature(['signature_1'=>SuperM::removeSpacesNTabs(request()->signature), 'signature_confirm'=>SuperM::removeSpacesNTabs(request()->signature)]));
            return redirect('/loans')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Your offer has been signed successfully!',showConfirmButton: false,timer: 3000});});</script>");
        }
    }
}
