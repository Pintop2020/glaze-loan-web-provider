<?php

namespace App\Repositories\Offer;

use App\Entities\Offer\Status;
use App\Interfaces\Offer\StatusInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class StatusRepository
 * @package App\Repositories\Offer
 * @property-read Status $model
 */
class StatusRepository extends AbstractRepository implements StatusInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Status::class;
    }
}
