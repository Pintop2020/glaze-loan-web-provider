<?php

namespace App\Repositories;

use App\Entities\SupportResponse;
use App\Interfaces\SupportResponseInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Entities\Support;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class SupportResponseRepository
 * @package App\Repositories
 * @property-read SupportResponse $model
 */
class SupportResponseRepository extends AbstractRepository implements SupportResponseInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return SupportResponse::class;
    }

    public function index()
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['supports'] = Support::latest()->get();
        return view('supportResponses.index', $data);
    }

    public function store()
    {
        $user = Auth::user();
        $support = Support::find(request()->support);
        $response = $this->model->create(['response'=>request()->message]);
        $support->responses()->save($response);
        $user->support_responses()->save($response);
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Response submitted!',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function show($id)
    {
        $data['support'] = Support::find(SuperM::decrypt($id));
        return view('supportResponses.show', $data);
    }

    public function closeSupport($id)
    {
        $support = Support::find(SuperM::decrypt($id));
        $support->update(['status'=>'closed']);
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Ticket closed!',showConfirmButton: false,timer: 3000});});</script>");
    }
}
