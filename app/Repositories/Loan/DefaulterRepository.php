<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Defaulter;
use App\Interfaces\Loan\DefaulterInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Entities\Loan\Loan;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class DefaulterRepository
 * @package App\Repositories\Loan
 * @property-read Defaulter $model
 */
class DefaulterRepository extends AbstractRepository implements DefaulterInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Defaulter::class;
    }

    public function clear($id)
    {
        $loan = Loan::find(SuperM::decrypt($id));
        $loan->defaults()->delete();
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'User Default charges cleared successfully',showConfirmButton: false,timer: 3000});});</script>");
    }
}
