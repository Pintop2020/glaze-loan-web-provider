<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Comment;
use App\Interfaces\Loan\CommentInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Entities\User\User;
use App\Entities\Loan\Loan;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;
/**
 * Class CommentRepository
 * @package App\Repositories\Loan
 * @property-read Comment $model
 */
class CommentRepository extends AbstractRepository implements CommentInterface
{
    protected $with = [];

    protected $routeIndex = 'comments';

    protected $pageTitle = 'Comment';
    protected $createRoute = 'comments.create';

    protected $viewIndex = 'comments.index';
    protected $viewCreate = 'comments.create';
    protected $viewEdit = 'comments.edit';
    protected $viewShow = 'comments.show';
    protected $model = 'App\Entities\Loan\Comment';
    protected $modelLoan = 'App\Entities\Loan\Loan';

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Comment::class;
    }

    public function index()
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['entries'] = $this->modelLoan::whereHas('comments', function($query) use($user){
            $query->where('user_id', $user->id)->orWhereHas('tagged_staff', function($query2) use($user){
                $query2->where('email', $user->email);
            });
        })->paginate(10);
        return view('comments.index', $data);
    }

    public function add($id)
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['loan'] = Loan::find(SuperM::decrypt($id));
        $data['staff'] = User::where('is_admin', true)->where('id','<>',$user->id)->get();
        return view('comments.create', $data);
    }

    public function store()
    {
        $user = Auth::user();
        $comment = $this->model->create(['comments'=>request()->message]);
        $user->comments()->save($comment);
        $comment->loan()->attach(request()->loan);
        $comment->tagged_staff()->attach(request()->staff);
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Comment sent to all tagged staff.',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function show($id)
    {
        $user = Auth::user();
        $data['user'] = $user;
        $comment = $this->model->find($id);
        if(!$comment->user_read()->where('users.id', $user->id)->first()) $comment->user_read()->attach($user->id);
        $data['comment'] = $comment;
        $data['entries'] = $this->modelLoan::whereHas('comments', function($query) use($user){
            $query->where('user_id', $user->id)->orWhereHas('tagged_staff', function($query2) use($user){
                $query2->where('email', $user->email);
            });
        })->paginate(10);
        $data['loan'] = $comment->loan()->first();
        return view('comments.show', $data);
    }
}
