<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Loan;
use App\Interfaces\Loan\LoanInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use App\Traits\AppResponse;
use App\Traits\FileUploadManager;
use App\Entities\Offer\Status;
use App\Entities\Loan\Folder;
use App\Entities\Transaction\Transaction;
use App\Entities\Loan\Repayment;
use App\Http\Controllers\GlobalMethods as SuperM;
use Spatie\Permission\Models\Role;

/**
 * Class LoanRepository
 * @package App\Repositories\Loan
 * @property-read Loan $model
 */
class LoanRepository extends AbstractRepository implements LoanInterface
{
    use AppResponse, FileUploadManager;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Loan::class;
    }

    public function index()
    {
        $user = Auth::user();
        if(request()->wantsJson()){
            $data['loans'] = $user->loans()->with(['status', 'repayments', 'folder', 'offer', 'defaults', 'disbursement'])->latest()->get();
            $data['counts'] = count($data['loans']);
            if(request()->from && request()->to){
                $from = date(request()->from);
                $to = date(request()->to);
                $options['loans'] = $this->model->with(['status', 'repayments', 'folder', 'offer', 'defaults', 'disbursement'])->whereBetween('created_at', [$from, $to])->get();
                $options['counts'] = count($options['loans']);
                return $this->success("Loans retrieved", $options);
            }
            return $this->success("Loans retrieved", $data);
        }else {
            $data['user'] = $user;
            $data['loans'] = $user->loans()->latest()->get();
            return view('loans.user.index', $data);
        }
    }

    public function custom_index($type)
    {
        $user = Auth::user();;
        $data['user'] = $user;
        $data['type'] = $type;
        if($user->roles()->count() < 2 && $user->hasRole('marketer')){
            if($type == 'active')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','active');})->whereHas('user', function($query) use($user){
                    $query->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                        $query->where('email', $user->email);
                    });
                })->get();
            }elseif($type == 'pending')
            {
                $data['entities'] = $this->model->doesntHave('status')->whereHas('user', function($query) use($user){
                    $query->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                        $query->where('email', $user->email);
                    });
                })->get();
            }elseif($type == 'declined')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','declined');})->whereHas('user', function($query) use($user){
                    $query->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                        $query->where('email', $user->email);
                    });
                })->get();
            }elseif($type == 'approved')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','approved');})->whereHas('user', function($query) use($user){
                    $query->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                        $query->where('email', $user->email);
                    });
                })->get();
            }elseif($type == 'repaid')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','repaid');})->whereHas('user', function($query) use($user){
                    $query->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                        $query->where('email', $user->email);
                    });
                })->get();
            }
        }else {
            if($type == 'active')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','active');})->get();
            }elseif($type == 'pending')
            {
                $data['entities'] = $this->model->doesntHave('status')->get();
            }elseif($type == 'declined')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','declined');})->get();
            }elseif($type == 'approved')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','approved');})->get();
            }elseif($type == 'repaid')
            {
                $data['entities'] = $this->model->whereHas('status', function ($query) { $query->where('status','repaid');})->get();
            }
        }
        
        return view('loans.index', $data);
    }

    public function create($attributes=[])
    {
        $user = Auth::user();
        $loan1 = $user->loans()->whereHas('status', function ($query) { 
            $query->where(['status'=>'active', 'status'=>'approved']);
        })->count();
        $loan2 = $user->loans()->doesntHave('status')->count();
        $loans = $loan1+$loan2;
        if($loans > 0){
            return view('loans.active_loan_error');
        }else {
            $data['user'] = $user;
            return view('loans.user.create', $data);
        }
    }

    public function store()
    {
        $user = Auth::user();
        $pin = $user->account_security;
        if(request()->wantsJson()){
            $has = $user->loans()->whereHas('status', function ($query) { $query->where('status', 'active');})->count();
            $dontHave = $user->loans()->whereDoesntHave('status')->count();
            if($has > 0){
                return $this->error('You have an active loan');
            }else if($dontHave > 0){
                return $this->error('You have a loan pending approval');
            }else {
                $data = request()->except(['_token', 'utility_bill', 'statements', 'employment_letter']);
                $data['credit_check'] = false;
                $data['uploads'] = [
                    'utility_bill'=>$this->uploadSingle(request('utility_bill'), 'Utilities'), 
                    'employment_letter'=>$this->uploadSingle(request('employment_letter'), 'Employments'), 
                    'statements'=>$this->uploadSingle(request('statements'), 'Statements')
                ];
                $user->loans()->save(new Loan(['details'=>$data]));
                return $this->success('Loan queued for processing');
                
            }
        }else {
            if($pin->account_pin == request()->pin){
                $has = $user->loans()->whereHas('status', function ($query) { $query->where('status', 'active');})->count();
                $dontHave = $user->loans()->whereDoesntHave('status')->count();
                if($has > 0){
                    return $this->error('You have an active loan');
                }else if($dontHave > 0){
                    return $this->error('You have a loan pending approval');
                }else {
                    if(session('employment_letter') && session('utility_bill') && session('statements')){
                        $data = request()->except(['_token', 'pin']);
                        $data['credit_check'] = false;
                        $data['uploads'] = [
                            'utility_bill'=>session('utility_bill'), 
                            'employment_letter'=>session('employment_letter'), 
                            'statements'=>session('statements')
                        ];
                        $user->loans()->save(new Loan(['details'=>$data]));
                        return $this->success('Loan queued for processing');
                    }else {
                        return $this->error('Please upload the required documents');
                    }
                    
                }
                session()->forget('utility_bill', 'employment_letter', 'statements');
            }else {
                return $this->error('Invalid pin supplied');
            }
        }
    }

    public function saveEmployment(){
        $image = $this->uploadSingle(request('file'), 'Employments');
        session(['employment_letter'=>$image]);
    }

    public function saveUtility(){
        $image = $this->uploadSingle(request('file'), 'Utilities');
        session(['utility_bill'=>$image]);
    }

    public function saveStatements(){
        $image = $this->uploadSingle(request('file'), 'Statements');
        session(['statements'=>$image]);
    }

    public function destroy($id)
    {
        $loan = Loan::find(SuperM::decrypt($id));
        $loan->stages()->first()->delete();
    }

    public function show($id)
    {
        $user = Auth::user();
        if($user->is_admin){
            $loan = $this->model->find(SuperM::decrypt($id));;
            $data['loan'] = $loan;
            $data['user'] = Auth::user();
            if($loan->status){
                return view('loans.show', $data);
            }else {
                return view('loans.show_pending', $data);
            }
        }else {
            if(request()->wantsJson()){
                $data['loan'] = $this->model->with(['stages', 'repayments', 'folder', 'status', 'offer', 'defaults', 'disbursement', 'credit_checks'])->find($id);
                $data['repayments'] = $data['loan']->repayments()->sum('amount');
                $data['defaults'] = $data['loan']->defaults()->sum('amount');
                $data['pending_repayments'] = SuperM::getSingleDueLoans();
                $data['inv'] = $data['loan']->id();
                $data['sub'] = $this->getLoanData($data['loan']->id);
                return $this->success("Entry retrieved", $data);
            }else {
                $data['loan'] = $this->model->find(SuperM::decrypt($id));
                $data['repayments'] = SuperM::getSingleDueLoans();
                return view('loans.user.show', $data);
            }
            
        }
    }

    private function getLoanData($loan)
    {
        $loan = $this->model->find($loan);
        $disbursed = SuperM::getDisburseAmount($loan->offer);
        $amount_requested = json_decode($loan->details)->amount;
        $amouunt_approved = json_decode($loan->offer->offer_details)->amount;
        $percent = ($amouunt_approved/$amount_requested)*100;
        //
        $offer = $loan->offer;
        $user = $loan->user->my_details->data();
        $offerDets = $offer->data();
        $processing = $offerDets->amount  * ($offerDets->processing/100);
        $interest_monthly = $offerDets->amount * ($offerDets->interest/100);
        $moratorium = 0;
        $montlyRepay = $offerDets->amount / $offerDets->tenor;
        $principal_repay = $offerDets->amount / $offerDets->tenor;
        if($offerDets->moratorium > 0){
            $moratorium = ($offerDets->moratorium/30)*$interest_monthly;
            $mdivi = $moratorium/$offerDets->tenor;
            $montlyRepay += $mdivi;;
        }
        $deduct = $processing + $offerDets->outstanding;
        $todisburse = $offerDets->amount - $deduct;
        $totalRepay = $montlyRepay + $interest_monthly;
        $ddifm = '';
        $dpro = $offerDets->moratorium*1;
        $dten = $offerDets->tenor*1;
        $deductMonth = 0;
        if($offerDets->payment_starts == "this")
            $deductMonth = 1;
        if($dpro > 0){
            $datetime = new DateTime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at));
            $datetime->modify('+'.$dpro.' day');
            $ddifm = $datetime->format('Y-m-d H:i:s');
        }
        $perPaid = $loan->repayments()->sum('amount') > 0 ? ($loan->repayments()->sum('amount') / ($totalRepay*$offerDets->tenor))/100 : 0;
        $data = [
            'disbursed'=>$disbursed,
            'amount_requested' =>$amount_requested*1,
            'amouunt_approved'=>$amouunt_approved*1,
            'percent'=>$percent,
            'processing'=>$processing,
            'interest_monthly'=>$interest_monthly,
            'moratorium'=>$moratorium,
            'montlyRepay'=>$montlyRepay,
            'principal_repay'=>$principal_repay,
            'deduct'=>$deduct,
            'todisburse'=>$todisburse,
            'totalRepay'=>$totalRepay,
            'ddifm'=>$ddifm,
            'dpro'=>$dpro,
            'dten'=>$dten,
            'perPaid'=>$perPaid,
            'term_ends'=>date('M ', strtotime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at))).' '.SuperM::getRepayDate($offerDets->pay_date, SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at)).', '.date('Y', strtotime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at))).' '.date('h:i A', strtotime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at)))
        ];
        return $data;
    }

    public function action($action, $id)
    {
        $user = Auth::user();
        $loan = Loan::find(SuperM::decrypt($id));
        if($user->hasRole($loan->stages()->first()->name)){
            if($action == 'decline'){
                $user->loan_tended_to()->attach($loan);
                $loan->status()->save(new Status(['status'=>'declined']));
                return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'You have successfully declined this file.',showConfirmButton: false,timer: 3000});});</script>");  
            }else {
                if($loan->credit_checks){
                    if($loan->folder){
                        $user->loan_tended_to()->attach($loan);
                        $next_user = $this->return_next_stage($loan->stages()->first()->id);
                        // change stages if not director or chairman
                        if($loan->stages()->first()->name != 'director' && $loan->stages()->first()->name != 'chairman'){
                            if($loan->user->marketer_id == null && $user->hasRole('marketer')){
                                $loan->user->update(['marketer_id'=>$user->id]);
                            }
                            $loan->stages()->sync([$next_user]);
                        }elseif($loan->stages()->first()->name == 'director'){
                            if($loan->offer->data()->amount < 2000001){
                                $loan->status()->save(new Status(['status'=>'approved']));
                            }
                            $loan->stages()->sync([$this->return_next_stage($loan->stages()->first()->id, $loan->offer->data()->amount)]);
                        }elseif($loan->stages()->first()->name == 'chairman'){
                            $loan->status()->save(new Status(['status'=>'approved']));
                            $loan->stages()->sync([$next_user]);
                        }
                        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'You have successfully approved this file.',showConfirmButton: false,timer: 3000});});</script>");
                        
                    }else{
                        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'No folder found for this loan.', 'error');});</script>");
                    }
                }else {
                    return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'Credit check fee has not been marked paid!', 'error');});</script>");
                }
            }
        }else {
            return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'It seems you don\'t have permission to perform this action or the file has left your desk!', 'error');});</script>");
        }
    }

    private static function return_next_stage($stage_id, $amount = 0){
        $stage = Role::find($stage_id)->name;
        $next = 'transaction officer';
        if($stage == 'marketer'){
            $next = 'risk';
        }elseif($stage == 'risk'){
            $next = 'coo';
        }elseif($stage == 'coo'){
            $next = 'director';
        }elseif($stage == 'director'){
            $next = $amount < 2000001 ? 'transaction officer':'chairman';
        }elseif($stage == 'chairman'){
            $next = 'transaction officer';
        }
        $role = Role::where('name', $next)->first();
        return $role->id;
    }

    public function returnBack($id)
    {
        $loan = $this->model->find(SuperM::decrypt($id));
        $amount = 0;

        if($loan->status) $loan->status->delete();
        if($loan->offer) $amount += $loan->offer->data()->amount;
        
        $prev = $this->return_back_stage($loan->stages()->first()->id, $amount);
        $loan->stages()->sync([$prev]);
        $nstage = $loan->stages()->first();
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'You returned a file back to ".$nstage->name." desk.',showConfirmButton: false,timer: 3000});});</script>");
    }

    private static function return_back_stage($stage_id, $amount = 0){
        $stage = Role::find($stage_id)->name;
        $next = 'marketer';
        if($stage == 'transaction officer'){
            $next = $amount < 2000001 ? 'director':'chairman';
        }elseif($stage == 'chairman'){
            $next = 'director';
        }elseif($stage == 'director'){
            $next = 'coo';
        }elseif($stage == 'coo'){
            $next = 'risk';
        }elseif($stage == 'risk'){
            $next = 'marketer';
        }
        $role = Role::where('name', $next)->first();
        return $role->id;
    }

    public function disburse($id)
    {
        $loan = Loan::find(SuperM::decrypt($id));
        $user = Auth::user();
        if($loan->offer->signature){
            $data = [
                'amount'=>SuperM::getDisburseAmount($loan->offer),
                'type'=>'credit',
                'description'=>'Loan Disbursement for loan INV'.str_pad($loan->id,6,0,STR_PAD_LEFT),
                'payment_option'=>'wallet',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $loan->user->transactions()->save(new Transaction(['details'=>$data]));
            $new = $loan->user->wallet->amount + SuperM::getDisburseAmount($loan->offer);
            $loan->user->wallet()->update(['amount'=>$new]);
            $loan->status->update(['status'=>'active']);
            $user->loan_tended_to()->attach($loan);
            return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Funds disbursed to user wallet successfully',showConfirmButton: false,timer: 3000});});</script>");
        }else {
            return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'The offer for this loan has not been signed by the user yet', 'error');});</script>");
        }
    }

    public function clearLoan($id) {
        if(request()->wantsJson()){
            $loan = Loan::find($id);
            $user = Auth::user();
            $defaults = $loan->defaults()->sum('amount');
            $offer = $loan->offer->data();
            $repayments = $loan->repayments()->sum('amount');
            $loanBal = ((SuperM::duePayment($loan)*$offer->tenor)+$defaults) -$repayments;
            $bal = $user->wallet->amount - $loanBal;
            if($bal < 0){
                return $this->error("Please ensure you have up to ₦".number_format($loanBal,2)." deposited in your wallet!");
            }else {
                $loan->repayments()->save(new Repayment(['amount'=>$loanBal, 'date_paid'=>\Carbon\Carbon::now(), 'status'=>'success']));
                $data = [
                    'amount'=>$loanBal,
                    'type'=>'debit',
                    'description'=>'Payment of loan',
                    'payment_option'=>'Wallet',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data]));
                $user->wallet->update(['amount'=>$bal]);
                return $this->success("Your loan has been cleared successfully!");
            }
        }else {
            $loan = Loan::find(SuperM::decrypt($id));
            $user = Auth::user();
            $defaults = $loan->defaults()->sum('amount');
            $offer = $loan->offer->data();
            $repayments = $loan->repayments()->sum('amount');
            $loanBal = ((SuperM::duePayment($loan)*$offer->tenor)+$defaults) -$repayments;
            $bal = $user->wallet->amount - $loanBal;
            if($bal < 0){
                return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'Please ensure you have up to ₦".number_format($loanBal,2)." deposited in your wallet!', 'error');});</script>");
            }else {
                $loan->repayments()->save(new Repayment(['amount'=>$loanBal, 'date_paid'=>\Carbon\Carbon::now(), 'status'=>'success']));
                $data = [
                    'amount'=>$loanBal,
                    'type'=>'debit',
                    'description'=>'Payment of loan',
                    'payment_option'=>'Wallet',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data]));
                $user->wallet->update(['amount'=>$bal]);
                return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Your loan has been cleared successfully!',showConfirmButton: false,timer: 3000});});</script>");
            }
        }
    }
}
