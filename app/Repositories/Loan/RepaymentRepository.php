<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Repayment;
use App\Interfaces\Loan\RepaymentInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Traits\AppResponse;
use App\Entities\Loan\Loan;
use App\Entities\Offer\Status;
use App\Entities\Loan\Folder;
use App\Entities\Transaction\Transaction;
use DB;
use App\Entities\User\Role;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;
use Carbon\Carbon;

/**
 * Class RepaymentRepository
 * @package App\Repositories\Loan
 * @property-read Repayment $model
 */
class RepaymentRepository extends AbstractRepository implements RepaymentInterface
{
    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Repayment::class;
    }

    public function updatePayments($id)
    {
        $data['loan'] = Loan::find(SuperM::decrypt($id));
        return view('repayments.edit', $data);
    }

    public function update($entityId=0, $attributes=[])
    {
        $loan = Loan::find($entityId);
        $user = $loan->user;
        $source = explode('|', request()->source);
        if($source[0] == 'card'){
            $card = $user->cards()->where('id', $source[1])->first();
            $param = [
                "authorization_code"=>$card->data()->authorization_code,
                "email"=>$user->email,
                "amount"=>SuperM::transCharge(request()->amount),
                "currency"=>"NGN",
                "meta_data" => [
                    'offer_details' => $loan->offer->data()
                ]
            ];
            $data = SuperM::paystackPost("https://api.paystack.co/transaction/charge_authorization", $param);
            if($data['status']){
                if($data['data']['status'] == 'success'){
                    $loan->repayments()->save(new Repayment(['amount'=>request()->amount, 'date_paid'=>Carbon::now(), 'status'=>'success']));
                    $data = [
                        'amount'=>request()->amount,
                        'type'=>'debit',
                        'description'=>'Payment of loan',
                        'payment_option'=>'Card',
                        'reference'=>SuperM::randomId('transactions', 'details->reference'),
                        'status'=>'success',
                    ];
                    $user->transactions()->save(new Transaction(['details'=>$data]));
                    return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Payment successfully charged.',showConfirmButton: false,timer: 1500});});</script>");
                }else {
                    return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data['message']."',showConfirmButton: false,timer: 1500});});</script>");
                }
            }else {
                return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data['message']."',showConfirmButton: false,timer: 1500});});</script>");
            }
        }elseif($source[0] == 'wallet'){
            $bal = $user->wallet->amount - request()->amount;
            if($bal < 0){
                return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: 'Insufficient balance',showConfirmButton: false,timer: 1500});});</script>");
            }else {
                $loan->repayments()->save(new Repayment(['amount'=>request()->amount, 'date_paid'=>Carbon::now(), 'status'=>'success']));
                $data = [
                    'amount'=>request()->amount,
                    'type'=>'debit',
                    'description'=>'Payment of loan',
                    'payment_option'=>'Wallet',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data]));
                $user->wallet->update(['amount'=>$bal]);
                return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Payment successfully charged.',showConfirmButton: false,timer: 1500});});</script>");
            }
        }else {
            $loan->repayments()->save(new Repayment(['amount'=>request()->amount, 'date_paid'=>Carbon::now(), 'status'=>'success']));
            $data = [
                'amount'=>request()->amount,
                'type'=>'debit',
                'description'=>'Payment of loan',
                'payment_option'=>'Transfer',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $user->transactions()->save(new Transaction(['details'=>$data]));
            return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Payment successfully charged.',showConfirmButton: false,timer: 1500});});</script>");
        }
    }

    public function payFromWallet()
    {
        $user = Auth::user();
        $loan = $user->loans()->whereHas('status', function($query){
            $query->where('status', 'active');
        })->latest()->first();
        $repayments = SuperM::getSingleDueLoans();
        $sum = $repayments['amount_due'] + $repayments['defaults'];
        $walletBalance = $user->wallet->amount - $sum;
        if($walletBalance >= 0){
            $user->wallet->update(['amount'=>$walletBalance]);
            $loan->repayments()->save(new Repayment(['amount'=>$repayments['amount_due'], 'date_paid'=>Carbon::now(), 'status'=>'success']));
            $data = [
                'amount'=>$repayments['amount_due'],
                'type'=>'debit',
                'description'=>'Payment of loan',
                'payment_option'=>'Wallet',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $user->transactions()->save(new Transaction(['details'=>$data]));
            if($repayments['defaults'] > 0){
                $data2 = [
                    'amount'=>$repayments['defaults'],
                    'type'=>'debit',
                    'description'=>'Default charge on loan',
                    'payment_option'=>'Wallet',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data2]));
                $loan->defaults()->delete();
            }
            if(request()->wantsJson()){
                return $this->success('Payment successful');
            }else {
                return redirect('/loans')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Payment successful',showConfirmButton: false,timer: 3000});});</script>");
            }
        }else {
            if(request()->wantsJson()){
                return $this->error("You don't have enough funds in your wallet to pay for this loan");
            }else 
                return redirect('/loans')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'error',title: 'You don\'t have enough funds in your wallet to pay for this loan',showConfirmButton: false,timer: 3000});});</script>");
        }
    }

    public function initiateCardLink(){
        $user = Auth::user();
        $loan = $user->loans()->whereHas('status', function($query){
            $query->where('status', 'active');
        })->latest()->first();
        $repayments = SuperM::getSingleDueLoans();
        $sum = $repayments['amount_due'] + $repayments['defaults'];
        $param = [
            "email"=>$user->email,
            "amount"=>SuperM::transCharge($sum),
            "currency"=>"NGN",
            "callback_url"=>url('/repayments/from-card/confirm'),
            "meta_data" => [
                'offer_details' => $loan->offer->data()
            ]
        ];
        $data = SuperM::paystackPost("https://api.paystack.co/transaction/initialize", $param);
        return response()->json($data)->header('Content-Type', 'application/json');
    }

    public function charge_auth(){
        $user = Auth::user();
        $source = request()->source;
        $loan = $user->loans()->whereHas('status', function($query){
            $query->where('status', 'active');
        })->latest()->first();
        $repayments = SuperM::getSingleDueLoans();
        $sum = $repayments['amount_due'] + $repayments['defaults'];
        $card = $user->cards()->where('id', $source)->first();
        $param = [
            "authorization_code"=>$card->data()->authorization_code,
            "email"=>$user->email,
            "amount"=>SuperM::transCharge($sum),
            "currency"=>"NGN",
        ];
        $data = SuperM::paystackPost("https://api.paystack.co/transaction/charge_authorization", $param);
        if($data['status']){
            if($data['data']['status'] == 'success'){
                $loan->repayments()->save(new Repayment(['amount'=>$repayments['amount_due'], 'date_paid'=>Carbon::now(), 'status'=>'success']));
                $loan->defaults()->delete();
                $data = [
                    'amount'=>$sum,
                    'type'=>'debit',
                    'description'=>'Payment of loan',
                    'payment_option'=>'Card',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data]));
                return $this->success('Payment successfully charged.');
            }else {
                return $this->error($data['message']);
            }
        }else {
            return $this->error($data['message']);
        }
    }

    public function payFromCardConfirm(){
        $user = Auth::user();
        $data = SuperM::paystackGet("https://api.paystack.co/transaction/verify/".request()->reference);
        $loan = $user->loans()->whereHas('status', function($query){
            $query->where('status', 'active');
        })->latest()->first();
        $repayments = SuperM::getSingleDueLoans();
        $amount = $repayments['amount_due'] + $repayments['defaults'];
        if($data->status){
            $loan->repayments()->save(new Repayment(['amount'=>$repayments['amount_due'], 'date_paid'=>Carbon::now(), 'status'=>'success']));
            $data = [
                'amount'=>$amount,
                'type'=>'debit',
                'description'=>'Payment of loan',
                'payment_option'=>'Card',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $user->transactions()->save(new Transaction(['details'=>$data]));
            if($repayments['defaults'] > 0){
                $data2 = [
                    'amount'=>$repayments['defaults'],
                    'type'=>'debit',
                    'description'=>'Default charge on loan',
                    'payment_option'=>'Card',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data2]));
                $loan->defaults()->delete();
            }
            if(request()->wantsJson()){
                return $this->success("Your payment has been received.");
            }else {
                return redirect('/loans')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Your payment has been received.',showConfirmButton: false,timer: 3000});});</script>");
            }
        }else {
            if(request()->wantsJson()){
                return $this->error($data->message);
            }else {
                return redirect('/loans')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data->message."',showConfirmButton: false,timer: 1500});});</script>");
            }
        }
    }

    public function index()
    {
        $data['user'] = Auth::user();
        $data['transactions'] = Repayment::paginate(10);
        return view('repayments.index', $data);
    }
}
