<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Upload;
use App\Interfaces\Loan\UploadInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Traits\FileUploadManager;
use App\Entities\Loan\Folder;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class UploadRepository
 * @package App\Repositories\Loan
 * @property-read Upload $model
 */
class UploadRepository extends AbstractRepository implements UploadInterface
{
    use FileUploadManager;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Upload::class;
    }

    public function create($attributes=[])
    {
        $folder = Folder::find(session('upload_folder'));
        if (request()->hasFile('file')) {
            $original_files = request()->file('file');
            foreach($original_files as $original_file){
                $original_name = pathinfo($original_file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $this->uploadSingle($original_file, 'Uploads');
                $folder->uploads()->save(new Upload(['resource_name'=>$original_name, 'resource_url'=>$image]));
            }
        } 
    }

    public function destroy($entityId=0,$attributes=[])
    {
        $upload = $this->model->find($entityId);
        $upload->delete();
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'You have successfully moved a file to trash.',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function trash(){
        $data['entities'] = $this->model->onlyTrashed()->get();
        return view('folders.trash', $data);
    }

    public function restore($entityId=0)
    {
        $upload = $this->model->withTrashed()->find($entityId);
        $upload->restore();
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'You have successfully retored a file back to its original folder.',showConfirmButton: false,timer: 3000});});</script>");
    }
}
