<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Disbursement;
use App\Interfaces\Loan\DisbursementInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class DisbursementRepository
 * @package App\Repositories\Loan
 * @property-read Disbursement $model
 */
class DisbursementRepository extends AbstractRepository implements DisbursementInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Disbursement::class;
    }
}
