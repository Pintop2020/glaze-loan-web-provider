<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\Folder;
use App\Entities\Loan\Loan;
use App\Interfaces\Loan\FolderInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Http\Controllers\GlobalMethods as SuperM;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Entities\User\User;

/**
 * Class FolderRepository
 * @package App\Repositories\Loan
 * @property-read Folder $model
 */
class FolderRepository extends AbstractRepository implements FolderInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Folder::class;
    }

    public function index()
    {
        $users = User::whereHas('loans')->latest()->get();
        $data['entities'] = $users->sortByDesc('loans.status');
        return view('folders.index', $data);
    }

    public function create_admin($id)
    {
        $user = Auth::user();
        if($user->hasRole("marketer")){
            $loan = Loan::find(SuperM::decrypt($id));
            if($loan->folder){
                return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'This loan already have a folder attached to it', 'error');});</script>");
            }else {
                if($user->hasRole($loan->stages()->first()->name)){
                    $loan->folder()->save(new Folder(['name'=>date('d_F_Y_H_i_s_').mt_rand(000000,999999)]));
                    return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Loan folder created successfully!',showConfirmButton: false,timer: 3000});});</script>");
                }else {
                    return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'It seems you don\'t have permission to perform this action or the file has left your desk!', 'error');});</script>");
            
                }
            }
        }else {
            return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire('Oops!', 'Only marketers can create a folder on the platform', 'error');});</script>");
        }
    }

    public function show_main($id)
    {
        $user = User::find(SuperM::decrypt($id));
        $data['entities'] = $user->loans()->whereHas('folder')->orderBy('id', 'asc')->get();
        return view('folders.show_main', $data);
    }

    public function show($id)
    {
        $folder = Folder::find(SuperM::decrypt($id));
        $data['folder'] = $folder;
        $data['loan'] = $folder->loan;
        $data['user'] = Auth::user();
        session(['upload_folder'=>$folder->id]);
        return view('folders.show', $data);
    }
}
