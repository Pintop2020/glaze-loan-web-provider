<?php

namespace App\Repositories\Loan;

use App\Entities\Loan\CreditCheck;
use App\Entities\Transaction\Transaction;
use App\Interfaces\Loan\CreditCheckInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Http\Controllers\GlobalMethods as SuperM;
use Illuminate\Support\Facades\Auth;
use App\Entities\Loan\Loan;
use App\Traits\AppResponse;

/**
 * Class CreditCheckRepository
 * @package App\Repositories\Loan
 * @property-read CreditCheck $model
 */
class CreditCheckRepository extends AbstractRepository implements CreditCheckInterface
{

    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return CreditCheck::class;
    }

    public function initiatePayment($attributes=[])
    {
        $user = Auth::user();
        $loan = $user->loans()->doesntHave('status')->first();
        if($loan && !$loan->credit_checks){
            $param = [
                "email"=>$user->email,
                "amount"=>SuperM::transCharge(3000),
                "currency"=>"NGN",
                "callback_url"=>url('/credit-checks/create'),
            ];
            $data = SuperM::paystackPost("https://api.paystack.co/transaction/initialize", $param);
            return redirect()->to($data['data']['authorization_url']);
        }else {
            return redirect('/dashboard');
        }
    }

    public function create($attributes=[])
    {
        $user = Auth::user();
        $loan = $user->loans()->doesntHave('status')->first();
        if($loan){
            $data = SuperM::paystackGet("https://api.paystack.co/transaction/verify/".request()->reference);
            if($data->status){
                $data = [
                    'amount'=>"3000",
                    'type'=>'debit',
                    'description'=>'Credit check payment',
                    'payment_option'=>'Card',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data]));
                $loan->credit_checks()->save(new CreditCheck(['is_paid'=>false]));
                $loan->update(['details->credit_check'=>true]);
                if(request()->wantsJson()){
                    return $this->success("Your credit check payment has been received.");
                }else {
                    return redirect('/loans')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Your credit check payment has been received.',showConfirmButton: false,timer: 3000});});</script>");
                }
            }else {
                if(request()->wantsJson()){
                    return $this->error($data->message);
                }else {
                    return redirect('/loans')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data->message."',showConfirmButton: false,timer: 1500});});</script>");
                }
            }
        }
    }

    public function initiateFromWallet()
    {
        $user = Auth::user();
        $loan = $user->loans()->doesntHave('status')->first();
        if($user->wallet->amount >= 3000){
            $newB = $user->wallet->amount - 3000;
            $user->wallet->update(['amount'=>$newB]);
            $data = [
                'amount'=>"3000",
                'type'=>'debit',
                'description'=>'Credit check payment',
                'payment_option'=>'Wallet',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $user->transactions()->save(new Transaction(['details'=>$data]));
            $loan->credit_checks()->save(new CreditCheck(['is_paid'=>false]));
            $loan->update(['details->credit_check'=>true]);
            return $this->success("Payment charged");
        }else {
            return $this->error("Insufficient funds");
        }
    }

    public function initiateFromCard()
    {
        $user = Auth::user();
        $source = request()->source;
        $card = $user->cards()->where('id', $source)->first();
        $loan = $user->loans()->doesntHave('status')->first();
        $param = [
            "authorization_code"=>$card->data()->authorization_code,
            "email"=>$user->email,
            "amount"=>SuperM::transCharge(3000),
            "currency"=>"NGN",
        ];
        $data = SuperM::paystackPost("https://api.paystack.co/transaction/charge_authorization", $param);
        if($data['status']){
            if($data['data']['status'] == 'success'){
                $data = [
                    'amount'=>request()->amount,
                    'type'=>'debit',
                    'description'=>'Payment of loan',
                    'payment_option'=>'Card',
                    'reference'=>SuperM::randomId('transactions', 'details->reference'),
                    'status'=>'success',
                ];
                $user->transactions()->save(new Transaction(['details'=>$data]));
                $loan->credit_checks()->save(new CreditCheck(['is_paid'=>false]));
                $loan->update(['details->credit_check'=>true]);
                return $this->success("Payment successfully charged.");
            }else {
                return $this->error($data['message']);
            }
        }else {
            return $this->error($data['message']);
        }
    }

    public function create_admin($id)
    {
        $loan = Loan::find(SuperM::decrypt($id));
        if($loan){
            $loan->credit_checks()->save(new CreditCheck(['is_paid'=>false]));
            $loan->update(['details->credit_check'=>true]);
            return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Loan credit check fee updated!',showConfirmButton: false,timer: 3000});});</script>");
        }else {
            return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'danger',title: 'Loan not found!',showConfirmButton: false,timer: 3000});});</script>");
        }
    }
}
