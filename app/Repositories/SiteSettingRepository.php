<?php

namespace App\Repositories;

use App\Entities\SiteSetting;
use App\Interfaces\SiteSettingInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class SiteSettingRepository
 * @package App\Repositories
 * @property-read SiteSetting $model
 */
class SiteSettingRepository extends AbstractRepository implements SiteSettingInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return SiteSetting::class;
    }
}
