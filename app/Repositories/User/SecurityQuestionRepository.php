<?php

namespace App\Repositories\User;

use App\Entities\User\SecurityQuestion;
use App\Interfaces\User\SecurityQuestionInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class SecurityQuestionRepository
 * @package App\Repositories\User
 * @property-read SecurityQuestion $model
 */
class SecurityQuestionRepository extends AbstractRepository implements SecurityQuestionInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return SecurityQuestion::class;
    }
}
