<?php

namespace App\Repositories\User;

use App\Entities\User\AccountSecurity;
use App\Interfaces\User\AccountSecurityInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class AccountSecurityRepository
 * @package App\Repositories\User
 * @property-read AccountSecurity $model
 */
class AccountSecurityRepository extends AbstractRepository implements AccountSecurityInterface
{
    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return AccountSecurity::class;
    }

    public function store()
    {
        try {
            $user = Auth::user();
            $user->account_security()->save(new AccountSecurity(['account_pin'=>request()->pin]));
            return $this->success("Pin created successfully.");
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }
        
    }

    public function validatePin(){
        $user = Auth::user();
        $pin = $user->account_security->account_pin;
        if(request()->pin == $pin){
            return $this->success('Pin validated!');
        }else {
            return $this->error('Invalid pin provided!');
        }
    }
}
