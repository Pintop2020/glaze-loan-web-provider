<?php

namespace App\Repositories\User\Admin;

use Spatie\Permission\Models\Role;
use App\Interfaces\User\Admin\RoleInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class RoleRepository
 * @package App\Repositories\User\Admin
 * @property-read Role $model
 */
class RoleRepository extends AbstractRepository implements RoleInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Role::class;
    }
}
