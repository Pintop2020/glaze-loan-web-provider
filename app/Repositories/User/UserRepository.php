<?php

namespace App\Repositories\User;

use App\Entities\User\User;
use App\Interfaces\User\UserInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use App\Entities\User\SecurityQuestion;
use App\Entities\User\Wallet;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Entities\User\Referral\RefereeDetail;
use App\Entities\User\Referral\Referral;
use App\Entities\Transaction\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Traits\AppResponse;
use DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

/**
 * Class UserRepository
 * @package App\Repositories\User
 * @property-read User $model
 */
class UserRepository extends AbstractRepository implements UserInterface
{
    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return User::class;
    }

    public function create($attributes=[])
    {
        return view('users.application.auth.register');
    }

    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'email' => ['required', 'email', 'string', 'unique:users', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'mobile' => ['required', 'unique:users'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'security_question' => ['required', 'string'],
            'security_answer' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            if(request()->wantsJson()){
                return $this->error($validator->errors());
            }
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $regData = request()->except(['_token', '_method', 'security_question', 'security_answer', 'password_confirmation', 'password', 'referree']);
        $regData['password'] = Hash::make(request()->password);
        $user = $this->model->create($regData);
        $security = ['question'=>request()->security_question, 'answer'=>request()->security_answer];
        $user->security_questions()->save(new SecurityQuestion($security));
        if(request()->wantsJson()){
            if(request()->referree){
                $details = RefereeDetail::find(request()->referree);
                $ref = $this->model::find($details->user_id);
                $referral = Referral::create(['user_id'=>$user->id]);
                $ref->referrals()->attach($referral->id);
            }
        }else {
            if(session('referee')){
                $details = RefereeDetail::find(session('referee'));
                $ref = $this->model::find($details->user_id);
                $referral = Referral::create(['user_id'=>$user->id]);
                $ref->referrals()->attach($referral->id);
            }
        }
        $user->sendEmailVerificationNotification();
        if(request()->wantsJson())
            return $this->success('Account created sucessfully', $user);
        if(Auth::attempt(['email'=>request()->email, 'password'=>\Hash::make(request()->password)])){
            return redirect()->route('home');
        }else {
            return redirect()->route('login');
        }
    }

    public function home()
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['ref_earnings'] = $user->transactions()->where('details->type', 'earnings')->sum('details->amount');
        return view('users.home', $data);
    }

    public function index()
    {
        $user = Auth::user();
        $data['user'] = $user;
        if($user->roles()->count() < 2 && $user->hasRole('marketer')){
            $data['users'] = $this->model::where('is_admin', 0)->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                $query->where('email', $user->email);
            })->latest()->get();
        }else {
            $data['users'] = $this->model::where('is_admin', 0)->latest()->get();
        }
        return view('users.index', $data);
    }

    public function custom_index($type)
    {
        $active = false;
        if($type == 'active') $active = true;
        $user = Auth::user();
        $data['user'] = $user;
        if($user->roles()->count() < 2 && $user->hasRole('marketer')){
            $data['users'] = $this->model::where([['active', $active],['is_admin', 0]])->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) use($user){
                $query->where('email', $user->email);
            })->orderBy('id', 'desc')->get();
        }else {
            $data['users'] = $this->model::where([['active', $active],['is_admin', 0]])->orderBy('id', 'desc')->get();
        }
        $data['type'] = $type;
        return view('users.index', $data);
    }

    public function show($id)
    {
        $user = $this->model::find(SuperM::decrypt($id));
        $data['id'] = $id;
        $data['user'] = $user;
        $data['details'] = $user->my_details;
        if($user->my_details){
            $banks = SuperM::paystackGet('https://api.paystack.co/bank')->data;
            $id = SuperM::search_banks($data['details']->data()->financial_info->bank, $banks);
            $data['bank'] = $banks[$id]->name;
        }
        return view('users.show', $data);
    }

    public function perm($id)
    {
        $user = $this->model::find(SuperM::decrypt($id));
        $user->active = $user->active ? false : true;
        $user->save();
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: '".$user->getFullname()." status was updated sucessfully!',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function verify_referee($ref){
        if(request()->wantsJson()){
            $user = RefereeDetail::where('code', $ref)->first();
            if(!$user){
                return $this->error("User with referral code not found");
            }else {
                return $this->success("Referral retrieved", $user->user);
            }
        }else {
            $user = RefereeDetail::where('code', $ref)->first();
            if(!$user) return "error";
            session(['referee'=>$user->id]);
            return $user->user->getFullName();
        }
    }

    public function staff(){
        $user = Auth::user();
        $data['user'] = $user;
        $data['users'] = $this->model::where('is_admin', 1)->orderBy('id', 'desc')->get();
        return view('users.staff.all', $data);
    }

    public function new_staff(){
        $user = Auth::user();
        $data['user'] = $user;
        $data['users'] = $this->model::orderBy('id', 'desc')->get();
        $data['roles'] = Role::all();
        return view('users.staff.new', $data);
    }

    public function new_staff_post(){
        $validator = Validator::make(request()->all(), [
            'email' => ['required', 'email', 'string', 'unique:users', 'max:255'],
            'mobile' => ['required', 'unique:users'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'middle_name' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data = request()->except(['_token', 'roles']);
        $data['password'] = \Hash::make('secret_glazecredit@2021');
        $data['is_admin'] = true;
        $data['email_verified_at'] = \Carbon\Carbon::now();;
        $roles = Role::findMany(request()->roles);
        $roleAdd = [];
        foreach($roles as $role){
            $roleAdd[] = $role->name;
        }
        $user = $this->model->create($data);
        $user->syncRoles($roleAdd);
        return redirect('/staff')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: '".$user->getFullname()." has been added to admin and role has been set.',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function edit($id)
    {
        $data['entity'] = $this->model->find(SuperM::decrypt($id));
        $data['roles'] = Role::all();
        return view('users.edit', $data);
    }

    public function update($entityId=0, $attributes=[])
    {
        $user = $this->model->find($entityId);
        $validator = Validator::make(request()->all(), [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'middle_name' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data = request()->except(['_token', 'roles', 'email', 'mobile']);
        $roles = Role::findMany(request()->roles);
        $roleAdd = [];
        foreach($roles as $role){
            $roleAdd[] = $role->name;
        }
        $user->update($data);
        $user->syncRoles($roleAdd);
        return redirect('/staff')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: '".$user->getFullname()." has been updated successfully.',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function profile()
    {
        $data['user'] = Auth::user();
        return view('users.profile', $data);
    }

    public function security()
    {
        $data['user'] = Auth::user();
        return view('users.profile_security', $data);
    }

    public function notificationSetting()
    {
        $data['user'] = Auth::user();
        return view('users.notification_settings', $data);
    }

    public function notificationSettingPost()
    {
        $user = Auth::user();
        $setting = $user->setting->data();
        $data = [
            "news_letter"=> request()->news_letter ? true : false,
            "save_activity_log"=> $setting->save_activity_log,
            "email_when_activity"=> request()->email_when_activity ? true : false,
            "email_when_new_browser_logged"=> request()->email_when_new_browser_logged ? true : false
        ];
        $user->setting->update(['details'=>json_encode($data)]);
        return redirect('/notifications-settings')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Profile updated sucessfully.',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function securityPost()
    {
        $user = Auth::user();
        $setting = $user->setting->data();
        $data = [
            "news_letter"=> $setting->news_letter,
            "save_activity_log"=> request()->save_activity_log ? true : false,
            "email_when_activity"=> $setting->email_when_activity,
            "email_when_new_browser_logged"=> $setting->email_when_new_browser_logged
        ];
        $user->setting->update(['details'=>json_encode($data)]);
        return redirect('/security')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Profile updated sucessfully.',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function login()
    {
        if(Auth::attempt(['email' => request()->email, 'password' => request()->password])){ 
          $user = Auth::user(); 
          if($user->is_admin){
              Auth::logout();
              return $this->error('You cannot access the mobile application with a staff account!', 401);
          }else {
              if($user->active){
                  if($user->hasVerifiedEmail()){
                    $token = $user->createToken(request()->device_name);

                      $data['token'] =  $token->plainTextToken;
                      $data['user'] = $user;
                      return $this->success('Account authentication successful', $data);
                  }else{
                      $user->sendEmailVerificationNotification();
                      Auth::logout();
                      return $this->error('Please check your email to confirm your account!', 401);
                  }
              }else {
                  Auth::logout();
                  return $this->error('Your account has been restricted. Please contact support for assistance.', 401);
              }
          }
          
      } else {
          return $this->error('Email address or password not found on our server!', 401);
      }
    }

    public function logout()
    {
        $user = Auth::user();
        $user->tokens()->delete();
        return $this->success("Account logged out");
    }
}
