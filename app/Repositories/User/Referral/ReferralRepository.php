<?php

namespace App\Repositories\User\Referral;

use App\Entities\User\Referral\Referral;
use App\Interfaces\User\Referral\ReferralInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Entities\User\User;
use App\Entities\Transaction\Transaction;

/**
 * Class ReferralRepository
 * @package App\Repositories\User\Referral
 * @property-read Referral $model
 */
class ReferralRepository extends AbstractRepository implements ReferralInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Referral::class;
    }

    public function index()
    {
        $user = Auth::user();
        if($user->is_admin){
            $data['users'] = $this->model->latest()->get();
            return view('referrals.index', $data);
        }else{
            $data['user'] = $user;
            return view('referrals.all', $data);
        }
    }

    public function userChart()
    {
        $user = Auth::user();
        $referrals = $user->referrals()->get()->groupBy(function($val) {
              return Carbon::parse($val->created_at)->format('Y m');
        })->toArray();
        $data = [];
        foreach($referrals as $key => $ref){
            $data['month'] = date('M', explode(' ', $key)[1]).' '.explode(' ', $key)[0];
            $data['value'] = count($ref);
        }
        return $data;
    }
}
