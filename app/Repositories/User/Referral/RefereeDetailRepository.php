<?php

namespace App\Repositories\User\Referral;

use App\Entities\User\Referral\RefereeDetail;
use App\Interfaces\User\Referral\RefereeDetailInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class RefereeDetailRepository
 * @package App\Repositories\User\Referral
 * @property-read RefereeDetail $model
 */
class RefereeDetailRepository extends AbstractRepository implements RefereeDetailInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return RefereeDetail::class;
    }
}
