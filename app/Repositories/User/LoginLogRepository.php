<?php

namespace App\Repositories\User;

use App\Entities\User\LoginLog;
use App\Interfaces\User\LoginLogInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Entities\User\User;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class LoginLogRepository
 * @package App\Repositories\User
 * @property-read LoginLog $model
 */
class LoginLogRepository extends AbstractRepository implements LoginLogInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return LoginLog::class;
    }

    public function user_login_logs($id)
    {
        $user = User::find(SuperM::decrypt($id));
        $data['id'] = $id;
        $data['user'] = $user;
        $data['details'] = $user->my_details;
        return view('loginLogs.show', $data);
    }
}
