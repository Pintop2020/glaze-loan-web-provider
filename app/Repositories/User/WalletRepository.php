<?php

namespace App\Repositories\User;

use App\Entities\User\Wallet;
use App\Interfaces\User\WalletInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Entities\Transaction\Transaction;
use App\Entities\Transaction\Card;
use App\Traits\AppResponse;

/**
 * Class WalletRepository
 * @package App\Repositories\User
 * @property-read Wallet $model
 */
class WalletRepository extends AbstractRepository implements WalletInterface
{
    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Wallet::class;
    }

    public function index()
    {
        $data['user'] = Auth::user();
        return view('wallets.index', $data);
    }

    public function deposit()
    {
        $data['user'] = Auth::user();
        return view('wallets.deposit', $data);
    }

    public function depositPost()
    {
        $user = Auth::user();
        $source = request()->source;
        if($source == 'card'){
            $user = Auth::user();
            $param = [
                "email"=>$user->email,
                "amount"=>SuperM::transCharge(request()->amount),
                "currency"=>"NGN",
                "callback_url"=>url('/wallet/deposit/confirm'),
            ];
            $data = SuperM::paystackPost("https://api.paystack.co/transaction/initialize", $param);
            return redirect()->away($data['data']['authorization_url']);
        }else {
            $user = Auth::user();
            $card = Card::find(request()->source);
            $param = [
                "authorization_code"=>$card->data()->authorization_code,
                "email"=>$user->email,
                "amount"=>SuperM::transCharge(request()->amount),
                "currency"=>"NGN",
            ];
            $data = SuperM::paystackPost("https://api.paystack.co/transaction/charge_authorization", $param);
            if($data['status'] && $data['data']['status'] != 'failed'){
                if(request()->wantsJson()){
                    return $this->success($data['message'], $data['data']);
                }else {
                    return redirect('/wallet/deposit/confirm?reference='.$data['data']['reference']);
                }
            }else {
                if(request()->wantsJson()){
                    return $this->error($data['data']['gateway_response']);
                }else {
                    return redirect('/wallet/deposit')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data['data']['gateway_response']."',showConfirmButton: false,timer: 3000});});</script>");
                }
            }
        }
    }

    public function confirmDeposit()
    {
        $user = Auth::user();
        $data = SuperM::paystackGet("https://api.paystack.co/transaction/verify/".request()->reference);
        if($data->status){
            $amount = $data->data->amount/100;
            $data = [
                'amount'=>$amount,
                'type'=>'credit',
                'description'=>'Wallet deposit',
                'payment_option'=>'Card',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $user->transactions()->save(new Transaction(['details'=>$data]));
            $wallet = $user->wallet->amount + $amount;
            $user->wallet->update(['amount'=>$wallet]);
            if(request()->wantsJson()){
                return $this->success('Your deposit was successful', $data);
            }else {
                return redirect('/wallet')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Your deposit was successful',showConfirmButton: false,timer: 1500});});</script>");
            }
        }else {
            if(request()->wantsJson()){
                return $this->error($data->message);
            }else {
                return redirect('/wallet/deposit')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data->message."',showConfirmButton: false,timer: 1500});});</script>");
            }
        }
    }

    public function withdraw()
    {
        $user = Auth::user();
        if($user->my_details){
            $data['user'] = $user;
            $banks = SuperM::paystackGet('https://api.paystack.co/bank')->data;
            $id = SuperM::search_banks($user->my_details->data()->financial_info->bank, $banks);
            $data['bank'] = $banks[$id]->name;
            return view('wallets.withdraw', $data);
        }else {
            return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: 'Please update your kyc',showConfirmButton: false,timer: 1500});});</script>");
        }
    }

    public function withdrawPost()
    {
        $user = Auth::user();
        $amount = request()->amount;
        if($amount > $user->wallet->amount)
        {
            if(request()->wantsJson()){
                return $this->error("Insufficient funds");
            }else {
                return redirect()->back()->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: 'Insufficient funds',showConfirmButton: false,timer: 1500});});</script>");
            }
        }else 
        {
            $param = [
                'type'=>'nuban',
                'name'=>$user->first_name.' '.$user->last_name,
                'description'=>'Wallet withdrawal from Glaze Credit Limited',
                'account_number'=>$user->my_details->data()->financial_info->account_number,
                'bank_code'=>$user->my_details->data()->financial_info->bank,
                'currency'=>'NGN',
            ];
            $data = SuperM::paystackPost("https://api.paystack.co/transferrecipient", $param);
            if($data['status']){
                $param2 = [
                    'source'=>'balance',
                    'reason'=>'Cashout from Glaze wallet',
                    'amount'=>SuperM::transCharge($amount, false),
                    'recipient'=>$data['data']['recipient_code'],
                ];
                $data2 = SuperM::paystackPost("https://api.paystack.co/transfer", $param2);
                if($data2['status']){
                    $bal = $user->wallet->amount - $amount;
                    $user->wallet->update(['amount'=>$bal]);
                    $transData = [
                        'amount'=>$amount,
                        'type'=>'withdrawal',
                        'description'=>'You withdraw to your registered bank account',
                        'payment_option'=>'Wallet',
                        'reference'=>$data2['data']['transfer_code'],
                        'status'=>'success',
                    ];
                    $user->transactions()->save(new Transaction(['details'=>$transData]));
                    if(request()->wantsJson()){
                        return $this->success($data2['message']);
                    }else {
                        return redirect('/wallet')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: '".$data2['message']."',showConfirmButton: false,timer: 1500});});</script>");
                    }
                }else {
                    if(request()->wantsJson()){
                        return $this->error($data2['message']);
                    }else {
                        return redirect('/wallet/withdraw')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data2['message']."',showConfirmButton: false,timer: 1500});});</script>");
                    }
                }
            }else {
                if(request()->wantsJson()){
                    return $this->error($data['message']);
                }else {
                    return redirect('/wallet/withdraw')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data['message']."',showConfirmButton: false,timer: 1500});});</script>");
                }
            }
        }
    }
}
