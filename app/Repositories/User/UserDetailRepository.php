<?php

namespace App\Repositories\User;

use App\Entities\User\UserDetail;
use App\Interfaces\User\UserDetailInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Traits\FileUploadManager;
use App\Traits\AppResponse;

/**
 * Class UserDetailRepository
 * @package App\Repositories\User
 * @property-read UserDetail $model
 */
class UserDetailRepository extends AbstractRepository implements UserDetailInterface
{
    use FileUploadManager, AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return UserDetail::class;
    }

    public function kyc()
    {
        $user = Auth::user();
        $data['user'] = $user;
        return view('applications.kyc', $data);
    }

    public function create($attributes=[])
    {
        session()->forget('passport_url', 'id_url', 'company_id', 'letter_of_employment');
        $user = Auth::user();
        $data['user'] = $user;
        $data['banks'] = SuperM::paystackGet('https://api.paystack.co/bank')->data;
        return view('userDetails.create', $data);
    }

    public function store()
    {
        $user = Auth::user();
        $bvn = SuperM::paystackGet("https://api.paystack.co/bank/resolve_bvn/".request()->bvn);
        if(request()->wantsJson()){
            if(!$bvn->status){
              return $this->error($bvn->message);
            }else {
                try {
                    $passport = $this->uploadSingle(request('passport'), 'Passports');
                    $id = $this->uploadSingle(request('id'), 'ID');
                    $company_id = $this->uploadSingle(request('company_id'), 'Company');
                    $data = [
                        'first_name'=>$bvn->data->first_name, 
                        'last_name'=>$bvn->data->last_name, 
                    ];
                    $user->update($data);
                    $personal = request(['gender', 'marital_status', 'number_of_children', 'educational_status']);
                    $personal['date_of_birth'] = $bvn->data->formatted_dob;
                    $address = request(['address_1', 'address_2', 'lga_of_residence', 'city_of_residence']);
                    $address['state_of_residence'] = 'Lagos';
                    $employment = request(['employer', 'employer_phone', 'official_email', 'industry', 'staff_type', 'office_address', 'office_lga', 'office_city', 'monthly_income']);
                    $employment['office_state'] = 'Lagos';
                    $financial = request(['bank', 'account_number', 'bvn']);
                    $identity = ['identity_type'=>request()->id_proof, 'passport'=>$passport, 'id'=>$id, 'company_id'=>$company_id];
                    $data = ['personal_info'=>$personal, 'address_info'=>$address, 'employment_info'=>$employment, 'financial_info'=>$financial, 'identity'=>$identity];
                    $user->my_details()->save(new UserDetail(['details'=>$data]));
                    return $this->success('Submission successful');
                }catch(\Exception $e){
                    return $this->error($e->getMessage());
                }
            }
        }else {
            if(!$bvn->status){
                return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'error',title: '".$bvn->message."',showConfirmButton: false,timer: 3000});});</script>"); 
            }else {
              $data = [
                'first_name'=>$bvn->data->first_name, 
                'last_name'=>$bvn->data->last_name, 
              ];
              $user->update($data);
              $personal = request(['gender', 'marital_status', 'number_of_children', 'educational_status']);
              $personal['date_of_birth'] = $bvn->data->formatted_dob;
              $address = request(['address_1', 'address_2', 'lga_of_residence', 'city_of_residence']);
              $address['state_of_residence'] = 'Lagos';
              $employment = request(['employer', 'employer_phone', 'official_email', 'industry', 'staff_type', 'office_address', 'office_lga', 'office_city', 'monthly_income']);
              $employment['office_state'] = 'Lagos';
              $financial = request(['bank', 'account_number', 'bvn']);
              $identity = ['identity_type'=>request()->id_proof, 'passport'=>session('passport_url'), 'id'=>session('id_url'), 'company_id'=>session('company_id')];
              $data = ['personal_info'=>$personal, 'address_info'=>$address, 'employment_info'=>$employment, 'financial_info'=>$financial, 'identity'=>$identity];
              $user->my_details()->save(new UserDetail(['details'=>$data]));
              session()->forget('passport_url', 'id_url', 'company_id');
              return redirect('/application/preview')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'KYC application successful',showConfirmButton: false,timer: 3000});});</script>");
            }
        }
    }

    public function savePassport(){
        $image = $this->uploadSingle(request('file'), 'Passports');
        session(['passport_url'=>$image]);
    }

    public function saveID(){
        $image = $this->uploadSingle(request('file'), 'ID');
        session(['id_url'=>$image]);
    }

    public function saveCompanyID(){
        $image = $this->uploadSingle(request('file'), 'Company');
        session(['company_id'=>$image]);
    }

    public function checkUploads(){
        if(session('passport_url') && session('id_url') && session('company_id')){
            return $this->success('Uploads confirmed');
        }else {
            return $this->error('Please ensure to upload all the required documents.');
        }
    }

    public function getBvn($bvn, $phone){
        $bvn = SuperM::paystackGet("https://api.paystack.co/bank/resolve_bvn/".$bvn);
        if(!$bvn->status){
            return json_encode($bvn);
        }else {
            if($bvn->data->mobile != $phone){
                return $this->error("Bvn number and your registered number does not match!");
            }else {
                return $this->success("Validation through");
            }

        }
    }

    public function getBank($bank, $account){
        $bank = SuperM::paystackGet("https://api.paystack.co/bank/resolve?account_number=".$account."&bank_code=".$bank);
        return json_encode($bank);
    }

    public function preview() {
        $user = Auth::user();
        $data['user'] = $user;
        $data['data'] = $user->my_details;
        $data['data_info'] = $user->my_details->data();
        $banks = SuperM::paystackGet('https://api.paystack.co/bank')->data;
        $id = SuperM::search_banks($data['data_info']->financial_info->bank, $banks);
        $data['bank'] = $banks[$id]->name;
        return view('applications.preview', $data);
    }

    public function destroy($id)
    {
        $entity = $this->model->find($id);
        $entity->delete();
        return redirect()->back();
    }
}
