<?php

namespace App\Repositories\Transaction;

use App\Entities\Transaction\Transaction;
use App\Interfaces\Transaction\TransactionInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Entities\User\User;
use App\Traits\AppResponse;

/**
 * Class TransactionRepository
 * @package App\Repositories\Transaction
 * @property-read Transaction $model
 */
class TransactionRepository extends AbstractRepository implements TransactionInterface
{
    use AppResponse; 

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Transaction::class;
    }

    public function index()
    {
        $user = Auth::user();
        if($user->is_admin){
            $data['entities'] = $this->model->latest()->paginate(20);
            return view('transactions.index', $data);
        }else {
            $data['entities'] = $user->transactions()->latest()->get();
            $data['counts'] = count($data['entities']);
            if(request()->wantsJson()){
                if(request()->from && request()->to){
                    $from = date(request()->from);
                    $to = date(request()->to);
                    $options['entities'] = $this->model->whereBetween('created_at', [$from, $to])->get();
                    $options['counts'] = count($options['entities']);
                    return $this->success("Transactions retrieved", $options);
                }
                return $this->success("Transactions retrieved", $data);
            }else {
                return view('transactions.user.index', $data);
            }
        }
    }

    public function user_transactions($id)
    {
        $user = User::find(SuperM::decrypt($id));
        $data['id'] = $id;
        $data['user'] = $user;
        $data['details'] = $user->my_details;
        return view('transactions.show', $data);
    }
}
