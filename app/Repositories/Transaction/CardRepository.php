<?php

namespace App\Repositories\Transaction;

use App\Entities\Transaction\Card;
use App\Entities\Transaction\Transaction;
use App\Interfaces\Transaction\CardInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class CardRepository
 * @package App\Repositories\Transaction
 * @property-read Card $model
 */
class CardRepository extends AbstractRepository implements CardInterface
{
    use AppResponse;

    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Card::class;
    }

    public function index(){
        $cards = $this->model->latest()->get();
        return $this->success("Cards retrieved", $cards);
    }

    public function initiateCardLink(){
        $user = Auth::user();
        $param = [
            "email"=>$user->email,
            "amount"=>10000,
            "currency"=>"NGN",
            "callback_url"=>url('/application/card/verify'),
        ];
        $data = SuperM::paystackPost("https://api.paystack.co/transaction/initialize", $param);
        return response()->json($data)->header('Content-Type', 'application/json');
    }

    public function initiateCardAccess(){
        $user = request()->user();
        $amount = request()->amount;
        $param = [
            "email"=>$user->email,
            "amount"=>SuperM::transCharge($amount),
            "currency"=>"NGN",
        ];
        $data = SuperM::paystackPost("https://api.paystack.co/transaction/initialize", $param);
        return response()->json($data)->header('Content-Type', 'application/json');
    }

    public function verifyPayment(){
        $user = Auth::user();
        $data = SuperM::paystackGet("https://api.paystack.co/transaction/verify/".request()->reference);
        if($data->status){
            $user->cards()->save(new Card(['details'=>$data->data->authorization]));
            $data = [
                'amount'=>"100",
                'type'=>'debit',
                'description'=>'Payment card confirmation charge',
                'payment_option'=>'Card',
                'reference'=>SuperM::randomId('transactions', 'details->reference'),
                'status'=>'success',
            ];
            $user->transactions()->save(new Transaction(['details'=>$data]));
            if(request()->wantsJson()){
                return $this->success("Your card has been saved");
            }else {
                return redirect('/application')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'success',title: 'Your card has been saved',showConfirmButton: false,timer: 1500});});</script>");
            }
        }else {
            if(request()->wantsJson()){
                return $this->error($data->message);
            }else {
                return redirect('/application')->with('error_bottom', "<script>$(function(){ Swal.fire({ position: 'top-end', icon: 'error',title: '".$data->message."',showConfirmButton: false,timer: 1500});});</script>");   
            }
        }
    }
}
