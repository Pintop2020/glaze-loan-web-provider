<?php

namespace App\Repositories;

use App\Entities\Support;
use App\Interfaces\SupportInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\GlobalMethods as SuperM;

/**
 * Class SupportRepository
 * @package App\Repositories
 * @property-read Support $model
 */
class SupportRepository extends AbstractRepository implements SupportInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Support::class;
    }

    public function index()
    {
        $user = Auth::user();
        $data['user'] = $user;
        $data['supports'] = $user->supports()->latest()->get();
        return view('supports.index', $data);
    }

    public function create($attributes=[])
    {
        $data['user'] = Auth::user();
        return view('supports.create', $data);
    }

    public function store()
    {
        $user = Auth::user();
        $user->supports()->save(new Support(request()->except(['_token', 'files'])));
        return redirect('/supports')->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Ticket Submitted!',showConfirmButton: false,timer: 3000});});</script>");
    }

    public function show($id)
    {
        $data['support'] = $this->model->find(SuperM::decrypt($id));
        return view('supports.show', $data);
    }

    public function closeSupport($id)
    {
        $support = $this->model->find(SuperM::decrypt($id));
        $support->update(['status'=>'closed']);
        return redirect()->back()->with('error_bottom', "<script>$(function(){Swal.fire({position: 'top-end',icon: 'success',title: 'Ticket closed!',showConfirmButton: false,timer: 3000});});</script>");
    }
}
