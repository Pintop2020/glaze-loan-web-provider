<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Loan\Defaulter;
use App\Entities\Loan\Loan;
use App\Http\Controllers\GlobalMethods as SuperM;

class UpdateDefaulters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loans:update_default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check defaulting users and update them into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $loans = Loan::whereHas('status', function($query){
            $query->where('status', 'active');
        })->get();
        foreach($loans as $loan){
            $offer = $loan->offer->data();
            $counts = $offer->tenor;
            $repayments = $loan->repayments()->sum('amount');
            $amount = 0;
            for($i=0;$i<$counts;$i++){
                $date = new \DateTime(SuperM::getPaymentDate($i, $loan));
                $expected = SuperM::duePayment($loan)*($i+1);
                $mdiff = $repayments - $expected;
                $date->modify('+5 day');
                $cur = time();
                $future = strtotime($date->format('Y-m-d H:m:s'));
                $dateDiff = $future - $cur;
                if($dateDiff < 1){
                    if($expected >= 0)
                        $amount += SuperM::duePayment($loan);
                }
            }
            $defaults = ($amount-$repayments) * (0.5/100);
            $loan->defaults()->save(new Defaulter(['amount'=>$defaults]));
        }
    }
}
