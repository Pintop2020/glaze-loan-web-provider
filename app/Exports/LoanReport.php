<?php

namespace App\Exports;

use App\Entities\Loan\Loan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class LoanReport implements FromView
{
    public function view(): View
    {
        return view('reports.exports.loanReports', [
            'loans' => Loan::all()
        ]);
    }
}