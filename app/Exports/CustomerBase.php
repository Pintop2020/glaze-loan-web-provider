<?php

namespace App\Exports;

use App\Entities\User\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CustomerBase implements FromView
{
    public function view(): View
    {
        return view('reports.exports.customerBase', [
            'users' => User::all()
        ]);
    }
}