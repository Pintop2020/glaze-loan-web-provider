<?php

namespace App\Exports;

use App\Entities\Loan\Repayment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RepaymentReport implements FromView
{
    public function view(): View
    {
        return view('reports.exports.repaymentReports', [
            'repayments' => Repayment::all()
        ]);
    }
}