<?php

namespace App\Providers;

use App\Interfaces\Loan\CommentInterface;
use App\Interfaces\Loan\DefaulterInterface;
use App\Interfaces\Loan\DisbursementInterface;
use App\Interfaces\Loan\FolderInterface;
use App\Interfaces\Loan\LoanDetailInterface;
use App\Interfaces\Loan\LoanInterface;
use App\Interfaces\Loan\LoanStageInterface;
use App\Interfaces\Loan\RepaymentInterface;
use App\Interfaces\Loan\StaffAttendInterface;
use App\Interfaces\Loan\UploadInterface;
use App\Interfaces\Offer\OfferInterface;
use App\Interfaces\Offer\StatusInterface;
use App\Interfaces\User\Admin\AdminInterface;
use App\Interfaces\User\UserDetailInterface;
use App\Interfaces\User\UserInterface;
use App\Interfaces\Offer\SignatureInterface;
use App\Interfaces\Transaction\TransactionInterface;
use App\Interfaces\Transaction\CouponInterface;
use App\Interfaces\Transaction\CardInterface;
use App\Interfaces\User\Referral\ReferralDetailInterface;
use App\Interfaces\User\Referral\ReferralInterface;
use App\Interfaces\User\AccountSecurityInterface;
use App\Interfaces\User\LoginLogInterface;
use App\Interfaces\User\RoleInterface;
use App\Interfaces\User\SecurityQuestionInterface;
use App\Interfaces\User\SettingInterface;
use App\Interfaces\User\WalletInterface;
use App\Interfaces\ApplicationInterface;
use App\Interfaces\SupportInterface;
use App\Interfaces\SupportResponseInterface;
use App\Repositories\Loan\CommentRepository;
use App\Repositories\Loan\DefaulterRepository;
use App\Repositories\Loan\DisbursementRepository;
use App\Repositories\Loan\FolderRepository;
use App\Repositories\Loan\LoanDetailRepository;
use App\Repositories\Loan\LoanRepository;
use App\Repositories\Loan\LoanStageRepository;
use App\Repositories\Loan\RepaymentRepository;
use App\Repositories\Loan\StaffAttendRepository;
use App\Repositories\Loan\UploadRepository;
use App\Repositories\Offer\OfferRepository;
use App\Repositories\Offer\StatusRepository;
use App\Repositories\User\Admin\AdminRepository;
use App\Repositories\User\UserDetailRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Offer\SignatureRepository;
use App\Repositories\Transaction\TransactionRepository;
use App\Repositories\Transaction\CouponRepository;
use App\Repositories\Transaction\CardRepository;
use App\Repositories\User\Referral\ReferralDetailRepository;
use App\Repositories\User\Referral\ReferralRepository;
use App\Repositories\User\AccountSecurityRepository;
use App\Repositories\User\LoginLogRepository;
use App\Repositories\User\RoleRepository;
use App\Repositories\User\SecurityQuestionRepository;
use App\Repositories\User\SettingRepository;
use App\Repositories\User\WalletRepository;
use App\Repositories\ApplicationRepository;
use App\Repositories\SupportRepository;
use App\Repositories\SupportResponseRepository;
use Illuminate\Support\ServiceProvider;



class RepositoryServiceProvider extends ServiceProvider
{

  protected $repositories=[
      CommentInterface::class => CommentRepository::class,
      DefaulterInterface::class => DefaulterRepository::class,
      DisbursementInterface::class => DisbursementRepository::class,
      FolderInterface::class => FolderRepository::class,
      LoanDetailInterface::class => LoanDetailRepository::class,
      LoanInterface::class => LoanRepository::class,
      LoanStageInterface::class => LoanStageRepository::class,
      RepaymentInterface::class => RepaymentRepository::class,
      StaffAttendInterface::class => StaffAttendRepository::class,
      UploadInterface::class => UploadRepository::class,
      OfferInterface::class => OfferRepository::class,
      StatusInterface::class => StatusRepository::class,
      AdminInterface::class => AdminRepository::class,
      UserDetailInterface::class => UserDetailRepository::class,
      UserInterface::class => UserRepository::class,
      SignatureInterface::class => SignatureRepository::class,
      TransactionInterface::class => TransactionRepository::class,
      CouponInterface::class => CouponRepository::class,
      CardInterface::class => CardRepository::class,
      ReferralInterface::class => ReferralRepository::class,
      ReferralDetailInterface::class => ReferralDetailRepository::class,
      AccountSecurityInterface::class => AccountSecurityRepository::class,
      LoginLogInterface::class => LoginLogRepository::class,
      RoleInterface::class => RoleRepository::class,
      SecurityQuestionInterface::class => SecurityQuestionRepository::class,
      SettingInterface::class => SettingRepository::class,
      WalletInterface::class => WalletRepository::class,
      ApplicationInterface::class => ApplicationRepository::class,
      SupportInterface::class => SupportRepository::class,
      SupportResponseInterface::class => SupportResponseRepository::class,
  ];

  public function register()
  {
    foreach ($this->repositories as $interface => $implementation)
    {
        $this->app->bind($interface, $implementation);
    }
  }
}