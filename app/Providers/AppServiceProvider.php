<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\User\UserObserver;
use App\Observers\Application\KycObserver;
use App\Observers\Application\SecurityObserver;
use App\Observers\Application\CardObserver;
use App\Observers\Loan\LoanApplicationObserver;
use App\Observers\Loan\LoanStatusObserver;
use App\Observers\Loan\RepaymentObserver;
use App\Observers\User\WalletObserver;
use App\Observers\SupportObserver;
use App\Entities\User\User;
use App\Entities\User\UserDetail;
use App\Entities\User\AccountSecurity;
use App\Entities\Transaction\Card;
use App\Entities\Loan\Loan;
use App\Entities\Offer\Status;
use App\Entities\Loan\Repayment;
use App\Entities\User\Wallet;
use App\Entities\Support;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        Paginator::useBootstrap();
        
        User::observe(UserObserver::class);
        UserDetail::observe(KycObserver::class);
        AccountSecurity::observe(SecurityObserver::class);
        Card::observe(CardObserver::class);
        Loan::observe(LoanApplicationObserver::class);
        Status::observe(LoanStatusObserver::class);
        Repayment::observe(RepaymentObserver::class);
        Wallet::observe(WalletObserver::class);
        Support::observe(SupportObserver::class);

    }
}
