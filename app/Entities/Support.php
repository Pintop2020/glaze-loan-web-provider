<?php

namespace App\Entities;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;
use App\Entities\User\User;
use App\Entities\SupportResponse;
use \Illuminate\Support\Facades\Crypt;

/**
 * Class Support
 * @package App\Entities
 */
class Support extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function responses()
    {
        return $this->hasMany(SupportResponse::class);
    }

    public function id(): String 
    {
        return 'TID-'.str_pad($this->id,6,0,STR_PAD_LEFT);
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }
}
