<?php

namespace App\Entities;

use Shamaseen\Repository\Generator\Utility\Entity;

/**
 * Class SiteSetting
 * @package App\Entities
 */
class SiteSetting extends Entity
{
    protected $guarded = [];
}
