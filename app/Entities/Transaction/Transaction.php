<?php

namespace App\Entities\Transaction;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class Transaction
 * @package App\Entities
 */
class Transaction extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function data(){
        return json_decode($this->details);
    }

    public function status() : string
    {
        $status = $this->data()->status;
        switch (strtolower($status)) {
            case 'pending':
                return '<span class="dot bg-warning d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-warning d-none d-mb-inline-flex">Pending</span>';
                break;

            case 'failed':
                return '<span class="dot bg-danger d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-danger d-none d-mb-inline-flex">Failed</span>';
                break;

            case 'success':
                return '<span class="dot bg-success d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-success d-none d-mb-inline-flex">Success</span>';
                break;
            
            default:
                break;
        }
    }

    public function status2() : string
    {
        $status = $this->data()->status;
        switch (strtolower($status)) {
            case 'pending':
                return '<span class="badge badge-warning">Pending</span>';
                break;

            case 'failed':
                return '<span class="badge badge-danger">Failed</span>';
                break;

            case 'success':
                return '<span class="badge badge-success">Success</span>';
                break;
            
            default:
                break;
        }
    }

    public function type()
    {
        $type = $this->data()->type;
        switch (strtolower($type)) {
            case 'credit':
                return '<span class="badge badge-success text-bold">Credit</credit>';
                break;

            case 'debit':
                return '<span class="badge badge-danger text-bold">Debit</credit>';
                break;

            case 'withdrawal':
                return '<span class="badge badge-danger text-bold">Withdrawal</credit>';
                break;

            case 'earnings':
                return '<span class="badge badge-success text-bold">Earnings</credit>';
                break;
            
            default:
                # code...
                break;
        }
    }
}
