<?php

namespace App\Entities\Transaction;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class Card
 * @package App\Entities
 */
class Card extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function data() 
    {
    	return json_decode($this->details);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }
}
