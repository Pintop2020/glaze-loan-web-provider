<?php

namespace App\Entities\User;

use App\Entities\Loan\Loan;
use App\Entities\User\SecurityQuestion;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Entities\User\Referral\Referral;
use App\Entities\User\Referral\RefereeDetail;
use App\Entities\User\Wallet;
use App\Entities\User\UserDetail;
use App\Entities\User\AccountSecurity;
use App\Entities\Transaction\Transaction;
use App\Entities\Transaction\Card;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\User\Setting;
use App\Entities\User\LoginLog;
use App\Http\Controllers\GlobalMethods as SuperM;
// Admin Inclusion
use App\Entities\Loan\Comment;
use App\Entities\Loan\StaffAttend;
use App\Entities\User\Role;
use App\Entities\Loan\Disbursement;
use App\Entities\Support;
use App\Entities\SupportResponse;
use Spatie\Permission\Traits\HasRoles;
use \Illuminate\Support\Facades\Crypt;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 * @package App\Entities
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes, HasRoles, HasApiTokens;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullname(): String
    {
        return ucwords(strtolower($this->first_name).' '.strtolower($this->middle_name).' '.strtolower($this->last_name));
    }

    public function nameThumb(): String
    {
        $string = $this->first_name.' '.$this->last_name;
        $expr = '/(?<=\s|^)[a-z]/i';
        preg_match_all($expr, $string, $matches);
        return strtoupper(implode('', $matches[0]));;
    }

    public function id(): String 
    {
        return 'GID'.str_pad($this->id,6,0,STR_PAD_LEFT);
    }

    public function loans()
    {
        return $this->hasMany(Loan::class);
    }

    public function security_questions()
    {
        return $this->hasOne(SecurityQuestion::class);
    }

    public function referree()
    {
        return $this->belongsToMany(User::class, 'referral_users');
    }

    public function referrals()
    {
        return $this->belongsToMany(Referral::class, 'referral_users');
    }

    public function my_details()
    {
        return $this->hasOne(UserDetail::class);
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function my_referral_code()
    {
        return $this->hasOne(RefereeDetail::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function account_security()
    {
        return $this->hasOne(AccountSecurity::class);
    }

    public function setting()
    {
        return $this->hasOne(Setting::class);
    }

    public function login_logs()
    {
        return $this->hasMany(LoginLog::class);
    }

    public function getOwing() : float
    {
        return SuperM::getOwing($this);
    }

    public function supports()
    {
        return $this->hasMany(Support::class);
    }

    public function support_responses()
    {
        return $this->hasMany(SupportResponse::class);
    }

    public function my_marketer()
    {
        return $this->belongsTo(User::class, 'marketer_id');
    }

    ///////////////////////////////////////////////////////////
    /* Admin Relationships */
    //////////////////////////////////////////////////////////
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function loan_tended_to()
    {
        return $this->belongsToMany(Loan::class, 'staff_attends');
    }

    public function tagged_comment()
    {
        return $this->belongsToMany(Comment::class, 'comment_users');
    }

    public function loan_disbursed(){
        return $this->hasMany(Disbursement::class);
    }

    public function comment_read()
    {
        return $this->belongsToMany(Comment::class, 'comment_read');
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }

    public function my_customers()
    {
        return $this->hasMany(User::class, 'marketer_id');
    }
}
