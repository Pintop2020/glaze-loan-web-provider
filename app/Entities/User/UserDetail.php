<?php

namespace App\Entities\User;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class UserDetail
 * @package App\Entities
 */
class UserDetail extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    } 

    public function data()
    {
    	return json_decode($this->details);
    }
}
