<?php

namespace App\Entities\User\Referral;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class Referral
 * @package App\Entities
 */
class Referral extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function referree()
    {
        return $this->belongsToMany(User::class, 'referral_users');
    }
}
