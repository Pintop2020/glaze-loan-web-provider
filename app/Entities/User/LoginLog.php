<?php

namespace App\Entities\User;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class LoginLog
 * @package App\Entities
 */
class LoginLog extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
