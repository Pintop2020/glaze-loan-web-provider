<?php

namespace App\Entities\User;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class AccountSecurity
 * @package App\Entities
 */
class AccountSecurity extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
