<?php

namespace App\Entities\User\Admin;

use Shamaseen\Repository\Generator\Utility\Entity;

/**
 * Class Role
 * @package App\Entities
 */
class Role extends Entity
{
    protected $guarded = [];
}
