<?php

namespace App\Entities\User;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;

/**
 * Class Setting
 * @package App\Entities
 */
class Setting extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function full_data()
    {
    	return json_decode($this->details);
    }
}
