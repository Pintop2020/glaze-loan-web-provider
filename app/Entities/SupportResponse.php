<?php

namespace App\Entities;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Support;
use App\Entities\User\User;
use \Illuminate\Support\Facades\Crypt;

/**
 * Class SupportResponse
 * @package App\Entities
 */
class SupportResponse extends Entity
{
    protected $guarded = [];

    public function support()
    {
        return $this->belongsTo(Support::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }
}
