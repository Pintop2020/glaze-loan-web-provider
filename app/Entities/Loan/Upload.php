<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Folder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Upload
 * @package App\Entities
 */
class Upload extends Entity
{
	use SoftDeletes;
	
    protected $guarded = [];

    public function folder()
    {
        return $this->belongsTo(Folder::class);
    }
}
