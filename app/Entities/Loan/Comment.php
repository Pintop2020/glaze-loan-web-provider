<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;
use App\Entities\Loan\Loan;

/**
 * Class Comment
 * @package App\Entities
 */
class Comment extends Entity
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tagged_staff()
    {
        return $this->belongsToMany(User::class, 'comment_users');
    }

    public function loan()
    {
        return $this->belongsToMany(Loan::class, 'comment_loans');
    }

    public function user_read()
    {
        return $this->belongsToMany(User::class, 'comment_read');
    }

}
