<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;

/**
 * Class CreditCheck
 * @package App\Entities
 */
class CreditCheck extends Entity
{
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
