<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\User\User;
use App\Entities\User\UserDetail;
use App\Entities\Loan\Repayment;
use App\Entities\Loan\Folder;
use App\Entities\Loan\StaffAttend;
use App\Entities\Loan\Comment;
use App\Entities\Loan\Defaulter;
use App\Entities\Loan\Disbursement;
use App\Entities\Loan\CreditCheck;
use App\Entities\Offer\Status;
use App\Entities\Offer\Offer;
use App\Entities\Support;
use Spatie\Permission\Models\Role;
use \Illuminate\Support\Facades\Crypt;

/**
 * Class Loan
 * @package App\Entities
 */
class Loan extends Entity
{
    protected $guarded = [];

    public function data()
    {
        return json_decode($this->details);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function stages()
    {
        return $this->belongsToMany(Role::class, 'loan_stages');
    }

    public function repayments()
    {
        return $this->hasMany(Repayment::class);
    }

    public function folder()
    {
        return $this->hasOne(Folder::class);
    }

    public function staffs()
    {
        return $this->belongsToMany(User::class, 'staff_attends');
    }

    public function status()
    {
        return $this->hasOne(Status::class);
    }

    public function offer()
    {
        return $this->hasOne(Offer::class);
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class, 'comment_loans');
    }

    public function defaults()
    {
        return $this->hasMany(Defaulter::class);
    }

    public function disbursement()
    {
        return $this->hasOne(Disbursement::class);
    }

    public function support()
    {
        return $this->hasMany(Support::class);
    }

    public function id(): String 
    {
        return 'INV-'.str_pad($this->id,6,0,STR_PAD_LEFT);
    }

    public function credit_checks()
    {
        return $this->hasOne(CreditCheck::class);
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }

    public function dstatus()
    {
        $status = $this->status;
        if(!$status) return '<span class="dot bg-warning d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-warning d-none d-mb-inline-flex">Pending</span>';
        else{
            if($status->status == 'approved') {
                return '<span class="dot bg-info d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-info d-none d-mb-inline-flex">Approved</span>';
            }elseif($status->status == 'declined'){
                return '<span class="dot bg-danger d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-danger d-none d-mb-inline-flex">Declined</span>';
            }elseif($status->status == 'repaid'){
                return '<span class="dot bg-success d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-success d-none d-mb-inline-flex">Repaid</span>';
            }else{
                return '<span class="dot bg-primary d-mb-none"></span><span class="badge badge-sm badge-dot has-bg badge-primary d-none d-mb-inline-flex">Active</span>';
            }
        }
    }

    public function dstatus2()
    {
        $status = $this->status;
        if(!$status) return '<span class="badge badge-warning">Pending</span>';
        else{
            if($status->status == 'approved') {
                return '<span class="badge badge-info">Approved</span>';
            }elseif($status->status == 'declined'){
                return '<span class="badge badge-danger">Declined</span>';
            }elseif($status->status == 'repaid'){
                return '<span class="badge badge-success">Repaid</span>';
            }else{
                return '<span class="badge badge-primary">Active</span>';
            }
        }
    }

}
