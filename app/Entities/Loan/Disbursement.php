<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;

/**
 * Class Disbursement
 * @package App\Entities
 */
class Disbursement extends Entity
{
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
