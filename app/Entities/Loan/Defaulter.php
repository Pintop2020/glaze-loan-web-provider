<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;

/**
 * Class Defaulter
 * @package App\Entities
 */
class Defaulter extends Entity
{
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
