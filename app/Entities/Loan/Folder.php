<?php

namespace App\Entities\Loan;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;
use App\Entities\Loan\Upload;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Support\Facades\Crypt;

/**
 * Class Folder
 * @package App\Entities
 */
class Folder extends Entity
{
	use SoftDeletes;
	
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function uploads()
    {
        return $this->hasMany(Upload::class);
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }
}
