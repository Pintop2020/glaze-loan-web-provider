<?php

namespace App\Entities\Offer;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Offer\Offer;

/**
 * Class Signature
 * @package App\Entities
 */
class Signature extends Entity
{
    protected $guarded = [];

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }
}
