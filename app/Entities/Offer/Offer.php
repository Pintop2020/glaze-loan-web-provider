<?php

namespace App\Entities\Offer;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;
use App\Entities\Offer\Signature;
use \Illuminate\Support\Facades\Crypt;

/**
 * Class Offer
 * @package App\Entities
 */
class Offer extends Entity
{
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function setOfferDetailsAttribute($value) {
        $this->attributes['offer_details'] = json_encode($value);
    }

    public function signature()
    {
        return $this->hasOne(Signature::class);
    }

    public function data()
    {
        return json_decode($this->offer_details);
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }
}
