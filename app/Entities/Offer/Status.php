<?php

namespace App\Entities\Offer;

use Shamaseen\Repository\Generator\Utility\Entity;
use App\Entities\Loan\Loan;
use \Illuminate\Support\Facades\Crypt;

/**
 * Class Status
 * @package App\Entities
 */
class Status extends Entity
{
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function uuid(): String 
    {
        return Crypt::encryptString($this->id);
    }
}
