<?php

namespace App\Interfaces;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface SupportInterface
 * @package App\Interfaces
 */
interface SupportInterface extends ContractInterface
{

}
