<?php

namespace App\Interfaces\User;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface SettingInterface
 * @package App\Interfaces\User
 */
interface SettingInterface extends ContractInterface
{

}
