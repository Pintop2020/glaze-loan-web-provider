<?php

namespace App\Interfaces\User;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface SecurityQuestionInterface
 * @package App\Interfaces\User
 */
interface SecurityQuestionInterface extends ContractInterface
{

}
