<?php

namespace App\Interfaces\User;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface UserDetailInterface
 * @package App\Interfaces\User
 */
interface UserDetailInterface extends ContractInterface
{

}
