<?php

namespace App\Interfaces\User\Admin;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface RoleInterface
 * @package App\Interfaces\User\Admin
 */
interface RoleInterface extends ContractInterface
{

}
