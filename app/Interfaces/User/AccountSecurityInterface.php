<?php

namespace App\Interfaces\User;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface AccountSecurityInterface
 * @package App\Interfaces\User
 */
interface AccountSecurityInterface extends ContractInterface
{

}
