<?php

namespace App\Interfaces\User\Referral;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface RefereeDetailInterface
 * @package App\Interfaces\User\Referral
 */
interface RefereeDetailInterface extends ContractInterface
{

}
