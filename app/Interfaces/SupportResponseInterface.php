<?php

namespace App\Interfaces;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface SupportResponseInterface
 * @package App\Interfaces
 */
interface SupportResponseInterface extends ContractInterface
{

}
