<?php

namespace App\Interfaces\Offer;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface OfferInterface
 * @package App\Interfaces\Offer
 */
interface OfferInterface extends ContractInterface
{

}
