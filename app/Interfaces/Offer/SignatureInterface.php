<?php

namespace App\Interfaces\Offer;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface SignatureInterface
 * @package App\Interfaces\Offer
 */
interface SignatureInterface extends ContractInterface
{

}
