<?php

namespace App\Interfaces\Offer;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface StatusInterface
 * @package App\Interfaces\Offer
 */
interface StatusInterface extends ContractInterface
{

}
