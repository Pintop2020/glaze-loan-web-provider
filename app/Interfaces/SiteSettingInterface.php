<?php

namespace App\Interfaces;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface SiteSettingInterface
 * @package App\Interfaces
 */
interface SiteSettingInterface extends ContractInterface
{

}
