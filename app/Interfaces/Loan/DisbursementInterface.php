<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface DisbursementInterface
 * @package App\Interfaces\Loan
 */
interface DisbursementInterface extends ContractInterface
{

}
