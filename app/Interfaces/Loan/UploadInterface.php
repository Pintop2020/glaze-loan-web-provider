<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface UploadInterface
 * @package App\Interfaces\Loan
 */
interface UploadInterface extends ContractInterface
{

}
