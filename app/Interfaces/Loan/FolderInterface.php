<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface FolderInterface
 * @package App\Interfaces\Loan
 */
interface FolderInterface extends ContractInterface
{

}
