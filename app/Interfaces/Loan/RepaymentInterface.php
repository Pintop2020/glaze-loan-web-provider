<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface RepaymentInterface
 * @package App\Interfaces\Loan
 */
interface RepaymentInterface extends ContractInterface
{

}
