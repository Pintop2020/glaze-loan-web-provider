<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface CommentInterface
 * @package App\Interfaces\Loan
 */
interface CommentInterface extends ContractInterface
{

}
