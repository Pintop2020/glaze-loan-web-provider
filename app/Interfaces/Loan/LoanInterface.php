<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface LoanInterface
 * @package App\Interfaces\Loan
 */
interface LoanInterface extends ContractInterface
{

}
