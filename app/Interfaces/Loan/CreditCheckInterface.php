<?php

namespace App\Interfaces\Loan;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface CreditCheckInterface
 * @package App\Interfaces\Loan
 */
interface CreditCheckInterface extends ContractInterface
{

}
