<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\Admin\AdminController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\UserDetailController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Http\Controllers\User\LoginLogController;
use App\Http\Controllers\Loan\LoanController;
use App\Http\Controllers\Loan\CreditCheckController;
use App\Http\Controllers\Loan\FolderController;
use App\Http\Controllers\Loan\UploadController;
use App\Http\Controllers\Offer\StatusController;
use App\Http\Controllers\Offer\OfferController;
use App\Http\Controllers\Loan\DefaulterController;
use App\Http\Controllers\Loan\RepaymentController;
use App\Http\Controllers\Loan\CommentController;
use App\Http\Controllers\User\Referral\ReferralController;
use App\Http\Controllers\SupportResponseController;
use App\Http\Controllers\Report\CustomerBaseController;
use App\Http\Controllers\Report\LoanReportController;
use App\Http\Controllers\Report\LoanProcessController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "is_admin" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return redirect()->route('admin.home');
});

Route::group(['middleware'=>['is_admin']], function(){
	Route::get('/dashboard', [AdminController::class , 'home'])->name('admin.home');

	Route::resource('users', UserController::class)->except(['edit', 'update']);
	Route::resource('userDetails', UserDetailController::class)->only(['destroy']);
	Route::get('/users/perm/{id}', [UserController::class, 'perm']);
	Route::get('/platform/users/{type}', [UserController::class, 'custom_index']);
	Route::get('/transactions/user/{id}', [TransactionController::class, 'user_transactions']);
	Route::get('/loginLogs/user/{id}', [LoginLogController::class, 'user_login_logs']);

	Route::get('/loans/get/{type}', [LoanController::class, 'custom_index']);
	Route::resource('loans', LoanController::class)->only(['show']);
	Route::get('loans/{action}/{id}', [LoanController::class, 'action']);
	Route::get('loans/return/back/{id}', [LoanController::class, 'returnBack']);
	Route::get('credit_checks/create/{id}', [CreditCheckController::class, 'create_admin']);
	Route::resource('folders', FolderController::class)->only(['index', 'show']);
	Route::get('/folders/create/{id}', [FolderController::class, 'create_admin']);
	Route::get('/folders/show/{id}', [FolderController::class, 'show_main']);
	Route::post('/applications/uploads', [UploadController::class, 'create']);
	Route::resource('uploads', UploadController::class)->only(['destroy']);
	Route::get('/uploads/restore/{id}', [UploadController::class, 'restore']);
	Route::get('/trash', [UploadController::class, 'trash']);
	Route::get('/offers/create/{id}', [OfferController::class, 'createNew']);
	Route::resource('offers', OfferController::class)->only(['edit', 'update', 'show']);
	Route::post('/offers/store', [OfferController::class, 'store'])->name('createOffers');
	Route::get('/payout/loans/{id}', [LoanController::class, 'disburse']);
	Route::get('/defaulters/clear/{id}', [DefaulterController::class, 'clear']);
	Route::get('/repayments/update/user/{id}', [RepaymentController::class, 'updatePayments']);
	Route::resource('repayments', RepaymentController::class);
	Route::get('/comments/create/{id}', [CommentController::class, 'add']);
	Route::resource('comments', CommentController::class)->only(['index', 'store', 'show']);
	Route::resource('referrals', ReferralController::class)->only(['index']);

	Route::get('staff', [UserController::class, 'staff']);
	Route::get('staff/add', [UserController::class, 'new_staff']);
	Route::post('staff/add', [UserController::class, 'new_staff_post']);
	Route::get('staff/edit/{id}', [UserController::class, 'edit']);
	Route::post('staff/edit/{id}', [UserController::class, 'update']);
	Route::resource('transactions', TransactionController::class)->only(['index']);
	Route::resource('supportResponses', SupportResponseController::class)->except(['destroy', 'update', 'edit']);
	Route::get('/support/close/{id}', [SupportResponseController::class, 'closeSupport']);

	Route::get('/reports/customer-base', CustomerBaseController::class);
	Route::get('/reports/customer-base-generate', [CustomerBaseController::class, 'generate']);

	Route::get('/reports/loan-reports', LoanReportController::class);
	Route::get('/reports/loan-reports-generate', [LoanReportController::class, 'generate']);

	Route::get('/reports/loan-process', LoanProcessController::class);
	Route::get('/reports/loan-process-generate', [LoanProcessController::class, 'generate']);
});

