<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\UserDetailController;
use App\Http\Controllers\User\AccountSecurityController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\Loans\LoanHomeController;
use App\Http\Controllers\API\ForgotPasswordController;
use App\Http\Controllers\API\KYC\StatusCheckerController;
use App\Entities\User\User;
use App\Http\Controllers\User\WalletController;
use App\Http\Controllers\Transaction\CardController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Http\Controllers\Loan\LoanController;
use App\Http\Controllers\Loan\CreditCheckController;
use App\Http\Controllers\Loan\RepaymentController;
use App\Http\Controllers\Offer\OfferController;
use App\Http\Controllers\User\NotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix'=>'auth'], function(){
	Route::post('register', [UserController::class, 'store']);
	Route::patch('login', [UserController::class, 'login']);
	Route::post('forgot', ForgotPasswordController::class);
});
Route::get('/verify_referee/{ref}', [UserController::class, 'verify_referee']);


Route::group(['middleware'=>'auth:sanctum'], function(){
	Route::get('/auth/user', function (Request $request) {
		return [
	        'code' => 200,
	        'message' => 'Full profile details',
	        'status' => true,
	        'data' => [
				'user'=>User::where('id', $request->user()->id)->with(['transactions', 'wallet', 'loans', 'my_details', 'cards', 'account_security', 'security_questions', 'referree', 'referrals', 'setting', 'login_logs', 'supports', 'my_marketer', 'my_referral_code'])->first(),
                'total_transaction'=>$request->user()->transactions()->sum('details->amount'),
                'owing'=>$request->user()->getOwing(),
                'uuid'=>$request->user()->uuid(),
                'id'=>$request->user()->id(),
                'full_name'=>$request->user()->getFullname(),
			],
	    ];
	});
	Route::get('/dashboard', DashboardController::class);
	Route::group(['prefix'=>'utilities'], function(){
		Route::get('/check-status', StatusCheckerController::class);
		Route::get('/check-bvn/{bvn}/{phone}', [UserDetailController::class, 'getBvn']);
		Route::get('/check-bank/{bank}/{account}', [UserDetailController::class, 'getBank']);
	});
	Route::post('/store-kyc', [UserDetailController::class, 'store']);
	Route::post('/store-pin', [AccountSecurityController::class, 'store']);
	Route::get('/store-user-card', [CardController::class, 'verifyPayment']);
	Route::post('/initiate-card-pay', [CardController::class, 'initiateCardAccess']);
	// wallets
	Route::post('/wallet/deposits', [WalletController::class, 'depositPost']);
	Route::get('/wallet/deposits/confirm', [WalletController::class, 'confirmDeposit']);
	Route::post('/wallet/withdraw', [WalletController::class, 'withdrawPost']);
	// Transactions 
	Route::get('/transactions', [TransactionController::class, 'index']);
	// Loans
	Route::post('/loans/apply', [LoanController::class, 'store']);
	Route::get('/loans', [LoanController::class, 'index']);
	Route::get('/loans/{id}', [LoanController::class, 'show']);
	Route::get('/clear-loans/{id}', [LoanController::class, 'clearLoan']);
	// credit checks
	Route::get('/credit_check', [CreditCheckController::class, 'create']);
	Route::get('/credit_check/charge_wallet', [CreditCheckController::class, 'initiateFromWallet']);
	Route::get('/credit_check/charge_card', [CreditCheckController::class, 'initiateFromCard']);
	// offers
	Route::get('/offers/{id}', [OfferController::class, 'show']);
	//Repayments
	Route::get('/repayments/from-wallet', [RepaymentController::class, 'payFromWallet']);
	Route::get('/repayments/from-card-exists', [RepaymentController::class, 'charge_auth']);
	Route::get('/repayments/confirm', [RepaymentController::class, 'payFromCardConfirm']);
	// Cards 
	Route::apiResource('cards', CardController::class)->only(['index']);
	Route::get('logout', [UserController::class, 'logout']);
	// verify referree
	Route::get('/notifications', [NotificationController::class, 'index']);
});