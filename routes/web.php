<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserDetailController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\WalletController;
use App\Http\Controllers\User\Referral\ReferralController;
use App\Http\Controllers\User\ChangePasswordController;
use App\Http\Controllers\User\AccountSecurityController;
use App\Http\Controllers\User\NotificationController;
use App\Http\Controllers\Transaction\CardController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Http\Controllers\Loan\LoanController;
use App\Http\Controllers\Loan\CreditCheckController;
use App\Http\Controllers\Loan\RepaymentController;
use App\Http\Controllers\Offer\SignatureController;
use App\Http\Controllers\Offer\OfferController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\SupportResponseController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register'=>false, 'verify'=>true]);
Route::get('/migrate-my-db', function(){
	Artisan::call('migrate:fresh', [
	   '--force' => true, '--seed' => true
	]);
});

Route::group(['middleware'=>['auth','verified','is_user']], function(){
	/**Kyc Routes **/
	Route::get('/application', [UserDetailController::class, 'kyc']);
	Route::get('/application/preview', [UserDetailController::class, 'preview']);
	Route::group(['prefix'=>'application', 'middleware'=>'is_completed'], function() {
		Route::resource('kyc', UserDetailController::class)->only(['create', 'store']);
		Route::resource('pin', AccountSecurityController::class)->only(['create', 'store']);
		Route::get('/card/create', [CardController::class, 'initiateCardLink']);
		Route::get('/card/verify', [CardController::class, 'verifyPayment']);
	});
	/**Kyc Routes ends**/
	Route::group(['middleware'=>'is_complete'], function(){
		Route::get('/dashboard', [UserController::class, 'home'])->name('home');
		Route::resource('transactions', TransactionController::class)->only('index');
		Route::resource('loans', LoanController::class)->except(['edit', 'update']);
		Route::get('/credit-checks', [CreditCheckController::class, 'initiatePayment']);
		Route::get('/credit-checks/create', [CreditCheckController::class, 'create']);
		Route::resource('offers', OfferController::class)->only(['show']);
		Route::get('/loans/offer/sign/{id}', [OfferController::class, 'sign']);
		Route::resource('signatures', SignatureController::class)->only(['store']);
		Route::get('/repayments/from-wallet', [RepaymentController::class, 'payFromWallet']);
		Route::get('/repayments/from-card', [RepaymentController::class, 'initiateCardLink']);
		Route::get('/repayments/from-card/confirm', [RepaymentController::class, 'payFromCardConfirm']);

		Route::get('/profile', [UserController::class, 'profile']);
		Route::get('/security', [UserController::class, 'security']);
		Route::get('/notifications-settings', [UserController::class, 'notificationSetting']);
		Route::post('/notifications-settings', [UserController::class, 'notificationSettingPost'])->name('settings');
		Route::post('/security', [UserController::class, 'securityPost'])->name('secure_setting');
		Route::post('change-password', [ChangePasswordController::class, 'store'])->name('change.password');
		Route::get('/change-password', [ChangePasswordController::class, 'index']);

		Route::resource('referrals', ReferralController::class)->only(['index']);


		Route::group(['prefix'=>'wallet'], function(){
			Route::get('/', [WalletController::class, 'index']);
			Route::get('/deposit', [WalletController::class, 'deposit']);
			Route::post('/deposit', [WalletController::class, 'depositPost']);
			Route::get('/deposit/confirm', [WalletController::class, 'confirmDeposit']);
			Route::get('/withdraw', [WalletController::class, 'withdraw']);
			Route::post('/withdraw', [WalletController::class, 'withdrawPost']);
		});

		Route::resource('supports', SupportController::class)->except(['destroy', 'update', 'edit']);
		Route::resource('supportResponses', SupportResponseController::class)->except(['destroy', 'update', 'edit']);
		Route::get('/support/close/{id}', [SupportController::class, 'closeSupport']);

		Route::group(['prefix'=>'notifications'], function(){
			Route::get('/view/{id}', [NotificationController::class, 'view']);
			Route::get('/read_all', [NotificationController::class, 'read']);
		});

		Route::get('clear-loan/{id}', [LoanController::class, 'clearLoan']);
	});
	
});

Route::group(['middleware'=>'guest'], function(){
	Route::get('/', function(){
		return redirect()->route('home');
	});

	Route::group(['prefix'=>'auth'], function(){
		Route::get('register', [UserController::class, 'create'])->name('user.create');
		Route::post('register', [UserController::class, 'store'])->name('user.create');
	});
});


Route::get('/verify_referee/{ref}', [UserController::class, 'verify_referee'])->name('verify.referee');
Route::get('/charts', [ReferralController::class, 'userChart'])->name('referral.chart');
Route::post('/save-passport', [UserDetailController::class, 'savePassport']);
Route::post('/save-id', [UserDetailController::class, 'saveID']);
Route::post('/save-company-id', [UserDetailController::class, 'saveCompanyID']);
Route::get('/check-uploads', [UserDetailController::class, 'checkUploads']);
Route::get('/check-bvn/{bvn}/{phone}', [UserDetailController::class, 'getBvn']);
Route::get('/check-bank/{bank}/{account}', [UserDetailController::class, 'getBank']);
Route::post('/save-employment-letter', [LoanController::class, 'saveEmployment']);
Route::post('/save-utility-bill', [LoanController::class, 'saveUtility']);
Route::post('/save-statements', [LoanController::class, 'saveStatements']);
Route::post('/pin/validate', [AccountSecurityController::class, 'validatePin']);