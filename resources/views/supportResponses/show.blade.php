@extends('layouts.admin.app')

@section('title', __('Attending Ticket'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-body p-0">
	<div class="nk-msg">
		<div class="nk-msg-body bg-white profile-shown">
			<div class="nk-msg-head">
				<h4 class="title d-none d-lg-block">{{ $support->subject }}</h4>
				<div class="nk-msg-head-meta">
					<div class="d-none d-lg-block">
						<ul class="nk-msg-tags">
							<li><span class="label-tag"><em class="icon ni ni-flag-fill"></em> <span>{{ $support->department }}</span></span></li>
							<li><span class="label-tag"><em class="icon ni ni-circle-fill"></em> <span>{{ $support->priority }}</span></span></li>
						</ul>
					</div>
					<div class="d-lg-none"><a href="#" class="btn btn-icon btn-trigger nk-msg-hide ml-n1"><em class="icon ni ni-arrow-left"></em></a></div>
					<ul class="nk-msg-actions">
						@if($support->status == 'opened')
						<li><a href="/support/close/{{ \Illuminate\Support\Facades\Crypt::encryptString($support->id) }}" class="btn btn-dim btn-sm btn-outline-light"><em class="icon ni ni-check"></em><span>Mark as Closed</span></a></li>
						@endif
						@if($support->status == 'closed')
						<li><span class="badge badge-dim badge-success badge-sm"><em class="icon ni ni-check"></em><span>Closed</span></span></li>
						@endif
						<li class="d-lg-none"><a href="#" class="btn btn-icon btn-sm btn-white btn-light profile-toggle"><em class="icon ni ni-info-i"></em></a></li>
					</ul>
				</div>
				<a href="#" class="nk-msg-profile-toggle profile-toggle active"><em class="icon ni ni-arrow-left"></em></a>
			</div>
			<!-- .nk-msg-head -->
			<div class="nk-msg-reply nk-reply" data-simplebar>
				<div class="nk-msg-head py-4 d-lg-none">
					<h4 class="title">{{ $support->subject }}</h4>
					<ul class="nk-msg-tags">
						<li><span class="label-tag"><em class="icon ni ni-flag-fill"></em> <span>{{ $support->department }}</span></span></li>
						<li><span class="label-tag"><em class="icon ni ni-circle-fill"></em> <span>{{ $support->priority }}</span></span></li>
					</ul>
				</div>
				@foreach($support->responses()->latest()->get() as $response)
				<div class="nk-reply-item">
					<div class="nk-reply-header">
						<div class="user-card">
							<div class="user-avatar sm bg-blue">
								@if($response->user->my_details)
									<img src="{{ asset($response->user->my_details->data()->identity->passport) }}">
								@else
									<span>{{ $response->user->nameThumb() }}</span>
								@endif
							</div>
							<div class="user-name">{{ $response->user->getFullname() }}</div>
						</div>
						<div class="date-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($response->created_at))->diffForHumans() }}</div>
					</div>
					<div class="nk-reply-body">
						<div class="nk-reply-entry entry">
							{!! $response->response !!}
						</div>
					</div>
				</div>
				@endforeach
				<div class="nk-reply-item">
					<div class="nk-reply-header">
						<div class="user-card">
							<div class="user-avatar sm bg-blue">
								@if($support->user->my_details)
									<img src="{{ asset($support->user->my_details->data()->identity->passport) }}">
								@else
									<span>{{ $support->user->nameThumb() }}</span>
								@endif
							</div>
							<div class="user-name">{{ $support->user->getFullname() }}</div>
						</div>
						<div class="date-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($support->created_at))->diffForHumans() }}</div>
					</div>
					<div class="nk-reply-body">
						<div class="nk-reply-entry entry">
							{!! $support->message !!}
						</div>
					</div>
				</div>
				@if($support->status == 'opened')
				<form action="{{ route('supportResponses.store') }}" method="post">
					@csrf
					<input type="hidden" name="support" value="{{ $support->id }}">
					<div class="nk-reply-form">
						<div class="tab-content">
							<div class="tab-pane active" id="reply-form">
								<div class="nk-reply-form-editor">
									<div class="nk-reply-form-field">
										<textarea class="summernote-basic" name="message"></textarea>
										<button type="submit" class="btn btn-lg btn-primary mt-4">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				@endif
			</div>
			<div class="nk-msg-profile visible" data-simplebar>
				<div class="card">
					<div class="card-inner-group">
						<div class="card-inner">
							<div class="user-card user-card-s2 mb-2">
								<div class="user-avatar md bg-primary">
									@if($support->user->my_details)
										<img src="{{ asset($support->user->my_details->data()->identity->passport) }}">
									@else
										<span>{{ $support->user->nameThumb() }}</span>
									@endif
								</div>
								<div class="user-info">
									<h5>{{ $support->user->getFullname() }}</h5>
									<span class="sub-text">Customer</span>
								</div>
								<div class="user-card-menu dropdown">
									<a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="link-list-opt no-bdr">
											<li><a href="/users/{{ $support->user->uuid() }}"><em class="icon ni ni-eye"></em><span>View Profile</span></a></li>
											<li><a href="#"><em class="icon ni ni-exchange"></em><span>Restrict</span></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="row text-center g-1">
								<div class="col-4">
									<div class="profile-stats">
										<span class="amount">{{ $support->user->loans()->count() }}</span>
										<span class="sub-text">Total Loans</span>
									</div>
								</div>
								<div class="col-4">
									<div class="profile-stats">
										<span class="amount">{{ $support->user->loans()->whereHas('status', function ($query) { $query->where('status', 'approved');})->count() }}</span>
										<span class="sub-text">Approved Loans</span>
									</div>
								</div>
								<div class="col-4">
									<div class="profile-stats">
										<span class="amount">{{ $support->user->loans()->whereHas('status', function ($query) { $query->where('status', 'repaid');})->count() }}</span>
										<span class="sub-text">Cleared Loans</span>
									</div>
								</div>
							</div>
						</div>
						<!-- .card-inner -->
						<div class="card-inner">
							<div class="aside-wg">
								<h6 class="overline-title-alt mb-2">User Information</h6>
								<ul class="user-contacts">
									<li>
										<em class="icon ni ni-mail"></em><span>{{ $support->user->email }}</span>
									</li>
									<li>
										<em class="icon ni ni-call"></em><span>{{ $support->user->mobile }}</span>
									</li>
									@if($support->user->my_details)
									<li>
										<em class="icon ni ni-map-pin"></em><span>{{ $support->user->my_details->data()->address_info->address_1 }} <br>{{ $support->user->my_details->data()->address_info->city_of_residence }}, {{ $support->user->my_details->data()->address_info->state_of_residence }}</span>
									</li>
									@endif
								</ul>
							</div>
							<div class="aside-wg">
								<h6 class="overline-title-alt mb-2">Additional</h6>
								<div class="row gx-1 gy-3">
									<div class="col-6">
										<span class="sub-text">Ticket ID: </span>
										<span>{{ $support->id() }}</span>
									</div>
									<div class="col-6">
										<span class="sub-text">Requested:</span>
										<span>{{ $support->user->getFullname() }}</span>
									</div>
									<div class="col-6">
										<span class="sub-text">Status:</span>
										@if($support->status == 'opened')
										<span class="lead-text text-success">Open</span>
										@else
										<span class="lead-text text-danger">Closed</span>
										@endif
									</div>
									<div class="col-6">
										<span class="sub-text">Last Reply:</span>
										@if($support->responses()->count() > 0)
										<span>{{ $support->responses()->latest()->first()->user->getFullname() }}</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('more-scripts')
<link rel="stylesheet" href="{{ asset('assets/css/editors/summernote.css') }}">
<script src="{{ asset('assets/js/libs/editors/summernote.js') }}"></script>
<script src="{{ asset('assets/js/editors.js') }}"></script>
@endpush