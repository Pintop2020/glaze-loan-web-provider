@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('All Comments'))

@section('theB') <div class="nk-content p-0"><div class="nk-content-inner"> @endsection

@section('content')
<div class="nk-content-body p-0">
	<div class="nk-chat">
		<div class="nk-chat-aside">
			<div class="nk-chat-aside-head">
				<div class="nk-chat-aside-user">
					<div class="dropdown">
						<a href="#" class="dropdown-toggle dropdown-indicator">
							<div class="user-avatar">
								<span>{{ $user->nameThumb() }}</span>
							</div>
							<div class="title">Comments</div>
						</a>
					</div>
				</div>
			</div>
			<div class="nk-chat-aside-body" data-simplebar>
				<!--<div class="nk-chat-aside-search">
					<div class="form-group">
						<div class="form-control-wrap">
							<div class="form-icon form-icon-left">
								<em class="icon ni ni-search"></em>
							</div>
							<input type="text" class="form-control form-round" id="default-03" placeholder="Search by sender">
						</div>
					</div>
				</div>-->
				<div class="nk-chat-list">
					<h6 class="title overline-title-alt">Comments</h6>
					<ul class="chat-list">
						@foreach($entries as $data)
						@if($data->comments()->where('user_id', '!=', $user->id)->whereHas('user_read', function($query) use($user){
							$query->where('users.id', $user->id);
						})->count() > 0)
						<li class="chat-item">
						@else
						<li class="chat-item is-unread">
						@endif
							<a class="chat-link chat-open" href="/comments/{{ $data->id }}">
								<div class="chat-media user-avatar bg-{{ SuperM::getRandom() }}">
									<span>{{ $data->user->nameThumb() }}</span>
									@if(Cache::has('user-is-online-' . $data->user->id))
	                                <span class="status dot dot-lg dot-success"></span>
                                  	@else
                                  	<span class="status dot dot-lg dot-gray"></span>
                                  	@endif
								</div>
								<div class="chat-info">
									<div class="chat-from">
										<div class="name">{{ $data->user->last_name }} - {{ $data->id() }}</div>
										<span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($data->comments()->latest()->first()->created_at))->diffForHumans() }}</span>
									</div>
									<div class="chat-context">
										<div class="text">
											@if($data->comments()->latest()->first()->user->id == $user->id)
											<p>You: {{ substr($data->comments()->latest()->first()->comments, 0, 20) }}</p>
											@else
											{{ substr($data->comments()->latest()->first()->comments, 0, 20) }}
											@endif
										</div>
										@if($data->comments()->where('user_id', '!=', $user->id)->whereHas('user_read', function($query) use($user){
											$query->where('users.id', $user->id);
										})->count() > 0)
										<div class="status delivered">
											<em class="icon ni ni-check-circle-fill"></em>
										</div>
										@else 
										<div class="status unread">
											<em class="icon ni ni-check-circle"></em>
										</div>
										@endif
									</div>
								</div>
							</a>
						</li>
						@endforeach
					</ul>
					{{ $entries->links() }}
				</div>
			</div>
		</div>
		<div class="nk-chat-body profile-shown">
			<div class="nk-chat-head">
			</div>
			<div class="nk-chat-panel" data-simplebar>
			</div>
			<div class="nk-chat-profile visible" data-simplebar>
			</div>
		</div>
	</div>
</div>
@endsection