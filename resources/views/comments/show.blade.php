@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('Viewing Loan Comment'))

@section('theB') <div class="nk-content p-0"><div class="nk-content-inner"> @endsection

@section('content')
<div class="nk-content-body p-0">
    <div class="nk-chat">
        <div class="nk-chat-aside">
            <div class="nk-chat-aside-head">
                <div class="nk-chat-aside-user">
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle dropdown-indicator">
                            <div class="user-avatar">
                                <span>{{ $user->nameThumb() }}</span>
                            </div>
                            <div class="title">Comments</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="nk-chat-aside-body" data-simplebar>
                <!--<div class="nk-chat-aside-search">
                    <div class="form-group">
                        <div class="form-control-wrap">
                            <div class="form-icon form-icon-left">
                                <em class="icon ni ni-search"></em>
                            </div>
                            <input type="text" class="form-control form-round" id="default-03" placeholder="Search by sender">
                        </div>
                    </div>
                </div>-->
                <div class="nk-chat-list">
                    <h6 class="title overline-title-alt">Comments</h6>
                    <ul class="chat-list">
                        @foreach($entries as $data)
                        @if($data->comments()->where('user_id', '!=', $user->id)->whereHas('user_read', function($query) use($user){
                            $query->where('users.id', $user->id);
                        })->count() > 0)
                        <li class="chat-item">
                        @else
                        <li class="chat-item is-unread">
                        @endif
                            <a class="chat-link chat-open" href="/comments/{{ $data->id }}">
                                <div class="chat-media user-avatar bg-{{ SuperM::getRandom() }}">
                                    <span>{{ $data->user->nameThumb() }}</span>
                                    @if(Cache::has('user-is-online-' . $data->user->id))
                                    <span class="status dot dot-lg dot-success"></span>
                                    @else
                                    <span class="status dot dot-lg dot-gray"></span>
                                    @endif
                                </div>
                                <div class="chat-info">
                                    <div class="chat-from">
                                        <div class="name">{{ $data->user->last_name }} - {{ $data->id() }}</div>
                                        <span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($data->comments()->latest()->first()->created_at))->diffForHumans() }}</span>
                                    </div>
                                    <div class="chat-context">
                                        <div class="text">
                                            @if($data->comments()->latest()->first()->user->id == $user->id)
                                            <p>You: {{ substr($data->comments()->latest()->first()->comments, 0, 20) }}</p>
                                            @else
                                            {{ substr($data->comments()->latest()->first()->comments, 0, 20) }}
                                            @endif
                                        </div>
                                        @if($data->comments()->where('user_id', '!=', $user->id)->whereHas('user_read', function($query) use($user){
                                            $query->where('users.id', $user->id);
                                        })->count() > 0)
                                        <div class="status delivered">
                                            <em class="icon ni ni-check-circle-fill"></em>
                                        </div>
                                        @else 
                                        <div class="status unread">
                                            <em class="icon ni ni-check-circle"></em>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    {{ $entries->links() }}
                </div>
            </div>
        </div>
        <!-- .nk-chat-aside -->
        <div class="nk-chat-body profile-shown">
            <div class="nk-chat-head">
                <ul class="nk-chat-head-info">
                    <li class="nk-chat-body-close">
                        <a href="#" class="btn btn-icon btn-trigger nk-chat-hide ml-n1"><em class="icon ni ni-arrow-left"></em></a>
                    </li>
                    <li class="nk-chat-head-user">
                        <div class="user-card">
                            <div class="user-avatar bg-{{ SuperM::getRandom() }}">
                                <span>{{ $loan->comments()->first()->user->nameThumb() }}</span>
                            </div>
                            <div class="user-info">
                                <div class="lead-text">{{  $loan->comments()->first()->user->getFullname() }}</div>
                                <div class="sub-text"><span class="d-none d-sm-inline mr-1">Initiated this comment session </span> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($loan->comments()->first()->created_at))->diffForHumans() }}</div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="nk-chat-head-tools">
                    <li><a target="_blank" href="/loans/{{ $loan->uuid() }}" class="btn btn-icon btn-trigger text-primary"><em class="icon ni ni-eye"></em></a></li>
                </ul>
            </div>
            <div class="nk-chat-panel" data-simplebar>
                @foreach($loan->comments as $single)
                    @if($single->user->id != $user->id)
                    <div class="chat is-you">
                        <div class="chat-avatar">
                            <div class="user-avatar bg-purple">
                                <span>{{ $single->user->nameThumb() }}</span>
                            </div>
                        </div>
                        <div class="chat-content">
                            <div class="chat-bubbles">
                                <div class="chat-bubble">
                                    <div class="chat-msg"> {!! $single->comments !!} </div>
                                </div>
                            </div>
                            <ul class="chat-meta">
                                <li>{{ $single->user->getFullname() }}</li>
                                <li>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($single->created_at))->diffForHumans() }}</li>
                            </ul>
                        </div>
                    </div>
                    @else
                    <div class="chat is-me">
                        <div class="chat-content">
                            <div class="chat-bubbles">
                                <div class="chat-bubble">
                                    <div class="chat-msg"> {!! $single->comments !!} </div>
                                </div>
                            </div>
                            <ul class="chat-meta">
                                <li>{{ $single->user->getFullname() }}</li>
                                <li>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($single->created_at))->diffForHumans() }}</li>
                            </ul>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="nk-chat-profile visible" data-simplebar>
                <div class="user-card user-card-s2 my-4">
                    <div class="user-avatar md bg-{{ SuperM::getRandom() }}">
                        @if($loan->user->my_details) <img src="{{ asset($loan->user->my_details->data()->identity->passport) }}"> @else <span>{{ $loan->user->nameThumb() }}</span>  @endif
                    </div>
                    <div class="user-info">
                        <h5>{{ $loan->user->getFullname() }}</h5>
                        <span class="sub-text">Applied {{ \Carbon\Carbon::createFromTimeStamp(strtotime($loan->created_at))->diffForHumans() }}</span>
                    </div>
                    <div class="user-card-menu dropdown">
                        <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="link-list-opt no-bdr">
                                <li><a href="/users/{{ $loan->user->uuid() }}" target="_blank"><em class="icon ni ni-eye"></em><span>View Profile</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection