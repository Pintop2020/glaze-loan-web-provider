@extends('layouts.user.app')

@section('title', __('Loan error'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="kyc-app wide-sm m-auto">
			<div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
				<div class="nk-block-head-content text-center">
					<h2 class="nk-block-title fw-normal">Oops!!!</h2>
					<div class="nk-block-des">
						<p>It seems you can't apply for a loan at the moment. </p>
					</div>
				</div>
			</div>
			<!-- .nk-block-head -->
			<div class="nk-block">
				<div class="card card-bordered">
					<div class="card-inner card-inner-lg">
						<div class="nk-kyc-app p-sm-2 text-center">
							<div class="nk-kyc-app-icon">
								<em class="icon ni ni-files"></em>
							</div>
							<div class="nk-kyc-app-text mx-auto">
								<p class="lead">You currently have a loan pending approval or an active loan in your profile.</p>
							</div>
							<div class="nk-kyc-app-action">
								<a href="" class="btn btn-lg btn-primary disabled">Try again later</a>
							</div>
						</div>
					</div>
				</div>
				<!-- .card -->
				<div class="text-center pt-4">
					<p>If you have any question, please contact our support team <a href="mailto:info@glazecredit.com">info@glazecredit.com</a></p>
				</div>
			</div>
			<!-- .nk-block -->
		</div>
		<!-- .kyc-app -->
	</div>
</div>
@endsection

@push('more-scripts')
@endpush