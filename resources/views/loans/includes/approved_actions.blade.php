<li>
    <div class="drodown">
        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
        <div class="dropdown-menu dropdown-menu-right">
            <ul class="link-list-opt no-bdr">
                <li><a href="/users/{{$loan->user->uuid()}}" target="_blank"><em class="icon ni ni-focus"></em><span>View Profile</span></a></li>
                <li><a href="/comments/create/{{ $loan->uuid() }}"><em class="icon ni ni-chat-circle"></em><span>Comment</span></a></li>
                <li><a href="/folders/{{$loan->folder->uuid() }}" target="_blank"><em class="icon ni ni-folder"></em><span>View Folder</span></a></li>
                <li><a href="/offers/{{ $loan->offer->uuid() }}" target="_blank"><em class="icon ni ni-files"></em><span>View Offer</span></a></li>
                <li><a href="/loans/{{ $loan->uuid() }}" target="_blank"><em class="icon ni ni-files"></em><span>View Loan</span></a></li>
                @if($user->roles()->first()->name == "transaction officer" && $loan->status->status == 'approved')
                    <li><a href="/payout/loans/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-check-round"></em><span>Disburse to wallet</span></a></li>
                @endif
            </ul>
        </div>
    </div>
</li>