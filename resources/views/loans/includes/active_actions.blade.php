<li>
    <div class="drodown">
        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
        <div class="dropdown-menu dropdown-menu-right">
            <ul class="link-list-opt no-bdr">
                <li><a href="/users/{{$loan->user->uuid()}}" target="_blank"><em class="icon ni ni-focus"></em><span>View Profile</span></a></li>
                <li><a href="/comments/create/{{ $loan->uuid() }}"><em class="icon ni ni-chat-circle"></em><span>Comment</span></a></li>
                <li><a href="/folders/{{$loan->folder->uuid() }}" target="_blank"><em class="icon ni ni-folder"></em><span>View Folder</span></a></li>
                <li><a href="/offers/{{ $loan->offer->uuid() }}" target="_blank"><em class="icon ni ni-files"></em><span>View Offer</span></a></li>
                <li><a href="/loans/{{ $loan->uuid() }}" target="_blank"><em class="icon ni ni-files"></em><span>View Loan</span></a></li>
                <li><a href="/repayments/update/user/{{ $loan->uuid() }}"><em class="icon ni ni-master-card"></em><span>Update Payments</span></a></li>
                @if($loan->defaults()->sum('amount') > 0)
                <li><a href="/defaulters/clear/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-stop-circle-fill"></em><span>Clear Defaults</span></a></li>
                @endif
            </ul>
        </div>
    </div>
</li>