<li>
    <div class="drodown">
        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
        <div class="dropdown-menu dropdown-menu-right">
            <ul class="link-list-opt no-bdr">
                <li><a href="/users/{{$loan->user->uuid()}}" target="_blank"><em class="icon ni ni-focus"></em><span>View Profile</span></a></li>
                @if($user->hasRole($loan->stages()->first()->name))
                    @if($loan->stages()->first()->name != 'marketer' && $loan->offer)
                    <li><a href="/loans/approve/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-check-round"></em><span>Approve</span></a></li>
                    @elseif($loan->stages()->first()->name == 'marketer')
                    <li><a href="/loans/approve/{{ $loan->uuid() }}"><em class="icon ni ni-check-round"></em><span>Approve</span></a></li>
                    @endif
                    <li><a href="/loans/decline/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-na"></em><span>Decline</span></a></li>
                    @if(!$loan->folder)
                    <li><a href="/folders/create/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-folder"></em><span>Create Folder</span></a></li>
                    @endif
                    @if(!$loan->credit_checks)
                    <li><a href="/credit_checks/create/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-ubuntu"></em><span>Credit check paid</span></a></li>
                    @endif
                @endif
                @if($user->hasRole('risk') && $loan->stages()->first()->name == 'risk' && !$loan->offer)
                	<li><a href="/offers/create/{{ $loan->uuid() }}" class="showup"><em class="icon ni ni-repeat"></em><span>Generate Offer</span></a></li>
                @elseif(!$user->hasRole('marketer') && $loan->offer)
                    <li><a href="/offers/{{ $loan->offer->uuid() }}/edit" class="showup"><em class="icon ni ni-repeat"></em><span>Edit Offer</span></a></li>
                @endif
                <li><a href="/comments/create/{{ $loan->uuid() }}" class="miAction"><em class="icon ni ni-chat-circle"></em><span>Comment</span></a></li>
                @if($loan->folder)
                <li><a href="/folders/{{$loan->folder->uuid() }}" target="_blank"><em class="icon ni ni-folder"></em><span>View Folder</span></a></li>
                @endif
                @if($loan->offer)
                <li><a href="/offers/{{ $loan->offer->uuid() }}" target="_blank"><em class="icon ni ni-files"></em><span>View Offer</span></a></li>
                @endif
                <li><a href="/loans/{{ $loan->uuid() }}" target="_blank"><em class="icon ni ni-files"></em><span>View Loan</span></a></li>
                @if($loan->stages()->first()->name != 'marketer' && $user->hasRole($loan->stages()->first()->name))
                <li><a class="miAction" href="/loans/return/back/{{ $loan->uuid() }}"><em class="icon ni ni-files"></em><span>Return Back</span></a></li>
                @endif
            </ul>
        </div>
    </div>
</li>