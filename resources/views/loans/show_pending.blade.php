@php
use App\Http\Controllers\Loan\LoanController;
use App\Http\Controllers\Offer\OfferController as Utils;
use App\Http\Controllers\GlobalMethods as SuperM;
$amount_requested = $loan->data()->amount;
@endphp
<!DOCTYPE html>
<html lang="zxx" class="js">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Viewing loan || Glaze Credit</title>
		<link rel="stylesheet" href="{{ asset('assets/css/dashlite.css') }}">
		<link id="skin-default" rel="stylesheet" href="{{ asset('assets/css/skins/theme-green.css') }}">
    	<meta name="csrf-token" content="{{ csrf_token() }}">
    	<style type="text/css">
    		.invest-field .form-control-amount { padding: 1rem 4rem 1.25rem 1.5rem; height: 3.75rem; font-size: 1.25rem; color: #8094ae; border-color: #dbdfea; }

			.invest-field .form-control-amount ~ .form-range-slider { position: absolute; left: 0; bottom: 0; right: 0; }

			.invest-field .form-control-amount ~ .form-range-slider.noUi-horizontal { height: 4px; border-radius: 0 0 4px 4px; }

			.invest-field .form-control-amount ~ .form-range-slider.noUi-target { background: rgba(219, 223, 234, 0.6); }

			.invest-field .form-control-amount ~ .form-range-slider .noUi-connects { border-radius: 0 0 4px 4px; }

			.invest-field:not(:last-child) { margin-bottom: 1.75rem; }

			.invest-amount-group { display: flex; flex-wrap: wrap; }

			.invest-amount-item { position: relative; flex-grow: 1; width: 33.33%; }

			.invest-amount-label { cursor: pointer; border-radius: 4px; border: 1px solid #dbdfea; background: #fff; font-size: 14px; text-align: center; line-height: 1.25rem; padding: 1rem; width: 100%; margin-bottom: 0; transition: all .3s; }

			.invest-amount-control { position: absolute; opacity: 0; height: 1px; width: 1px; }

			.invest-amount-control:checked ~ .invest-amount-label { border-color: #6576ff; background-color: #6576ff; color: #fff; }
			/*
			**********************
			*/
			.nk-iv-wg4 .lead-text { font-weight: 400; }

			.nk-iv-wg4-title { margin-bottom: 0.75rem; }

			.nk-iv-wg4-sub { padding: 1.25rem 1.5rem; }

			.nk-iv-wg4-sub:not(:last-child) { border-bottom: 1px solid #e5e9f2; }

			.card .nk-iv-wg4-sub:last-child { border-radius: 0 0 3px 3px; }

			.nk-iv-wg4-sub .btn { margin-top: 0.5rem; margin-bottom: 0.5rem; }

			.nk-iv-wg4-overview { display: flex; flex-wrap: wrap; }

			.nk-iv-wg4-overview li { width: 50%; }

			.nk-iv-wg4-list li { display: flex; justify-content: space-between; }

			.nk-iv-wg4-list li:not(:last-child) { margin-bottom: 0.25rem; }

			@media (max-width: 420px) { .nk-iv-wg4-overview li { width: 100%; } }

			.nk-iv-wg5 { display: inline-flex; flex-direction: column; margin-left: auto; margin-right: auto; }

			.nk-iv-wg5-head { margin-bottom: 1rem; }

			.nk-iv-wg5-title { margin-bottom: .25rem; }

			.nk-iv-wg5-subtitle { color: #8094ae; }

			.nk-iv-wg5-ck { margin-top: auto; position: relative; display: inline-block; }

			.nk-iv-wg5-ck-result { position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); text-align: center; }

			.nk-iv-wg5-ck-result .text-lead { font-size: 40px; font-weight: 300; color: #364a63; line-height: 1.3; }

			.nk-iv-wg5-ck-result .text-lead.sm { font-size: 30px; }

			.nk-iv-wg5-ck-result .text-sub { font-size: 12px; font-weight: 500; color: #8094ae; }

			.nk-iv-wg5-ck-minmax { display: flex; justify-content: space-between; color: #8094ae; font-size: 12px; }

			.nk-iv-wg3-group { display: flex; align-items: flex-end; flex-wrap: wrap; }

			.nk-iv-wg3-ck { position: relative; height: 45px; margin-bottom: .5rem; }

			.nk-iv-wg3-title { font-size: 1.125rem; color: #8094ae; margin-bottom: .5rem; font-weight: 400; }

			.nk-iv-wg3-title .icon { font-size: .875rem; }

			.nk-iv-wg3-sub { font-size: .875rem; color: #8094ae; margin-top: .125rem; }

			.nk-iv-wg3-sub-group { display: flex; flex-shrink: 0; }

			.nk-iv-wg3-sub .icon { font-size: .75rem; }

			.nk-iv-wg3-plus { position: absolute; }

			.nk-iv-wg3-plus .icon { font-size: 1rem; line-height: 1.5rem; }

			.nk-iv-wg3-plus ~ * { padding-left: 24px; }

			.nk-iv-wg3-amount .number { font-size: 1.25rem; font-weight: 700; color: #364a63; line-height: 1.5rem; white-space: nowrap; }

			.nk-iv-wg3-amount .number small { font-weight: 400; }

			.nk-iv-wg3-amount .number-sm { font-size: 1.15rem; color: #364a63; line-height: 1.5rem; white-space: nowrap; }

			.nk-iv-wg3-amount .number .number-up, .nk-iv-wg3-amount .number .number-down { font-size: 50%; }

			.nk-iv-wg3-amount .number .number-up .icon, .nk-iv-wg3-amount .number .number-down .icon { color: #526484; }

			.nk-iv-wg3-amount .number .number-up:before, .nk-iv-wg3-amount .number .number-down:before { font-family: "Nioicon"; }

			.nk-iv-wg3-amount .number .number-up { color: #1ee0ac; }

			.nk-iv-wg3-amount .number .number-up:before { content: ""; }

			.nk-iv-wg3-amount .number .number-down { color: #e85347; }

			.nk-iv-wg3-amount .number .number-down:before { content: ""; }

			.nk-iv-wg3-nav { display: flex; align-items: center; flex-wrap: wrap; margin: -.5rem -1.25rem; }

			.nk-iv-wg3-nav li a { padding: .5rem 1.25rem; display: flex; align-items: center; }

			.nk-iv-wg3-nav li a .icon { font-size: 1.125rem; width: 1.75rem; line-height: 1.25rem; }

			.nk-iv-wg3-nav li a span { font-size: 0.875rem; line-height: 1.25rem; }

			.nk-iv-wg3-list li { display: flex; justify-content: space-between; align-items: center; padding: .5rem 1.25rem; border-bottom: 1px solid #e5e9f2; }

			.nk-iv-wg3-list li .lead-text { font-size: 13px; font-weight: 400; }

			.nk-iv-wg3-list:last-child { padding: 0 0 1rem; }

			.nk-iv-wg3-list:last-child li:last-child { border-bottom: none; }

			@media (max-width: 991.98px) { .nk-iv-wg3-sub:first-child { min-width: 160px; } }

			@media (max-width: 420px) { .nk-iv-wg3-sub { width: 100%; }
			  .nk-iv-wg3-sub-group { flex-wrap: wrap; }
			  .nk-iv-wg3-plus { margin-bottom: .75rem; display: inline-block; }
			  .nk-iv-wg3-plus .icon { font-size: 1.5rem; line-height: 2.25rem; }
			  .nk-iv-wg3-plus ~ * { padding-left: 0; }
			  .nk-iv-wg3-plus + * { padding-top: 40px; } }

			@media (min-width: 576px) { .nk-iv-wg3-list li { padding: .5rem 1.75rem; } }

			@media (min-width: 680px) and (max-width: 991px) { .nk-iv-wg3-list { display: flex; flex-wrap: wrap; }
			  .nk-iv-wg3-list li { width: 50%; }
			  .nk-iv-wg3-list li:last-child { border-bottom: 1px solid #e5e9f2; }
			  .nk-iv-wg3-list:last-child li:nth-last-child(2) { border-bottom: none; } }

			@media (min-width: 992px) { .nk-iv-wg3-list { width: 33.333333%; float: left; }
			  .nk-iv-wg3-list li:last-child { border-bottom: none; } }

			@media (min-width: 1200px) { .nk-iv-wg3-amount .number { font-size: 1.75rem; line-height: 2.25rem; font-weight: 500; }
			  .nk-iv-wg3-amount .number-sm { font-size: 1.25rem; line-height: 2.25rem; }
			  .nk-iv-wg3-plus .icon { font-size: 1.25rem; line-height: 2.25rem; }
			  .nk-iv-wg3-plus ~ * { padding-left: 32px; } }

    	</style>
	</head>
	<body class="nk-body npc-invest bg-lighter ">
		<div class="nk-app-root">
			<div class="nk-wrap ">
				<!-- main header @e -->
				<!-- content @s -->
				<div class="nk-content nk-content-lg nk-content-fluid">
					<div class="container-xl wide-lg">
						<div class="nk-content-inner">
							<div class="nk-content-body">
								<div class="nk-block-head">
									<div class="nk-block-head-content">
										<div class="nk-block-head-sub"><a href="{{ url()->previous() }}l" class="text-soft back-to"><em class="icon ni ni-arrow-left"> </em><span>Back</span></a></div>
										<div class="nk-block-between-md g-4">
											<div class="nk-block-head-content">
												<h2 class="nk-block-title fw-normal">Viewing Loan</h2>
												<div class="nk-block-des">
													<p>INV-{{ str_pad($loan->id,6,0,STR_PAD_LEFT) }} 
														@if($loan->status)
															@if($loan->status->status == 'pending')
															<span class="badge badge-outline badge-warning">Pending</span>
															@elseif($loan->status->status == 'approved')
															<span class="badge badge-outline badge-primary">Approved</span>
															@elseif($loan->status->status == 'active')
															<span class="badge badge-outline badge-info">Active</span>
															@elseif($loan->status->status == 'declined')
															<span class="badge badge-outline badge-danger">Declined</span>
															@else
															<span class="badge badge-outline badge-success">Repaid</span>
															@endif
														@else
														<span class="badge badge-outline badge-warning">Pending</span>
														@endif
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- .nk-block-head -->
								<div class="nk-block">
									<div class="card card-bordered">
										<div class="card-inner">
											<div class="row gy-gs">
												<div class="col-md-6">
													<div class="nk-iv-wg3">
														<div class="nk-iv-wg3-group flex-lg-nowrap gx-4">
															<div class="nk-iv-wg3-sub">
																<div class="nk-iv-wg3-amount">
																	<div class="number">{{ number_format($amount_requested,2) }}</div>
																</div>
																<div class="nk-iv-wg3-subtitle">Amount Requested</div>
															</div>
															<div class="nk-iv-wg3-sub">
																<span class="nk-iv-wg3-plus text-soft">
																	<em class="icon ni ni-minus"></em>
																</span>
																<div class="nk-iv-wg3-amount">
																	<div class="number">Pending</div>
																</div>
																<div class="nk-iv-wg3-subtitle">Amount Granted</div>
															</div>
														</div>
													</div>
												</div>
												<!-- .col -->
												<div class="col-md-6 col-lg-4 offset-lg-2">
													<div class="nk-iv-wg3 pl-md-3">
														<div class="nk-iv-wg3-group flex-lg-nowrap gx-4">
															<div class="nk-iv-wg3-sub">
																<div class="nk-iv-wg3-amount">
																	<div class="number">-</div>
																</div>
																<div class="nk-iv-wg3-subtitle">Amount Disbursed</div>
															</div>
														</div>
													</div>
												</div>
												<!-- .col -->
											</div>
											<!-- .row -->
										</div>
										<div id="schemeDetails" class="nk-iv-scheme-details">
											<ul class="nk-iv-wg3-list">
												<li>
													<div class="sub-text">Term</div>
													<div class="lead-text">{{ $loan->data()->tenor }} months</div>
												</li>
												<li>
													<div class="sub-text">Term start at</div>
													<div class="lead-text">-</div>
												</li>
												<li>
													<div class="sub-text">Term end at</div>
													<div class="lead-text">-</div>
												</li>
												<li>
													<div class="sub-text">Loan interest</div>
													<div class="lead-text">{{ $loan->data()->interest }}</div>
												</li>
											</ul>
											<ul class="nk-iv-wg3-list">
												<li>
													<div class="sub-text">Ordered date</div>
													<div class="lead-text">{{ date('M d, Y h:i A', strtotime($loan->created_at)) }}</div>
												</li>
												<li>
													<div class="sub-text">Approved date</div>
													<div class="lead-text">Pending</div>
												</li>
												<li>
													<div class="sub-text">Payment method</div>
													<div class="lead-text">Glaze Wallet</div>
												</li>
											</ul>
											<!-- .nk-iv-wg3-list -->
											<ul class="nk-iv-wg3-list">
												<li>
													<div class="sub-text">Monthly Interest</div>
													<div class="lead-text">Pending Approval</div>
												</li>
												<li>
													<div class="sub-text">Monthly Principal</div>
													<div class="lead-text">Pending Approval</div>
												</li>
												<li>
													<div class="sub-text">Monthly Rentals</div>
													<div class="lead-text">Pending Approval</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="nk-block nk-block-lg">
									<div class="nk-block-head">
										<h5 class="nk-block-title">Processed By</h5>
									</div>
									<div class="card card-bordered">
										<table class="table table-iv-tnx">
											<thead class="thead-light">
												<tr>
													<th class="tb-col-date"><span class="overline-title">Staff</span></th>
						                            <th class="tb-col-time tb-col-end"><span class="overline-title">Role</span></th>
						                        </tr>
											</thead>
											<tbody>
												@foreach($loan->staffs as $staff)
												<tr>
													<td class="tb-col-date"><span class="sub-text">{{ $staff->getFullname() }}</span></td>
													<td class="tb-col-status tb-col-end"><span class="lead-text">{{ $staff->roles()->first()->name }}</span></td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="nk-footer nk-footer-fluid bg-lighter">
					<div class="container-xl wide-lg">
						<div class="nk-footer-wrap">
							<div class="nk-footer-copyright"> &copy; {{ date('Y') }} Glaze Credit Limited. </div>
							<div class="nk-footer-links">
								Powered by <a href="//pintoptechnologies.com" target="_blank">Pintop Technologies</a>
							</div>
						</div>
					</div>
				</div>
				<!-- footer @e -->
			</div>
			<!-- wrap @e -->
		</div>
		<!-- app-root @e -->
		<!-- JavaScript -->
		<script src="{{ asset('assets/js/bundle.js?ver=2.2.0') }}"></script>
		<script src="{{ asset('assets/js/scripts.js?ver=2.2.0') }}"></script>
		@stack('more-scripts')
		@if(session('error_bottom'))
			{!! session('error_bottom') !!}
		@endif
	</body>
</html>