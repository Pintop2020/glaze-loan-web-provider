@extends('layouts.user.app')

@section('title', __('My loans'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">My loans</h2>
					<div class="nk-block-des">
						<p class="lead">Here are the list of all your loans.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Tenor</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Credit Check</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Date Applied</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right"></th>
								</tr>
							</thead>
							<tbody>
								@foreach($loans as $loan)
									@php
									$decoded = json_decode($loan->details);
									@endphp
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid1">
											<label class="custom-control-label" for="uid1"></label>
										</div>
									</td>
									<td class="nk-tb-col tb-col-mb">
										<span class="tb-amount">{{ number_format($decoded->amount, 2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-mb">
										<span class="tb-amount">{{ $decoded->tenor }} <span class="currency">Months</span></span>
									</td>
									<td class="nk-tb-col tb-col-mb">
										<span class="tb-amount"> @if($loan->credit_checks) <em class="icon ni ni-check"></em> @else <em class="icon ni ni-cross"></em> @endif </span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ \Carbon\Carbon::parse($loan->created_at)->diffForHumans() }}</span>
									</td>
									<td class="nk-tb-col tb-col-md">{!! $loan->dstatus2() !!}</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
											<li>
												<div class="drodown">
													<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="link-list-opt no-bdr">
															@if(!$loan->credit_checks && !$loan->status)
															<li><a href="/credit-checks"><em class="icon ni ni-focus"></em><span>Pay for credit check</span></a></li>
															@endif
															@if($loan->status && $loan->status->status != 'declined')
																@if($loan->offer)
																<li><a href="/offers/{{ $loan->offer->uuid() }}"  target="_blank"><em class="icon ni ni-focus"></em><span>View Offer</span></a></li>
																	@if(!$loan->offer->signature && $loan->status)
																	<li><a href="/loans/offer/sign/{{ $loan->offer->uuid() }}" target="_blank"><em class="icon ni ni-file-check-fill"></em><span>Sign Offer</span></a></li>
																	@endif
																@endif
																<li><a href="/loans/{{ $loan->uuid() }}"><em class="icon ni ni-eye"></em><span>View Details</span></a></li>
															@endif
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
			<!-- nk-block -->
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection