@extends('layouts.user.app')

@section('title', __('Apply for a new loan'))

@section('theB')
<div class="nk-content nk-content-fluid"><div class="container-xl wide-lg">
@endsection

@section('content')
	<div class="nk-content-body">
		<div class="nk-block-head nk-block-head-lg">
			<div class="nk-block-head-content">
				<div class="nk-block-head-sub"><a href="{{ url()->previous() }}" class="back-to"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
				<div class="nk-block-head-content">
					<h2 class="nk-block-title fw-normal">Ready to get started?</h2>
				</div>
			</div>
		</div>
		<!-- nk-block-head -->
		<div class="nk-block invest-block">
			<form action="#" class="invest-form">
				<div class="row g-gs">
					<div class="col-lg-7">
						<div class="invest-field form-group">
							<div class="form-label-group">
								<label class="form-label">Choose or enter amount to borrow</label>
							</div>
							<div class="invest-amount-group g-2">
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-1" value="100000">
									<label class="invest-amount-label" for="iv-amount-1">₦ 100,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-2" value="150000">
									<label class="invest-amount-label" for="iv-amount-2">₦ 150,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-3" value="200000">
									<label class="invest-amount-label" for="iv-amount-3">₦ 200,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-4" value="500000">
									<label class="invest-amount-label" for="iv-amount-4">₦ 500,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-5" value="1000000">
									<label class="invest-amount-label" for="iv-amount-5">₦ 1,000,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-6" value="1500000">
									<label class="invest-amount-label" for="iv-amount-6">₦ 1,500,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-7" value="2000000">
									<label class="invest-amount-label" for="iv-amount-7">₦ 2,000,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-8" value="3000000">
									<label class="invest-amount-label" for="iv-amount-8">₦ 3,000,000</label>
								</div>
								<div class="invest-amount-item">
									<input type="radio" class="invest-amount-control" name="iv_amount" id="iv-amount-9" value="4000000">
									<label class="invest-amount-label" for="iv-amount-9">₦ 4,000,000</label>
								</div>
							</div>
						</div>
						<!-- .invest-field -->
						<div class="invest-field form-group">
							<div class="form-label-group">
								<label class="form-label">Or Enter Your Amount</label>
							</div>
							<div class="form-control-group">
								<div class="form-info">NGN</div>
								<input type="number" step="any" class="form-control form-control-amount form-control-lg" id="custom-amount" name="amount">
							</div>
							<div class="form-note pt-2">Note: Minimum amount of 50,000 NGN and upto 4,000,000 NGN</div>
						</div>
						<div class="invest-field form-group">
							<div class="form-label-group">
								<label class="form-label">Tenor</label>
							</div>
							<div class="form-control-group">
								<div class="form-info">Month</div>
								<select class="form-select form-control" name="tenor" data-search="on">
                                    <option selected="true" disabled="true">Choose Option</option>
                                    @for($i = 3; $i< 25; $i++)
                                    <option value="{{ $i }}">{{ $i }} months</option>
                                    @endfor
                                </select>
							</div>
						</div>
						<div class="invest-field form-group">
							<div class="form-label-group">
								<label class="form-label">Loan Type</label>
							</div>
							<div class="form-control-group">
								<div class="form-info">Type</div>
								<select class="form-select form-control" name="type">
                                    <option value="personal">Personal Loans</option>
                                    <option value="sme">SME Loans</option>
                                </select>
							</div>
						</div>
						<div class="invest-field form-group">
							<div class="form-label-group">
								<label class="form-label">Salary Pay Day</label>
							</div>
							<div class="form-control-group">
								<div class="form-info">Type</div>
								<input type="number" name="pay_date" value="25" class="form-control" required="">
							</div>
						</div>
						<div class="invest-field form-group">
							<div class="nk-kycfm-upload">
	                            <h6 class="title nk-kycfm-upload-title">Upload Employment Letter Here (Max file size: 2MB)</h6>
	                            <div class="row align-items-center">
	                                <div class="col-sm-12">
	                                    <div class="nk-kycfm-upload-box">
	                                        <div class="employment_letter">
	                                            <div class="dz-message" data-dz-message>
	                                                <span class="dz-message-text">Drag and drop file</span>
	                                                <span class="dz-message-or">or</span>
	                                                <button class="btn btn-primary">SELECT</button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="invest-field form-group">
							<div class="nk-kycfm-upload">
	                            <h6 class="title nk-kycfm-upload-title">Upload Utility Bill Here (Max file size: 2MB)</h6>
	                            <div class="row align-items-center">
	                                <div class="col-sm-12">
	                                    <div class="nk-kycfm-upload-box">
	                                        <div class="utility_bill">
	                                            <div class="dz-message" data-dz-message>
	                                                <span class="dz-message-text">Drag and drop file</span>
	                                                <span class="dz-message-or">or</span>
	                                                <button class="btn btn-primary">SELECT</button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="invest-field form-group">
							<div class="nk-kycfm-upload">
	                            <h6 class="title nk-kycfm-upload-title">Upload Statement of Account Here (Max file size: 2MB)</h6>
	                            <div class="row align-items-center">
	                                <div class="col-sm-12">
	                                    <div class="nk-kycfm-upload-box">
	                                        <div class="statements">
	                                            <div class="dz-message" data-dz-message>
	                                                <span class="dz-message-text">Drag and drop file</span>
	                                                <span class="dz-message-or">or</span>
	                                                <button class="btn btn-primary">SELECT</button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
						<div class="invest-field form-group">
							<div class="custom-control custom-control-xs custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="checkbox" name="agreement">
								<label class="custom-control-label" for="checkbox">I agree the <a href="#">terms and &amp; conditions.</a></label>
							</div>
						</div>
						<!-- .invest-field -->
					</div>
					<!-- .col -->
					<div class="col-xl-4 col-lg-5 offset-xl-1">
						<div class="card card-bordered ml-lg-4 ml-xl-0">
							<div class="nk-iv-wg4">
								<div class="nk-iv-wg4-sub">
									<h6 class="nk-iv-wg4-title title">Your Loan Details</h6>
									<ul class="nk-iv-wg4-overview g-2">
										<li>
											<div class="sub-text">Amount applied</div>
											<div class="lead-text amount_disp"></div>
										</li>
										<li>
											<div class="sub-text">Tenor</div>
											<div class="lead-text tenor_disp"></div>
										</li>
										<li>
											<div class="sub-text">Monthly Payment</div>
											<div class="lead-text payment_disp"></div>
										</li>
										<li>
											<div class="sub-text">Interest(Flat) 4%</div>
											<div class="lead-text interest_disp"></div>
										</li>
										<li>
											<div class="sub-text">Processing Fee <small>(Deducted on Disbursement)</small></div>
											<div class="lead-text processing_fee"></div>
										</li>
										<li>
											<div class="sub-text">Management Fee</div>
											<div class="lead-text manage_fee"></div>
										</li>
									</ul>
								</div>
								<!-- .nk-iv-wg4-sub -->
								<div class="nk-iv-wg4-sub">
									<ul class="nk-iv-wg4-list">
										<li>
											<div class="sub-text">Payment Method</div>
											<div class="lead-text">Bank Transfer</div>
										</li>
									</ul>
								</div>
								<!-- .nk-iv-wg4-sub -->
								<div class="nk-iv-wg4-sub">
									<ul class="nk-iv-wg4-list">
										<li>
											<div class="sub-text">Amount to receive</div>
											<div class="lead-text receive_disp"></div>
										</li>
									</ul>
								</div>
								<!-- .nk-iv-wg4-sub -->
								<div class="nk-iv-wg4-sub">
									<ul class="nk-iv-wg4-list">
										<li>
											<div class="lead-text">Amount Return</div>
											<div class="caption-text text-primary return_disp"></div>
										</li>
									</ul>
								</div>
								<!-- .nk-iv-wg4-sub -->
								<div class="nk-iv-wg4-sub text-center bg-lighter">
									<a href="" class="btn btn-lg btn-primary ddu">Confirm &amp; proceed</a>
								</div>
								<!-- .nk-iv-wg4-sub -->
							</div>
							<!-- .nk-iv-wg4 -->
						</div>
						<!-- .card -->
					</div>
					<!-- .col -->
				</div>
				<!-- .row -->
			</form>
		</div>
		<!-- .nk-block -->
	</div>
</div>
<div class="modal fade" tabindex="-1" id="invest-plan">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-body modal-body-md text-center">
				<div class="nk-modal">
					<h4 class="nk-modal-title">Confirm Your Payment</h4>
					<div class="nk-modal-text">
						<p>To confirm your payment of you are applying for this loan. Please enter your Transaction Pin code in order complete the transaction.</p>
					</div>
					<div class="nk-modal-form">
						<div class="form-group">
							<input type="password" name="pin" class="form-control form-control-password-big text-center">
						</div>
					</div>
					<div class="nk-modal-action">
						<a href="#" class="btn btn-lg btn-mw btn-primary dds">Confirm Payment</a>
						<div class="sub-text sub-text-alt mt-3 mb-4">This transaction will appear on your wallet statement as Loan.</div>
						<a href="#" class="link link-soft" data-dismiss="modal">Cancel and return</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- -->
<div class="modal fade" tabindex="-1" id="confirm-invest">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-lg text-center">
                <div class="nk-modal">
                    <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                    <h4 class="nk-modal-title">Successfully applied for loan!</h4>
                    <div class="nk-modal-text">
                        <p class="sub-text">You have successfully applied for a loan. Please check your mail for confirmation. If you have not paid for Credit Bureau check, please click the button below to pay now</p>
                    </div>
                    <div class="nk-modal-action-lg">
                        <ul class="btn-group flex-wrap justify-center g-4">
                            <li><a href="/credit-checks" class="btn btn-lg btn-mw btn-primary">Pay Now</a></li>
                            <li><a href="/loans" class="btn btn-lg btn-mw btn-dim btn-primary"><em class="icon ni ni-reports"></em><span>Already transfered</span></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .modal-body -->
            <div class="modal-footer bg-lighter">
                <div class="text-center w-100">
                    <p>Earn up to NGN 500,000 for each friend your refer! <a href="{{ route('home') }}">Invite friends</a></p>
                </div>
            </div>
        </div><!-- .modal-content -->
    </div><!-- .modla-dialog -->
@endsection

@push('more-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/loader.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/loan.css') }}">
@endpush

@push('more-scripts')
<script src="{{ asset('assets/js/custom/apply_loan.js') }}"></script>
@endpush