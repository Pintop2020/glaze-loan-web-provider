@php
use App\Http\Controllers\GlobalMethods as SuperM;
$disbursed = SuperM::getDisburseAmount($loan->offer);
$amount_requested = json_decode($loan->details)->amount;
$amouunt_approved = json_decode($loan->offer->offer_details)->amount;
$percent = ($amouunt_approved/$amount_requested)*100;
//
$offer = $loan->offer;
$user = $loan->user->my_details->data();
$offerDets = $offer->data();
$processing = $offerDets->amount  * ($offerDets->processing/100);
$interest_monthly = $offerDets->amount * ($offerDets->interest/100);
$moratorium = 0;
$montlyRepay = $offerDets->amount / $offerDets->tenor;
$principal_repay = $offerDets->amount / $offerDets->tenor;
if($offerDets->moratorium > 0){
    $moratorium = ($offerDets->moratorium/30)*$interest_monthly;
    $mdivi = $moratorium/$offerDets->tenor;
    $montlyRepay += $mdivi;;
}
$deduct = $processing + $offerDets->outstanding;
$todisburse = $offerDets->amount - $deduct;
$totalRepay = $montlyRepay + $interest_monthly;
$ddifm = '';
$dpro = $offerDets->moratorium*1;
$dten = $offerDets->tenor*1;
$deductMonth = 0;
if($offerDets->payment_starts == "this")
    $deductMonth = 1;
if($dpro > 0){
    $datetime = new DateTime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at));
    $datetime->modify('+'.$dpro.' day');
    $ddifm = $datetime->format('Y-m-d H:i:s');
}
// 
$perPaid = $loan->repayments()->sum('amount') > 0 ? ($loan->repayments()->sum('amount') / ($totalRepay*$offerDets->tenor))/100 : 0;
@endphp

@extends('layouts.user.app')

@section('title', __('View loan'))

@section('theB')
<div class="nk-content nk-content-fluid"><div class="container-xl wide-lg">
@endsection

@section('content')
	<div class="nk-content-body">
		<div class="nk-block-head">
			<div class="nk-block-head-content">
				<div class="nk-block-head-sub"><a href="{{ url()->previous() }}" class="text-soft back-to"><em class="icon ni ni-arrow-left"> </em><span>Back</span></a></div>
				<div class="nk-block-between-md g-4">
					<div class="nk-block-head-content">
						<h2 class="nk-block-title fw-normal">Viewing Loan</h2>
						<div class="nk-block-des">
							<p>INV-{{ str_pad($loan->id,6,0,STR_PAD_LEFT) }} 
								@if($loan->status)
									@if($loan->status->status == 'pending')
									<span class="badge badge-outline badge-warning">Pending</span>
									@elseif($loan->status->status == 'approved')
									<span class="badge badge-outline badge-primary">Approved</span>
									@elseif($loan->status->status == 'active')
									<span class="badge badge-outline badge-info">Active</span>
									@elseif($loan->status->status == 'declined')
									<span class="badge badge-outline badge-danger">Declined</span>
									@else
									<span class="badge badge-outline badge-success">Repaid</span>
									@endif
								@else
								<span class="badge badge-outline badge-warning">Pending</span>
								@endif
							</p>
						</div>
					</div>
					<div class="nk-block-head-content">
						<ul class="nk-block-tools gx-3">
							@if(!$loan->status)
							<li class="order-md-last"><a href="#" class="btn btn-danger"><em class="icon ni ni-cross"></em> <span>Cancel this request</span> </a></li>
							@endif
							@if($loan->status && $loan->status->status == 'active')
							<li class="order-md-last"><a href="/clear-loan/{{ $loan->uuid() }}" class="btn btn-success clear-loan"><em class="icon ni ni-cross"></em> <span>Make Repaymnet</span> </a></li>
							@endif
							<li><a href="{{ url()->current() }}" class="btn btn-icon btn-light"><em class="icon ni ni-reload"></em></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- .nk-block-head -->
		<div class="nk-block">
			<div class="card card-bordered">
				<div class="card-inner">
					<div class="row gy-gs">
						<div class="col-md-6">
							<div class="nk-iv-wg3">
								<div class="nk-iv-wg3-group flex-lg-nowrap gx-4">
									<div class="nk-iv-wg3-sub">
										<div class="nk-iv-wg3-amount">
											<div class="number">{{ number_format($amount_requested,2) }}</div>
										</div>
										<div class="nk-iv-wg3-subtitle">Amount Requested</div>
									</div>
									<div class="nk-iv-wg3-sub">
										<span class="nk-iv-wg3-plus text-soft">
											<em class="icon ni ni-minus"></em>
										</span>
										<div class="nk-iv-wg3-amount">
											<div class="number">{{ number_format($amouunt_approved,2) }} 
												@if($percent < 100)
												<span class="number-down">
												@else
												<span class="number-up">
												@endif
												{{ round($percent,2 ) }} %</span></div>
										</div>
										<div class="nk-iv-wg3-subtitle">Amount Granted</div>
									</div>
								</div>
							</div>
						</div>
						<!-- .col -->
						<div class="col-md-6 col-lg-4 offset-lg-2">
							<div class="nk-iv-wg3 pl-md-3">
								<div class="nk-iv-wg3-group flex-lg-nowrap gx-4">
									<div class="nk-iv-wg3-sub">
										<div class="nk-iv-wg3-amount">
											<div class="number">{{ number_format($disbursed,2) }} <span class="number-down">{{ number_format($amouunt_approved-$disbursed,2) }} <em class="icon ni ni-info-fill" data-toggle="tooltip" data-placement="right" title="Processing fee / Upfront charge"></em></span></div>
										</div>
										<div class="nk-iv-wg3-subtitle">Amount Disbursed</div>
									</div>
								</div>
							</div>
						</div>
						<!-- .col -->
					</div>
					<!-- .row -->
				</div>
				<div id="schemeDetails" class="nk-iv-scheme-details">
					<ul class="nk-iv-wg3-list">
						<li>
							<div class="sub-text">Term</div>
							<div class="lead-text">{{ json_decode($loan->offer->offer_details)->tenor }} months</div>
						</li>
						<li>
							<div class="sub-text">Term start at</div>
							<div class="lead-text">{{ date('M d, Y h:i A', strtotime($loan->offer->created_at)) }}</div>
						</li>
						<li>
							<div class="sub-text">Term end at</div>
							<div class="lead-text">
								@if($dpro > 0 && $dten < 2)
                    			{{ date('M d, Y h:i A', strtotime($ddifm)) }}
                    			@else
                    		<td>{{ date('M ', strtotime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at))) }} {{ SuperM::getRepayDate($offerDets->pay_date, SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at)) }}, {{ date('Y', strtotime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at))) }} {{ date('h:i A', strtotime(SuperM::getTenorLastDay($offerDets->tenor-$deductMonth, $offer->updated_at))) }}</td>
                    @endif</div>
						</li>
						<li>
							<div class="sub-text">Loan interest</div>
							<div class="lead-text">{{ $offerDets->interest }}</div>
						</li>
					</ul>
					<!-- .nk-iv-wg3-list -->
					<ul class="nk-iv-wg3-list">
						<li>
							<div class="sub-text">Ordered date</div>
							<div class="lead-text">{{ date('M d, Y h:i A', strtotime($loan->created_at)) }}</div>
						</li>
						<li>
							<div class="sub-text">Approved date</div>
							<div class="lead-text">{{ date('M d, Y h:i A', strtotime($loan->status->created_at)) }}</div>
						</li>
						<li>
							<div class="sub-text">Payment method</div>
							<div class="lead-text">Glaze Wallet</div>
						</li>
						<li>
							<div class="sub-text">Paid <small>(fee included)</small></div>
							<div class="lead-text"><span class="currency currency-usd">NGN</span> {{ number_format($loan->repayments()->sum('amount'),2) }}</div>
						</li>
					</ul>
					<!-- .nk-iv-wg3-list -->
					<ul class="nk-iv-wg3-list">
						<li>
							<div class="sub-text">Monthly Interest</div>
							<div class="lead-text"><span class="currency currency-usd">NGN</span> {{ number_format($interest_monthly,2) }}</div>
						</li>
						<li>
							<div class="sub-text">Monthly Principal</div>
							<div class="lead-text"><span class="currency currency-usd">NGN</span> {{ number_format($principal_repay,2) }}</div>
						</li>
						<li>
							<div class="sub-text">Monthly Rentals</div>
							<div class="lead-text"><span class="currency currency-usd">NGN</span> {{ number_format($totalRepay,2) }}</div>
						</li>
						<li>
							<div class="sub-text">Loan Balance</div>
							<div class="lead-text"><span class="currency currency-usd">NGN</span> {{ number_format(($totalRepay*$offerDets->tenor)-$loan->repayments()->sum('amount'),2) }}</div>
						</li>
					</ul>
					<!-- .nk-iv-wg3-list -->
				</div>
				<!-- .nk-iv-scheme-details -->
			</div>
		</div>
		<div class="nk-block nk-block-lg">
			<div class="nk-block-head">
				<h5 class="nk-block-title">Due Payments</h5>
			</div>
			<div class="card card-preview">
				<div class="card-inner">
					<table class="nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
						<thead>
							<tr class="nk-tb-item nk-tb-head">
								<th class="nk-tb-col tb-col-mb"><span class="sub-text">Date Due</span></th>
								<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
								<th class="nk-tb-col tb-col-md"><span class="sub-text">Month</span></th>
								<th class="nk-tb-col tb-col-lg"><span class="sub-text">Defaults</span></th>
								<th class="nk-tb-col nk-tb-col-tools text-right"></th>
							</tr>
						</thead>
						<tbody>
							@if($repayments)
							<tr class="nk-tb-item">
								<td class="nk-tb-col tb-col-mb">
									<span>Since {{ $repayments['date_due'][0] }}</span>
								</td>
								<td class="nk-tb-col tb-col-mb" data-order="{{ $repayments['amount_due'] }}">
									<span class="tb-amount">{{ number_format($repayments['amount_due'], 2) }} <span class="currency">NGN</span></span>
								</td>
								<td class="nk-tb-col tb-col-md">
									<span>
									Since {{ SuperM::ordinal($repayments['count'][0]) }} month
									</span>
								</td>
								<td class="nk-tb-col tb-col-lg">
									<span class="tb-amount">{{ number_format($repayments['defaults'],2) }} <span class="currency">NGN</span></span>
								</td>
								<td class="nk-tb-col nk-tb-col-tools">
									<ul class="nk-tb-actions gx-1">
										<li>
											<div class="drodown">
												<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="link-list-opt no-bdr">
														<li><a href="/repayments/from-wallet" class="from-wallet"><em class="icon ni ni-wallet-out"></em><span>Pay From Wallet</span></a></li>
														<li><a href="/repayments/from-card" class="from-card"><em class="icon ni ni-master-card"></em><span>Pay With Card</span></a></li>
													</ul>
												</div>
											</div>
										</li>
									</ul>
								</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" tabindex="-1" id="the-confirm">
		<div class="modal-dialog modal-dialog-centered modal-md">
			<div class="modal-content">
				<div class="modal-body modal-body-md text-center">
					<div class="nk-modal">
						<h4 class="nk-modal-title">Confirm Your Payment</h4>
						<div class="nk-modal-text">
							<p>To confirm this action is being performed by you, please enter your Transaction Pin code in order complete the transaction.</p>
						</div>
						<div class="nk-modal-form">
							<div class="form-group">
								<input type="password" name="pin" class="form-control form-control-password-big text-center">
							</div>
						</div>
						<div class="nk-modal-action">
							<a href="#" class="btn btn-lg btn-mw btn-primary dds">Confirm Payment</a>
							<div class="sub-text sub-text-alt mt-3 mb-4">This transaction will appear on your wallet statement as Repayment.</div>
							<a href="#" class="link link-soft" data-dismiss="modal">Cancel and return</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>    
@endsection

@push('more-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/loan.css') }}">
@endpush

@push('more-scripts')
<script>
	$(function() {
    $('.from-wallet').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            //timer: 2000,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                            window.location.replace(url);
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
    // from card
    //
    //
    $('.from-card').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                            $.ajax({
                                type: "GET",
                                url: url,
                                dataType: 'json',
                                success: function(d) {
                                    if (d.status) {
                                        window.location.replace(d.data.authorization_url);
                                    } else {
                                        Swal.fire("Oops!", d.message, "error");
                                    }
                                }
                            });
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
    ////////////////////////////
    //////////////////////////////////////////
    ///////////////////////////////////////////////////
    $('.clear-loan').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                        	window.location.href = url;
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
});
</script>
@endpush