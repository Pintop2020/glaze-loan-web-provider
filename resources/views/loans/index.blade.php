@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('Loans'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All {{ strtolower($type) }} loans</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all {{ strtolower($type) }} loans</p>
					</div>
				</div>
			</div>
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Tenor</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Desk</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
									@if($type == 'declined')
									<th class="nk-tb-col tb-col-lg"><span>Declined By</span></th>
									@else
									<th class="nk-tb-col nk-tb-col-tools text-right">
									</th>
									@endif
								</tr>
							</thead>
							<tbody>
								@foreach($entities as $key => $loan)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $loan->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($loan->user->my_details) <img src="{{ asset('storage/'.$loan->user->my_details->data()->identity->passport) }}"> @else <span>{{ $loan->user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $loan->user->getFullname() }} @if(Cache::has('user-is-online-' . $loan->user->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>{{ $loan->user->id() }}</span>
											</div>
										</div>
									</td>
									<td class="nk-tb-col tb-col-mb" data-order="{{ number_format($loan->data()->amount,2) }}">
										<span class="tb-amount">{{ number_format($loan->data()->amount,2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ $loan->data()->tenor }} months</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ strtoupper($loan->stages()->first()->name) }}</span>
									</td>
									<td class="nk-tb-col tb-col-lg">{!! $loan->dstatus() !!}</td>
									@if($type == 'declined')
									<td class="nk-tb-col tb-col-lg">
										<div class="user-card">
                                            <div class="user-avatar user-avatar-xs bg-orange-dim">
                                                <span>{{ $loan->staffs()->latest()->first()->nameThumb() }}</span>
                                            </div>
                                            <div class="user-name">
                                                <span class="tb-lead">{{ $loan->staffs()->latest()->first()->getFullname() }}</span>
                                            </div>
                                        </div>
									</td>
									@else
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
											@if($type == 'active')
                                            @include('loans.includes.active_actions')
                                            @elseif($type == 'approved')
                                            @include('loans.includes.approved_actions')
                                            @elseif($type == 'pending')
                                            @include('loans.includes.pending_actions')
                                            @elseif($type == 'repaid')
                                            @include('loans.includes.repaid_actions')
                                            @endif
										</ul>
									</td>
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection