@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('Active Loans'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All Active loans</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all users and their profile details.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Tenor</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Desk</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right">
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach($loans as $key => $loan)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $loan->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($loan->user->my_details) <img src="{{ asset($loan->user->my_details->full_data()->identity->passport) }}"> @else <span>{{ $loan->user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $loan->user->getFullname() }} @if(Cache::has('user-is-online-' . $loan->user->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>{{ $loan->user->id() }}</span>
											</div>
										</div>
									</td>
									<td class="nk-tb-col tb-col-mb" data-order="{{ number_format(json_decode($loan->details)->amount,2) }}">
										<span class="tb-amount">{{ number_format(json_decode($loan->details)->amount,2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ json_decode($loan->details)->tenor }} months</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>Transaction Officer</span>
									</td>
									<td class="nk-tb-col tb-col-lg">
										<ul class="list-status">
											<li><em class="icon text-success ni ni-check-circle"></em> <span>{{ $loan->status->status }}</span></li>
										</ul>
									</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
                                            <li>
                                                <div class="drodown">
                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="link-list-opt no-bdr">
                                                            <li><a href="{{ route('admin.users.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->user->id)) }}" target="_blank"><em class="icon ni ni-focus"></em><span>View Profile</span></a></li>
                                                            <li><a href="{{ route('admin.loans.comments', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-chat-circle"></em><span>Comment</span></a></li>
                                                            <li><a href="{{ route('admin.folder.filemanager.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->folder->id)) }}" target="_blank"><em class="icon ni ni-folder"></em><span>View Folder</span></a></li>
                                                            <li><a href="{{ route('admin.offers.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}"><em class="icon ni ni-files"></em><span>View Offer</span></a></li>
                                                            <li><a href="{{ route('admin.loans.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" target="_blank"><em class="icon ni ni-files"></em><span>View Loans</span></a></li>
                                                            <li><a href="{{ route('admin.loans.payments.update', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" target="_blank"><em class="icon ni ni-master-card"></em><span>Update Payments</span></a></li>
                                                            @if($loan->defaults()->sum('amount') > 0)
                                                            <li><a href="{{ route('admin.loans.clear', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-stop-circle-fill"></em><span>Clear Defaults</span></a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
										</ul>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection