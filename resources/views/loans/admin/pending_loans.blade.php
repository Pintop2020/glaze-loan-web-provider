@extends('layouts.admin.app')

@section('title', __('Pending Loans'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All Pending loans</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all users and their profile details.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Tenor</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Desk</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right">
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach($loans as $key => $loan)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $loan->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($loan->user->my_details) <img src="{{ asset($loan->user->my_details->full_data()->identity->passport) }}"> @else <span>{{ $loan->user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $loan->user->getFullname() }} @if(Cache::has('user-is-online-' . $loan->user->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>GID{{ str_pad($loan->user->id,6,0,STR_PAD_LEFT) }}</span>
											</div>
										</div>
									</td>
									<td class="nk-tb-col tb-col-mb" data-order="{{ number_format($loan->full_data()->amount,2) }}">
										<span class="tb-amount">{{ number_format($loan->full_data()->amount,2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ $loan->full_data()->tenor }} months</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										@if($loan->stages()->first())
										<span>{{ $loan->stages()->first()->name }}</span>
										@endif
									</td>
									<td class="nk-tb-col tb-col-lg">
										<ul class="list-status">
											<li><em class="icon text-warning ni ni-check-circle"></em> <span>Pending</span></li>
										</ul>
									</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
											@if($user->roles()->first()->id == $loan->stages()->first()->id)
											<li class="nk-tb-action-hidden">
                                                <a href="{{ route('admin.users.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->user->id)) }}" target="_blank" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="View User">
                                                    <em class="icon ni ni-eye-fill"></em>
                                                </a>
                                            </li>
                                            <li class="nk-tb-action-hidden">
                                                <a href="{{ route('admin.loans.approve', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="btn btn-trigger btn-icon miAction" data-toggle="tooltip" data-placement="top" title="Approve">
                                                    <em class="icon ni ni-check-fill-c"></em>
                                                </a>
                                            </li>
                                            <li class="nk-tb-action-hidden">
                                                <a href="{{ route('admin.loans.decline', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="btn btn-trigger btn-icon miAction" data-toggle="tooltip" data-placement="top" title="Decline">
                                                    <em class="icon ni ni-cross-fill-c"></em>
                                                </a>
                                            </li>
                                            @endif
                                            <li>
                                                <div class="drodown">
                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="link-list-opt no-bdr">
                                                            <li><a href="{{ route('admin.users.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->user->id)) }}" target="_blank"><em class="icon ni ni-focus"></em><span>View Profile</span></a></li>
                                                            @if($user->roles()->first()->id == $loan->stages()->first()->id)
	                                                            <li><a href="{{ route('admin.loans.approve', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-check-round"></em><span>Approve</span></a></li>
	                                                            <li><a href="{{ route('admin.loans.decline', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-na"></em><span>Decline</span></a></li>
	                                                            @if(!$loan->folder)
	                                                            <li><a href="{{ route('admin.loans.folder', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-folder"></em><span>Create Folder</span></a></li>
	                                                            @endif
                                                            @endif
                                                            @if($user->roles()->first()->id == 3 && $loan->stages()->first()->id > 2 && !$loan->offer)
                                                            	<li><a href="{{ route('admin.loans.offer', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="showup"><em class="icon ni ni-repeat"></em><span>Generate Offer</span></a></li>
                                                            @endif
                                                            <li><a href="{{ route('admin.loans.comments', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-chat-circle"></em><span>Comment</span></a></li>
                                                            @if($loan->folder)
                                                            <li><a href="{{ route('admin.folder.filemanager.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->folder->id)) }}" target="_blank"><em class="icon ni ni-folder"></em><span>View Folder</span></a></li>
                                                            @endif
                                                            @if($loan->offer)
                                                            <li><a href="{{ route('admin.offers.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" target="_blank"><em class="icon ni ni-files"></em><span>View Offer</span></a></li>
                                                            @endif
                                                            <li><a href="{{ route('admin.loans.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" target="_blank"><em class="icon ni ni-files"></em><span>View Loans</span></a></li>
                                                            @if($loan->stages()->first()->id > 2)
                                                            <li><a href="{{ route('admin.loans.return', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}"><em class="icon ni ni-files"></em><span>Return Back</span></a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
										</ul>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
<!--<div class="modal fade" tabindex="-1" role="dialog" id="file-comment">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
			<div class="modal-body modal-body-md">
				<div class="nk-upload-form">
					<h5 class="title mb-3">Upload File</h5>
					<div class="upload-zone small bg-lighter">
						<div class="dz-message" data-dz-message>
							<span class="dz-message-text"><span>Drag and drop</span> file here or <span>browse</span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>-->
@endsection

@push('more-scripts')
<script>
	$(function(){
		$('.showup').click(function(e){
			e.preventDefault();
			var loader = $('.loading');
			var rreturn = $('.return');
        	var dhref = $(this).attr("href");
        	loader.show();
        	$.ajax({
                type: "GET",
                url: dhref,
                success: function(d) {
                	loader.hide();
                	rreturn.html(d);
                },
            });
		});
	});
</script>
@endpush