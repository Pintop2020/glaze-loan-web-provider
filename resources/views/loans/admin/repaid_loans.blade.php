@extends('layouts.admin.app')

@section('title', __('Repaid Loans'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All Repaid loans</h2>
					<div class="nk-block-des">
						<p class="lead"></p>
					</div>
				</div>
			</div>
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Tenor</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Desk</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right">
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach($loans as $key => $loan)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $loan->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($loan->user->my_details) <img src="{{ asset($loan->user->my_details->full_data()->identity->passport) }}"> @else <span>{{ $loan->user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $loan->user->getFullname() }} @if(Cache::has('user-is-online-' . $loan->user->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>GID{{ str_pad($loan->user->id,6,0,STR_PAD_LEFT) }}</span>
											</div>
										</div>
									</td>
									<td class="nk-tb-col tb-col-mb" data-order="{{ number_format($loan->full_data()->amount,2) }}">
										<span class="tb-amount">{{ number_format($loan->full_data()->amount,2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ $loan->full_data()->tenor }} months</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										@if($loan->stages()->first())
										<span>{{ $loan->stages()->first()->name }}</span>
										@endif
									</td>
									<td class="nk-tb-col tb-col-lg">
										<ul class="list-status">
											<li><em class="icon text-success ni ni-check-circle"></em> <span>Repaid</span></li>
										</ul>
									</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
                                            <li>
                                                <div class="drodown">
                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="link-list-opt no-bdr">
                                                            <li><a href="{{ route('admin.users.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->user->id)) }}" target="_blank"><em class="icon ni ni-focus"></em><span>View Profile</span></a></li>
                                                            <li><a href="{{ route('admin.loans.comments', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-chat-circle"></em><span>Comment</span></a></li>
                                                            <li><a href="{{ route('admin.folder.filemanager.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->folder->id)) }}" target="_blank"><em class="icon ni ni-folder"></em><span>View Folder</span></a></li>
                                                            <li><a href="{{ route('admin.offers.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}"><em class="icon ni ni-files"></em><span>View Offer</span></a></li>
                                                            @if($user->roles()->first()->id == 4 && $loan->status->status == 'active')
	                                                            <li><a href="{{ route('admin.loans.update', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-check-round"></em><span>Update Payment</span></a></li>
	                                                            <li><a href="{{ route('admin.loans.debit', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" class="miAction"><em class="icon ni ni-master-card"></em><span>Debit User</span></a></li>
                                                            @endif
                                                            <li><a href="{{ route('admin.loans.view', \Illuminate\Support\Facades\Crypt::encryptString($loan->id)) }}" target="_blank"><em class="icon ni ni-files"></em><span>View Loans</span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
										</ul>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection