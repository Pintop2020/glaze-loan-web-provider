@extends('layouts.user.app')

@section('title', __('Create Pin'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-inner">
    <div class="nk-content-body">
        <form>
            <div class="kyc-app wide-sm m-auto">
                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                    <div class="nk-block-head-content text-center">
                        <h2 class="nk-block-title fw-normal">Setup your pin</h2>
                    </div>
                </div>
                <!-- nk-block -->
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="nk-kycfm">
                            <div class="nk-kycfm-head">
                                <div class="nk-kycfm-count">01</div>
                                <div class="nk-kycfm-title">
                                    <h5 class="title">Create Pin</h5>
                                    <p class="sub-title">This can not be changed later.</p>
                                </div>
                            </div>
                            <!-- nk-kycfm-head -->
                            <div class="nk-kycfm-content">
                                <div class="nk-kycfm-note">
                                    <em class="icon ni ni-info-fill" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></em>
                                    <p>Please type carefully and fill out the form with your personal details. You can not edit these details later.</p>
                                </div>
                                <div class="row g-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-label-group">
                                                <label class="form-label">Pin <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="form-control-group">
                                                <input type="password" class="form-control form-control-lg" name="pin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-label-group">
                                                <label class="form-label">Confirm Pin <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="form-control-group">
                                                <input type="password" name="pin_confirmation" class="form-control form-control-lg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- nk-kycfm-content -->
                            <div class="nk-kycfm-footer">
                                <div class="nk-kycfm-action pt-2">
                                    <button type="submit" class="btn btn-lg btn-primary subm">Submit </button>
                                </div>
                            </div>
                            <!-- nk-kycfm-footer -->
                        </div>
                        <!-- nk-kycfm -->
                    </div>
                    <!-- .card -->
                </div>
                <!-- nk-block -->
            </div>
            <!-- .kyc-app -->
        </form>
    </div>
</div>
@endsection

@push('more-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/loader.css') }}">
@endpush

@push('more-scripts')
<script src="{{ asset('assets/js/custom/create-kyc-pin.js') }}"></script>
@endpush