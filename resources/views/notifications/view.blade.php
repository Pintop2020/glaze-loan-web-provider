@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('Viewing Notification'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-body p-0">
    <div class="nk-msg">
        <div class="nk-msg-body bg-white profile-shown">
            <div class="nk-msg-head">
                <h4 class="title d-none d-lg-block">System Notification</h4>
                <div class="nk-msg-head-meta">
                    <div class="d-lg-none"><a href="#" class="btn btn-icon btn-trigger nk-msg-hide ml-n1"><em class="icon ni ni-arrow-left"></em></a></div>
                </div>
            </div>
            <div class="nk-msg-reply nk-reply" data-simplebar>
                <div class="nk-msg-head py-4 d-lg-none">
                    <ul class="nk-msg-tags">
                        <li><span class="label-tag"><em class="icon ni ni-flag-fill"></em> <span>Action notifications</span></span></li>
                    </ul>
                </div>
                <div class="nk-reply-item">
                    <div class="nk-reply-header">
                        <div class="date-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans() }}</div>
                    </div>
                    <div class="nk-reply-body">
                        <div class="nk-reply-entry entry">
                            {!! $notification->data['body'] !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection