@extends('layouts.admin.app')

@section('title', __('Staff Dashboard'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between">
				<div class="nk-block-head-content">
					<h3 class="nk-block-title page-title">Overview</h3>
				</div>
				<div class="nk-block-head-content">
					<div class="toggle-wrap nk-block-tools-toggle">
						<a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
						<div class="toggle-expand-content" data-content="pageMenu">
							<ul class="nk-block-tools g-3">
								@if($user->roles()->first()->id != 1)
								<li>
									<div class="drodown">
										<a href="{{ route('home') }}" class="btn btn-white btn-dim btn-outline-light"><em class="d-none d-sm-inline icon ni ni-calender-date"></em><span><span class="d-none d-md-inline">My</span> Profile</span><em class="dd-indc icon ni ni-chevron-right"></em></a>
									</div>
								</li>
								@endif
								<li class="nk-block-tools-opt"><a href="/reports/customer-base" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Reports</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- .nk-block-head-content -->
			</div>
			<!-- .nk-block-between -->
		</div>
		<!-- .nk-block-head -->
		<div class="nk-block">
			<div class="row g-gs">
				<div class="col-xxl-12">
					<div class="row g-gs">
						<div class="col-lg-6 col-xxl-12">
							<div class="card">
								<div class="card-inner">
									<div class="card-title-group align-start mb-2">
										<div class="card-title">
											<h6 class="title">Loans Report</h6>
											<p>Loan report overview</p>
										</div>
										<div class="card-tools">
											<em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Quick Report for loan"></em>
										</div>
									</div>
									<div class="align-end gy-3 gx-5 flex-wrap flex-md-nowrap flex-lg-wrap flex-xxl-nowrap">
										<div class="nk-sale-data-group flex-md-nowrap g-4">
											<div class="nk-sale-data">
												<span class="amount">
													{{ \App\Http\Controllers\User\UserController::number_format_short($this_month_loans) }}
													@if($this_month_percent < 0)
													<span class="change down text-danger"><em class="icon ni ni-arrow-long-down"></em>{{ round(str_replace('-', '', $this_month_percent),2) }}%</span>
													@else
													<span class="change up text-success"><em class="icon ni ni-arrow-long-up"></em>{{ round($this_month_percent,2) }}%</span>
													@endif
												</span>
												<span class="sub-title">This Month</span>
											</div>
											<div class="nk-sale-data">
												<span class="amount">
													{{ \App\Http\Controllers\User\UserController::number_format_short($this_week_loans) }}
													@if($this_week_percent < 0)
													<span class="change down text-danger"><em class="icon ni ni-arrow-long-down"></em>{{ round(str_replace('-', '', $this_week_percent),2) }}%</span>
													@else
													<span class="change up text-success"><em class="icon ni ni-arrow-long-up"></em>{{ round($this_week_percent,2) }}%</span>
													@endif
												</span>
												<span class="sub-title">This Week</span>
											</div>
										</div>
										<div class="nk-sales-ck sales-revenue">
											<canvas class="sales-bar-chart" id="salesRevenue"></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- .col -->
						<div class="col-lg-6 col-xxl-12">
							<div class="row g-gs">
								<div class="col-sm-6 col-lg-12 col-xxl-6">
									<div class="card">
										<div class="card-inner">
											<div class="card-title-group align-start mb-2">
												<div class="card-title">
													<h6 class="title">Interest Accumulated Overtime</h6>
												</div>
												<div class="card-tools">
													<em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Interest Accumulated Overtime"></em>
												</div>
											</div>
											<div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
												<div class="nk-sale-data">
													<span class="amount">0.00</span>
													<span class="sub-title"><span class="change down text-danger"><em class="icon ni ni-arrow-long-down"></em>0%</span>since last month</span>
												</div>
												<div class="nk-sales-ck">
													<canvas class="sales-bar-chart" id="activeSubscription"></canvas>
												</div>
											</div>
										</div>
									</div>
									<!-- .card -->
								</div>
								<!-- .col -->
								<div class="col-sm-6 col-lg-12 col-xxl-6">
									<div class="card">
										<div class="card-inner">
											<div class="card-title-group align-start mb-2">
												<div class="card-title">
													<h6 class="title">Interest Acumulated monthly</h6>
												</div>
												<div class="card-tools">
													<em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="Interest Acumulated monthly"></em>
												</div>
											</div>
											<div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
												<div class="nk-sale-data">
													<span class="amount">0</span>
													<span class="sub-title"><span class="change up text-success"><em class="icon ni ni-arrow-long-up"></em>0%</span>since last week</span>
												</div>
												<div class="nk-sales-ck">
													<canvas class="sales-bar-chart" id="totalSubscription"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .col -->
				
				<!-- .col -->
				<div class="col-xxl-12">
					<div class="card card-full">
						<div class="card-inner">
							<div class="card-title-group">
								<div class="card-title">
									<h6 class="title"><span class="mr-2">Transaction</span> <a href="/transactions" class="link d-none d-sm-inline">See History</a></h6>
								</div>
							</div>
						</div>
						<div class="card-inner p-0 border-top">
							<div class="nk-tb-list nk-tb-orders">
								<div class="nk-tb-item nk-tb-head">
									<div class="nk-tb-col"><span>Reference</span></div>
									<div class="nk-tb-col tb-col-md"><span>Date</span></div>
									<div class="nk-tb-col"><span class="d-none d-mb-block">Status</span></div>
									<div class="nk-tb-col tb-col-sm"><span>Customer</span></div>
									<div class="nk-tb-col"><span>Total</span></div>
									<div class="nk-tb-col tb-col-sm"><span>Method</span></div>
									<div class="nk-tb-col tb-col-md"><span>Description</span></div>
								</div>
								<!-- .nk-tb-item -->
								@foreach($transactions as $key => $trans)
								<div class="nk-tb-item">
									<div class="nk-tb-col">
										<span class="tb-lead"><a href="javascript:void(0);">{{ json_decode($trans->details)->reference }}</a></span>
									</div>
									<div class="nk-tb-col tb-col-md">
										<span class="tb-sub">{{ date('M d, Y', strtotime($trans->created_at)) }}</span>
									</div>
									<div class="nk-tb-col">
										<span class="dot bg-warning d-mb-none"></span>
										@if(json_decode($trans->details)->status == 'success')
										<span class="badge badge-sm badge-dot has-bg badge-success d-none d-mb-inline-flex">Completed</span>
										@elseif(json_decode($trans->details)->status == 'failed')
										<span class="badge badge-sm badge-dot has-bg badge-danger d-none d-mb-inline-flex">Failed</span>
										@elseif(json_decode($trans->details)->status == 'pending')
										<span class="badge badge-sm badge-dot has-bg badge-warning d-none d-mb-inline-flex">Pending</span>
										@endif
									</div>
									<div class="nk-tb-col tb-col-sm">
										<span class="tb-sub">{{ $trans->user->getFullname() }}</span>
									</div>
									<div class="nk-tb-col">
										<span class="tb-lead">₦{{ number_format(json_decode($trans->details)->amount,2) }}</span>
									</div>
									<div class="nk-tb-col tb-col-sm">
										<span class="tb-lead">{{ json_decode($trans->details)->payment_option }}</span>
									</div>
									<div class="nk-tb-col tb-col-md">
										<span class="tb-sub">{{ json_decode($trans->details)->description }}</span>
									</div>
								</div>
								@endforeach
							</div>
						</div>
						<div class="card-inner-sm border-top text-center d-sm-none">
							<a href="#" class="btn btn-link btn-block">See History</a>
						</div>
					</div>
					<!-- .card -->
				</div>
<!--				<div class="col-md-6 col-xxl-4">
					<div class="card card-full">
						<div class="card-inner-group">
							<div class="card-inner">
								<div class="card-title-group">
									<div class="card-title">
										<h6 class="title">New Users</h6>
									</div>
									<div class="card-tools">
										<a href="html/user-list-regular.html" class="link">View All</a>
									</div>
								</div>
							</div>
							<div class="card-inner card-inner-md">
								<div class="user-card">
									<div class="user-avatar bg-primary-dim">
										<span>AB</span>
									</div>
									<div class="user-info">
										<span class="lead-text">Abu Bin Ishtiyak</span>
										<span class="sub-text">info@softnio.com</span>
									</div>
									<div class="user-action">
										<div class="drodown">
											<a href="#" class="dropdown-toggle btn btn-icon btn-trigger mr-n1" data-toggle="dropdown" aria-expanded="false"><em class="icon ni ni-more-h"></em></a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="link-list-opt no-bdr">
													<li><a href="#"><em class="icon ni ni-setting"></em><span>Action Settings</span></a></li>
													<li><a href="#"><em class="icon ni ni-notify"></em><span>Push Notification</span></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-inner card-inner-md">
								<div class="user-card">
									<div class="user-avatar bg-pink-dim">
										<span>SW</span>
									</div>
									<div class="user-info">
										<span class="lead-text">Sharon Walker</span>
										<span class="sub-text">sharon-90@example.com</span>
									</div>
									<div class="user-action">
										<div class="drodown">
											<a href="#" class="dropdown-toggle btn btn-icon btn-trigger mr-n1" data-toggle="dropdown" aria-expanded="false"><em class="icon ni ni-more-h"></em></a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="link-list-opt no-bdr">
													<li><a href="#"><em class="icon ni ni-setting"></em><span>Action Settings</span></a></li>
													<li><a href="#"><em class="icon ni ni-notify"></em><span>Push Notification</span></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-inner card-inner-md">
								<div class="user-card">
									<div class="user-avatar bg-warning-dim">
										<span>GO</span>
									</div>
									<div class="user-info">
										<span class="lead-text">Gloria Oliver</span>
										<span class="sub-text">gloria_72@example.com</span>
									</div>
									<div class="user-action">
										<div class="drodown">
											<a href="#" class="dropdown-toggle btn btn-icon btn-trigger mr-n1" data-toggle="dropdown" aria-expanded="false"><em class="icon ni ni-more-h"></em></a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="link-list-opt no-bdr">
													<li><a href="#"><em class="icon ni ni-setting"></em><span>Action Settings</span></a></li>
													<li><a href="#"><em class="icon ni ni-notify"></em><span>Push Notification</span></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-inner card-inner-md">
								<div class="user-card">
									<div class="user-avatar bg-success-dim">
										<span>PS</span>
									</div>
									<div class="user-info">
										<span class="lead-text">Phillip Sullivan</span>
										<span class="sub-text">phillip-85@example.com</span>
									</div>
									<div class="user-action">
										<div class="drodown">
											<a href="#" class="dropdown-toggle btn btn-icon btn-trigger mr-n1" data-toggle="dropdown" aria-expanded="false"><em class="icon ni ni-more-h"></em></a>
											<div class="dropdown-menu dropdown-menu-right">
												<ul class="link-list-opt no-bdr">
													<li><a href="#"><em class="icon ni ni-setting"></em><span>Action Settings</span></a></li>
													<li><a href="#"><em class="icon ni ni-notify"></em><span>Push Notification</span></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xxl-4">
					<div class="card h-100">
						<div class="card-inner border-bottom">
							<div class="card-title-group">
								<div class="card-title">
									<h6 class="title">Support Requests</h6>
								</div>
								<div class="card-tools">
									<a href="html/subscription/tickets.html" class="link">All Tickets</a>
								</div>
							</div>
						</div>
						<ul class="nk-support">
							<li class="nk-support-item">
								<div class="user-avatar">
									<img src="./images/avatar/a-sm.jpg" alt="">
								</div>
								<div class="nk-support-content">
									<div class="title">
										<span>Vincent Lopez</span><span class="badge badge-dot badge-dot-xs badge-warning ml-1">Pending</span>
									</div>
									<p>Thanks for contact us with your issues...</p>
									<span class="time">6 min ago</span>
								</div>
							</li>
							<li class="nk-support-item">
								<div class="user-avatar bg-purple-dim">
									<span>DM</span>
								</div>
								<div class="nk-support-content">
									<div class="title">
										<span>Daniel Moore</span><span class="badge badge-dot badge-dot-xs badge-info ml-1">Open</span>
									</div>
									<p>Thanks for contact us with your issues...</p>
									<span class="time">2 Hours ago</span>
								</div>
							</li>
							<li class="nk-support-item">
								<div class="user-avatar">
									<img src="./images/avatar/b-sm.jpg" alt="">
								</div>
								<div class="nk-support-content">
									<div class="title">
										<span>Larry Henry</span><span class="badge badge-dot badge-dot-xs badge-success ml-1">Solved</span>
									</div>
									<p>Thanks for contact us with your issues...</p>
									<span class="time">3 Hours ago</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-xxl-4">
					<div class="card h-100">
						<div class="card-inner border-bottom">
							<div class="card-title-group">
								<div class="card-title">
									<h6 class="title">Notifications</h6>
								</div>
								<div class="card-tools">
									<a href="html/subscription/tickets.html" class="link">View All</a>
								</div>
							</div>
						</div>
						<div class="card-inner">
							<div class="timeline">
								<h6 class="timeline-head">November, 2019</h6>
								<ul class="timeline-list">
									<li class="timeline-item">
										<div class="timeline-status bg-primary is-outline"></div>
										<div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
										<div class="timeline-data">
											<h6 class="timeline-title">Submited KYC Application</h6>
											<div class="timeline-des">
												<p>Re-submitted KYC Application form.</p>
												<span class="time">09:30am</span>
											</div>
										</div>
									</li>
									<li class="timeline-item">
										<div class="timeline-status bg-primary"></div>
										<div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
										<div class="timeline-data">
											<h6 class="timeline-title">Submited KYC Application</h6>
											<div class="timeline-des">
												<p>Re-submitted KYC Application form.</p>
												<span class="time">09:30am</span>
											</div>
										</div>
									</li>
									<li class="timeline-item">
										<div class="timeline-status bg-pink"></div>
										<div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
										<div class="timeline-data">
											<h6 class="timeline-title">Submited KYC Application</h6>
											<div class="timeline-des">
												<p>Re-submitted KYC Application form.</p>
												<span class="time">09:30am</span>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div>
</div>
@endsection

@push('more-scripts')
<script src="{{ asset('admin_assets/assets/js/charts/chart-sales.js') }}"></script>
@endpush