@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('Create new ticket'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block nk-block-lg">
				<div class="nk-block-head">
					<div class="nk-block-head-content">
						<h4 class="title nk-block-title">Create new support tickets</h4>
						<div class="nk-block-des">
							<p>If you need solutions or support guidance, you can submit a ticket by selecting the appropriate department below.</p>
						</div>
					</div>
				</div>
				<form action="{{ route('supports.store') }}" method="post">
					@csrf
					<div class="card card-bordered">
						<div class="card-inner">
							<div class="card-head">
								<h5 class="card-title">Ticket Information</h5>
							</div>
							<div class="row g-4">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Name</label>
										<div class="form-control-wrap">
											<input type="text" class="form-control" value="{{ $user->getFullname() }}" readonly="true">
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Email address</label>
										<div class="form-control-wrap">
											<input type="text" class="form-control" value="{{ $user->email }}" readonly="true">
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Department</label>
										<div class="form-control-wrap">
											<select class="form-control" name="department">
												<option>Marketing Department</option>
												<option>Transaction Department</option>
												<option>Risk Deparment</option>
												<option>Technical Deparment</option>
												<option>Abuse Deparment</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Priority</label>
										<div class="form-control-wrap">
											<select class="form-control" name="priority">
												<option>High</option>
												<option>Medium</option>
												<option>Low</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card card-bordered">
						<div class="card-inner">
							<div class="card-head">
								<h5 class="card-title">Message</h5>
							</div>
							<div class="row g-4">
								<div class="col-lg-12">
									<div class="form-group">
										<label class="form-label">Subject</label>
										<div class="form-control-wrap">
											<input type="text" class="form-control" name="subject" required="true">
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="form-label">Message</label>
										<div class="form-control-wrap">
											<textarea class="summernote-basic" name="message"></textarea>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group">
										<button type="submit" class="btn btn-lg btn-primary">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@push('more-scripts')
<link rel="stylesheet" href="{{ asset('assets/css/editors/summernote.css') }}">
<script src="{{ asset('assets/js/libs/editors/summernote.js') }}"></script>
<script src="{{ asset('assets/js/editors.js') }}"></script>
@endpush