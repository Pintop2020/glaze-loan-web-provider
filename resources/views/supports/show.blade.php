@extends('layouts.user.app')

@section('title', __('My Ticket'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-body p-0">
	<div class="nk-msg">
		<div class="nk-msg-body bg-white profile-shown">
			<div class="nk-msg-head">
				<h4 class="title d-none d-lg-block">{{ $support->subject }}</h4>
				<div class="nk-msg-head-meta">
					<div class="d-none d-lg-block">
						<ul class="nk-msg-tags">
							<li><span class="label-tag"><em class="icon ni ni-flag-fill"></em> <span>{{ $support->department }}</span></span></li>
							<li><span class="label-tag"><em class="icon ni ni-circle-fill"></em> <span>{{ $support->priority }}</span></span></li>
						</ul>
					</div>
					<div class="d-lg-none"><a href="#" class="btn btn-icon btn-trigger nk-msg-hide ml-n1"><em class="icon ni ni-arrow-left"></em></a></div>
					<ul class="nk-msg-actions">
						@if($support->status == 'opened')
						<li><a href="/support/close/{{ \Illuminate\Support\Facades\Crypt::encryptString($support->id) }}" class="btn btn-dim btn-sm btn-outline-light"><em class="icon ni ni-check"></em><span>Mark as Closed</span></a></li>
						@endif
						@if($support->status == 'closed')
						<li><span class="badge badge-dim badge-success badge-sm"><em class="icon ni ni-check"></em><span>Closed</span></span></li>
						@endif
						<!--<li class="d-lg-none"><a href="#" class="btn btn-icon btn-sm btn-white btn-light profile-toggle"><em class="icon ni ni-info-i"></em></a></li>-->
					</ul>
				</div>
				<a href="#" class="nk-msg-profile-toggle profile-toggle active"><em class="icon ni ni-arrow-left"></em></a>
			</div>
			<!-- .nk-msg-head -->
			<div class="nk-msg-reply nk-reply" data-simplebar>
				<div class="nk-msg-head py-4 d-lg-none">
					<h4 class="title">{{ $support->subject }}</h4>
					<ul class="nk-msg-tags">
						<li><span class="label-tag"><em class="icon ni ni-flag-fill"></em> <span>{{ $support->department }}</span></span></li>
						<li><span class="label-tag"><em class="icon ni ni-circle-fill"></em> <span>{{ $support->priority }}</span></span></li>
					</ul>
				</div>
				@foreach($support->responses()->latest()->get() as $response)
				<div class="nk-reply-item">
					<div class="nk-reply-header">
						<div class="user-card">
							<div class="user-avatar sm bg-blue">
								@if($response->user->my_details)
									<img src="{{ asset($response->user->my_details->data()->identity->passport) }}">
								@else
									<span>{{ $response->user->nameThumb() }}</span>
								@endif
							</div>
							<div class="user-name">{{ $response->user->getFullname() }}</div>
						</div>
						<div class="date-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($response->created_at))->diffForHumans() }}</div>
					</div>
					<div class="nk-reply-body">
						<div class="nk-reply-entry entry">
							{!! $response->response !!}
						</div>
					</div>
				</div>
				@endforeach
				<div class="nk-reply-item">
					<div class="nk-reply-header">
						<div class="user-card">
							<div class="user-avatar sm bg-blue">
								@if($support->user->my_details)
									<img src="{{ asset($support->user->my_details->data()->identity->passport) }}">
								@else
									<span>{{ $support->user->nameThumb() }}</span>
								@endif
							</div>
							<div class="user-name">{{ $support->user->getFullname() }}</div>
						</div>
						<div class="date-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($support->created_at))->diffForHumans() }}</div>
					</div>
					<div class="nk-reply-body">
						<div class="nk-reply-entry entry">
							{!! $support->message !!}
						</div>
					</div>
				</div>
				@if($support->status == 'opened')
				<form action="{{ route('supportResponses.store') }}" method="post">
					@csrf
					<input type="hidden" name="support" value="{{ $support->id }}">
					<div class="nk-reply-form">
						<div class="tab-content">
							<div class="tab-pane active" id="reply-form">
								<div class="nk-reply-form-editor">
									<div class="nk-reply-form-field">
										<textarea class="summernote-basic" name="message"></textarea>
										<button type="submit" class="btn btn-lg btn-primary mt-4">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				@endif
			</div>
			<!-- .nk-reply -->
			<div class="nk-msg-profile visible" data-simplebar>
				<div class="card">
					<div class="card-inner-group">
						<div class="card-inner">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('more-scripts')
<link rel="stylesheet" href="{{ asset('assets/css/editors/summernote.css') }}">
<script src="{{ asset('assets/js/libs/editors/summernote.js') }}"></script>
<script src="{{ asset('assets/js/editors.js') }}"></script>
@endpush