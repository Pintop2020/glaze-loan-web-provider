@extends('layouts.user.app')

@section('title', __('My Tickets'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">My Tickets</h2>
					<div class="nk-block-des">
						<p class="lead">Here are the list of all your tickets.</p>
					</div>
				</div>
			</div>
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">TID</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Department</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Subject</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Status</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Last Updated</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right"></th>
								</tr>
							</thead>
							<tbody>
								@foreach($supports as $support)
								<tr class="nk-tb-item">
									<td class="nk-tb-col tb-col-mb">
										<span>{{ $support->id() }}</span>
									</td>
									<td class="nk-tb-col tb-col-mb">
										<span>{{ $support->department }}</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span class="text-muted">{{ $support->subject }}</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										@if($support->status == 'opened')
										<span class="badge badge-sm badge-dot has-bg badge-success d-mb-inline-flex">Opened</span>
										@else
										<span class="badge badge-sm badge-dot has-bg badge-danger d-mb-inline-flex">Closed</span>
										@endif
									</td>
									<td class="nk-tb-col tb-col-md">
										@if($support->responses()->count() > 0)
										<span class="text-muted">{{ date('M d, Y h:i A', strtotime($support->responses()->latest()->first()->created_at)) }}</span>
										@else
										<span class="text-muted">{{ date('M d, Y h:i A', strtotime($support->created_at)) }}</span>
										@endif
									</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
											<li>
												<div class="drodown">
													<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="link-list-opt no-bdr">
															<li><a href="/supports/{{ $support->uuid() }}"><em class="icon ni ni-eye"></em><span>View Ticket</span></a></li>
															@if($support->status == 'opened')
															<li><a href="/support/close/{{ $support->uuid() }}"><em class="icon ni ni-focus"></em><span>Close Ticket</span></a></li>
															@endif
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection