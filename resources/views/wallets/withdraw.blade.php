@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('Withdraw'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-body">
    <div class="components-preview wide-md mx-auto">
        <div class="nk-block-head nk-block-head-lg wide-sm">
            <div class="nk-block-head-content">
                <div class="nk-block-head-sub"><a class="back-to" href="#"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
                <h2 class="nk-block-title fw-normal">Withdrawal</h2>
            </div>
        </div>
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-head">
                        <h5 class="card-title">Withdraw Input</h5>
                    </div>
                    <form action="/wallet/withdraw" class="gy-3" method="post">
                        @csrf
                        <div class="row g-3 align-center">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-label">Amount</label>
                                    <span class="form-note">Specify the amount to withdraw.</span>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <input type="number" step="any" class="form-control" required="" name="amount">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row g-3 align-center">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-label">Destination</label>
                                    <span class="form-note">Your registered bank account will be your destination source.</span>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <div class="text-muted">
                                            <b>Bank :</b> {{ $bank }}<br>
                                            <b>Account Number :</b> {{ $user->my_details->data()->financial_info->account_number }}<br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row g-3">
                            <div class="col-lg-7 offset-lg-5">
                                <div class="form-group mt-2">
                                    <button type="submit" class="btn btn-lg btn-primary">Withdraw</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection