@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('Deposit'))

@section('theB')
<div class="nk-content nk-content-fluid"><div class="container-xl wide-lg">
@endsection

@section('content')
<div class="nk-content-body">
    <div class="components-preview wide-md mx-auto">
        <div class="nk-block-head nk-block-head-lg wide-sm">
            <div class="nk-block-head-content">
                <div class="nk-block-head-sub"><a class="back-to" href="#"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
                <h2 class="nk-block-title fw-normal">Wallet Deposit</h2>
            </div>
        </div>
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="card-head">
                        <h5 class="card-title">Deposit Input</h5>
                    </div>
                    <form action="/wallet/deposit" class="gy-3" method="post">
                        @csrf
                        <div class="row g-3 align-center">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-label">Amount</label>
                                    <span class="form-note">Specify the amount to deposit.</span>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <input type="number" step="any" class="form-control" required="" name="amount">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row g-3 align-center">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-label">Source</label>
                                    <span class="form-note">Select the source of deposit.</span>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <select class="form-select form-control form-control-lg state" name="source"  data-search="on">
                                            <option selected="true" disabled>Choose Option</option>
                                            @foreach($user->cards as $card)
                                            <option value="{{ $card->id }}">Pay with my {{ $card->data()->bank }} - {{ strtoupper($card->data()->brand) }} Card - {{ $card->data()->last4 }}</option>
                                            @endforeach
                                            <option value="card">Pay with new card</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row g-3">
                            <div class="col-lg-7 offset-lg-5">
                                <div class="form-group mt-2">
                                    <button type="submit" class="btn btn-lg btn-primary">Deposit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection