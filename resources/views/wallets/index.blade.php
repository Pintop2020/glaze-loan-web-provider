@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('My Wallet'))

@section('theB')
<div class="nk-content nk-content-fluid"><div class="container-xl wide-lg">
@endsection

@section('content')
<div class="nk-content-body">
    <div class="nk-block-head">
        <div class="nk-block-head-sub"><span>Account Wallet</span> </div>
        <div class="nk-block-between-md g-4">
            <div class="nk-block-head-content">
                <h2 class="nk-block-title fw-normal">Wallet / Assets</h2>
                <div class="nk-block-des">
                    <p>Here is the list of your assets / wallets!</p>
                </div>
            </div>
            <!--<div class="nk-block-head-content">
                <ul class="nk-block-tools gx-3">
                    <li><a href="#" class="btn btn-primary"><span>Send</span> <em class="icon ni ni-arrow-long-right"></em></a></li>
                    <li><a href="/wallet/withdraw" class="btn btn-dim btn-outline-light"><span>Withdraw</span> <em class="icon ni ni-arrow-long-right"></em></a></li>
                </ul>
            </div>-->
        </div>
    </div>
    <div class="nk-block">
        <div class="nk-block-head-sm">
            <div class="nk-block-head-content">
                <h5 class="nk-block-title title">My Accounts</h5>
            </div>
        </div>
        <div class="row g-gs">
            <div class="col-sm-6 col-lg-4 col-xl-6 col-xxl-4">
                <div class="card card-bordered is-dark">
                    <div class="nk-wgw">
                        <div class="nk-wgw-inner">
                            <a class="nk-wgw-name" href="#">
                                <div class="nk-wgw-icon is-default">
                                    <em class="icon ni ni-sign-kobo"></em>
                                </div>
                                <h5 class="nk-wgw-title title">Glaze Wallet</h5>
                            </a>
                            <div class="nk-wgw-balance">
                                <div class="amount">{{ number_format($user->wallet->amount,2) }}<span class="currency currency-nio">NGN</span></div>
                                <!--<div class="amount-sm">000<span class="currency currency-usd">NGN</span></div>-->
                            </div>
                        </div>
                        <div class="nk-wgw-actions">
                            <ul>
                                <li><a href="#"><em class="icon ni ni-arrow-up-right"></em> <span>Send</span></a></li>
                                <li><a href="/wallet/deposit"><em class="icon ni ni-arrow-down-left"></em><span>Deposit</span></a></li>
                                <li><a href="/wallet/withdraw"><em class="icon ni ni-arrow-to-right"></em><span>Withdraw</span></a></li>
                            </ul>
                        </div>
                        <!--
                        <div class="nk-wgw-more dropdown">
                            <a href="#" class="btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                <ul class="link-list-plain sm">
                                    <li><a href="#">Details</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    	-->
                    </div>
                </div><!-- .card -->
            </div><!-- .col -->
            <div class="col-sm-6 col-lg-4 col-xl-6 col-xxl-4">
                <div class="card card-bordered">
                    <div class="nk-wgw">
                        <div class="nk-wgw-inner">
                            <a class="nk-wgw-name" href="#">
                                <div class="nk-wgw-icon">
                                    <em class="icon ni ni-sign-cc-alt2"></em>
                                </div>
                                <h5 class="nk-wgw-title title">Investment</h5>
                            </a>
                            <div class="nk-wgw-balance">
                                <div class="amount">0.00<span class="currency currency-eth">NGN</span></div>
                                <!--<div class="amount-sm">0.00<span class="currency currency-usd">NGN</span></div>-->
                            </div>
                        </div>
                        <div class="nk-wgw-actions">
                            <ul>
                                <li><a href="#"><em class="icon ni ni-arrow-up-right"></em> <span>Send</span></a></li>
                                <li><a href="#"><em class="icon ni ni-arrow-down-left"></em><span>Reinvest</span></a></li>
                                <li><a href="#"><em class="icon ni ni-arrow-to-right"></em><span>Withdraw</span></a></li>
                            </ul>
                        </div><!--
                        <div class="nk-wgw-more dropdown">
                            <a href="#" class="btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                <ul class="link-list-plain sm">
                                    <li><a href="#">Details</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Delete</a></li>
                                    <li><a href="#">Make Default</a></li>
                                </ul>
                            </div>
                        </div>-->
                    </div>
                </div><!-- .card -->
            </div><!-- .col -->
            <div class="col-sm-6 col-lg-4 col-xl-6 col-xxl-4">
                <div class="card card-bordered">
                    <div class="nk-wgw">
                        <div class="nk-wgw-inner">
                            <a class="nk-wgw-name" href="#">
                                <div class="nk-wgw-icon">
                                    <em class="icon ni ni-sign-dash-alt"></em>
                                </div>
                                <h5 class="nk-wgw-title title">Loan</h5>
                            </a>
                            <div class="nk-wgw-balance">
                                <div class="amount">{{ number_format(SuperM::getOwing($user)) }}<span class="currency currency-btc">NGN</span></div>
                                <!--<div class="amount-sm">000<span class="currency currency-usd">USD</span></div>-->
                            </div>
                        </div>
                        <div class="nk-wgw-actions">
                            <ul>
                                <li><a href="#"><em class="icon ni ni-arrow-up-right"></em> <span>Clear Loan</span></a></li>
                                @if(SuperM::getSingleDueLoans($user))
                                <li><a href="#"><em class="icon ni ni-arrow-down-left"></em><span>Pay due payment</span></a></li>
                                @endif
                            </ul>
                        </div><!--
                        <div class="nk-wgw-more dropdown">
                            <a href="#" class="btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                <ul class="link-list-plain sm">
                                    <li><a href="#">Details</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Delete</a></li>
                                    <li><a href="#">Make Default</a></li>
                                </ul>
                            </div>
                        </div>-->
                    </div>
                </div><!-- .card -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div>
</div>
@endsection