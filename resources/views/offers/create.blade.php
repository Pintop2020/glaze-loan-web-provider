@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('Create Offer Letter'))

@push('more-scripts')
<link rel="stylesheet" href="{{ asset('assets/css/editors/summernote.css') }}">
<script src="{{ asset('assets/js/libs/editors/summernote.js') }}"></script>
<script src="{{ asset('assets/js/editors.js') }}"></script>
@endpush

@section('content')
<div class="nk-content-inner">
    <div class="nk-content-body">
        <div class="components-preview wide-md mx-auto">
            <div class="nk-block-head nk-block-head-lg wide-sm">
                <div class="nk-block-head-content">
                    <div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
                    <h2 class="nk-block-title fw-normal">Create Offer Letter</h2>
                </div>
            </div>
            <!-- nk-block -->
            <div class="nk-block nk-block-lg">
                <div class="card card-preview">
                    <div class="card-inner">
                        <div class="nk-upload-form">
                            <h5 class="title mb-3">Create Offer</h5>
                            <form method="post" action="{{ route('createOffers') }}">
                                @csrf
                                <input type="hidden" name="loan" value="{{ $loan->id }}">
                                <div class="row">
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Facility Type</label>
                                        <input type="text" class="form-control" name="facility_type" value="{{ strtoupper($loan->data()->type) }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Amount</label>
                                        <input type="number" class="form-control" name="amount" value="{{ $loan->data()->amount }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Mode of Disbursement</label>
                                        <input type="text" class="form-control" name="mode_of_disbursement" value="Bank Transfer">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Repayment Source</label>
                                        <input type="text" class="form-control" name="repayment_source">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Interest</label>
                                        <input type="number" class="form-control" name="interest" value="{{ $loan->data()->interest }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Processing Fee</label>
                                        <input type="number" class="form-control" name="processing" value="{{ $loan->data()->processing }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Management Fee</label>
                                        <input type="number" class="form-control" name="management" value="{{ $loan->data()->management }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Outstanding balance</label>
                                        <input type="number" class="form-control" name="outstanding" value="0" step="any">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Tenor</label>
                                        <select class="form-control" name="tenor">
                                           @for($i=3;$i<25;$i++)
                                           <option value="{{ $i }}">{{ $i }} months</option>
                                           @endfor 
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Moratorium</label>
                                        <input type="number" class="form-control" name="moratorium" value="0">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Pay Date</label>
                                        <input type="number" class="form-control" name="pay_date" value="{{ $loan->data()->pay_date }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Payment Starts</label>
                                        <select class="form-control" name="payment_starts">
                                            <option value="this">This month</option>
                                            <option value="next">Next Month</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>CONDITION PRECEDENT TO DRAWDOWN</label>
                                        <textarea class="summernote-minimal" name="condition_precedent_to_dropdown">
                                            <p>
                                                <ul>
                                                    <li>Duly accepted and executed offer letter signed by the Borrower.</li>
                                                    <li>Receipt and payment of all upfront fees.</li>
                                                    <li>Receipt of cheque (s) covering the total facility amount with all interests.</li>
                                                </ul>
                                            </p>
                                        </textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>OTHER CONDITIONS</label>
                                        <textarea class="summernote-minimal" name="other_conditions">
                                            <p>
                                                <ul>
                                                    <li>Disbursement is subject to availability of funds.</li>
                                                    <li>If any scheduled repayment on this facility is not made by the date which the same shall become payable then
                                without prejudice to any or all the rights and remedies accruing to GLAZE CREDIT, the unpaid amount shall attract a default interest at the rate of 15% per month on both principal and interest outstanding pro-rated per day outstanding.</li>
                                                    <li>The renewal of the facility shall be subject to GLAZE CREDIT being satisfied that the facility was properly utilized and liquidated or the balance paid down by 50%.</li>
                                                    <li>All other terms and conditions as contained in the Loan Agreement between the Borrower and the Lender shall be binding upon the Borrower.</li>
                                                </ul>
                                            </p>
                                        </textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>EVENTS OF DEFAULT</label>
                                        <textarea class="summernote-minimal" name="event_of_defaults">
                                            <p>
                                                <ul>
                                                    <li>If the Borrower fails to pay any sum outstanding as and when due, and such sum outstanding remain unpaid for 3days.</li>
                                                    <li>If the Borrower stops repayment, and/or if the Borrower proposes any moratorium on the Borrower's debt in respect of the facility.</li>
                                                    <li>If there should in the opinion of GLAZE CREDIT be a material adverse change in the financial condition of the Borrower.</li>
                                                    <li>No failure nor delay in exercising on the part of GLAZE CREDIT, any right, power or remedy hereunder, shall operate as a waiver thereof, nor shall any single or partial exercise of any right, power or remedy prevent any further or other exercise thereof or the exercise of any other right power or remedy. The rights and remedies herein provided are cumulative and not exclusive of any rights or remedies provided by law.</li>
                                                    <li>Notwithstanding anything herein before contained, the facility or balance thereof and other monies herein covenanted to be paid whether by way of interest or otherwise shall become immediately due and payable on the demand in the advent of default.</li>
                                                    <li>In case the Borrower has failed to pay as at when due, the GLAZE CREDIT has the right under the cosigned to debit all accounts of the Borrower with the overdue installment.</li>
                                                </ul>
                                            </p>
                                        </textarea>
                                    </div>
                                </div>
                                <button class="btn btn-primary mt-4" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection