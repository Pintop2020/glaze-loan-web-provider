@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('Edit Offer Letter'))

@push('more-scripts')
<link rel="stylesheet" href="{{ asset('assets/css/editors/summernote.css') }}">
<script src="{{ asset('assets/js/libs/editors/summernote.js') }}"></script>
<script src="{{ asset('assets/js/editors.js') }}"></script>
@endpush

@section('content')
<div class="nk-content-inner">
    <div class="nk-content-body">
        <div class="components-preview wide-md mx-auto">
            <div class="nk-block-head nk-block-head-lg wide-sm">
                <div class="nk-block-head-content">
                    <div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
                    <h2 class="nk-block-title fw-normal">Create Offer Letter</h2>
                </div>
            </div>
            <!-- nk-block -->
            <div class="nk-block nk-block-lg">
                <div class="card card-preview">
                    <div class="card-inner">
                        <div class="nk-upload-form">
                            <h5 class="title mb-3">Create Offer</h5>
                            <form method="post" action="{{ route('offers.update', $entity->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Facility Type</label>
                                        <input type="text" class="form-control" name="facility_type" value="{{ strtoupper($entity->data()->facility_type) }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Amount</label>
                                        <input type="number" class="form-control" name="amount" value="{{ $entity->data()->amount }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Mode of Disbursement</label>
                                        <input type="text" class="form-control" name="mode_of_disbursement" value="Bank Transfer" value="{{ $entity->data()->mode_of_disbursement }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Repayment Source</label>
                                        <input type="text" class="form-control" name="repayment_source" value="{{ $entity->data()->repayment_source }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Interest</label>
                                        <input type="number" class="form-control" name="interest" value="{{ $entity->data()->interest }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Processing Fee</label>
                                        <input type="number" class="form-control" name="processing" value="{{ $entity->data()->processing }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Management Fee</label>
                                        <input type="number" class="form-control" name="management" value="{{ $entity->data()->management }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Outstanding balance</label>
                                        <input type="number" class="form-control" name="outstanding" value="0" step="any" value="{{ $entity->data()->outstanding }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Tenor</label>
                                        <select class="form-control" name="tenor">
                                           @for($i=3;$i<25;$i++)
                                           <option value="{{ $i }}" {{ $entity->data()->tenor == $i ? 'checked':'' }}>{{ $i }} months</option>
                                           @endfor 
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Moratorium</label>
                                        <input type="number" class="form-control" name="moratorium" value="{{ $entity->data()->moratorium }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Pay Date</label>
                                        <input type="number" class="form-control" name="pay_date" value="{{ $entity->data()->pay_date }}">
                                    </div>
                                    <div class="form-group col-xl-3 col-lg-4 col-md-6">
                                        <label>Payment Starts</label>
                                        <select class="form-control" name="payment_starts">
                                            <option value="this" {{ $entity->data()->payment_starts == 'this' ? 'checked':'' }}>This month</option>
                                            <option value="next" {{ $entity->data()->payment_starts == 'next' ? 'checked':'' }}>Next Month</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>CONDITION PRECEDENT TO DRAWDOWN</label>
                                        <textarea class="summernote-minimal" name="condition_precedent_to_dropdown">
                                            {!! $entity->data()->condition_precedent_to_dropdown !!}
                                        </textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>OTHER CONDITIONS</label>
                                        <textarea class="summernote-minimal" name="other_conditions">
                                            {!! $entity->data()->other_conditions !!}
                                        </textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>EVENTS OF DEFAULT</label>
                                        <textarea class="summernote-minimal" name="event_of_defaults">
                                            {!! $entity->data()->event_of_defaults !!}
                                        </textarea>
                                    </div>
                                </div>
                                <button class="btn btn-primary mt-4" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection