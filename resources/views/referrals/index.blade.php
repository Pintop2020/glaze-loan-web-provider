@extends('layouts.admin.app')

@section('title', __('Referrals'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All Referrals</h2>
					<div class="nk-block-des">
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="true">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $key => $user)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $user->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($user->user->my_details) <img src="{{ asset($user->user->my_details->full_data()->identity->passport) }}"> @else <span>{{ $user->user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $user->user->getFullname() }} @if(Cache::has('user-is-online-' . $user->user->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>{{ $user->user->email }}</span>
											</div>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection