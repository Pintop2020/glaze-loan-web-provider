@extends('layouts.user.app')

@section('title', __('Referrals'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All Referrals</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all referrals.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="true">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Phone</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Verified</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
								</tr>
							</thead>
							<tbody>
								@foreach($user->referrals as $key => $ref)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $ref->user->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($ref->user->my_details) <img src="{{ asset($ref->user->my_details->full_data()->identity->passport) }}"> @else <span>{{ $ref->user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $ref->user->getFullname() }} @if(Cache::has('user-is-online-' . $ref->user->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>{{ $ref->user->email }}</span>
											</div>
										</div>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ $ref->user->mobile }}</span>
									</td>
									@if($ref->user->my_details)
									<td class="nk-tb-col tb-col-lg" data-order="Email Verified - Kyc Verified">
									@else
									<td class="nk-tb-col tb-col-lg" data-order="Email Verified - Kyc Unverified">
									@endif
										<ul class="list-status">
											<li><em class="icon text-success ni ni-check-circle"></em> <span>Email</span></li>
											@if($ref->user->my_details)
											<li><em class="icon text-success ni ni-check-circle"></em> <span>KYC</span></li>
											@else
											<li><em class="icon ni ni-alert-circle"></em> <span>KYC</span></li>
											@endif
										</ul>
									</td>
									<td class="nk-tb-col tb-col-md">
										@if($ref->user->active)
										<span class="tb-status text-success">Active</span>
										@else
										<span class="tb-status text-danger">Restricted</span>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection