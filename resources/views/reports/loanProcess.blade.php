@extends('layouts.admin.app')

@section('title', __('Loan process reports'))

@section('content')
<div class="nk-content-body">
	<div class="components-preview wide-md mx-auto">
		<div class="nk-block nk-block-lg">
			<div class="nk-block-head">
				<div class="nk-block-head-content">
					<h4 class="title nk-block-title">Generate Report</h4>
					<div class="nk-block-des">
						<p>You can generate loan process reports from here.</p>
					</div>
				</div>
			</div>
			<div class="card card-bordered">
				<div class="card-inner">
					<div class="card-head">
						<h5 class="card-title">Report inputs</h5>
					</div>
					<form class="gy-3" method="get" action="/reports/loan-process-generate">
						<div class="row g-3 align-center">
							<div class="col-lg-5">
								<div class="form-group">
									<label class="form-label">Selection range</label>
									<span class="form-note">Specify start date of report. Leave empty if you wanna select all.</span>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<div class="form-control-wrap">
										<input type="text" class="form-control" id="range" name="range">
									</div>
								</div>
							</div>
						</div>
						<div class="row g-3 align-center">
							<div class="col-lg-5">
								<div class="form-group">
									<label class="form-label">Marketer</label>
									<span class="form-note">Select desired marketer.</span>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<div class="form-control-wrap">
										<select class="form-control" name="marketer">
											<option>All</option>
											@foreach($staff as $staf)
											<option value="{{ $staf->id }}">{{ $staf->getFullname() }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row g-3 align-center">
							<div class="col-lg-5">
								<div class="form-group">
									<label class="form-label">Status</label>
									<span class="form-note">Status of loan.</span>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<div class="form-control-wrap">
										<select class="form-control" name="status">
											<option>All</option>
											<option>Declined</option>
											<option>Pending</option>
											<option>Approved</option>
											<option>Active</option>
											<option>Repaid</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row g-3">
							<div class="col-lg-7 offset-lg-5">
								<div class="form-group mt-2">
									<button type="submit" class="btn btn-lg btn-primary">Generate</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- card -->
		</div>
		<!-- .nk-block -->
	</div>
	<!-- .components-preview -->
</div>
@endsection

@push('more-styles')
<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/daterangepicker.css') }}" />
@endpush

@push('more-scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('js/daterangepicker.js') }}"></script>
<script>
	$(function(){
		$('#range').daterangepicker({
		    "timePicker": true,
		    "ranges": {
		        "Today": [
		            "2021-01-23T20:26:18.203Z",
		            "2021-01-23T20:26:18.203Z"
		        ],
		        "Yesterday": [
		            "2021-01-22T20:26:18.203Z",
		            "2021-01-22T20:26:18.203Z"
		        ],
		        "Last 7 Days": [
		            "2021-01-17T20:26:18.203Z",
		            "2021-01-23T20:26:18.203Z"
		        ],
		        "Last 30 Days": [
		            "2020-12-25T20:26:18.203Z",
		            "2021-01-23T20:26:18.203Z"
		        ],
		        "This Month": [
		            "2020-12-31T23:00:00.000Z",
		            "2021-01-31T22:59:59.999Z"
		        ],
		        "Last Month": [
		            "2020-11-30T23:00:00.000Z",
		            "2020-12-31T22:59:59.999Z"
		        ]
		    },
		    "startDate": "01/17/2021",
		    "endDate": "01/23/2021"
		}, function(start, end, label) {
		  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
		});
	});
</script>
@endpush