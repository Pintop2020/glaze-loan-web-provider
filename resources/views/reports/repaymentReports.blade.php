@extends('layouts.admin.app')

@section('title', __('Repayment reports'))

@section('content')
<div class="nk-content-body">
	<div class="components-preview wide-md mx-auto">
		<div class="nk-block nk-block-lg">
			<div class="nk-block-head">
				<div class="nk-block-head-content">
					<h4 class="title nk-block-title">Generate Report</h4>
					<div class="nk-block-des">
						<p>You can generate repayment reports from here.</p>
					</div>
				</div>
			</div>
			<div class="card card-bordered">
				<div class="card-inner">
					<div class="card-head">
						<h5 class="card-title">Report inputs</h5>
					</div>
					<form class="gy-3" method="get" action="/reports/repay-report-generate">
						<div class="row g-3">
							<div class="col-lg-7 offset-lg-5">
								<div class="form-group mt-2">
									<button type="submit" class="btn btn-lg btn-primary">Generate</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection