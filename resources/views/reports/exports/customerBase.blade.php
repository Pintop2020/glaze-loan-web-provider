@php
use App\Entities\User\User;
use App\Http\Controllers\GlobalMethods as SuperM;
$users = User::where('is_admin', 0)->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query){
    $query->where('email', auth()->user()->email);
});
$type = urldecode($_GET['type']);
if(isset($_GET['range']) && $_GET['range'] != null){
    $range = urldecode($_GET['range']);
    $arr = explode(' - ', $range);
    $start = new DateTime($arr[0]);
    $end = new DateTime($arr[1]);
    $users->whereBetween('created_at',[$start,$end]);
}
if($type == "Active") $users->where('active', 1);
elseif($type == "Inactive") $users->where('active', 0);
$df = $users->get();
$i = 1;
@endphp
<table>
    <thead>
    <tr>
        <th>S/N</th>
        <th colspan="6">PRIMARY INFO</th>
        <th colspan="5">PERSONAL INFO</th>
        <th colspan="4">IDENTITY INFO</th>
        <th colspan="5">ADDRESS INFO</th>
        <th colspan="3">FINANCIAL INFO</th>
        <th colspan="10">EMPLOYMENT INFO</th>
        <th colspan="3">TRANSACTION OVERVIEW</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Middel Name</td>
            <td>Email</td>
            <td>Phone Number</td>
            <td>Joined At</td>
            <td>Gender</td>
            <td>DOB</td>
            <td>Marital Status</td>
            <td>Educational Status</td>
            <td>Number of Children</td>
            <td>Identity Type</td>
            <td>ID CARD</td>
            <td>Company ID</td>
            <td>Passport</td>
            <td>Address 1</td>
            <td>Address 2</td>
            <td>L.G.A</td>
            <td>City</td>
            <td>State</td>
            <td>Bank</td>
            <td>Account Number</td>
            <td>BVN</td>
            <td>Employer</td>
            <td>Industry</td>
            <td>L.G.A</td>
            <td>Staff Type</td>
            <td>City</td>
            <td>State</td>
            <td>Employer's Phone</td>
            <td>Monthly Income</td>
            <td>Address</td>
            <td>Official Email</td>
            <td>Wallet</td>
            <td>Loans</td>
            <td>Transactions</td>
        </tr>
        @foreach($df as $key => $user)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ ucwords($user->first_name) }}</td>
                <td>{{ ucwords($user->last_name) }}</td>
                <td>{{ ucwords($user->middle_name) }}</td>
                <td>{{ strtolower($user->email) }}</td>
                <td>{{ $user->mobile }}</td>
                <td>{{ date('M d, Y h:i A', strtotime($user->created_at)) }}</td>
                @if($user->my_details)
                    @php
                    $details = $user->my_details->data();
                    $banks = SuperM::paystackGet('https://api.paystack.co/bank')->data;
                    $id = SuperM::search_banks($details->financial_info->bank, $banks);
                    $bank = $banks[$id]->name;
                    @endphp
                    <td>{{ $details->personal_info->gender }}</td>
                    <td>{{ $details->personal_info->date_of_birth }}</td>
                    <td>{{ $details->personal_info->marital_status }}</td>
                    <td>{{ $details->personal_info->educational_status }}</td>
                    <td>{{ $details->personal_info->number_of_children }}</td>
                    <td>{{ $details->identity->identity_type }}</td>
                    <td>{{ asset($details->identity->id) }}</td>
                    <td>{{ asset($details->identity->company_id) }}</td>
                    <td>{{ asset($details->identity->passport) }}</td>
                    <td>{{ $details->address_info->address_1 }}</td>
                    <td>{{ $details->address_info->address_2 }}</td>
                    <td>{{ $details->address_info->lga_of_residence }}</td>
                    <td>{{ $details->address_info->city_of_residence }}</td>
                    <td>{{ $details->address_info->state_of_residence }}</td>
                    <td>{{ $bank }}</td>
                    <td>{{ $details->financial_info->account_number }}</td>
                    <td>{{ $details->financial_info->bvn }}</td>
                    <td>{{ $details->employment_info->employer }}</td>
                    <td>{{ $details->employment_info->industry }}</td>
                    <td>{{ $details->employment_info->office_lga }}</td>
                    <td>{{ $details->employment_info->staff_type }}</td>
                    <td>{{ $details->employment_info->office_city }}</td>
                    <td>{{ $details->employment_info->office_state }}</td>
                    <td>{{ $details->employment_info->employer_phone }}</td>
                    <td>{{ $details->employment_info->monthly_income }}</td>
                    <td>{{ $details->employment_info->office_address }}</td>
                    <td>{{ $details->employment_info->official_email }}</td>
                @else
                    @for($i=0;$i<27;$i++)
                    <td>-</td>
                    @endfor
                @endif
                <td>{{ number_format($user->wallet->amount,2) }}</td>
                <td>{{ $user->loans->count() }}</td>
                <td>{{ number_format($user->transactions()->sum('details->amount'),2) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>