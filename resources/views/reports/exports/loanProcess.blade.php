@php
use App\Entities\Loan\Loan;
use App\Http\Controllers\GlobalMethods as SuperM;
$marketer = urldecode($_GET['marketer']);
$loans = Loan::where('created_at', '!=', null);
$type = urldecode($_GET['status']);
if(isset($_GET['range']) && $_GET['range'] != null){
    $range = urldecode($_GET['range']);
    $arr = explode(' - ', $range);
    $start = new DateTime($arr[0]);
    $end = new DateTime($arr[1]);
    $loans->whereBetween('created_at',[$start,$end]);
}
if($type == "Pending") $loans->doesntHave('status');
elseif($type == "Declined") $loans->wherehas('status', function($query){ $query->where('status', 'declined'); });
elseif($type == "Approved") $loans->wherehas('status', function($query){ $query->where('status', 'approved'); });
elseif($type == "Active") $loans->wherehas('status', function($query){ $query->where('status', 'active'); });
elseif($type == "Repaid") $loans->wherehas('status', function($query){ $query->where('status', 'repaid'); });
if($marketer != "All")
$loans->whereHas('user', function($query){
    $query->doesntHave('my_marketer')->orWhereHas('my_marketer', function($query) {
        $query->where('email', auth()->user()->email);
    });
});
$df = $loans->get();
@endphp
<table>
    <thead>
        <tr>
            <th>S/N</th>
            <th colspan="3">CUSTOMER DETAILS</th>
            <th colspan="5">LOAN DETAILS</th>
            <th colspan="12">OFFER DETAILS</th>
            <th>DATE DISBURSED</th>
            <th>STATUS</th>
            <th>DUE PAYMENT</th>
            <th>DATE DUE</th>
            <th>DEFAULT INTEREST</th>
            <th>PAYMENT RECEIVED</th>
            <th>EXPECTED MONTHLY REPAYMENT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>

            <td>Name</td>
            <td>Phone</td>
            <td>Email</td>

            <td>Tenor</td>
            <td>Amount</td>
            <td>Interest</td>
            <td>Processing Fee</td>
            <td>Date Applied</td>

            <td>Tenor</td>
            <td>Amount</td>
            <td>Interest</td>
            <td>Pay Date</td>
            <td>Moratorium</td>
            <td>Processing Fee</td>
            <td>Outstanding</td>
            <td>Facility Type</td>
            <td>Repayment Source</td>
            <td>Mode of Disbursement</td>
            <td>Date Created</td>
            <td>Date Signed</td>

            <td></td>
            <td></td>

            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @foreach($df as $key => $loan)
            @php
            $details = $loan->data();
            $repayments = SuperM::getSingleDueLoans($loan);
            @endphp
            <tr>
                <td>{{ $key+1 }}</td>

                <td>{{ $loan->user->getFullname() }}</td>
                <td>{{ $loan->user->mobile }}</td>
                <td>{{ $loan->user->email }}</td>

                <td>{{ $details->tenor }} Months</td>
                <td>{{ number_format($details->amount,2) }}</td>
                <td>{{ $details->interest }}%</td>
                <td>{{ $details->processing }}%</td>
                <td>{{ date('M D, Y h:i A', strtotime($loan->created_at)) }}</td>
                @if($loan->offer)
                    @php
                    $odetails = $loan->offer->data();
                    @endphp
                    <td>{{ $odetails->tenor }} Months</td>
                    <td>{{ number_format($odetails->amount,2) }}</td>
                    <td>{{ $odetails->interest }}%</td>
                    <td>{{ $odetails->pay_date }}</td>
                    <td>{{ $odetails->moratorium }} days</td>
                    <td>{{ $odetails->processing }}%</td>
                    <td>{{ number_format($odetails->outstanding,2) }}</td>
                    <td>{{ $odetails->facility_type }}</td>
                    <td>{{ $odetails->repayment_source }}</td>
                    <td>{{ $odetails->mode_of_disbursement }}</td>
                    <td>{{ date('M d, Y h:i A', strtotime($loan->offer->created_at)) }}</td>
                    @if($loan->offer->signature)
                        <td>{{ date('M d, Y h:i A', strtotime($loan->offer->signature->created_at)) }}</td>
                    @else
                        <td>-</td>
                    @endif
                @else
                    @for($i=0;$i<12;$i++)
                    <td>-</td>
                    @endfor
                @endif
                @if($loan->disbursement)
                    <td>{{ date('M d, Y h:i A', strtotime($loan->disbursement->created_at)) }}</td>
                @else
                    <td>-</td>
                @endif
                @if($loan->status)
                    <td>{{ $loan->status->status }}</td>
                @else
                    <td>Pending</td>
                @endif
                @if($repayments)
                    <td>{{ number_format($repayments['amount_due'],2) }}</td>
                    <td>{{ $repayments['date_due'] }}</td>
                    <td>{{ number_format($repayments['defaults'],2) }}</td>
                    <td>{{ number_format($loan->repayments()->sum('amount'),2) }}</td>
                @else
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                @endif
                <td>{{ number_format(($details->amount/$details->tenor) + ($details->amount*($details->interest/100)),2) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>