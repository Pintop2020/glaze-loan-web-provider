@extends('layouts.user.app')

@section('title', __('My Transactions'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">All Transactions</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all your transactions on our platform.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="true">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col"><span>#</span></th>
									<th class="nk-tb-col"><span>Reference</span></th>
									<th class="nk-tb-col tb-col-md"><span>Date</span></th>
									<th class="nk-tb-col"><span>Status</span></th>
									<th class="nk-tb-col tb-col-sm"><span>Type</span></th>
									<th class="nk-tb-col"><span>Amount</span></th>
									<th class="nk-tb-col tb-col-sm"><span>Method</span></th>
									<th class="nk-tb-col tb-col-md"><span>Description</span></th>
								</tr>
							</thead>
							<tbody>
								@foreach($entities as $entity)
								<tr class="nk-tb-item">
									<th class="nk-tb-col">{{ $loop->iteration }}</th>
									<th class="nk-tb-col">
										<span class="tb-lead"><a href="javascript:void(0);">{{ $entity->data()->reference }}</a></span>
									</th>
									<th class="nk-tb-col tb-col-md">
										<span class="tb-sub">{{ date('M d, Y', strtotime($entity->created_at)) }}</span>
									</th>
									<th class="nk-tb-col">
										{!! $entity->status2() !!}
									</th>
									<th class="nk-tb-col tb-col-sm">
										<span class="tb-sub">{!! $entity->type() !!}</span>
									</th>
									<th class="nk-tb-col">
										<span class="tb-lead">₦{{ number_format($entity->data()->amount,2) }}</span>
									</th>
									<th class="nk-tb-col tb-col-sm">
										<span class="tb-lead">{{ $entity->data()->payment_option }}</span>
									</th>
									<th class="nk-tb-col tb-col-md">
										<span class="tb-sub">{{ $entity->data()->description }}</span>
									</th>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection