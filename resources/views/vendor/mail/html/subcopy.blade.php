<table style="width:100%;max-width:620px;margin:0 auto;background-color:#ffffff;" class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td>
{{ Illuminate\Mail\Markdown::parse($slot) }}
</td>
</tr>
</table>
