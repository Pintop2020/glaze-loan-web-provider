<tr>
<td>
<table style="width:100%;max-width:620px;margin:0 auto;" class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="content-cell" align="center">
{{ Illuminate\Mail\Markdown::parse($slot) }}
<center>
	<p style="padding-top: 15px; font-size: 12px;">This email was sent to you as a registered user of <a style="color: #6576ff; text-decoration:none;" href="https://glazecredit.com">glazecredit.com</a>. To update your emails preferences <a style="color: #6576ff; text-decoration:none;" href="//app.glazecredit.com">click here</a>.</p>
</center>
</td>
</tr>
</table>
</td>
</tr>
