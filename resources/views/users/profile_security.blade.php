@extends('layouts.user.app')

@section('title', __('Account Security'))

@section('content')
<div class="nk-content-body">
	<div class="nk-block-head">
		<div class="nk-block-head-content">
			<div class="nk-block-head-sub"><span>Account Setting</span></div>
			<h2 class="nk-block-title fw-normal">My Profile</h2>
			<div class="nk-block-des">
				<p>You have full control to manage your own account setting. <span class="text-primary"><em class="icon ni ni-info"></em></span></p>
			</div>
		</div>
	</div>
	<ul class="nk-nav nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link" href="/profile">Personal</a>
		</li>
		<li class="nav-item active">
			<a class="nav-link" href="/security">Security</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/notifications-settings">Notifications</a>
		</li>
	</ul>
	<form action="{{ route('secure_setting') }}" method="post" id="dform">
		@csrf
		<div class="nk-block">
			<div class="nk-block-head">
				<div class="nk-block-head-content">
					<h5 class="nk-block-title">Security Settings</h5>
					<div class="nk-block-des">
						<p>These settings are helps you keep your account secure.</p>
					</div>
				</div>
			</div>
			<!-- .nk-block-head -->
			<div class="card card-bordered">
				<div class="card-inner-group">
					<div class="card-inner">
						<div class="between-center flex-wrap flex-md-nowrap g-3">
							<div class="nk-block-text">
								<h6>Save my Login Logs</h6>
								<p>You can save your all login logs including unusual logins detected.</p>
							</div>
							<div class="nk-block-actions">
								<ul class="align-center gx-3">
									<li class="order-md-last d-inline-flex">
										<div class="custom-control custom-switch mr-n2">
											<input type="checkbox" class="custom-control-input" id="activity-log" {{ $user->setting->full_data()->save_activity_log ? 'checked':'' }} name="save_activity_log" onchange="document.getElementById('dform').submit();">
											<label class="custom-control-label" for="activity-log"></label>
										</div>
									</li>
									<li>
										<a href="#recent-activity" class="link link-sm link-primary">See Recent Activity</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="card-inner">
						<div class="between-center flex-wrap flex-md-nowrap g-3">
							<div class="nk-block-text">
								<h6>Security Pin Code</h6>
								<p>You can set your pin code, we will ask you on your withdraw and transfer funds.</p>
							</div>
							<div class="nk-block-actions">
								<div class="custom-control custom-switch mr-n2">
									<input type="checkbox" disabled="true" class="custom-control-input" id="security-pin" {{ $user->account_security ? 'checked':'' }}>
									<label class="custom-control-label" for="security-pin"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="card-inner">
						<div class="between-center flex-wrap flex-md-nowrap g-3">
							<div class="nk-block-text">
								<h6>Security Question</h6>
								<p>{{ $user->security_questions->question }}</p>
							</div>
							<div class="nk-block-actions">
								<div class="custom-control custom-switch mr-n2">
									********
								</div>
							</div>
						</div>
					</div>
					<div class="card-inner">
						<div class="between-center flex-wrap flex-md-nowrap g-3">
							<div class="nk-block-text">
								<h6>Change Password</h6>
								<p>Set a unique password to protect your account.</p>
							</div>
							<div class="nk-block-actions flex-shrink-sm-0">
								<ul class="align-center flex-wrap flex-sm-nowrap gx-3 gy-2">
									<li class="order-md-last">
										<a href="/change-password" class="btn btn-primary">Change Password</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!--<div class="card-inner">
						<div class="between-center flex-wrap flex-md-nowrap g-3">
							<div class="nk-block-text">
								<h6>2FA Authentication <span class="badge badge-success">Enabled</span></h6>
								<p>Secure your account with 2FA security. When it is activated you will need to enter not only your password, but also a special code using app. You can receive this code by in mobile app. </p>
							</div>
							<div class="nk-block-actions">
								<a href="#" class="btn btn-primary">Disable</a>
							</div>
						</div>
					</div>-->
				</div>
			</div>
			<div class="nk-block-head nk-block-head-sm" id="recent-activity">
				<div class="nk-block-head-content">
					<div class="nk-block-title-group">
						<h6 class="nk-block-title title">Recent Activity</h6>
						<!--<a href="#" class="link">See full log</a>-->
					</div>
					<div class="nk-block-des">
						<p>This information about the last login activity on your account.</p>
					</div>
				</div>
			</div>
			<div class="card card-bordered">
				<table class="table table-ulogs">
					<thead class="thead-light">
						<tr>
							<th class="tb-col-os"><span class="overline-title">Browser <span class="d-sm-none">/ IP</span></span></th>
							<th class="tb-col-ip"><span class="overline-title">IP</span></th>
							<th class="tb-col-time"><span class="overline-title">Time</span></th>
						</tr>
					</thead>
					<tbody>
						@foreach($user->login_logs()->latest()->take(6)->get() as $log)
						<tr>
							<td class="tb-col-os">{{ $log->login_browser_device }}</td>
							<td class="tb-col-ip"><span class="sub-text">{{ $log->login_ip }}</span></td>
							<td class="tb-col-time"><span class="sub-text">{{ date('M d, Y h:i A', strtotime($log->created_at)) }}</span></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</form>
</div>
@endsection