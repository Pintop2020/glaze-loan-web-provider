@extends('layouts.user.app')

@section('title', __('My Profile'))

@section('content')
<div class="nk-content-body">
	<div class="nk-block-head">
		<div class="nk-block-head-content">
			<div class="nk-block-head-sub"><span>Account Setting</span></div>
			<h2 class="nk-block-title fw-normal">My Profile</h2>
			<div class="nk-block-des">
				<p>You have full control to chnge your primary details. <span class="text-primary"><em class="icon ni ni-info" data-toggle="tooltip" data-placement="right" title="You can always edit your primary info"></em></span></p>
			</div>
		</div>
	</div>
	<ul class="nk-nav nav nav-tabs">
		<li class="nav-item active">
			<a class="nav-link" href="/profile">Personal</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/security">Security</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/notifications-settings">Notifications</a>
		</li><!--
		<li class="nav-item">
			<a class="nav-link" href="html/crypto/profile-connected.html">Connect Social</a>
		</li>-->
	</ul>
	<div class="nk-block">
		@if(!$user->my_details || !$user->account_security || $user->cards()->count() < 1)
		<div class="alert alert-warning">
			<div class="alert-cta flex-wrap flex-md-nowrap">
				<div class="alert-text">
					<p>Upgrade your account to unlock full feature and increase your limit of transaction amount.</p>
				</div>
				<ul class="alert-actions gx-3 mt-3 mb-1 my-md-0">
					<li class="order-md-last">
						<a href="/kyc" class="btn btn-sm btn-warning">Upgrade</a>
					</li>
				</ul>
			</div>
		</div>
		@endif
		<div class="nk-block-head">
			<div class="nk-block-head-content">
				<h5 class="nk-block-title">Personal Information</h5>
				<div class="nk-block-des">
					<p>Basic info, like your name and address etc.</p>
				</div>
			</div>
		</div>
		<div class="nk-data data-list">
			<div class="data-head">
				<h6 class="overline-title">Basics</h6>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">First Name</span>
					<span class="data-value">{{ ucwords($user->first_name) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Last Name</span>
					<span class="data-value">{{ ucwords($user->last_name) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Other Name</span>
					<span class="data-value">{{ ucwords($user->middle_name) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Glaze ID</span>
					<span class="data-value">{{ $user->id() }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item">
				<div class="data-col">
					<span class="data-label">Email</span>
					<span class="data-value">{{ $user->email }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more disable"><em class="icon ni ni-lock-alt"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Phone Number</span>
					<span class="data-value text-soft">{{ $user->mobile }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			@if($user->my_details)
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Gender</span>
					<span class="data-value">{{ $user->my_details->data()->personal_info->gender }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Date of Birth</span>
					<span class="data-value">{{ $user->my_details->data()->personal_info->date_of_birth }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Marital Status</span>
					<span class="data-value">{{ $user->my_details->data()->personal_info->marital_status }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Educational Status</span>
					<span class="data-value">{{ $user->my_details->data()->personal_info->educational_status }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Children</span>
					<span class="data-value">{{ $user->my_details->data()->personal_info->number_of_children }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			@endif
		</div>
		@if($user->my_details)
		<div class="nk-data data-list">
			<div class="data-head">
				<h6 class="overline-title">Address Info</h6>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Address 1</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->address_info->address_1) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Address 2</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->address_info->address_2) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">LGA of Residence</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->address_info->lga_of_residence) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">City of Residence</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->address_info->city_of_residence) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">State of Residence</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->address_info->state_of_residence) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
		</div>
		@endif
		@if($user->my_details)
		<div class="nk-data data-list">
			<div class="data-head">
				<h6 class="overline-title">Employment Info</h6>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Employer</span>
					<span class="data-value">{{ $user->my_details->data()->employment_info->employer }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Industry</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->industry) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">LGA</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->office_lga) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Staff Type</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->staff_type) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">City</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->office_city) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">State</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->office_state) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Phone</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->employer_phone) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Monthly Income</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->monthly_income) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Address</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->office_address) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Official Email</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->employment_info->official_email) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
		</div>
		@endif
		@if($user->my_details)
		<div class="nk-data data-list">
			<div class="data-head">
				<h6 class="overline-title">Financial Info</h6>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">BVN</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->financial_info->bvn) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
			<div class="data-item" data-toggle="modal" data-target="#profile-edit">
				<div class="data-col">
					<span class="data-label">Account Number</span>
					<span class="data-value">{{ ucwords($user->my_details->data()->financial_info->account_number) }}</span>
				</div>
				<div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
			</div>
		</div>
		@endif
	</div>
</div>
@endsection