@extends('layouts.admin.app')

@section('title', __('Users'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					@isset($type)
					<h2 class="nk-block-title fw-normal">All {{ ucwords($type) }} Users</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all {{ strtolower($type) }} users and their profile details.</p>
					</div>
					@else
					<h2 class="nk-block-title fw-normal">All Users</h2>
					<div class="nk-block-des">
						<p class="lead">Here is a list of all users and their profile details.</p>
					</div>
					@endisset
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="true">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid">
											<label class="custom-control-label" for="uid"></label>
										</div>
									</th>
									<th class="nk-tb-col"><span class="sub-text">User</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Wallet</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Phone</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Verified</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Last Login</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right">
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $key => $user)
								<tr class="nk-tb-item">
									<td class="nk-tb-col nk-tb-col-check">
										<div class="custom-control custom-control-sm custom-checkbox notext">
											<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $user->id }}">
											<label class="custom-control-label" for="uid{{ $key }}"></label>
										</div>
									</td>
									<td class="nk-tb-col">
										<div class="user-card">
											<div class="user-avatar bg-dim-primary d-none d-sm-flex">
												@if($user->my_details) <img src="{{ asset('storage/'.$user->my_details->data()->identity->passport) }}"> @else <span>{{ $user->nameThumb() }}</span>  @endif
											</div>
											<div class="user-info">
												<span class="tb-lead">{{ $user->getFullname() }} @if(Cache::has('user-is-online-' . auth()->user()->id))<span class="dot dot-success d-md-none ml-1"></span>@endif </span>
												<span>{{ $user->email }}</span>
											</div>
										</div>
									</td>
									<td class="nk-tb-col tb-col-mb" data-order="{{ $user->wallet->amount }}">
										<span class="tb-amount">{{ number_format($user->wallet->amount,2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>{{ $user->mobile }}</span>
									</td>
									@if($user->my_details)
									<td class="nk-tb-col tb-col-lg" data-order="Email Verified - Kyc Verified">
									@else
									<td class="nk-tb-col tb-col-lg" data-order="Email Verified - Kyc Unverified">
									@endif
										<ul class="list-status">
											<li><em class="icon text-success ni ni-check-circle"></em> <span>Email</span></li>
											@if($user->my_details)
											<li><em class="icon text-success ni ni-check-circle"></em> <span>KYC</span></li>
											@else
											<li><em class="icon ni ni-alert-circle"></em> <span>KYC</span></li>
											@endif
										</ul>
									</td>
									<td class="nk-tb-col tb-col-lg">
										@if($user->login_logs()->count() > 0)
										<span>{{ date('d M Y', strtotime($user->login_logs()->orderBy('id', 'desc')->skip(1)->first()->created_at ?? $user->login_logs()->orderBy('id', 'desc')->first()->created_at)) }}</span>
										@endif
									</td>
									<td class="nk-tb-col tb-col-md">
										@if($user->active)
										<span class="tb-status text-success">Active</span>
										@else
										<span class="tb-status text-danger">Restricted</span>
										@endif
									</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
											<li>
												<div class="drodown">
													<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="link-list-opt no-bdr">
															<li><a href="/users/{{ $user->uuid() }}"><em class="icon ni ni-eye"></em><span>View Details</span></a></li>
															<li><a href="/users/perm/{{ $user->uuid() }}" class="miAction"><em class="icon ni ni-repeat"></em><span>{{ $user->active ? 'Restrict' : 'Unrestrict' }}</span></a></li>
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- .card-preview -->
			</div>
		</div>
		<!-- .components-preview -->
	</div>
</div>
@endsection