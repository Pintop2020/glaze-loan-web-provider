@extends('layouts.app')

@section('content')

<form method="post" action="#">
           <input type="hidden" name="__method" value="post">
<div class="form-group">
    <label for="id">Id</label>
    <input type="text" class="form-control" id="id" name="id" value="{{old('id',$entity ? $entity->id : "")}}" required >
</div>
<div class="form-group">
    <label for="first_name">First name</label>
    <input type="text" class="form-control" id="first_name" name="first_name" value="{{old('first_name',$entity ? $entity->first_name : "")}}" required >
</div>
<div class="form-group">
    <label for="middle_name">Middle name</label>
    <input type="text" class="form-control" id="middle_name" name="middle_name" value="{{old('middle_name',$entity ? $entity->middle_name : "")}}" required >
</div>
<div class="form-group">
    <label for="other_name">Other name</label>
    <input type="text" class="form-control" id="other_name" name="other_name" value="{{old('other_name',$entity ? $entity->other_name : "")}}" required >
</div>
<div class="form-group">
    <label for="firebase_token">Firebase token</label>
    <input type="text" class="form-control" id="firebase_token" name="firebase_token" value="{{old('firebase_token',$entity ? $entity->firebase_token : "")}}"  >
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name="email" value="{{old('email',$entity ? $entity->email : "")}}" required >
</div>
<div class="form-group">
    <label for="email_verified_at">Email verified at</label>
    <input type="text" class="form-control" id="email_verified_at" name="email_verified_at" value="{{old('email_verified_at',$entity ? $entity->email_verified_at : "")}}"  >
</div>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" value="{{old('password',$entity ? $entity->password : "")}}" required >
</div>
<div class="form-group">
    <label for="remember_token">Remember token</label>
    <input type="text" class="form-control" id="remember_token" name="remember_token" value="{{old('remember_token',$entity ? $entity->remember_token : "")}}"  >
</div>
<div class="form-group">
    <label for="created_at">Created at</label>
    <input type="text" class="form-control" id="created_at" name="created_at" value="{{old('created_at',$entity ? $entity->created_at : "")}}"  >
</div>
<div class="form-group">
    <label for="updated_at">Updated at</label>
    <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{old('updated_at',$entity ? $entity->updated_at : "")}}"  >
</div><button type='submit' class='btn btn-primary'>Submit</button></form>

@endsection