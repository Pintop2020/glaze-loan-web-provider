@extends('layouts.user.auth')

@section('content')
@if(session('error'))
	<div class="text-danger">{{ session('error') }}</div>
@endif
<div class="card card-bordered">
	<div class="card-inner card-inner-lg">
		<div class="nk-block-head">
			<div class="nk-block-head-content">
				<h4 class="nk-block-title">Let's get started!</h4>
				<div class="nk-block-des">
					<p>Login to your account</p>
				</div>
			</div>
		</div>
		<form method="post" id="priva">
			@csrf
			<div class="form-group">
				<div class="form-label-group">
					<label class="form-label" for="password">Password</label>
					<a class="link link-primary link-sm" href="{{ Route::has('password.request') }}">Forgot Code?</a>
				</div>
				<div class="form-control-wrap">
					<a href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
					<em class="passcode-icon icon-show icon ni ni-eye"></em>
					<em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
					</a>
					<input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Enter your passcode">
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-lg btn-primary btn-block onAct">Proceed</button>
			</div>
		</form>
	</div>
</div>
@endsection