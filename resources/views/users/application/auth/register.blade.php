@extends('layouts.user.application')

@section('title', __('Glaze Credit'))

@section('content')
<div class="steps-area steps-area-fixed">
	<div class="image-holder">
		<img src="{{ asset('multilevel/img/side-img.jpg') }}" alt="">
	</div>
</div>
<form class="multisteps-form__form dform" id="wizard" method="POST" action="{{ route('user.create') }}">
	@csrf
	<div class="form-area position-relative">
		<div class="multisteps-form__panel js-active" data-animation="slideHorz">
			<div class="wizard-forms">
				<div class="form-content pera-content">
					<div class="step-inner-content">
						<h2>Let’s get you started!</h2>
						<p>Hey there, let’s set up your Glaze account</p>
						<hr/>
						<input type="hidden" name="__method" value="post">
						<div class="form-inner-area">
							<div class="row">
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="first_name">First name</label>
									<input type="text" class="form-control" id="first_name" name="first_name" value="{{old('first_name')}}" >
									@error('first_name')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="middle_name">Middle name</label>
									<input type="text" class="form-control" id="middle_name" name="middle_name" value="{{old('middle_name')}}" >
									@error('middle_name')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="other_name">Surname</label>
									<input type="text" class="form-control" id="other_name" name="last_name" value="{{old('last_name')}}" >
									@error('last_name')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="phone">Phone</label>
									<input type="text" class="form-control" id="phone" name="mobile" value="{{old('phone')}}">
									@error('mobile')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-12">
									<label for="email">Email</label>
									<input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" >
									@error('email')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="password">Password</label>
									<input type="password" class="form-control" id="password" name="password" value="{{old('password')}}" >
									@error('password')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="confirm-password">Confirm Password</label>
									<input type="password" class="form-control" id="confirm-password" name="password_confirmation" >
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="security">Security Question</label>
									<div class="language-select">
										<select name="security_question" id="security">
											<option value=""></option>
											<option>What was your first car?</option>
											<option>What is your mother's maiden name?</option>
											<option>What is your father's middle name?</option>
											<option>What is your favorite food?</option>
											<option>What is the name of your first pet?</option>
											<option>What city were you born in?</option>
										</select>
									</div>
									@error('security_question')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<label for="security-answer">Security answer</label>
									<input type="text" class="form-control" id="security-answer" name="security_answer" >
									@error('security_answer')
		                                <div class="text-danger">{{ $message }}</div>
		                            @enderror
								</div>
							</div>
							<button class="btn btn-success btn-block btn-xl mb-5 onAct" title="NEXT">Create Your Account <i class="fa fa-arrow-right"></i></button>
							<div class="text-center text-muted">Already have an account? <a href="{{ route('login') }}" class="text-success">Sign in</a></div>
							<div class="text-center text-muted">By continuing, you agree to our <a href="" class="text-success">Terms and Conditions</a> of service and <a href="" class="text-success">Privacy Policy</a>.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@push('more-scripts')
<script>
	$(function(){
		$('.onAct').on("click", function (e) {
		    Swal.fire({
		      html: 'Were you referred to us by an active user?<br> Please enter the user\'s referral code below.',
		      input: 'text',
		      inputAttributes: {
		        autocapitalize: 'off'
		      },
		      showCancelButton: true,
		      cancelButtonText: 'No referral link',
		      confirmButtonText: 'Look up',
		      showLoaderOnConfirm: true,
		      inputValue: (value) => {
		      	alert(value);
		      },
		      inputValidator: (value) => {
			    if (!value) {
			      return 'You need to write the referral code!'
			    }
			  },
		      preConfirm: function preConfirm(login) {
		        return fetch("{{ url('verify_referee') }}/".concat(login)).then(function (response) {
		          if (!response.ok) {
		            throw new Error(response.statusText);
		          }

		          return response.text();
		          
		        })["catch"](function (error) {
		          Swal.showValidationMessage("Request failed: ".concat(error));
		        });
		      },
		      allowOutsideClick: function allowOutsideClick() {
		        return !Swal.isLoading();
		      }
		    }).then(function (result) {
		    	if(result.isConfirmed){
		    		if (result.value != 'error') {
				        Swal.fire({
					      title: 'Are you sure?',
					      text: 'Your referee\'s name is '.concat(result.value),
					      icon: 'warning',
					      showCancelButton: true,
					      confirmButtonText: 'Yes, I am!'
					    }).then(function (result) {
					      //isConfirmed // isDenied
					      if(result.isConfirmed){
							$('.dform').submit();
					      }
					    });
				      }else {
				      	Swal.fire("Oops!", "Invalid Referree code supplied", "error");
				      }
		    	}else {
		    		$('.dform').submit();
		    	}
		      
		    });
		    e.preventDefault();
		});
	});
</script>
@endpush