@extends('layouts.user.auth')

@section('content')
<div class="card card-bordered">
	<div class="card-inner card-inner-lg">
		<div class="nk-block-head">
			<div class="nk-block-head-content">
				<h4 class="nk-block-title">Let's get started!</h4>
				<div class="nk-block-des">
					<p>Login or Create an Account</p>
				</div>
			</div>
		</div>
		<form method="post">
			@csrf
			<div class="form-group">
				<div class="form-label-group">
					<label class="form-label" for="inputEmail">Email</label>
				</div>
				<input type="email" class="form-control" id="inputEmail" aria-describedby="email" placeholder="Enter email" name="email">
			</div>
			<div class="form-group">
				<button class="btn btn-lg btn-primary btn-block">Sign in</button>
			</div>
		</form>
	</div>
</div>
<div class="form-note-s2 pt-4"> New on our platform? <a href="/application/calculator">Create an account</a></div>
@endsection