@extends('layouts.user.application')

@section('title', __('Glaze Credit'))

@push('more-styles')
    <link rel="stylesheet" href="{{ asset('multilevel/css/custom.css') }}">
@endpush

@push('more-scripts')
    <script type="text/javascript" src="{{ asset('multilevel/js/custom.js') }}"></script>
@endpush

@section('content')
<div class="steps-area steps-area-fixed">
	<div class="image-holder">
		<img src="{{ asset('multilevel/img/side-img.jpg') }}" alt="">
	</div>
</div>
<form class="multisteps-form__form" id="wizard" method="POST">
	@csrf
	<div class="form-area position-relative">
		<div class="multisteps-form__panel js-active" data-animation="slideHorz">
			<div class="wizard-forms">
				<div class="form-content pera-content">
					<div class="step-inner-content">
						<h2>Let's get started</h2>
						<p>Get a convenient loan of up to ₦4 million today. Apply now to meet needs like growing your business, paying rent, buying a car, paying school fees, etc.</p>
						<div class="form-inner-area">
							<div class="input">
								<label>AMOUNT</label>
								<input type="text" name="amount" id="amount" class="ty" readonly>
								<table class="ddesc">
									<tr>
										<th>50,000</th>
										<th>2,000,000</th>
									</tr>
								</table>
								<input type="range" name="amountRange" id="amountRange" min="40000" max="2000000">
							</div>
							<div class="input">
								<label>TERM</label>
								<div class="termDesc ddTerm ty"><span>9</span> Months</div>
								<table class="ddesc">
									<tr>
										<th>3 Months</th>
										<th>24 Months</th>
									</tr>
								</table>
								<input type="range" name="monthRange" id="monthRange" min="3" max="24">
							</div>
							<div class="input">
								<table class="dddesc">
									<tr>
										<th>Loan</th>
										<th class="ddPrice"></th>
									</tr>
									<tr>
										<td>Term</td>
										<td class="ddTerm"></td>
									</tr>
									<tr>
										<td>Monthly Repayment</td>
										<td class="ddRepay"></td>
									</tr>
								</table>
							</div>
						</div>
						<p>The amount on the loan calculator is an initial estimate only. We will make you a loan offer based on the information you share with us in your loan application.</p>
					</div>
					<a href="{{ route('user.create') }}" class="btn btn-success btn-block btn-lg mb-5 text-white">Get Started</a>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection