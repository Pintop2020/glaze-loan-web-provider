@extends('layouts.admin.app')

@section('title', __('All Staff'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between">
				<div class="nk-block-head-content">
					<h3 class="nk-block-title page-title">Staff</h3>
					<div class="nk-block-des text-soft">
						<p>You have total {{ count($users) }} staff.</p>
					</div>
				</div>
				<!-- .nk-block-head-content -->
				<div class="nk-block-head-content">
					<div class="toggle-wrap nk-block-tools-toggle">
						<a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
						<div class="toggle-expand-content" data-content="pageMenu">
							<ul class="nk-block-tools g-3">
								<li class="nk-block-tools-opt">
									<div class="drodown">
										<a href="/staff/add" class="btn btn-primary"><em class="icon ni ni-plus"></em> <span>Add Staff</span></a>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- .toggle-wrap -->
				</div>
				<!-- .nk-block-head-content -->
			</div>
			<!-- .nk-block-between -->
		</div>
		<!-- .nk-block-head -->
		<div class="nk-block">
			<div class="row g-gs">
				@foreach($users as $staff)
					@php
					$allLoans = \App\Entities\Loan\Loan::all()->count();
					$mine = $staff->loan_tended_to()->count();
					$percent = $mine > 0 ? ($mine/$allLoans)*100 : 0;
					@endphp
				<div class="col-sm-6 col-lg-4 col-xxl-3">
					<div class="card">
						<div class="card-inner">
							<div class="team">
								@if(!$staff->active)
									<div class="team-status bg-danger text-white"><em class="icon ni ni-na"></em></div>
								@else
									@if(Cache::has('user-is-online-' . $staff->id))
										<div class="team-status bg-success text-white"><em class="icon ni ni-check-thick"></em></div>
									@else
										<div class="team-status bg-light text-black"><em class="icon ni ni-check-thick"></em></div>
									@endif
								@endif
								@if($user->hasRole('super admin'))
								<div class="team-options">
									<div class="drodown">
										<a href="#" class="dropdown-toggle btn btn-sm btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
										<div class="dropdown-menu dropdown-menu-right">
											<ul class="link-list-opt no-bdr">
												@if($staff->active)
												<li><a href="/users/perm/{{ $staff->uuid() }}" class="miAction"><em class="icon ni ni-na"></em><span>Suspend Staff</span></a></li>
												@else
												<li><a href="/users/perm/{{ $staff->uuid() }}" class="miAction"><em class="icon ni ni-na"></em><span>Suspend Staff</span></a></li>
												@endif
												<li><a href="/staff/edit/{{ $staff->uuid() }}"><em class="icon ni ni-edit"></em><span>Edit Staff</span></a></li>
											</ul>
										</div>
									</div>
								</div>
								@endif
								<div class="user-card user-card-s2">
									<div class="user-avatar md bg-primary">
										@if($staff->my_details) <img src="{{ asset($staff->my_details->data()->identity->passport) }}"> @else <span>{{ $staff->nameThumb() }}</span>  @endif
										@if(Cache::has('user-is-online-' . $staff->id))
											<div class="status dot dot-lg dot-success"></div>
										@else
											<div class="status dot dot-lg dot-light"></div>
										@endif
									</div>
									<div class="user-info">
										<h6>{{ $staff->getFullname() }}</h6>
										<span class="sub-text">{{ $staff->email }}</span>
									</div>
								</div>
								<div class="team-details">
									@foreach($staff->roles as $role)
                                    <p>{{ strtoupper($role->name) }}</p>
                                    @endforeach
                                </div>
								<ul class="team-statistics">
									<li><span>{{ round($percent,2) }}%</span><span>Loan Proceesed</span></li>
									<li><span>{{ $staff->comments()->count() + $staff->tagged_comment()->count() }}</span><span>Comments</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		<!-- .nk-block -->
	</div>
</div>
@endsection