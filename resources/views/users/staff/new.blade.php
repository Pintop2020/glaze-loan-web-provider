@extends('layouts.admin.app')

@section('title', __('Add Staff'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">Add new staff</h2>
				</div>
			</div>
			<div class="nk-block nk-block-lg">
				<div class="card">
					<div class="card-inner">
						<div class="card-head">
							<h5 class="card-title">Staff Info</h5>
						</div>
						<form action="/staff/add" method="post">
							@csrf
							<div class="row g-4">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">First name</label>
										<input type="text" name="first_name" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Last name</label>
										<input type="text" name="last_name" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Middle name</label>
										<input type="text" name="middle_name" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Phone Number</label>
										<input type="number" name="mobile" class="form-control" required>
									</div>
									@error('mobile')
									<div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label">Email</label>
										<input type="email" name="email" class="form-control" required>
									</div>
									@error('email')
									<div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-label" for="email-address-1">ROLES</label>
										<div class="form-control-wrap">
											<select class="form-select form-control form-control-lg" name="roles[]"  data-search="on" multiple>
			                                	@foreach($roles as $role)
			                                		<option value="{{ $role->id }}">{{ strtoupper($role->name) }}</option>
			                                	@endforeach
			                                </select>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group">
										<button type="submit" class="btn btn-lg btn-primary">Submit</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection