@extends('layouts.user.app')

@section('title', __('Notification Setting'))

@section('content')
<div class="nk-content-body">
	<div class="nk-block-head">
		<div class="nk-block-head-content">
			<div class="nk-block-head-sub"><span>Notification Setting</span></div>
			<h2 class="nk-block-title fw-normal">My Profile</h2>
			<div class="nk-block-des">
				<p>You have full control to manage your own account setting. <span class="text-primary"><em class="icon ni ni-info"></em></span></p>
			</div>
		</div>
	</div>
	<ul class="nk-nav nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link" href="/profile">Personal</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/security">Security</a>
		</li>
		<li class="nav-item active">
			<a class="nav-link" href="/notifications-settings">Notifications</a>
		</li>
	</ul>
	<form action="{{ route('settings') }}" method="post" id="dform">
		@csrf
		<div class="nk-block">
			<div class="nk-block-head nk-block-head-sm">
				<div class="nk-block-head-content">
					<h5 class="nk-block-title">Security Alerts</h5>
					<div class="nk-block-des">
						<p>You will get only those email notification what you want.</p>
					</div>
				</div>
			</div>
			<div class="nk-block-content">
				<div class="gy-3">
					<div class="g-item">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" id="unusual-activity" name="email_when_activity" {{ $user->setting->full_data()->email_when_activity ? 'checked':'' }} onchange="document.getElementById('dform').submit();">
							<label class="custom-control-label" for="unusual-activity">Email me whenever encounter unusual activity</label>
						</div>
					</div>
					<div class="g-item">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" name="email_when_new_browser_logged" id="new-browser" {{ $user->setting->full_data()->email_when_new_browser_logged ? 'checked':'' }} onchange="document.getElementById('dform').submit();">
							<label class="custom-control-label" for="new-browser">Email me if new browser is used to sign in</label>
						</div>
					</div>
				</div>
			</div>
			<div class="nk-block-head nk-block-head-sm">
				<div class="nk-block-head-content">
					<h6 class="nk-block-title">News</h6>
					<div class="nk-block-des">
						<p>You will get only those email notification when you want.</p>
					</div>
				</div>
			</div>
			<div class="nk-block-content">
				<div class="gy-3">
					<div class="g-item">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" id="latest-sale" {{ $user->setting->full_data()->news_letter ? 'checked':'' }} name="news_letter" onchange="document.getElementById('dform').submit();">
							<label class="custom-control-label" for="latest-sale">Notify me by email about sales, latest news, new features and updates</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection