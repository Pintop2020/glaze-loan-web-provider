@extends('layouts.error')

@section('title', __('Unauthorized'))
@section('content')
<div class="nk-content ">
	<div class="nk-block nk-block-middle wide-xs mx-auto">
		<div class="nk-block-content nk-error-ld text-center">
			<h1 class="nk-error-head">401</h1>
			<h3 class="nk-error-title">Unauthorized Access</h3>
			<p class="nk-error-text">We are very sorry for inconvenience. It looks like you don't have access to this page you are trying to access</p>
			<a href="{{ route('admin.home') }}" class="btn btn-lg btn-primary mt-2">Back To Home</a>
		</div>
	</div>
	<!-- .nk-block -->
</div>
@endsection