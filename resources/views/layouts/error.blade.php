<!DOCTYPE html>
<html lang="zxx" class="js">
	<head>
		<meta charset="utf-8">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="{{ asset('assets/css/dashlite.css') }}">
	    <link id="skin-default" rel="stylesheet" href="{{ asset('assets/css/skins/theme-green.css') }}">
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
	    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
	    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
	    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
	    <meta name="msapplication-TileColor" content="#da532c">
	    <meta name="theme-color" content="#ffffff">
	</head>
	<body class="nk-body bg-white npc-general pg-error">
		<div class="nk-app-root">
			<!-- main @s -->
			<div class="nk-main ">
				<!-- wrap @s -->
				<div class="nk-wrap nk-wrap-nosidebar">
					<!-- content @s -->
					@yield('content')
					<!-- wrap @e -->
				</div>
				<!-- content @e -->
			</div>
			<!-- main @e -->
		</div>
		<!-- app-root @e -->
		<!-- JavaScript -->
		<script src="{{ asset('assets/js/bundle.js') }}"></script>
		<script src="{{ asset('assets/js/scripts.js') }}"></script>
	</body>
</html>