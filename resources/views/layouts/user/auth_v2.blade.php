<!DOCTYPE html>
<html lang="zxx" class="js">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Glaze Credit | Dashboard</title>
		<link rel="stylesheet" href="{{ asset('assets/css/dashlite.css') }}">
		<link id="skin-default" rel="stylesheet" href="{{ asset('assets/css/skins/theme-green.css') }}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
	    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
	    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
	    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
	    <meta name="msapplication-TileColor" content="#da532c">
	    <meta name="theme-color" content="#ffffff">
	</head>
	<body class="nk-body bg-white npc-general pg-auth">
		<div class="nk-app-root">
			<div class="nk-main ">
				<div class="nk-wrap nk-wrap-nosidebar">
					<!-- content @s -->
					<div class="nk-content ">
						<div class="nk-split nk-split-page nk-split-md">
							<div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white">
								<div class="absolute-top-right d-lg-none p-3 p-sm-5">
									<a href="#" class="toggle btn-white btn btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
								</div>
								<div class="nk-block nk-block-middle nk-auth-body">
									<div class="brand-logo pb-5">
										<a href="/" class="logo-link">
			                                <img class="logo-light logo-img logo-img-lg" src="{{ asset('logo.png') }}" srcset="{{ asset('logo.png') }} 2x" alt="logo">
			                                <img class="logo-dark logo-img logo-img-lg" src="{{ asset('logo.png') }}" srcset="{{ asset('logo.png') }} 2x" alt="logo-dark">
			                            </a>
									</div>
									@yield('content')
								</div>
								<div class="nk-block nk-auth-footer">
									<div class="mt-3">
										<p>&copy; {{ date('Y') }} Glaze Credit Limited. All Rights Reserved.</p>
									</div>
								</div>
							</div>
							<div class="nk-split-content nk-split-stretch bg-lighter d-flex toggle-break-lg toggle-slide toggle-slide-right" data-content="athPromo" data-toggle-screen="lg" data-toggle-overlay="true">
								<div class="slider-wrap w-100 w-max-550px p-3 p-sm-5 m-auto">
									<div class="slider-init" data-slick='{"dots":true, "arrows":false}'>
										<div class="slider-item">
											<div class="nk-feature nk-feature-center">
												<div class="nk-feature-img">
													<img class="round" src="./images/slides/promo-a.png" srcset="./images/slides/promo-a2x.png 2x" alt="">
												</div>
												<div class="nk-feature-content py-4 p-sm-5">
													<h4>Glaze Credit</h4>
													<p>You can start to create your products easily with its user-friendly design & most completed responsive layout.</p>
												</div>
											</div>
										</div>
										<div class="slider-item">
											<div class="nk-feature nk-feature-center">
												<div class="nk-feature-img">
													<img class="round" src="./images/slides/promo-b.png" srcset="./images/slides/promo-b2x.png 2x" alt="">
												</div>
												<div class="nk-feature-content py-4 p-sm-5">
													<h4>Dashlite</h4>
													<p>You can start to create your products easily with its user-friendly design & most completed responsive layout.</p>
												</div>
											</div>
										</div>
										<div class="slider-item">
											<div class="nk-feature nk-feature-center">
												<div class="nk-feature-img">
													<img class="round" src="./images/slides/promo-c.png" srcset="./images/slides/promo-c2x.png 2x" alt="">
												</div>
												<div class="nk-feature-content py-4 p-sm-5">
													<h4>Dashlite</h4>
													<p>You can start to create your products easily with its user-friendly design & most completed responsive layout.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slider-dots"></div>
									<div class="slider-arrows"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- app-root @e -->
		<!-- JavaScript -->
		<script src="{{ asset('assets/js/bundle.js') }}"></script>
		<script src="{{ asset('assets/js/scripts.js') }}"></script>
</html>