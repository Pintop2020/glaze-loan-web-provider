<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Glaze Credit</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="{{ asset('multilevel/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
		<link rel="stylesheet" href="{{ asset('multilevel/css/animate.min.css') }}">
		<link rel="stylesheet" href="{{ asset('multilevel/css/fontawesome-all.css') }}">
		<link rel="stylesheet" href="{{ asset('multilevel/css/style.css') }}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
	    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
	    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
	    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
	    <meta name="msapplication-TileColor" content="#da532c">
	    <meta name="theme-color" content="#ffffff">
		@stack('more-styles')
	</head>
	<body class="boxed-version">
		<div class="clearfix"></div>
		<div class="wrapper">
			@yield('content')
		</div>
		<script src="{{ asset('multilevel/js/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ asset('multilevel/js/jquery.validate.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
		<script src="{{ asset('multilevel/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('multilevel/js/main.js') }}"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		@stack('more-scripts')
	</body>
</html>