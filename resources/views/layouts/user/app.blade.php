@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp
<!DOCTYPE html>
<html lang="zxx" class="js">
	<head>
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>@yield('title') || Glaze Credit</title>
	    <link rel="stylesheet" href="{{ asset('assets/css/dashlite.css?ver=2.2.0') }}">
	    <link id="skin-default" rel="stylesheet" href="{{ asset('assets/css/skins/theme-green.css?ver=2.2.0') }}">
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
	    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
	    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
	    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
	    <meta name="msapplication-TileColor" content="#da532c">
	    <meta name="theme-color" content="#ffffff">
		@stack('more-styles')
	</head>
	<body class="nk-body npc-crypto bg-white has-sidebar ">
	    <div class="nk-app-root">
	        <!-- main @s -->
	        <div class="nk-main ">
	            <!-- sidebar @s -->
	            <div class="nk-sidebar nk-sidebar-fixed " data-content="sidebarMenu">
	                <div class="nk-sidebar-element nk-sidebar-head">
	                    <div class="nk-sidebar-brand">
	                        <a href="/dashboard" class="logo-link nk-sidebar-logo">
	                            <img class="logo-light logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo">
									<img class="logo-dark logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo-dark">
	                        </a>
	                    </div>
	                    <div class="nk-menu-trigger mr-n2">
	                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
	                    </div>
	                </div><!-- .nk-sidebar-element -->
	                <div class="nk-sidebar-element">
	                    <div class="nk-sidebar-body" data-simplebar>
	                        <div class="nk-sidebar-content">
	                            <div class="nk-sidebar-widget d-none d-xl-block">
	                                <div class="user-account-info between-center">
	                                    <div class="user-account-main">
	                                        <h6 class="overline-title-alt">Available Balance</h6>
	                                        <div class="user-balance">{{ number_format(auth()->user()->wallet->amount, 2) }} <small class="currency currency-btc">NGN</small></div>
	                                    </div>
	                                    <a href="/wallet" class="btn btn-white btn-icon btn-light"><em class="icon ni ni-line-chart"></em></a>
	                                </div>
	                                <ul class="user-account-data gy-1">
	                                    <li>
	                                        <div class="user-account-label">
	                                            <span class="sub-text">Profits (7d)</span>
	                                        </div>
	                                        <div class="user-account-value">
	                                            <span class="lead-text">+ {{ SuperM::getEarnings(7)['earnings'] }} <span class="currency currency-btc">NGN</span></span>
	                                            @if(SuperM::getEarnings(7)['this_day'] > SuperM::getEarnings(7)['last_day'])
	                                            <span class="text-success ml-2">{{ round(SuperM::getEarnings(7)['this_day'],1) }}% <em class="icon ni ni-arrow-long-up"></em></span>
	                                            @else
	                                            <span class="text-danger ml-2">{{ round(SuperM::getEarnings(7)['last_day'],1) }}% <em class="icon ni ni-arrow-long-down"></em></span>
	                                            @endif
	                                        </div>
	                                    </li>
	                                    <li>
	                                        <div class="user-account-label">
	                                            <span class="sub-text">Transactions</span>
	                                        </div>
	                                        <div class="user-account-value">
	                                            <span class="sub-text">{{ number_format(auth()->user()->transactions()->sum('details->amount'),2) }} <span class="currency currency-btc">NGN</span></span>
	                                        </div>
	                                    </li>
	                                </ul>
	                                <div class="user-account-actions">
	                                    <ul class="g-3">
	                                        <li><a href="/wallet/deposit" class="btn btn-lg btn-primary"><span>Deposit</span></a></li>
	                                        <li><a href="/wallet/withdraw" class="btn btn-lg btn-warning"><span>Withdraw</span></a></li>
	                                    </ul>
	                                </div>
	                            </div><!-- .nk-sidebar-widget -->
	                            <div class="nk-sidebar-widget nk-sidebar-widget-full d-xl-none pt-0">
	                                <a class="nk-profile-toggle toggle-expand" data-target="sidebarProfile" href="#">
	                                    <div class="user-card-wrap">
	                                        <div class="user-card">
	                                            <div class="user-avatar">
	                                                @if(auth()->user()->my_details) <img src="{{ asset('storage/'.json_decode(auth()->user()->my_details->details)->identity->passport) }}"> @else <span>{{ auth()->user()->nameThumb() }}</span>  @endif
	                                            </div>
	                                            <div class="user-info">
	                                                <span class="lead-text">{{ auth()->user()->getFullname() }}</span>
	                                                <span class="sub-text">{{ auth()->user()->id() }}</span>
	                                            </div>
	                                            <div class="user-action">
	                                                <em class="icon ni ni-chevron-down"></em>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </a>
	                                <div class="nk-profile-content toggle-expand-content" data-content="sidebarProfile">
	                                    <div class="user-account-info between-center">
	                                        <div class="user-account-main">
	                                            <h6 class="overline-title-alt">Available Balance</h6>
	                                            <div class="user-balance">{{ number_format(auth()->user()->wallet->amount, 2) }} <small class="currency currency-btc">NGN</small></div>
	                                            <div class="user-balance-alt text-danger">{{ number_format(auth()->user()->getOwing(),2) }} <span class="currency currency-btc">NGN</span></div>
	                                        </div>
	                                        <a href="/wallet" class="btn btn-icon btn-light"><em class="icon ni ni-line-chart"></em></a>
	                                    </div>
	                                    <ul class="user-account-data">
	                                        <li>
	                                            <div class="user-account-label">
	                                                <span class="sub-text">Profits (7d)</span>
	                                            </div>
	                                            <div class="user-account-value">
		                                            <span class="lead-text">+ {{ SuperM::getEarnings(7)['earnings'] }} <span class="currency currency-btc">NGN</span></span>
		                                            @if(SuperM::getEarnings(7)['this_day'] > SuperM::getEarnings(7)['last_day'])
		                                            <span class="text-success ml-2">{{ round(SuperM::getEarnings(7)['this_day'],1) }}% <em class="icon ni ni-arrow-long-up"></em></span>
		                                            @else
		                                            <span class="text-danger ml-2">{{ round(SuperM::getEarnings(7)['last_day'],1) }}% <em class="icon ni ni-arrow-long-down"></em></span>
		                                            @endif
		                                        </div>
	                                        </li>
	                                        <li>
	                                            <div class="user-account-label">
	                                                <span class="sub-text">Deposit in orders</span>
	                                            </div>
	                                            <div class="user-account-value">
	                                                <span class="sub-text text-base">0.005400 <span class="currency currency-btc">BTC</span></span>
	                                            </div>
	                                        </li>
	                                    </ul>
	                                    <ul class="user-account-links">
	                                        <li><a href="/wallet/withdraw" class="link"><span>Withdraw Funds</span> <em class="icon ni ni-wallet-out"></em></a></li>
	                                        <li><a href="/wallet/deposit" class="link"><span>Deposit Funds</span> <em class="icon ni ni-wallet-in"></em></a></li>
	                                    </ul>
	                                    <ul class="link-list">
	                                        <ul class="link-list">
												<li><a href="/profile"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
												<li><a href="/profile-setting"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
												<li><a href="#"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li>
												<li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
											</ul>
	                                    </ul>
	                                    <ul class="link-list">
	                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
		                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
		                                        @csrf
		                                    </form>
	                                    </ul>
	                                </div>
	                            </div><!-- .nk-sidebar-widget -->
	                            <div class="nk-sidebar-menu">
	                                <!-- Menu -->
	                                <ul class="nk-menu">
	                                    <li class="nk-menu-heading">
	                                        <h6 class="overline-title">Dashboard</h6>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="/dashboard" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-dashboard"></em></span>
	                                            <span class="nk-menu-text">Overview</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item has-sub">
	                                        <a href="#" class="nk-menu-link nk-menu-toggle">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-trend-up"></em></span>
	                                            <span class="nk-menu-text">Loans</span>
	                                        </a>
	                                        <ul class="nk-menu-sub">
	                                            <li class="nk-menu-item">
	                                                <a href="/loans" class="nk-menu-link"><span class="nk-menu-text">All Loans</span></a>
	                                            </li>
	                                            <li class="nk-menu-item">
	                                                <a href="/loans/create" class="nk-menu-link"><span class="nk-menu-text">New Loan</span><span class="nk-menu-badge badge-danger">HOT</span></a>
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="#" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-user-c"></em></span>
	                                            <span class="nk-menu-text">Invesments</span><span class="nk-menu-badge badge-danger">COMING SOON</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="/transactions" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-growth"></em></span>
	                                            <span class="nk-menu-text">Transactions</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item has-sub">
	                                        <a href="#" class="nk-menu-link nk-menu-toggle">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-trend-up"></em></span>
	                                            <span class="nk-menu-text">Earn</span>
	                                        </a>
	                                        <ul class="nk-menu-sub">
	                                            <li class="nk-menu-item">
	                                                <a href="#" class="nk-menu-link"><span class="nk-menu-text">Coupon</span><span class="nk-menu-badge badge-danger">COMING SOON</span></a>
	                                            </li>
	                                            <li class="nk-menu-item">
	                                                <a href="/referrals" class="nk-menu-link"><span class="nk-menu-text">Referrals</span></a>
	                                            </li>
	                                        </ul>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="/wallet" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-wallet-alt"></em></span>
	                                            <span class="nk-menu-text">Wallets</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="#" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
	                                            <span class="nk-menu-text">Buy Airtime</span><span class="nk-menu-badge badge-danger">COMING SOON</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="#" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-repeat"></em></span>
	                                            <span class="nk-menu-text">Transfer funds</span><span class="nk-menu-badge badge-danger">COMING SOON</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item">
	                                        <a href="#" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-master-card"></em></span>
	                                            <span class="nk-menu-text">Pay Bills</span><span class="nk-menu-badge badge-danger">COMING SOON</span>
	                                        </a>
	                                    </li>
	                                    <li class="nk-menu-item has-sub">
	                                        <a href="#" class="nk-menu-link nk-menu-toggle">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-help-alt"></em></span>
	                                            <span class="nk-menu-text">Support</span>
	                                        </a>
	                                        <ul class="nk-menu-sub">
	                                            <li class="nk-menu-item">
	                                                <a href="/supports/create" class="nk-menu-link"><span class="nk-menu-text">Create Ticket</span></a>
	                                            </li>
	                                            <li class="nk-menu-item">
	                                                <a href="/supports" class="nk-menu-link"><span class="nk-menu-text">My Tickets</span></a>
	                                            </li>
	                                        </ul>
	                                    </li>
	                                </ul>
	                            </div>
	                            <div class="nk-sidebar-footer">
	                                <ul class="nk-menu nk-menu-footer">
	                                    <li class="nk-menu-item">
	                                        <a href="#" class="nk-menu-link">
	                                            <span class="nk-menu-icon"><em class="icon ni ni-help-alt"></em></span>
	                                            <span class="nk-menu-text">Support</span>
	                                        </a>
	                                    </li>
	                                </ul><!-- .nk-footer-menu -->
	                            </div><!-- .nk-sidebar-footer -->
	                        </div><!-- .nk-sidebar-content -->
	                    </div><!-- .nk-sidebar-body -->
	                </div><!-- .nk-sidebar-element -->
	            </div>
	            <!-- sidebar @e -->
	            <!-- wrap @s -->
	            <div class="nk-wrap ">
	                <!-- main header @s -->
	                <div class="nk-header nk-header-fluid nk-header-fixed is-light">
	                    <div class="container-fluid">
	                        <div class="nk-header-wrap">
	                            <div class="nk-menu-trigger d-xl-none ml-n1">
	                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
	                            </div>
	                            <div class="nk-header-brand d-xl-none">
	                                <a href="html/crypto/index.html" class="logo-link">
	                                    <img class="logo-light logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo">
										<img class="logo-dark logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo-dark">
	                                </a>
	                            </div>
	                            @if(auth()->user()->unreadNotifications()->count() > 0)
	                            <div class="nk-header-news d-none d-xl-block">
	                                <div class="nk-news-list">
	                                    <a class="nk-news-item" href="/notifications/view/{{ auth()->user()->unreadNotifications()->first()->id }}">
	                                        <div class="nk-news-icon">
	                                            <em class="icon ni ni-card-view"></em>
	                                        </div>
	                                        <div class="nk-news-text">
                                        		<p> {!! auth()->user()->unreadNotifications()->first()->data['body'] !!}</p>
	                                            <em class="icon ni ni-external"></em>
	                                        </div>
	                                    </a>
	                                </div>
	                            </div>
	                            @endif
	                            <div class="nk-header-tools">
	                                <ul class="nk-quick-nav">
	                                    <li class="dropdown user-dropdown">
	                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                                            <div class="user-toggle">
	                                                <div class="user-avatar sm">
	                                                    <em class="icon ni ni-user-alt"></em>
	                                                </div>
	                                                <div class="user-info d-none d-md-block">
	                                                	@if(auth()->user()->hasVerifiedEmail())
														<div class="user-status user-status-verified">Verified</div>
														@else
														<div class="user-status user-status-unverified">Unverified</div>
														@endif
	                                                    <div class="user-name dropdown-indicator">{{ auth()->user()->getFullname() }}</div>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
	                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
	                                                <div class="user-card">
	                                                    <div class="user-avatar">
	                                                        @if(auth()->user()->my_details) <img src="{{ asset('storage/'.json_decode(auth()->user()->my_details->details)->identity->passport) }}"> @else <span>{{ auth()->user()->nameThumb() }}</span>  @endif
	                                                    </div>
	                                                    <div class="user-info">
	                                                        <span class="lead-text">{{ auth()->user()->getFullname() }}</span>
	                                                        <span class="sub-text">{{ auth()->user()->id() }}</span>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="dropdown-inner user-account-info">
	                                                <h6 class="overline-title-alt">Account Balance</h6>
													<div class="user-balance">{{ number_format(auth()->user()->wallet->amount, 2) }} <small class="currency currency-usd">NGN</small></div>
													<div class="user-balance-sub">Owing <span class="text-danger">{{ number_format(auth()->user()->getOwing(),2) }} <span class="currency currency-usd">NGN</span></span></div>
													<a href="/wallet/withdraw" class="link"><span>Withdraw Balance</span> <em class="icon ni ni-wallet-out"></em></a>
	                                            </div>
	                                            <div class="dropdown-inner">
	                                                <ul class="link-list">
	                                                    <li><a href="/profile"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
														<li><a href="/security"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
														<li><a href="/notifications-settings"><em class="icon ni ni-activity-alt"></em><span>Notification Setting</span></a></li>
														<li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="dropdown-inner">
	                                                <ul class="link-list">
	                                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </li>
	                                    <li class="dropdown notification-dropdown mr-n1">
	                                        <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
												@if(count(auth()->user()->unreadNotifications) > 0)<div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div> @else <div class="icon-status icon-status-none"><em class="icon ni ni-bell"></em></div>@endif
											</a>
											<div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
												<div class="dropdown-head">
													<span class="sub-title nk-dropdown-title">Notifications</span>
													<a href="/notifications/read_all">Mark All as Read</a>
												</div>
												<div class="dropdown-body">
													<div class="nk-notification">
														@forelse(auth()->user()->unreadNotifications as $notification)
														<a href="/notifications/view/{{ $notification->id }}">
															<div class="nk-notification-item dropdown-inner">
																<div class="nk-notification-icon">
																	<em class="{{ $notification->data['icon'] }}"></em>
																</div>
																<div class="nk-notification-content">
																	<div class="nk-notification-text">{!! $notification->data['body'] !!}</div>
																	<div class="nk-notification-time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans() }}</div>
																</div>
															</div>
														</a>
														@empty
															<div class="nk-notification-item dropdown-inner">
																No, Notification found!
															</div>
														@endforelse
													</div>
												</div><!--
												<div class="dropdown-foot center">
													<a href="/notifications">View All</a>
												</div>-->
											</div>
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <!-- main header @e -->
	                <!-- content @s -->
	                @hasSection('theB')
	                	@yield('theB')
	                @endif
	                @sectionMissing('theB')
	                	<div class="nk-content nk-content-fluid"><div class="container-xl wide-lg">
	                @endif
	                        @if(session('error'))
								{!! session('error') !!}
							@endif
							@yield('content')
	                    </div>
	                </div>
	                <!-- content @e -->
	                <!-- footer @s -->
	                <div class="nk-footer nk-footer-fluid">
	                    <div class="container-fluid">
	                        <div class="nk-footer-wrap">
	                            <div class="nk-footer-copyright"> &copy; {{ date('Y') }} Glaze Credit Limited. </div>
								<div class="nk-footer-links">
									Powered by <a href="//pintoptechnologies.com" target="_blank">Pintop Technologies</a>
								</div>
	                        </div>
	                    </div>
	                </div>
	                <!-- footer @e -->
	            </div>
	            <!-- wrap @e -->
	        </div>
	        <!-- main @e -->
	    </div>
	    <!-- app-root @e -->
	    <!-- JavaScript -->
	    <script src="{{ asset('assets/js/bundle.js') }}"></script>
	    <script src="{{ asset('assets/js/scripts.js') }}"></script>
	    @stack('more-scripts')
		@if(session('error_bottom'))
			{!! session('error_bottom') !!}
		@endif
	</body>
</html>