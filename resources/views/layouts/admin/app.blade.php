@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp
<html lang="zxx" class="js">
  <head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') | Glaze Credit</title>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/css/dashlite.css') }}">
    <link id="skin-default" rel="stylesheet" href="{{ asset('admin_assets/assets/css/skins/theme-green.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    @stack('more-styles')
  </head>
  <body class="nk-body bg-lighter npc-default has-sidebar ">
    <div class="nk-app-root">
      <div class="nk-main ">
        <div class="nk-sidebar nk-sidebar-fixed is-light " data-content="sidebarMenu">
          <div class="nk-sidebar-element nk-sidebar-head">
            <div class="nk-sidebar-brand">
              <a href="/" class="logo-link nk-sidebar-logo">
              <img class="logo-light logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo">
              <img class="logo-dark logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo-dark">
              <img class="logo-small logo-img logo-img-small" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo-small">
              </a>
            </div>
            <div class="nk-menu-trigger mr-n2">
              <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
              <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
            </div>
          </div>
          <!-- .nk-sidebar-element -->
          <div class="nk-sidebar-element">
            <div class="nk-sidebar-content">
              <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">
                  <li class="nk-menu-heading">
                    <h6 class="overline-title text-primary-alt">Dashboard</h6>
                  </li>
                  <!-- .nk-menu-item -->
                  <li class="nk-menu-item">
                    <a href="{{ route('admin.home') }}" class="nk-menu-link">
                    <span class="nk-menu-icon"><em class="icon ni ni-dashboard"></em></span>
                    <span class="nk-menu-text">Dashboard</span>
                    </a>
                  </li>
                  <li class="nk-menu-heading">
                    <h6 class="overline-title text-primary-alt">Applications</h6>
                  </li>
                  <li class="nk-menu-item">
                    <a href="/folders" class="nk-menu-link">
                    <span class="nk-menu-icon"><em class="icon ni ni-folder"></em></span>
                    <span class="nk-menu-text">File Manager</span>
                    </a>
                  </li>
                  <li class="nk-menu-heading">
                    <h6 class="overline-title text-primary-alt">Pages</h6>
                  </li>
                  <li class="nk-menu-item has-sub">
                    <a href="#" class="nk-menu-link nk-menu-toggle">
                    <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                    <span class="nk-menu-text">Users</span>
                    </a>
                    <ul class="nk-menu-sub">
                      <li class="nk-menu-item">
                        <a href="/users" class="nk-menu-link"><span class="nk-menu-text">All Users</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/platform/users/restricted" class="nk-menu-link"><span class="nk-menu-text">Restricted Users</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/platform/users/active" class="nk-menu-link"><span class="nk-menu-text">Active Users</span></a>
                      </li>
                    </ul>
                  </li>
                  <li class="nk-menu-item has-sub">
                    <a href="#" class="nk-menu-link nk-menu-toggle">
                    <span class="nk-menu-icon"><em class="icon ni ni-files-fill"></em></span>
                    <span class="nk-menu-text">Loans</span>
                    </a>
                    <ul class="nk-menu-sub">
                      <li class="nk-menu-item">
                        <a href="/loans/get/active" class="nk-menu-link"><span class="nk-menu-text">Active Loans</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/loans/get/approved" class="nk-menu-link"><span class="nk-menu-text">Approved Loans</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/loans/get/pending" class="nk-menu-link"><span class="nk-menu-text">Pending Loans</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/loans/get/declined" class="nk-menu-link"><span class="nk-menu-text">Declined Loans</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/loans/get/repaid" class="nk-menu-link"><span class="nk-menu-text">Repaid Loans</span></a>
                      </li>
                    </ul>
                  </li>
                  <li class="nk-menu-item">
                    <a href="/referrals" class="nk-menu-link">
                    <span class="nk-menu-icon"><em class="icon ni ni-user-list"></em></span>
                    <span class="nk-menu-text">Referral</span>
                    </a>
                  </li>
                  @if(auth()->user()->hasRole(['super admin', 'coo', 'director', 'chairman']))
                  <li class="nk-menu-item">
                    <a href="/staff" class="nk-menu-link">
                    <span class="nk-menu-icon"><em class="icon ni ni-user-list-fill"></em></span>
                    <span class="nk-menu-text">Staff</span>
                    </a>
                  </li>
                  @endif
                  @if(auth()->user()->hasRole(['super admin', 'coo', 'director', 'chairman', 'transaction officer']))
                  <li class="nk-menu-item has-sub">
                    <a href="#" class="nk-menu-link nk-menu-toggle">
                    <span class="nk-menu-icon"><em class="icon ni ni-tranx"></em></span>
                    <span class="nk-menu-text">Transactions</span>
                    </a>
                    <ul class="nk-menu-sub">
                      <li class="nk-menu-item">
                        <a href="/repayments" class="nk-menu-link"><span class="nk-menu-text">Repayments</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="#" class="nk-menu-link"><span class="nk-menu-text">Investments</span><span class="nk-menu-badge badge-danger">Coming Soon</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/transactions" class="nk-menu-link"><span class="nk-menu-text">Transactions</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="#" class="nk-menu-link"><span class="nk-menu-text">Others</span><span class="nk-menu-badge badge-danger">Coming Soon</span></a>
                      </li>
                    </ul>
                  </li>
                  <li class="nk-menu-item">
                    <a href="#" class="nk-menu-link">
                    <span class="nk-menu-icon"><em class="icon ni ni-repeat-fill"></em></span>
                    <span class="nk-menu-text">Investments</span>
                    <span class="nk-menu-badge badge-danger">Coming Soon</span>
                    </a>
                  </li>
                  @endif
                  <li class="nk-menu-item has-sub">
                    <a href="#" class="nk-menu-link nk-menu-toggle">
                    <span class="nk-menu-icon"><em class="icon ni ni-growth"></em></span>
                    <span class="nk-menu-text">Reports</span>
                    </a>
                    <ul class="nk-menu-sub">
                      @if(auth()->user()->hasRole('marketer'))
                      <li class="nk-menu-item">
                        <a href="/reports/customer-base" class="nk-menu-link"><span class="nk-menu-text">Customer base</span></a>
                      </li>
                      @endif
                      @if(auth()->user()->hasRole(['risk', 'coo', 'transaction officer', 'director', 'chairman']))
                      <li class="nk-menu-item">
                        <a href="/reports/loan-reports" class="nk-menu-link"><span class="nk-menu-text">Loans</span></a>
                      </li>
                      <li class="nk-menu-item">
                        <a href="/reports/loan-process" class="nk-menu-link"><span class="nk-menu-text">Loan Process</span></a>
                      </li>
                      @endif
                    </ul>
                  </li>
                  @if(auth()->user()->hasRole(['super admin', 'support']))
                  <li class="nk-menu-item">
                    <a href="/supportResponses" class="nk-menu-link">
                    <span class="nk-menu-icon"><em class="icon ni ni-help-alt"></em></span>
                    <span class="nk-menu-text">Supports</span>
                    </a>
                  </li>
                  @endif
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="nk-wrap ">
          <div class="nk-header nk-header-fixed is-light">
            <div class="container-fluid">
              <div class="nk-header-wrap">
                <div class="nk-menu-trigger d-xl-none ml-n1">
                  <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                </div>
                <div class="nk-header-brand d-xl-none">
                  <a href="{{ route('admin.home') }}" class="logo-link">
                  <img class="logo-light logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo">
                  <img class="logo-dark logo-img" src="{{ asset('logo-icon.png') }}" srcset="{{ asset('logo-icon.png') }} 2x" alt="logo-dark">
                  </a>
                </div>
                <!-- .nk-header-brand -->
                <div class="nk-header-tools">
                  <ul class="nk-quick-nav">
                    <li class="dropdown chats-dropdown hide-mb-xs">
                      <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
                        @if(auth()->user()->tagged_comment()->doesntHave('user_read')->count() > 0)
                        <div class="icon-status icon-status-info"><em class="icon ni ni-comments"></em></div>
                        @else 
                        <div class="icon-status icon-status-none"><em class="icon ni ni-comments"></em></div>
                        @endif
                      </a>
                      <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                        <div class="dropdown-head">
                          <span class="sub-title nk-dropdown-title">Recent Chats</span>
                        </div>
                        <div class="dropdown-body">
                          <ul class="chat-list">
                            @forelse(auth()->user()->tagged_comment()->doesntHave('user_read')->get() as $comment)
                            <li class="chat-item">
                              <a class="chat-link" href="/comments/{{ $comment->loan()->first()->id }}">
                                <div class="chat-media user-avatar bg-{{ SuperM::getRandom() }}">
                                  <span>{{ $comment->user->nameThumb() }}</span>
                                  @if(Cache::has('user-is-online-' . $comment->user->id))
                                  <span class="status dot dot-lg dot-success"></span>
                                  @else
                                  <span class="status dot dot-lg dot-gray"></span>
                                  @endif
                                </div>
                                <div class="chat-info">
                                  <div class="chat-from">
                                    <div class="name">{{ $comment->user->getFullname() }}</div>
                                    <span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}</span>
                                  </div>
                                  <div class="chat-context">
                                    <div class="text">{{ $comment->comments }}</div>
                                    <div class="status unread">
                                        <em class="icon ni ni-bullet-fill"></em>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </li>
                            @empty
                            <li class="chat-item">
                              <a class="chat-link" href="#">
                                <div class="chat-info">
                                  No, Comment found!
                                </div>
                              </a>
                            </li>
                            @endforelse
                          </ul>
                        </div>
                        <div class="dropdown-foot center">
                          <a href="/comments">View All</a>
                        </div>
                      </div>
                    </li>
                    <li class="dropdown user-dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <div class="user-toggle">
                          <div class="user-avatar sm">
                            <em class="icon ni ni-user-alt"></em>
                          </div>
                          <div class="user-info d-none d-xl-block">
                            @if(auth()->user()->hasVerifiedEmail())
                            <div class="user-status user-status-verified">Verified</div>
                            @else
                            <div class="user-status user-status-unverified">Unverified</div>
                            @endif
                            <div class="user-name dropdown-indicator">{{ ucwords(auth()->user()->getFullname()) }}</div>
                          </div>
                        </div>
                      </a>
                      <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                        <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                          <div class="user-card">
                            <div class="user-avatar">
                              @if(auth()->user()->my_details) <img src="{{ asset(json_decode(auth()->user()->my_details->details)->identity->passport) }}"> @else <span>{{ auth()->user()->nameThumb() }}</span>  @endif
                            </div>
                            <div class="user-info">
                              <span class="lead-text">{{ ucwords(auth()->user()->getFullname()) }}</span>
                              <span class="sub-text">{{ strtolower(auth()->user()->email)  }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="dropdown-inner">
                          <ul class="link-list">
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                            </form>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- .nk-header-wrap -->
            </div>
            <!-- .container-fliud -->
          </div>
          <!-- main header @e -->
          <!-- content @s -->
          @hasSection('theB')
            @yield('theB')
          @endif
          @sectionMissing('theB')
          <div class="nk-content ">
            <div class="container-fluid">
          @endif
              @if(session('error'))
              {!! session('error') !!}
              @endif
              @yield('content')
            </div>
          </div>
          <div class="return"></div>
          <!-- content @e -->
          <!-- footer @s -->
          <div class="nk-footer">
            <div class="container-fluid">
              <div class="nk-footer-wrap">
                <div class="nk-footer-copyright"> &copy; {{ date('Y') }} Glaze Credit Limited. </div>
                <div class="nk-footer-links">
                  Powered by <a href="//pintoptechnologies.com" target="_blank">Pintop Technologies</a>
                </div>
              </div>
            </div>
          </div>
          <!-- footer @e -->
        </div>
        <!-- wrap @e -->
      </div>
      <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="{{ asset('admin_assets/assets/js/bundle.js') }}"></script>
    <script src="{{ asset('admin_assets/assets/js/scripts.js') }}"></script>
    <script src="{{ asset('admin_assets/assets/js/custom/main.js') }}"></script>
    @if(session('error_bottom'))
    {!! session('error_bottom') !!}
    @endif
    @stack('more-scripts')
  </body>
</html>