@extends('layouts.admin.app')

@section('title', __('Viewing user > Login Logs'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between g-3">
				<div class="nk-block-head-content">
					<h3 class="nk-block-title page-title">Users / <strong class="text-primary small">{{ $user->getFullname() }}</strong></h3>
					<div class="nk-block-des text-soft">
						<ul class="list-inline">
							<li>User ID: <span class="text-base">GID{{ str_pad($user->id,6,0,STR_PAD_LEFT) }}</span></li>
							<li>Last Login: @if($user->login_logs()->count() > 0) <span class="text-base">{{ date('d M, Y h:i A', strtotime($user->login_logs()->orderBy('id', 'desc')->skip(1)->first()->created_at ?? $user->login_logs()->orderBy('id', 'desc')->first()->created_at)) }}</span> @endif </li>
						</ul>
					</div>
				</div>
				<div class="nk-block-head-content">
					<a href="{{ url()->previous() }}" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></a>
					<a href="{{ url()->previous() }}" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></a>
				</div>
			</div>
		</div>
		<!-- .nk-block-head -->
		<div class="nk-block">
			<div class="card">
				<div class="card-aside-wrap">
					<div class="card-content">
						<ul class="nav nav-tabs nav-tabs-mb-icon nav-tabs-card">
							<li class="nav-item">
								<a class="nav-link" href="/users/{{ $user->uuid() }}"><em class="icon ni ni-user-circle"></em><span>Personal</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/transactions/user/{{ $user->uuid() }}"><em class="icon ni ni-repeat"></em><span>Transactions</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link active" href="/loginLogs/user/{{ $user->uuid() }}"><em class="icon ni ni-bell"></em><span>Login Logs</span></a>
							</li>
							<li class="nav-item nav-item-trigger d-xxl-none">
								<a href="#" class="toggle btn btn-icon btn-trigger" data-target="userAside"><em class="icon ni ni-user-list-fill"></em></a>
							</li>
						</ul>
						<!-- .nav-tabs -->
						<div class="card-inner">
							<div class="nk-block">
								<div class="nk-block-head">
									<h5 class="title">Login Logs</h5>
								</div>
								<table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
									<thead>
										<tr class="nk-tb-item nk-tb-head">
											<th class="nk-tb-col nk-tb-col-check">
												<div class="custom-control custom-control-sm custom-checkbox notext">
													<input type="checkbox" class="custom-control-input" id="uid">
													<label class="custom-control-label" for="uid"></label>
												</div>
											</th>
											<th class="nk-tb-col"><span class="sub-text">Browser</span></th>
											<th class="nk-tb-col tb-col-mb"><span class="sub-text">IP Address</span></th>
											<th class="nk-tb-col tb-col-md"><span class="sub-text">Time</span></th>
										</tr>
									</thead>
									<tbody>
										@foreach($user->login_logs as $key => $log)
										<tr class="nk-tb-item">
											<td class="nk-tb-col nk-tb-col-check">
												<div class="custom-control custom-control-sm custom-checkbox notext">
													<input type="checkbox" class="custom-control-input" id="uid{{ $key }}" value="{{ $log->id }}">
													<label class="custom-control-label" for="uid{{ $key }}"></label>
												</div>
											</td>
											<td class="nk-tb-col">
												<span class="tb-col-os"><a href="#">#{{ $log->login_browser_device }}</a></span>
											</td>
											<td class="nk-tb-col">
												<span class="tb-col-ip"><a href="#">#{{ $log->login_ip }}</a></span>
											</td>
											<td class="nk-tb-col">
												<span class="tb-col-time"><a href="#">#{{ $log->login_time }}</a></span>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="nk-divider divider md"></div>
						</div>
						<!-- .card-inner -->
					</div>
					<div class="card-aside card-aside-right user-aside toggle-slide toggle-slide-right toggle-break-xxl" data-content="userAside" data-toggle-screen="xxl" data-toggle-overlay="true" data-toggle-body="true">
						<div class="card-inner-group" data-simplebar>
							<div class="card-inner">
								<div class="user-card user-card-s2">
									<div class="user-avatar lg bg-primary">
										@if($details) <img src="{{ asset($details->data()->identity->passport) }}"> @else <span>{{ $user->nameThumb() }}</span>  @endif
									</div>
									<div class="user-info">
										<div class="badge badge-outline-light badge-pill ucap">Customer</div>
										<h5>{{ $user->getFullname() }}</h5>
										<span class="sub-text">{{ $user->email }}</span>
									</div>
								</div>
							</div>
							<!-- .card-inner -->
							<div class="card-inner card-inner-sm">
								<ul class="btn-toolbar justify-center gx-1">
									@if($user->active)
									<li><a href="/users/perm/{{ $user->uuid() }}" class="btn btn-trigger btn-icon text-danger miAction"><em class="icon ni ni-na"></em></a></li>
									@else
									<li><a href="/users/perm/{{ $user->uuid() }}" class="btn btn-trigger btn-icon text-success miAction"><em class="icon ni ni-check-thick"></em></a></li>
									@endif
									@if($user->my_details)
									<li><a href="#" class="btn btn-trigger btn-icon text-success deleteAction" data-target="#delete-kyc{{$user->my_details->id}}"><em class="icon ni ni-edit"></em></a></li>
									<form id="delete-kyc{{$user->my_details->id}}" action="{{ route('userDetails.destroy', $user->my_details->id) }}" method="POST" class="d-none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
									@endif
								</ul>
							</div>
							<div class="card-inner">
								<div class="overline-title-alt mb-2">In Account</div>
								<div class="profile-balance">
									<div class="profile-balance-group gx-4">
										<div class="profile-balance-sub">
											<div class="profile-balance-amount">
												<div class="number">{{ number_format($user->wallet->amount,2) }}<small class="currency currency-usd">NGN</small></div>
											</div>
											<div class="profile-balance-subtitle">Amount in wallet</div>
										</div>
										<div class="profile-balance-sub">
											<span class="profile-balance-plus text-soft"><em class="icon ni ni-plus"></em></span>
											<div class="profile-balance-amount">
												<div class="number">{{ number_format($user->transactions()->where('details->type_for', 'earnings')->orWhere('details->type_for', 'profits')->sum('details->amount'), 2) }}</div>
											</div>
											<div class="profile-balance-subtitle">Profit Earned</div>
										</div>
									</div>
								</div>
							</div>
							<!-- .card-inner -->
							<div class="card-inner">
								<div class="row text-center">
									<div class="col-4">
										<div class="profile-stats">
											<span class="amount">{{ $user->loans()->count() }}</span>
											<span class="sub-text">Total Loans</span>
										</div>
									</div>
									<div class="col-4">
										<div class="profile-stats">
											<span class="amount">{{ $user->loans()->whereHas('status', function ($query) { $query->where('status', 'approved');})->count() }}</span>
											<span class="sub-text">Approved Loans</span>
										</div>
									</div>
									<div class="col-4">
										<div class="profile-stats">
											<span class="amount">{{ $user->loans()->whereHas('status', function ($query) { $query->where('status', 'repaid');})->count() }}</span>
											<span class="sub-text">Cleared Loans</span>
										</div>
									</div>
								</div>
							</div>
							<!-- .card-inner -->
							<div class="card-inner">
								<h6 class="overline-title-alt mb-2">Additional</h6>
								<div class="row g-3">
									<div class="col-6">
										<span class="sub-text">User ID:</span>
										<span>GID{{ str_pad($user->id,6,0,STR_PAD_LEFT) }}</span>
									</div>
									<div class="col-6">
										<span class="sub-text">Last Login:</span>
										<span>@if($user->login_logs()->count() > 0) {{ date('d M, Y h:i A', strtotime($user->login_logs()->orderBy('id', 'desc')->skip(1)->first()->created_at ?? $user->login_logs()->orderBy('id', 'desc')->first()->created_at)) }} @endif</span>
									</div>
									<div class="col-6">
										<span class="sub-text">KYC Status:</span>
										@if($details)
										<span class="lead-text text-success">Approved</span>
										@else
										<span class="lead-text text-info">No KYC found</span>
										@endif
									</div>
									<div class="col-6">
										<span class="sub-text">Register At:</span>
										<span>{{ date('M d, Y', strtotime($user->created_at)) }}</span>
									</div>
								</div>
							</div>
						</div>
						<!-- .card-inner -->
					</div>
					<!-- .card-aside -->
				</div>
				<!-- .card-aside-wrap -->
			</div>
			<!-- .card -->
		</div>
		<!-- .nk-block -->
	</div>
</div>
@endsection