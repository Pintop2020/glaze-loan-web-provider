@extends('layouts.user.auth_v2')

@section('content')
<div class="nk-block-head">
    <div class="nk-block-head-content">
        <h5 class="nk-block-title">{{ __('Verify Your Email Address') }}</h5>
        @if (session('resent'))
            <div class="nk-block-des text-success">
                <p>{{ __('A fresh verification link has been sent to your email address.') }}</p>
            </div>
        @endif
        {{ __('Before proceeding, please check your email for a verification link.') }}
        {{ __('If you did not receive the email') }},
        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
        </form>
    </div>
</div>
@endsection
