@extends('layouts.user.auth_v2')

@section('content')
<div class="nk-block-head">
    <div class="nk-block-head-content">
        <h5 class="nk-block-title">Reset Password</h5>
        <div class="nk-block-des">
            <p>If you forgot your password, well, then we’ll email you instructions to reset your password.</p>
        </div>
    </div>
</div>
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<form method="post" action="{{ route('password.email') }}">
    @csrf
    <div class="form-group">
        <div class="form-label-group">
            <label class="form-label" for="inputEmail">Email</label>
        </div>
        <input type="email" class="form-control  @error('email') is-invalid @enderror" id="inputEmail" aria-describedby="email" placeholder="Enter email" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block">Submit</button>
    </div>
</form>
@endsection
