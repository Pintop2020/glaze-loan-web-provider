@extends('layouts.user.auth_v2')

@section('content')
<div class="nk-block-head">
    <div class="nk-block-head-content">
        <h5 class="nk-block-title">Sign-In</h5>
        <div class="nk-block-des">
            <p>Access your dashboard using your email and passcode.</p>
        </div>
    </div>
</div>
<form method="post">
    @csrf
    <div class="form-group">
        <div class="form-label-group">
            <label class="form-label" for="inputEmail">Email</label>
        </div>
        <input type="email" class="form-control  @error('email') is-invalid @enderror" id="inputEmail" aria-describedby="email" placeholder="Enter email" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <div class="form-label-group">
            <label class="form-label" for="password">Password</label>
            <a class="link link-primary link-sm" href="{{ route('password.request') }}">Forgot Code?</a>
        </div>
        <div class="form-control-wrap">
            <a href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
            <em class="passcode-icon icon-show icon ni ni-eye"></em>
            <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
            </a>
            <input type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" id="password" name="password" placeholder="Enter your passcode">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block">Sign in</button>
    </div>
</form>
<div class="form-note-s2 pt-4"> New on our platform? <a href="/auth/register">Create an account</a></div>
@endsection