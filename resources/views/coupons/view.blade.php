@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('My Coupons'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">My coupons</h2>
					<div class="nk-block-des">
						<p class="lead">Here are the list of all your coupons.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Code</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Desc</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Expires</span></th>
								</tr>
							</thead>
							<tbody>
								@foreach($coupons as $coupon)
								<tr class="nk-tb-item">
									<td class="nk-tb-col tb-col-mb">
										<span class="tb-amount">{{ number_format($coupon->amount,2) }} NGN</span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span class="sub-text">{{ $coupon->code }}</span>
									</td>
									<td class="nk-tb-col tb-col-lg">
										<span>{{ $coupon->desc }}</span>
									</td>
									<td class="nk-tb-col tb-col-lg">
										<span>{{ date('d M, Y h:i A', strtotime($coupon->expires)) }}</span>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection