@extends('layouts.user.app')

@section('title', __('My KYC details'))

@section('theB')
<div class="nk-content nk-content-fluid"><div class="container-xl wide-lg">
@endsection

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between g-3">
				<div class="nk-block-head-content">
					<h3 class="nk-block-title page-title">KYCs / <strong class="text-primary small">{{ $user->getFullname() }}</strong></h3>
					<div class="nk-block-des text-soft">
						<ul class="list-inline">
							<li>Application ID: <span class="text-base">{{ $user->id() }}</span></li>
							<li>Submitted At: <span class="text-base">{{ date('d M, Y h:i A', strtotime($data->created_at))}}</span></li>
						</ul>
					</div>
				</div>
				<div class="nk-block-head-content">
					<a href="/loans/apply" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></a>
					<a href="/loans/apply" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></a>
				</div>
			</div>
		</div>
		<!-- .nk-block-head -->
		<div class="nk-block">
			<div class="row gy-5">
				<div class="col-lg-5">
					<div class="nk-block-head">
						<div class="nk-block-head-content">
							<h5 class="nk-block-title title">Application Info</h5>
							<p>Submission date, approve date, status etc.</p>
						</div>
					</div>
					<!-- .nk-block-head -->
					<div class="card card-bordered">
						<ul class="data-list is-compact">
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Submitted By</div>
									<div class="data-value">{{ strtoupper($user->id()) }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Submitted At</div>
									<div class="data-value">{{ date('d M, Y h:i A', strtotime($data->created_at))}}</div>
								</div>
							</li>
						</ul>
					</div>
					<!-- .card -->
					<div class="nk-block-head">
						<div class="nk-block-head-content">
							<h5 class="nk-block-title title">Uploaded Documents</h5>
							<p>Here are your uploaded documents.</p>
						</div>
					</div>
					<!-- .nk-block-head -->
					<div class="card card-bordered">
						<ul class="data-list is-compact">
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Document Type</div>
									<div class="data-value">{{ $data_info->identity->identity_type }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">ID </div>
									<div class="data-value"><img src="{{ asset('storage/'.$data_info->identity->id) }}" width="70"></div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Company ID</div>
									<div class="data-value"><img src="{{ asset('storage/'.$data_info->identity->company_id) }}" width="70"></div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Proof/Selfie</div>
									<div class="data-value"><img src="{{ asset('storage/'.$data_info->identity->passport) }}" width="70"></div>
								</div>
							</li>
						</ul>
					</div>
					<!-- .card -->
				</div>
				<!-- .col -->
				<div class="col-lg-7">
					<div class="nk-block-head">
						<div class="nk-block-head-content">
							<h5 class="nk-block-title title">Applicant Information</h5>
							<p>Basic info, like name, phone, address, bank etc.</p>
						</div>
					</div>
					<div class="card card-bordered">
						<ul class="data-list is-compact">
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">First Name</div>
									<div class="data-value">{{ ucwords($user->first_name) }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Last Name</div>
									<div class="data-value">{{ ucwords($user->last_name) }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Email Address</div>
									<div class="data-value">{{ $user->email }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Phone Number</div>
									<div class="data-value text-soft"><em>{{ $user->mobile }}</em></div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Date of Birth</div>
									<div class="data-value">{{ $data_info->personal_info->date_of_birth }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Full Address</div>
									<div class="data-value">{{ $data_info->address_info->address_1 }}<br>{{ $data_info->address_info->address_2 }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">State of Residence</div>
									<div class="data-value">{{ $data_info->address_info->state_of_residence }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">LGA of Residence</div>
									<div class="data-value">{{ $data_info->address_info->lga_of_residence }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">City of Residence</div>
									<div class="data-value">{{ $data_info->address_info->city_of_residence }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Employer Name</div>
									<div class="data-value">{{ $data_info->employment_info->employer }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Employer Phone</div>
									<div class="data-value">{{ $data_info->employment_info->employer_phone }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Official Email</div>
									<div class="data-value">{{ $data_info->employment_info->official_email }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Industry</div>
									<div class="data-value">{{ $data_info->employment_info->industry }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Staff type</div>
									<div class="data-value">{{ $data_info->employment_info->staff_type }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Employer Address</div>
									<div class="data-value">{{ $data_info->employment_info->office_address }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Office State</div>
									<div class="data-value">{{ $data_info->employment_info->office_state }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Office LGA</div>
									<div class="data-value">{{ $data_info->employment_info->office_lga }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Office City</div>
									<div class="data-value">{{ $data_info->employment_info->office_city }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Monthly Income</div>
									<div class="data-value">₦{{ number_format($data_info->employment_info->monthly_income,2) }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Bank</div>
									<div class="data-value">{{ $bank }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Bvn</div>
									<div class="data-value">{{ $data_info->financial_info->bvn }}</div>
								</div>
							</li>
							<li class="data-item">
								<div class="data-col">
									<div class="data-label">Account Number</div>
									<div class="data-value">
										{{ $data_info->financial_info->account_number }}
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection