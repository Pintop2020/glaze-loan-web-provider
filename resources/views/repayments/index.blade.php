@extends('layouts.admin.app')

@section('title', __('All Repayments'))

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="nk-block-head nk-block-head-sm">
			<div class="nk-block-between">
				<div class="nk-block-head-content">
					<h3 class="nk-block-title page-title">Repayments</h3>
				</div>
			</div>
			<!-- .nk-block-between -->
		</div>
		<!-- .nk-block-head -->
		<div class="nk-block">
			<div class="card card-stretch">
				<div class="card-inner-group">
					<div class="card-inner">
						<div class="card-title-group">
							<div class="card-title">
								<h5 class="title">All Repayments</h5>
							</div>
							<!--
							<div class="card-tools mr-n1">
								<ul class="btn-toolbar gx-1">
									<li>
										<a href="#" class="search-toggle toggle-search btn btn-icon" data-target="search"><em class="icon ni ni-search"></em></a>
									</li>
									<li class="btn-toolbar-sep"></li>
									<li>
										<div class="dropdown">
											<a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown">
												<div class="badge badge-circle badge-primary">4</div>
												<em class="icon ni ni-filter-alt"></em>
											</a>
											<div class="filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right">
												<div class="dropdown-head">
													<span class="sub-title dropdown-title">Advance Filter</span>
													<div class="dropdown">
														<a href="#" class="link link-light">
														<em class="icon ni ni-more-h"></em>
														</a>
													</div>
												</div>
												<div class="dropdown-body dropdown-body-rg">
													<div class="row gx-6 gy-4">
														<div class="col-6">
															<div class="form-group">
																<label class="overline-title overline-title-alt">Type</label>
																<select class="form-select form-select-sm">
																	<option value="any">Any Type</option>
																	<option value="deposit">Deposit</option>
																	<option value="buy">Buy Coin</option>
																	<option value="sell">Sell Coin</option>
																	<option value="transfer">Transfer</option>
																	<option value="withdraw">Withdraw</option>
																</select>
															</div>
														</div>
														<div class="col-6">
															<div class="form-group">
																<label class="overline-title overline-title-alt">Status</label>
																<select class="form-select form-select-sm">
																	<option value="any">Any Status</option>
																	<option value="pending">Pending</option>
																	<option value="cancel">Cancel</option>
																	<option value="process">Process</option>
																	<option value="completed">Completed</option>
																</select>
															</div>
														</div>
														<div class="col-6">
															<div class="form-group">
																<label class="overline-title overline-title-alt">Pay Currency</label>
																<select class="form-select form-select-sm">
																	<option value="any">Any Coin</option>
																	<option value="bitcoin">Bitcoin</option>
																	<option value="ethereum">Ethereum</option>
																	<option value="litecoin">Litecoin</option>
																</select>
															</div>
														</div>
														<div class="col-6">
															<div class="form-group">
																<label class="overline-title overline-title-alt">Method</label>
																<select class="form-select form-select-sm">
																	<option value="any">Any Method</option>
																	<option value="paypal">PayPal</option>
																	<option value="bank">Bank</option>
																</select>
															</div>
														</div>
														<div class="col-6">
															<div class="form-group">
																<div class="custom-control custom-control-sm custom-checkbox">
																	<input type="checkbox" class="custom-control-input" id="includeDel">
																	<label class="custom-control-label" for="includeDel"> Including Deleted</label>
																</div>
															</div>
														</div>
														<div class="col-12">
															<div class="form-group">
																<button type="button" class="btn btn-secondary">Filter</button>
															</div>
														</div>
													</div>
												</div>
												<div class="dropdown-foot between">
													<a class="clickable" href="#">Reset Filter</a>
													<a href="#savedFilter" data-toggle="modal">Save Filter</a>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown">
											<a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown">
											<em class="icon ni ni-setting"></em>
											</a>
											<div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
												<ul class="link-check">
													<li><span>Show</span></li>
													<li class="active"><a href="#">10</a></li>
													<li><a href="#">20</a></li>
													<li><a href="#">50</a></li>
												</ul>
												<ul class="link-check">
													<li><span>Order</span></li>
													<li class="active"><a href="#">DESC</a></li>
													<li><a href="#">ASC</a></li>
												</ul>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="card-search search-wrap" data-search="search">
								<div class="search-content">
									<a href="#" class="search-back btn btn-icon toggle-search" data-target="search"><em class="icon ni ni-arrow-left"></em></a>
									<input type="text" class="form-control border-transparent form-focus-none" placeholder="Quick search by transaction">
									<button class="search-submit btn btn-icon"><em class="icon ni ni-search"></em></button>
								</div>
							</div>-->
						</div>
					</div>
					<div class="card-inner p-0">
						<div class="nk-tb-list nk-tb-tnx">
							<div class="nk-tb-item nk-tb-head">
								<div class="nk-tb-col nk-tb-col-check">
									<div class="custom-control custom-control-sm custom-checkbox notext">
										<input type="checkbox" class="custom-control-input" id="uid">
										<label class="custom-control-label" for="uid"></label>
									</div>
								</div>
								<div class="nk-tb-col"><span>Reference</span></div>
								<div class="nk-tb-col tb-col-md"><span>Date Due</span></div>
								<div class="nk-tb-col"><span class="d-none d-mb-block">Status</span></div>
								<div class="nk-tb-col tb-col-sm"><span>Customer</span></div>
								<div class="nk-tb-col"><span>Amount</span></div>
							</div>
							<!-- .nk-tb-item -->
							@foreach($transactions as $key => $trans)
							<div class="nk-tb-item">
								<div class="nk-tb-col nk-tb-col-check">
									<div class="custom-control custom-control-sm custom-checkbox notext">
										<input type="checkbox" class="custom-control-input" id="uid{{ $key }}">
										<label class="custom-control-label" for="uid{{ $key }}"></label>
									</div>
								</div>
								<div class="nk-tb-col">
									<span class="tb-lead"><a href="#">#{{ str_pad($trans->id,6,0,STR_PAD_LEFT) }}</a></span>
								</div>
								<div class="nk-tb-col tb-col-md">
									<span class="tb-sub">{{ date('M d, Y', strtotime($trans->created_at)) }}</span>
								</div>
								<div class="nk-tb-col">
									<span class="dot bg-warning d-mb-none"></span>
									@if($trans->status == 'success')
									<span class="badge badge-sm badge-dot has-bg badge-success d-none d-mb-inline-flex">Completed</span>
									@elseif($trans->status == 'failed')
									<span class="badge badge-sm badge-dot has-bg badge-danger d-none d-mb-inline-flex">Failed</span>
									@elseif($trans->status == 'pending')
									<span class="badge badge-sm badge-dot has-bg badge-warning d-none d-mb-inline-flex">Pending</span>
									@endif
								</div>
								<div class="nk-tb-col tb-col-sm">
									<span class="tb-sub">{{ $trans->loan->user->getFullname() }}</span>
								</div>
								<div class="nk-tb-col">
									<span class="tb-lead">₦{{ number_format($trans->amount,2) }}</span>
								</div>
							</div>
							@endforeach
						</div>
						<!-- .nk-tb-list -->
					</div>
					{{ $transactions->links() }}
				</div>
				<!-- .card-inner-group -->
			</div>
			<!-- .card -->
		</div>
		<!-- .nk-block -->
	</div>
</div>
@endsection