@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.user.app')

@section('title', __('Pending payments'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-inner">
	<div class="nk-content-body">
		<div class="components-preview wide-md mx-auto">
			<div class="nk-block-head nk-block-head-lg wide-sm">
				<div class="nk-block-head-content">
					<div class="nk-block-head-sub"><a class="back-to" href="{{ url()->previous() }}"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
					<h2 class="nk-block-title fw-normal">My payments</h2>
					<div class="nk-block-des">
						<p class="lead">Here are the list of all your pending repayments.</p>
					</div>
				</div>
			</div>
			<!-- nk-block -->
			<div class="nk-block nk-block-lg">
				<div class="card card-preview">
					<div class="card-inner">
						<table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
							<thead>
								<tr class="nk-tb-item nk-tb-head">
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Date Due</span></th>
									<th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
									<th class="nk-tb-col tb-col-md"><span class="sub-text">Month</span></th>
									<th class="nk-tb-col tb-col-lg"><span class="sub-text">Defaults</span></th>
									<th class="nk-tb-col nk-tb-col-tools text-right"></th>
								</tr>
							</thead>
							<tbody>
								@if($repayments)
								<tr class="nk-tb-item">
									<td class="nk-tb-col tb-col-mb">
										<span>Since {{ $repayments['date_due'][0] }}</span>
									</td>
									<td class="nk-tb-col tb-col-mb" data-order="{{ $repayments['amount_due'] }}">
										<span class="tb-amount">{{ number_format($repayments['amount_due'], 2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col tb-col-md">
										<span>
										Since {{ SuperM::ordinal($repayments['count'][0]) }} month
										</span>
									</td>
									<td class="nk-tb-col tb-col-lg">
										<span class="tb-amount">{{ number_format($repayments['defaults'],2) }} <span class="currency">NGN</span></span>
									</td>
									<td class="nk-tb-col nk-tb-col-tools">
										<ul class="nk-tb-actions gx-1">
											<li>
												<div class="drodown">
													<a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="link-list-opt no-bdr">
															<li><a href="/repayments/from-wallet" class="from-wallet"><em class="icon ni ni-wallet-out"></em><span>Pay From Wallet</span></a></li>
															<li><a href="/repayments/from-card" class="from-card"><em class="icon ni ni-master-card"></em><span>Pay With Card</span></a></li>
															<li><a href="#" data-toggle="modal" data-target="#from-transfer"><em class="icon ni ni-exchange"></em><span>Pay Via Transfer</span></a></li>
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</td>
								</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" id="the-confirm">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-body modal-body-md text-center">
				<div class="nk-modal">
					<h4 class="nk-modal-title">Confirm Your Payment</h4>
					<div class="nk-modal-text">
						<p>To confirm this action is being performed by you, please enter your Transaction Pin code in order complete the transaction.</p>
					</div>
					<div class="nk-modal-form">
						<div class="form-group">
							<input type="password" name="pin" class="form-control form-control-password-big text-center">
						</div>
					</div>
					<div class="nk-modal-action">
						<a href="#" class="btn btn-lg btn-mw btn-primary dds">Confirm Payment</a>
						<div class="sub-text sub-text-alt mt-3 mb-4">This transaction will appear on your wallet statement as Repayment.</div>
						<a href="#" class="link link-soft" data-dismiss="modal">Cancel and return</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" id="from-transfer">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-lg text-center">
                <div class="nk-modal">
                    <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                    <h4 class="nk-modal-title">Pay via bank transfer!</h4>
                    <div class="nk-modal-text">
                        <p class="sub-text">Send the <big>Amount</big> to <br><b>Bank: <i>Zenith Bank</i></b><br><b>Account number: <i>0296758758</i></b><br><b>Account Name: <i>Glaze Credit Limited</i></b><br>Quoting your Customer ID which is <b><q>{{ auth()->user()->id() }}</q></b></p>
                    </div>
                </div>
            </div><!-- .modal-body -->
            <div class="modal-footer bg-lighter">
                <div class="text-center w-100">
                    <p>Your payment will be confirmed within 24 working hours!</p>
                </div>
            </div>
        </div><!-- .modal-content -->
    </div><!-- .modla-dialog -->
</div>
@endsection

@push('more-scripts')
<script>
	$(function() {
    $('.from-wallet').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        var pinSaved = '{{ session('
        user_pin ') }}';
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            //timer: 2000,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                            window.location.replace(url);
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
    // from card
    //
    //
    $('.from-card').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        var pinSaved = '{{ session('
        user_pin ') }}';
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                            $.ajax({
                                type: "GET",
                                url: url,
                                dataType: 'json',
                                success: function(d) {
                                    if (d.status) {
                                        window.location.replace(d.data.authorization_url);
                                    } else {
                                        Swal.fire("Oops!", d.message, "error");
                                    }
                                }
                            });
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
});
</script>
@endpush