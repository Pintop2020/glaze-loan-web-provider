@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('File Manager'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-body">
	<div class="nk-fmg">
		<div class="nk-fmg-aside" data-content="files-aside" data-toggle-overlay="true" data-toggle-body="true" data-toggle-screen="lg" data-simplebar>
			<div class="nk-fmg-aside-wrap">
				<div class="nk-fmg-aside-top" data-simplebar>
					<ul class="nk-fmg-menu">
						<li class="active">
							<a class="nk-fmg-menu-item" href="/folders">
							<em class="icon ni ni-home-alt"></em>
							<span class="nk-fmg-menu-text">Home</span>
							</a>
						</li>
						<li>
							<a class="nk-fmg-menu-item" href="/trash">
							<em class="icon ni ni-trash-alt"></em>
							<span class="nk-fmg-menu-text">Recovery</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- .nk-fmg-aside -->
		<div class="nk-fmg-body">
			<div class="nk-fmg-body-head d-none d-lg-flex">
				<div class="nk-fmg-search">
					<em class="icon ni ni-search"></em>
					<input type="text" class="form-control border-transparent form-focus-none" placeholder="Search files, folders">
				</div>
			</div>
			<div class="nk-fmg-body-content">
				<div class="nk-block-head nk-block-head-sm">
					<div class="nk-block-between position-relative">
						<div class="nk-block-head-content">
							<h3 class="nk-block-title page-title">Files</h3>
						</div>
						<div class="nk-block-head-content">
							<ul class="nk-block-tools g-1">
								<li class="d-lg-none">
									<a href="#" class="btn btn-trigger btn-icon search-toggle toggle-search" data-target="search"><em class="icon ni ni-search"></em></a>
								</li>
								<li class="d-lg-none mr-n1"><a href="#" class="btn btn-trigger btn-icon toggle" data-target="files-aside"><em class="icon ni ni-menu-alt-r"></em></a></li>
							</ul>
						</div>
						<div class="search-wrap px-2 d-lg-none" data-search="search">
							<div class="search-content">
								<a href="#" class="search-back btn btn-icon toggle-search" data-target="search"><em class="icon ni ni-arrow-left"></em></a>
								<input type="text" class="form-control border-transparent form-focus-none" placeholder="Search by user or message">
								<button class="search-submit btn btn-icon"><em class="icon ni ni-search"></em></button>
							</div>
						</div>
						<!-- .search-wrap -->
					</div>
				</div>
				<div class="nk-fmg-listing nk-block-lg">
					<div class="nk-block-head-xs">
						<div class="nk-block-between g-2">
							<div class="nk-block-head-content">
								<h6 class="nk-block-title title">Browse Files</h6>
							</div>
						</div>
					</div>
					<!-- .nk-block-head -->
					<div class="tab-content">
						<div class="tab-pane active" id="file-grid-view">
							<div class="nk-files nk-files-view-grid">
								<div class="nk-files-list">
									@foreach($entities as $entity)
									<div class="nk-file-item nk-file">
										<div class="nk-file-info">
											<div class="nk-file-title">
												<div class="nk-file-icon">
													<a class="nk-file-icon-link" href="/folders/{{ $entity->folder->uuid() }}">
														<span class="nk-file-icon-type">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72">
																<g>
																	<rect x="32" y="16" width="28" height="15" rx="2.5" ry="2.5" style="fill:#f29611" />
																	<path d="M59.7778,61H12.2222A6.4215,6.4215,0,0,1,6,54.3962V17.6038A6.4215,6.4215,0,0,1,12.2222,11H30.6977a4.6714,4.6714,0,0,1,4.1128,2.5644L38,24H59.7778A5.91,5.91,0,0,1,66,30V54.3962A6.4215,6.4215,0,0,1,59.7778,61Z" style="fill:#ffb32c" />
																	<path d="M8.015,59c2.169,2.3827,4.6976,2.0161,6.195,2H58.7806a6.2768,6.2768,0,0,0,5.2061-2Z" style="fill:#f2a222" />
																	<path d="M42.2227,40H41.5V37.4443a5.5,5.5,0,0,0-11,0V40h-.7227A2.8013,2.8013,0,0,0,27,42.8184v6.3633A2.8013,2.8013,0,0,0,29.7773,52H42.2227A2.8013,2.8013,0,0,0,45,49.1816V42.8184A2.8013,2.8013,0,0,0,42.2227,40ZM36,48a2,2,0,1,1,2-2A2.0023,2.0023,0,0,1,36,48Zm3.5-8h-7V37.4443a3.5,3.5,0,0,1,7,0Z" style="fill:#c67424" />
																</g>
															</svg>
														</span>
													</a>
												</div>
												<div class="nk-file-name">
													<div class="nk-file-name-text">
														<a href="/folders/{{ $entity->folder->uuid() }}" class="title">{{ $entity->folder->name }}</a>
													</div>
												</div>
											</div>
											<ul class="nk-file-desc">
												<li class="date">{{ \Carbon\Carbon::parse($entity->folder->created_at)->diffForHumans() }}</li>
												<li class="size">{{ SuperM::getSizeOfFolder($entity->folder->id) }}</li>
											</ul>
										</div>
									</div>
									@endforeach
								</div>
							</div>
							<!-- .nk-files -->
						</div>
					</div>
					<!-- .tab-content -->
				</div>
				<!-- .nk-block -->
			</div>
			<!-- .nk-fmg-body-content -->
		</div>
		<!-- .nk-fmg-body -->
	</div>
	<!-- .nk-fmg -->
</div>
@endsection