@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('File Manager'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@section('content')
<div class="nk-content-body">
	<div class="nk-fmg">
		<div class="nk-fmg-aside" data-content="files-aside" data-toggle-overlay="true" data-toggle-body="true" data-toggle-screen="lg" data-simplebar>
			<div class="nk-fmg-aside-wrap">
				<div class="nk-fmg-aside-top" data-simplebar>
					<ul class="nk-fmg-menu">
						<li>
							<a class="nk-fmg-menu-item" href="/folders">
							<em class="icon ni ni-home-alt"></em>
							<span class="nk-fmg-menu-text">Home</span>
							</a>
						</li>
						<li class="active">
							<a class="nk-fmg-menu-item" href="/trash">
							<em class="icon ni ni-trash-alt"></em>
							<span class="nk-fmg-menu-text">Recovery</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- .nk-fmg-aside -->
		<div class="nk-fmg-body">
			<div class="nk-fmg-body-head d-none d-lg-flex">
				<div class="nk-fmg-search">
					<em class="icon ni ni-search"></em>
					<input type="text" class="form-control border-transparent form-focus-none" placeholder="Search files, folders">
				</div>
				<div class="nk-fmg-actions">
					<ul class="nk-block-tools g-3">
						<li><a href="#file-upload" class="btn btn-primary" data-toggle="modal"><em class="icon ni ni-upload-cloud"></em> <span>Upload</span></a></li>
					</ul>
				</div>
			</div>
			<div class="nk-fmg-body-content">
				<div class="nk-block-head nk-block-head-sm">
					<div class="nk-block-between position-relative">
						<div class="nk-block-head-content">
							<h3 class="nk-block-title page-title">All Deleted files</h3>
						</div>
					</div>
				</div>
				<div class="nk-fmg-listing nk-block-lg">
					<div class="nk-block-head-xs">
						<div class="nk-block-between g-2">
							<div class="nk-block-head-content">
								<h6 class="nk-block-title title">Browse Files</h6>
							</div>
						</div>
					</div>
					<!-- .nk-block-head -->
					<div class="tab-content">
						<div class="tab-pane active" id="file-grid-view">
							<div class="nk-files nk-files-view-grid">
								<div class="nk-files-list">
									<!-- -->
									@foreach($entities as $upload)
									<div class="nk-file-item nk-file">
										<div class="nk-file-info">
											<div class="nk-file-title">
												<div class="nk-file-icon">
													<a class="nk-file-icon-link" href="{{ asset($upload->resource_url) }}" target="_blank">
														<span class="nk-file-icon-type">
															{!!  SuperM::getFileThumb($upload->resource_url) !!}
														</span>
													</a>
												</div>
												<div class="nk-file-name">
													<div class="nk-file-name-text">
														<a href="{{ asset($upload->resource_url) }}" target="_blank" class="title">{{ $upload->resource_name }}</a>
													</div>
												</div>
											</div>
											<ul class="nk-file-desc">
												<li class="date">{{ \Carbon\Carbon::parse($upload->created_at)->diffForHumans() }}</li>
												<li class="size">{{ SuperM::size_converter(\Storage::size('public/'.$upload->resource_url)) }}</li>
											</ul>
										</div>
										<div class="nk-file-actions">
											<div class="dropdown">
												<a href="" class="dropdown-toggle btn btn-sm btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="link-list-plain no-bdr">
														<li><a class="miAction" href="/uploads/restore/{{$upload->id}}"><em class="icon ni ni-trash"></em><span>Restore</span></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									@endforeach
									<!-- -->
								</div>
							</div>
							<!-- .nk-files -->
						</div>
					</div>
					<!-- .tab-content -->
				</div>
				<!-- .nk-block -->
			</div>
			<!-- .nk-fmg-body-content -->
		</div>
		<!-- .nk-fmg-body -->
	</div>
	<!-- .nk-fmg -->
</div>
@endsection