@extends('layouts.app')

@section('content')

<form method="post" action="#">
           <input type="hidden" name="__method" value="post">
<div class="form-group">
    <label for="id">Id</label>
    <input type="text" class="form-control" id="id" name="id" value="{{old('id',$entity ? $entity->id : "")}}" required >
</div>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{old('name',$entity ? $entity->name : "")}}" required >
</div>
<div class="form-group">
    <label for="loan_id">Loan id</label>
    <input type="text" class="form-control" id="loan_id" name="loan_id" value="{{old('loan_id',$entity ? $entity->loan_id : "")}}"  >
</div>
<div class="form-group">
    <label for="created_at">Created at</label>
    <input type="text" class="form-control" id="created_at" name="created_at" value="{{old('created_at',$entity ? $entity->created_at : "")}}"  >
</div>
<div class="form-group">
    <label for="updated_at">Updated at</label>
    <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{old('updated_at',$entity ? $entity->updated_at : "")}}"  >
</div>
<div class="form-group">
    <label for="deleted_at">Deleted at</label>
    <input type="text" class="form-control" id="deleted_at" name="deleted_at" value="{{old('deleted_at',$entity ? $entity->deleted_at : "")}}"  >
</div><button type='submit' class='btn btn-primary'>Submit</button></form>

@endsection