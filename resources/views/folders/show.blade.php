@php
use App\Http\Controllers\GlobalMethods as SuperM;
@endphp

@extends('layouts.admin.app')

@section('title', __('File Manager'))

@section('theB')
<div class="nk-content p-0"><div class="nk-content-inner">
@endsection

@push('more-scripts')
<script>
    $(function(){
        $('.deleteAction').click(function(e){
            e.preventDefault();
            var form = $(this).attr('data-target');
            Swal.fire({
                title: 'Are you sure?',
                text: "This file will be moved to trash!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger ml-2',
                buttonsStyling: false
            }).then((result) => {
                if(result.isConfirmed){
                    $(form).submit();
                }
            });
        });
    });
</script>
@endpush

@section('content')
<div class="nk-content-body">
	<div class="nk-fmg">
		<div class="nk-fmg-aside" data-content="files-aside" data-toggle-overlay="true" data-toggle-body="true" data-toggle-screen="lg" data-simplebar>
			<div class="nk-fmg-aside-wrap">
				<div class="nk-fmg-aside-top" data-simplebar>
					<ul class="nk-fmg-menu">
						<li class="active">
							<a class="nk-fmg-menu-item" href="/folders">
							<em class="icon ni ni-home-alt"></em>
							<span class="nk-fmg-menu-text">Home</span>
							</a>
						</li>
						<li>
							<a class="nk-fmg-menu-item" href="/trash">
							<em class="icon ni ni-trash-alt"></em>
							<span class="nk-fmg-menu-text">Recovery</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="nk-fmg-body">
			<div class="nk-fmg-body-head d-none d-lg-flex">
				<div class="nk-fmg-search">
					<em class="icon ni ni-search"></em>
					<input type="text" class="form-control border-transparent form-focus-none" placeholder="Search files, folders">
				</div>
				@if($user->hasRole(['marketer', 'risk']))
				<div class="nk-fmg-actions">
					<ul class="nk-block-tools g-3">
						<li><a href="#file-upload" class="btn btn-primary" data-toggle="modal"><em class="icon ni ni-upload-cloud"></em> <span>Upload</span></a></li>
					</ul>
				</div>
				@endif
			</div>
			<div class="nk-fmg-body-content">
				<div class="nk-block-head nk-block-head-sm">
					<div class="nk-block-between position-relative">
						<div class="nk-block-head-content">
							<h3 class="nk-block-title page-title">Files</h3>
						</div>
						<div class="nk-block-head-content">
							<ul class="nk-block-tools g-1">
								<li class="d-lg-none">
									<a href="#" class="btn btn-trigger btn-icon search-toggle toggle-search" data-target="search"><em class="icon ni ni-search"></em></a>
								</li>
								<li class="d-lg-none">
									<div class="dropdown">
										<a href="#" class="btn btn-trigger btn-icon" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
										<div class="dropdown-menu dropdown-menu-right">
											<ul class="link-list-opt no-bdr">
												<li><a href="#file-upload" data-toggle="modal"><em class="icon ni ni-upload-cloud"></em><span>Upload File</span></a></li>
											</ul>
										</div>
									</div>
								</li>
								<li class="d-lg-none mr-n1"><a href="#" class="btn btn-trigger btn-icon toggle" data-target="files-aside"><em class="icon ni ni-menu-alt-r"></em></a></li>
							</ul>
						</div>
						<div class="search-wrap px-2 d-lg-none" data-search="search">
							<div class="search-content">
								<a href="#" class="search-back btn btn-icon toggle-search" data-target="search"><em class="icon ni ni-arrow-left"></em></a>
								<input type="text" class="form-control border-transparent form-focus-none" placeholder="Search by user or message">
								<button class="search-submit btn btn-icon"><em class="icon ni ni-search"></em></button>
							</div>
						</div>
					</div>
				</div>
				<div class="nk-block-head nk-block-head-sm">
					<div class="nk-block-between position-relative">
						<div class="nk-block-head-content">
							<h3 class="nk-block-title page-title">{{ $folder->loan->user->getFullname() }} Folder for Loan INV{{ str_pad($folder->loan->id,6,0,STR_PAD_LEFT) }}</h3>
						</div>
					</div>
				</div>
				<div class="nk-fmg-listing nk-block-lg">
					<div class="nk-block-head-xs">
						<div class="nk-block-between g-2">
							<div class="nk-block-head-content">
								<h6 class="nk-block-title title">Browse Files</h6>
							</div>
						</div>
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="file-grid-view">
							<div class="nk-files nk-files-view-grid">
								<div class="nk-files-list">
									<div class="nk-file-item nk-file">
										<div class="nk-file-info">
											<div class="nk-file-title">
												<div class="nk-file-icon">
													<a class="nk-file-icon-link" href="{{ asset('storage/'.$folder->loan->data()->uploads->statements) }}" target="_blank">
														<span class="nk-file-icon-type">
															{!!  SuperM::getFileThumb($folder->loan->data()->uploads->statements) !!}
														</span>
													</a>
												</div>
												<div class="nk-file-name">
													<div class="nk-file-name-text">
														<a href="{{ asset('storage/'.$folder->loan->data()->uploads->statements) }}" target="_blank" class="title">Statement of Account</a>
													</div>
												</div>
											</div>
											<ul class="nk-file-desc">
												<li class="date">{{ \Carbon\Carbon::parse($folder->loan->created_at)->diffForHumans() }}</li>
												<li class="size">{{ SuperM::size_converter(\Storage::size('public/'.$folder->loan->data()->uploads->statements)) }}</li>
											</ul>
										</div>
										<div class="nk-file-actions">
											<div class="dropdown">
												<a href="" class="dropdown-toggle btn btn-sm btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="link-list-plain no-bdr">
														<li><a href="{{ asset('storage/'.$folder->loan->data()->uploads->statements) }}" target="_blank" class="file-dl-toast"><em class="icon ni ni-download"></em><span>Download</span></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="nk-file-item nk-file">
										<div class="nk-file-info">
											<div class="nk-file-title">
												<div class="nk-file-icon">
													<a class="nk-file-icon-link" href="{{ asset('storage/'.$folder->loan->data()->uploads->utility_bill) }}" target="_blank">
														<span class="nk-file-icon-type">
															{!!  SuperM::getFileThumb('storage/'.$folder->loan->data()->uploads->utility_bill) !!}
														</span>
													</a>
												</div>
												<div class="nk-file-name">
													<div class="nk-file-name-text">
														<a href="{{ asset('storage/'.$folder->loan->data()->uploads->statements) }}" target="_blank" class="title">Utility Bill</a>
													</div>
												</div>
											</div>
											<ul class="nk-file-desc">
												<li class="date">{{ \Carbon\Carbon::parse($folder->loan->created_at)->diffForHumans() }}</li>
												<li class="size">{{ SuperM::size_converter(\Storage::size('public/'.$folder->loan->data()->uploads->utility_bill)) }}</li>
											</ul>
										</div>
										<div class="nk-file-actions">
											<div class="dropdown">
												<a href="" class="dropdown-toggle btn btn-sm btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="link-list-plain no-bdr">
														<li><a href="{{ asset('storage/'.$folder->loan->data()->uploads->utility_bill) }}" target="_blank" class="file-dl-toast"><em class="icon ni ni-download"></em><span>Download</span></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="nk-file-item nk-file">
										<div class="nk-file-info">
											<div class="nk-file-title">
												<div class="nk-file-icon">
													<a class="nk-file-icon-link" href="{{ asset('storage/'.$folder->loan->data()->uploads->employment_letter) }}" target="_blank">
														<span class="nk-file-icon-type">
															{!!  SuperM::getFileThumb('storage/'.$folder->loan->data()->uploads->employment_letter) !!}
														</span>
													</a>
												</div>
												<div class="nk-file-name">
													<div class="nk-file-name-text">
														<a href="{{ asset('storage/'.$folder->loan->data()->uploads->employment_letter) }}" target="_blank" class="title">Employment Letter</a>
													</div>
												</div>
											</div>
											<ul class="nk-file-desc">
												<li class="date">{{ \Carbon\Carbon::parse($folder->loan->created_at)->diffForHumans() }}</li>
												<li class="size">{{ SuperM::size_converter(\Storage::size('public/'.$folder->loan->data()->uploads->employment_letter)) }}</li>
											</ul>
										</div>
										<div class="nk-file-actions">
											<div class="dropdown">
												<a href="" class="dropdown-toggle btn btn-sm btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="link-list-plain no-bdr">
														<li><a href="{{ asset('storage/'.$folder->loan->data()->uploads->employment_letter) }}" target="_blank" class="file-dl-toast"><em class="icon ni ni-download"></em><span>Download</span></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									@foreach($folder->uploads as $upload)
									<div class="nk-file-item nk-file">
										<div class="nk-file-info">
											<div class="nk-file-title">
												<div class="nk-file-icon">
													<a class="nk-file-icon-link" href="{{ asset('storage/'.$upload->resource_url) }}" target="_blank">
														<span class="nk-file-icon-type">
															{!!  SuperM::getFileThumb('storage/'.$upload->resource_url) !!}
														</span>
													</a>
												</div>
												<div class="nk-file-name">
													<div class="nk-file-name-text">
														<a href="{{ asset('storage/'.$upload->resource_url) }}" target="_blank" class="title">{{ $upload->resource_name }}</a>
													</div>
												</div>
											</div>
											<ul class="nk-file-desc">
												<li class="date">{{ \Carbon\Carbon::parse($upload->created_at)->diffForHumans() }}</li>
												<li class="size">{{ SuperM::size_converter(\Storage::size('public/'.$upload->resource_url)) }}</li>
											</ul>
										</div>
										<div class="nk-file-actions">
											<div class="dropdown">
												<a href="" class="dropdown-toggle btn btn-sm btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="link-list-plain no-bdr">
														<li><a href="{{ asset('storage/'.$upload->resource_url) }}" target="_blank" class="file-dl-toast"><em class="icon ni ni-download"></em><span>Download</span></a></li>
														<li><a class="deleteAction" href="" data-target="#delete-file{{$upload->id}}"><em class="icon ni ni-trash"></em><span>Delete</span></a></li>
														<form id="delete-file{{$upload->id}}" action="{{ route('uploads.destroy', $upload->id) }}" method="POST" class="d-none">
						                                    @csrf
						                                    @method('DELETE')
						                                </form>
													</ul>
												</div>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="file-upload">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
				<div class="modal-body modal-body-md">
					<div class="nk-upload-form">
						<h5 class="title mb-3">Upload File</h5>
						<div class="upload-zone small bg-lighter">
							<div class="dz-message" data-dz-message>
								<span class="dz-message-text"><span>Drag and drop</span> file here or <span>browse</span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection