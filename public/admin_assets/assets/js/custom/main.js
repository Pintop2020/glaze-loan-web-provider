$(function(){
    $('.miAction').click(function(e){
        e.preventDefault();
        var dhref = $(this).attr("href");
        Swal.fire({
          title: 'Are you sure?',
          text: "You might not be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, Proceed!'
        }).then(function (result) {
          if (result.isConfirmed) {
            window.location.href = dhref;
          }
        });
    });
});