$(function() {
    $("button").click(function(e) {
        e.preventDefault();
    });
    $('input[name="iv_amount"]').on('input', function() {
        $('input[name="amount"]').val($(this).val());
        setOtherValues();
    });
    $('input[name="amount"]').on('input', function() {
        setOtherValues();
    });
    $('select[name="tenor"]').change(function() {
        setOtherValues();
    });
    $('select[name="type"]').change(function() {
        setOtherValues();
    });
    $('input[name="pay_date"]').change(function() {
        setOtherValues();
    });
    $('.ddu').click(function(e) {
        e.preventDefault();
        var amount = $('input[name="amount"]').val();
        var type = $('select[name="type"]').val();
        var pay_date = $('input[name="pay_date"]').val();
        var tenor = $('select[name="tenor"]').children("option:selected").val();
        if (amount == '') {
            Swal.fire("Oops!", "Please enter amount applying for!", "error");
        } else if (amount < 50000 || amount > 4000000) {
            Swal.fire("Oops!", "You can only apply for NGN 50,000 up to NGN 4,000,000", "error");
        } else if (tenor == 'Choose Option') {
            Swal.fire("Oops!", "Please select a tenor", "error");
        }else if(!$('input[name="agreement"]').is(":checked")){
            Swal.fire("Oops!", "Please accept our terms and conditions!", "error");
        } else {
            $('#invest-plan').modal('show');
            $('.dds').click(function(e) {
                e.preventDefault();
                $('#invest-plan').modal('hide');
                var pin = $('input[name="pin"]').val();
                if (isNaN(pin)) {
                    Swal.fire("Oops!", "Your pin can only be numbers", "error");
                } else if (pin.length > 4 || pin.length < 4) {
                    Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
                } else {
                    var interest = 4;
                    var processing = 0;
                    var management = 4000;
                    if(type == 'sme'){
                        processing = 2;
                        management = 1000;
                    }
                    var data = {
                        amount: amount,
                        tenor: tenor,
                        pin: pin,
                        interest: interest,
                        processing: processing,
                        type: type,
                        pay_date: pay_date,
                        management: management
                    }
                    data['_token'] = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: "POST",
                        url: '/loans',
                        data: data,
                        beforeSend: function(){
                            Swal.fire({
                            title: 'Submitting',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                              Swal.showLoading();
                            }
                          })
                        },
                        success: function(d) {
                            Swal.close();
                            if (d.status) {
                                $('#confirm-invest').modal('show');
                            } else {
                                Swal.fire("Oops!", d.message, "error");
                            }
                        },
                    });
                }
            });
        }
    });
});

function setOtherValues() {
    var locale = 'en';
    var options = {
        style: 'currency',
        currency: 'ngn',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    };
    var formatter = new Intl.NumberFormat(locale, options);
    var amount = $('input[name="amount"]').val();
    var tenor = $('select[name="tenor"]').children("option:selected").val();
    var interest = 4;
    var processing = 0;
    var type = $('select[name="type"]').val();
    var management = 4000;
    if(type == 'sme'){
        processing = 2;
        management = 1000;
    }
    if (tenor != "Choose Option" && amount != '') {
        var amount_disp = $('.amount_disp');
        var tenor_disp = $('.tenor_disp');
        var payment_disp = $('.payment_disp');
        var interest_disp = $('.interest_disp');
        var receive_disp = $('.receive_disp');
        var return_disp = $('.return_disp');
        var processing_fee = $('.processing_fee');
        var manage_fee = $('.manage_fee');
        amount_disp.html(formatter.format(amount));
        tenor_disp.html(tenor + ' months');
        var dinterest = amount * (interest / 100);
        var monthly = ((parseFloat(amount) + parseFloat(management)) / tenor) + dinterest;
        var receive = amount - (amount * (processing / 100));
        var rreturn = monthly * tenor;
        var fullProcessing = amount * (processing/100);
        payment_disp.html(formatter.format(monthly));
        interest_disp.html(formatter.format(dinterest));
        receive_disp.html(formatter.format(receive));
        return_disp.html(formatter.format(rreturn));
        processing_fee.html(formatter.format(fullProcessing));
        manage_fee.html(formatter.format(management));
    }
}