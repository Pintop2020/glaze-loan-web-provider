$(function() {
    $("button").click(function(e) {
        e.preventDefault();
    });
    $("button.subm").click(function(e) {
        e.preventDefault();
        var data = {
            mobile: $('input[name="mobile"]').val(),
            email: $('input[name="email"]').val(),
            gender: $('select[name="gender"]').children("option:selected").val(),
            marital_status: $('select[name="marital_status"]').children("option:selected").val(),
            number_of_children: $('select[name="number_of_children"]').children("option:selected").val(),
            educational_status: $('select[name="educational_status"]').children("option:selected").val(),
            address_1: $('input[name="address_1"]').val(),
            state_of_residence: 'Lagos',
            lga_of_residence: $('select[name="lga_of_residence"]').children("option:selected").val(),
            city_of_residence: $('input[name="city_of_residence"]').val(),
            employer: $('input[name="employer"]').val(),
            employer_phone: $('input[name="employer_phone"]').val(),
            official_email: $('input[name="official_email"]').val(),
            industry: $('select[name="industry"]').children("option:selected").val(),
            staff_type: $('select[name="staff_type"]').children("option:selected").val(),
            office_address: $('input[name="office_address"]').val(),
            office_state: 'Lagos',
            office_lga: $('select[name="office_lga"]').children("option:selected").val(),
            office_city: $('input[name="office_city"]').val(),
            monthly_income: $('input[name="monthly_income"]').val(),
            bank: $('select[name="bank"]').children("option:selected").val(),
            account_number: $('input[name="account_number"]').val(),
            bvn: $('input[name="bvn"]').val(),
            id_proof: $('input[name="id_proof"]').val()
        }
        var error = 'Please fill in your ';
        var errorInt = 0;
        for (var key in data) {
            if (data[key] == '' || data[key] == 'Choose Option') {
                errorInt = errorInt + 1;
                if (key != 'account_name') error = error + key + ', ';
                else error = error + key;
            }
        }
        if (errorInt > 0) Swal.fire("Oops!", error, "error");
        else if(!$('input[name="agreement"]').is(":checked")){
            Swal.fire("Oops!", "Please accept our terms and confitions!", "error");
        } else if(!$('input[name="correct_info"]').is(":checked")){
            Swal.fire("Oops!", "Please confirm you are uploading correct information!", "error");
        } else {
            var url = '/check-uploads';
            $.ajax({
                type: "GET",
                url: url,
                dataType: "json",
                success: function(d) {
                    if (!d.status) {
                        Swal.fire("Oops!", d.message, "error");
                    } else {
                        $.ajax({
                            type: "GET",
                            url: '/check-bvn/' + data['bvn'] + '/' + data['mobile'],
                            dataType: 'json',
                            beforeSend: function(){
                                Swal.fire({
                                title: 'Validating BVN',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                onOpen: () => {
                                  Swal.showLoading();
                                }
                              })
                            },
                            success: function(d) {
                                if (!d.status) {
                                    Swal.fire("Oops!", d.message, "error");
                                } else {
                                    $.ajax({
                                        type: "GET",
                                        url: '/check-bank/' + data['bank'] + '/' + data['account_number'],
                                        dataType: 'json',
                                        beforeSend: function(){
                                            Swal.fire({
                                            title: 'Validating Bank',
                                            allowEscapeKey: false,
                                            allowOutsideClick: false,
                                            onOpen: () => {
                                              Swal.showLoading();
                                            }
                                          })
                                        },
                                        success: function(d) {
                                            if (!d.status) {
                                                Swal.fire("Oops!", d.message, "error");
                                            } else {
                                                data['address_2'] = $('input[name="address_2"]').val();
                                                data['_token'] = $('meta[name="csrf-token"]').attr('content');
                                                Swal.fire({
                                                    title: 'Are you sure?',
                                                    html: "Your account account name is <big>" + d.data.account_name + "<big>",
                                                    icon: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonText: 'Yes, Process!',
                                                    cancelButtonText: 'Change it!'
                                                }).then(function(result) {
                                                    if (result.isConfirmed) {
                                                        $('.dform').submit();
                                                        /*$.ajax({
                                                            type: "POST",
                                                            url: '/application/kyc',
                                                            data: data,
                                                            beforeSend: function(){
                                                                Swal.fire({
                                                                title: 'Creating KYC',
                                                                allowEscapeKey: false,
                                                                allowOutsideClick: false,
                                                                onOpen: () => {
                                                                  Swal.showLoading();
                                                                }
                                                              })
                                                            },
                                                            success: function(d) {
                                                                if(!d.status){

                                                                }else {
                                                                    Swal.fire({
                                                                        title: 'Your data has been saved.',
                                                                        html: 'Please wait to be redirected in <b></b>',
                                                                        timer: 2000,
                                                                        timerProgressBar: true,
                                                                        allowOutsideClick: false,
                                                                        onBeforeOpen: function onBeforeOpen() {
                                                                            Swal.showLoading();
                                                                            timerInterval = setInterval(function() {
                                                                                Swal.getContent().querySelector('b').textContent = Swal.getTimerLeft();
                                                                            }, 100);
                                                                        },
                                                                        onClose: function onClose() {
                                                                            clearInterval(timerInterval);
                                                                        }
                                                                    }).then(function(result) {
                                                                        if (result.dismiss === Swal.DismissReason.timer) {
                                                                            window.location.href = "/application/preview";
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });*/
                                                    } 
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
    });
});