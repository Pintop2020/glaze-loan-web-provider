$(function() {
    $("button").click(function(e) {
        e.preventDefault();
        var data = {
            pin: $('input[name="pin"]').val(),
            pin_confirmation: $('input[name="pin_confirmation"]').val(),
        }
        if(isNaN(data['pin']) || isNaN(data['pin_confirmation'])){
            Swal.fire("Oops!", "Your pin can only be numbers", "error");
        }else if(data['pin'].length > 4 || data['pin'].length < 4){
            Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
        }else if(data['pin'] != data['pin_confirmation']){
            Swal.fire("Oops!", "Your pin doesn't match", "error");
        }else {
            data['_token'] = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: '/application/pin',
                data: data,
                beforeSend: function(){
                    Swal.fire({
                    title: 'Submitting',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                      Swal.showLoading();
                    }
                  })
                },
                success: function(d) {
                    if(d.status){
                        Swal.fire({
                            title: 'Your data has been saved.',
                            html: 'Please wait to be redirected in <b></b>',
                            timer: 2000,
                            timerProgressBar: true,
                            allowOutsideClick: false,
                            onBeforeOpen: function onBeforeOpen() {
                                Swal.showLoading();
                                timerInterval = setInterval(function() {
                                    Swal.getContent().querySelector('b').textContent = Swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function onClose() {
                                clearInterval(timerInterval);
                            }
                        }).then(function(result) {
                            if (result.dismiss === Swal.DismissReason.timer) {
                                window.location.href = "/application";
                            }
                        });
                    }
                    
                }
            });
        }
    });
});