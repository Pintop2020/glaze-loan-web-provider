$(document).ready(function(){
	function numberWithCommas(x) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		return parts.join(".");
	}
	var defaultRange = $("#amountRange").val();
	$("#amount").val(numberWithCommas(defaultRange));
	var defaultMonth = $("#monthRange").val();
	$("#month").val(defaultMonth+' months');

	var percentage = 5.0;
	$(".ddPrice").html('₦'+numberWithCommas(defaultRange));
	$(".ddTerm").html(defaultMonth+' months');

	var calc = (defaultRange / defaultMonth) + percentage * (defaultRange/100);
	calc = Math.round(calc*100)/100;

	$(".ddRepay").html('₦'+numberWithCommas(calc));

	$("#amount").on('input', function(){
		var ddmount = $("#amount").val();
		var newAmount = ddmount.replace(/\s/g, '');
		var newTerm = $("#monthRange").val();
		$("#amount").val(numberWithCommas(newAmount));

		$(".ddPrice").html('₦'+numberWithCommas(newAmount));

		var calc = (newAmount / newTerm) + percentage * (newAmount/100);
		calc = Math.round(calc*100)/100;

		$(".ddRepay").html('₦'+numberWithCommas(calc));
	});

	$("#amountRange").on('input', function(){
		var newAmount = $("#amountRange").val();
		var newTerm = $("#monthRange").val();
		$("#amount").val(numberWithCommas(newAmount));

		$(".ddPrice").html('₦'+numberWithCommas(newAmount));

		var calc = (newAmount / newTerm) + percentage * (newAmount/100);
		calc = Math.round(calc*100)/100;

		$(".ddRepay").html('₦'+numberWithCommas(calc));
	});
	$("#monthRange").on('input', function(){
		var newMonth = $("#monthRange").val();
		var newAmount = $("#amountRange").val();
		$("#month").val(newMonth+' months');

		$(".ddTerm").html(newMonth+' months');

		var calc = (newAmount / newMonth) + percentage * (newAmount/100);
		calc = Math.round(calc*100)/100;
		$(".ddRepay").html('₦'+numberWithCommas(calc));
	});
});