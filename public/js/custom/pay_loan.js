$(function() {
    $('.from-wallet').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        var pinSaved = '{{ session('
        user_pin ') }}';
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            //timer: 2000,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                            window.location.replace(url);
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
    // from card
    //
    //
    $('.from-card').click(function(e) {
        e.preventDefault();
        $('input[name="pin"]').val('');
        var url = $(this).attr('href');
        var pinSaved = '{{ session('
        user_pin ') }}';
        $('#the-confirm').modal('show');
        $('.dds').click(function(e) {
            e.preventDefault();
            $('#the-confirm').modal('hide');
            var pin = $('input[name="pin"]').val();
            if (isNaN(pin)) {
                Swal.fire("Oops!", "Your pin can only be numbers", "error");
            } else if (pin.length > 4 || pin.length < 4) {
                Swal.fire("Oops!", "Your pin must be 4 digits long.", "error");
            } else {
                var data = {
                    pin: pin
                }
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "POST",
                    url: '/pin/validate',
                    data: data,
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Validating',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                                Swal.showLoading();
                            }
                        })
                    },
                    success: function(d) {
                        if (d.status) {
                            $.ajax({
                                type: "GET",
                                url: url,
                                dataType: 'json',
                                success: function(d) {
                                    if (d.status) {
                                        window.location.replace(d.data.authorization_url);
                                    } else {
                                        Swal.fire("Oops!", d.message, "error");
                                    }
                                }
                            });
                        } else {
                            Swal.fire("Oops!", d.message, "error");
                        }
                    },
                });
            }
        });
    });
});