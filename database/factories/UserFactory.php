<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Entities\User\User::class, function (Faker $faker) {
    return [
		'mobile' => $faker->phoneNumber,
		'first_name'=>$faker->firstName,
		'last_name'=>$faker->lastName,
		'middle_name'=>'-',
		'email'=>$faker->unique()->safeEmail,
		'password'=> bcrypt('1234567000@1'),
    ];
});

$factory->define(App\Entities\User\SecurityQuestion::class, function (Faker $faker) {
    return [
        'question' => $faker->text,
        'answer' => $faker->text,
    ];
});
