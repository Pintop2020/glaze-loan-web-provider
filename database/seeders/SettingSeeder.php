<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Entities\SiteSetting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
			[
				'name'=>'loan_interest',
				'value'=>'5.0',
			],
			[
				'name'=>'processing_fee',
				'value'=>'2.0',
			],
			[
				'name'=>'referral_commission',
				'value'=>'10.0',
			],
			[
				'name'=>'loan_default_charge',
				'value'=>'10.0',
			],
		];
        foreach($data as $info){
        	SiteSetting::create($info);
        }
    }
}
