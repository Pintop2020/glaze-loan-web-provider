<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Entities\User\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'mobile' => "07000000000",
                'first_name'=>"Pintop",
                'last_name'=>"Technologies",
                'middle_name'=>'LTD',
                'email_verified_at'=>\Carbon\carbon::now(),
                'email'=>"pintoptechnologies@gmail.com",
                'password'=> \Hash::make('1234567000@1'),
                'is_admin'=>true,
            ],
            [
                'mobile' => "07000000000",
                'first_name'=>"Solomon",
                'last_name'=>"Obadia",
                'middle_name'=>'Taiwo',
                'email_verified_at'=>\Carbon\carbon::now(),
                'email'=>"tayuslord@gmail.com",
                'password'=> \Hash::make('ILOVEYOU2021'),
                'is_admin'=>true,
            ],
        ];

        foreach ($data as $dat) {
            $user = User::create($dat);
            $user->assignRole(['super admin']);
        }
    }
}
